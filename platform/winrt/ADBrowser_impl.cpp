#include <ADLib/Device/ADBrowser.h>
#include "cocos2d.h"
#include "WinRTAdlib.h"

using namespace cocos2d;

void ADBrowser::openWebURL(const std::string& url, const Fallback &)
{
	auto uri = ref new Windows::Foundation::Uri(toW8String(url));
	concurrency::task<bool> launchUriOperation(Windows::System::Launcher::LaunchUriAsync(uri));
	launchUriOperation.then([](bool success)
	{
		
	});
}
void ADBrowser::openOSUrl(const std::string& item_id, const Fallback&)
{
	CCLog("%s", item_id.c_str());
	openWebURL(item_id);
}
void ADBrowser::sendMail(const std::string& email, const std::string& subject, const Fallback&)
{
	//Not Supported
}
