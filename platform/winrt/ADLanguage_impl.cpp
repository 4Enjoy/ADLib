#include <ADLib/Device/ADLanguage.h>
#include <windows.h>
std::string ADLanguage::platformGetDeviceLanguage()
{
	const int N = 9;
	wchar_t buf[N];

	std::string res = "en";

	if (GetLocaleInfoEx(LOCALE_NAME_USER_DEFAULT, LOCALE_SISO639LANGNAME, buf, N))
	{
		res[0] = buf[0];
		res[1] = buf[1];

	}

	return res;
}