#include "ADDeviceEmulator.h"
#include "cocos2d.h"
#pragma comment(lib, "Shell32")

using namespace cocos2d;
DeviceConfig::DeviceConfig(const Device type)
    : _device(type),
      _name("undefined"),
      _width(100),
      _height(100),
      _bottom_margin(0),
      _dencity(1.0f)
{

}

ADDeviceEmulator* ADDeviceEmulator::getInstance()
{
    static ADDeviceEmulator obj;
    return &obj;
}

void ADDeviceEmulator::setDevice(const Device device)
{
    _device = device;
}

void ADDeviceEmulator::setOrientation(const Orientation orientation)
{
    _orientation = orientation;
}

void ADDeviceEmulator::setFitTheScreen(bool fit_the_screen)
{
    _fit_screen = fit_the_screen;
}

void ADDeviceEmulator::setLanguage(const std::string& lang_code)
{
    _language = lang_code;
}

std::string encode(const wchar_t* wstr, unsigned int codePage)
{
    int sizeNeeded = WideCharToMultiByte(codePage, 0, wstr, -1, NULL, 0, NULL, NULL);
    char* encodedStr = new char[sizeNeeded];
    WideCharToMultiByte(codePage, 0, wstr, -1, encodedStr, sizeNeeded, NULL, NULL);
    std::string res(encodedStr);
    delete[] encodedStr;
    return res;
}


int ADDeviceEmulator::run()
{
    //Read commanline for test launch
    LPWSTR *szArgList = nullptr;
    int argCount = 0;

    szArgList = CommandLineToArgvW(GetCommandLine(), &argCount);

    if(argCount)
    {
        for(unsigned int i=0; i<argCount; ++i)
        {
            std::string arg = encode(szArgList[i], CP_UTF8);
            if(arg == "TEST")
            {
                CCLog("Test mode enabled!!!");
                _is_auto_testing = true;
                _fit_screen = false;
            }
            else if(_is_auto_testing)
            {
                std::size_t found = arg.find(":");
                if(found != std::string::npos)
                {
                    std::string key = arg.substr(0, found);
                    std::string value = arg.substr(found+1);

                    if(key == "device")
                    {
                        bool found = false;

                        for(auto it: _devices)
                        {
                            if(it.second.getDeviceName() == value)
                            {
                                found = true;
                                _device = it.first;
                                CCLog("Test Device: %s", value.c_str());
                            }
                        }

                        if(!found)
                        {
                            std::cerr << "Undefined device " << value;
                            exit(1);
                        }
                    }
                    else if(key == "orientation")
                    {
                        if(value == "portrait")
                        {
                            _orientation = Orientation::Portrait;
                            CCLog("Test Device Orientation: Portrait");
                        }
                        else
                        {
                            _orientation = Orientation::Landscape;
                            CCLog("Test Device Orientation: Landscape");
                        }
                    }
                    else if(key == "language")
                    {
                        if(value.size() != 2)
                        {
                            std::cerr << "Wrong language '" << value << "' It should be 2 chars code" << std::endl;
                            exit(1);
                        }
                        CCLog("Test Device Language: %s", value.c_str());
                        _language = value;
                    }
                    else if(key == "output")
                    {
                        char last = value[value.size()-1];
                        if(last != '/' && last != '\\')
                            value += '/';

                        CCLog("Test Output Dir: %s", value.c_str());
                        _output_directory = value;
                    }
                    else
                    {
                        std::cerr << "Wrong key: '" << key << "' with value '" << value << "' " << std::endl;
                        exit(1);
                    }
                }
            }
        }
    }

    if(_is_auto_testing)
    {
        runLater(1.0f, [this](){
            performNextTest();
        });
    }
    return launchAsDevice();
}

#include <Windows.h>

void GetDesktopResolution(int& horizontal, int& vertical)
{
    RECT desktop;
    // Get a handle to the desktop window
    const HWND hDesktop = GetDesktopWindow();
    // Get the size of screen to the variable desktop
    GetWindowRect(hDesktop, &desktop);
    // The top left corner will have coordinates (0,0)
    // and the bottom right corner will have coordinates
    // (horizontal, vertical)
    horizontal = desktop.right;
    vertical = desktop.bottom;
}
DeviceConfig ADDeviceEmulator::getDeviceConfig()
{
    auto it = _devices.find(_device);
    if(it != _devices.end())
        return it->second;
    return _devices.begin()->second;
}

int ADDeviceEmulator::launchAsDevice()
{
    DeviceConfig config = getDeviceConfig();

    int swidth = 0;
    int sheight = 0;
    GetDesktopResolution(swidth, sheight);

    swidth -= 50;
    sheight -= 100;

    const float max_width = swidth;
    const float max_height = sheight;

    CCEGLView* eglView = CCEGLView::sharedOpenGLView();

    float width = config.getScreenWidth();
    float height = config.getScreenHeight();

    if(_orientation == Orientation::Landscape)
        height -= config.getScreenBottomMargin();
    else
    {
        width -= config.getScreenBottomMargin();
        std::swap(width, height);
    }


    eglView->setFrameSize(width, height);

    float zoom_factor = 1;
    if(_fit_screen)
    {
        float needed_zoom_factor = MIN(max_width/width, max_height/height);
        zoom_factor = MIN(zoom_factor, needed_zoom_factor);
    }
    eglView->setFrameZoomFactor(zoom_factor);
    return CCApplication::sharedApplication()->run();
}

ADDeviceEmulator::ADDeviceEmulator()
    : _device(Device::Galaxy_Tab2_10),
      _is_auto_testing(false),
      _orientation(Orientation::Landscape),
      _fit_screen(true),
      _language("en"),
      _devices(),
      _tests_performed(0)
{
    //iOS devices
    {
        DeviceConfig d(Device::IPhone3GS);
        d.setDeviceName("IPhone3GS");
        d.setScreenWidth(480);
        d.setScreenHeight(320);
        addDevice(d);
    }

    {
        DeviceConfig d(Device::IPhone4GS);
        d.setDeviceName("IPhone4GS");
        d.setScreenWidth(960);
        d.setScreenHeight(640);
        d.setScreenDencity(2.0);
        addDevice(d);
    }

    {
        DeviceConfig d(Device::IPhone5);
        d.setDeviceName("IPhone5");
        d.setScreenWidth(1136);
        d.setScreenHeight(640);
        d.setScreenDencity(2.0);
        addDevice(d);
    }

    {
        DeviceConfig d(Device::IPhone6);
        d.setDeviceName("IPhone6");
        d.setScreenWidth(1334);
        d.setScreenHeight(750);
        d.setScreenDencity(2.0);
        addDevice(d);
    }

    {
        DeviceConfig d(Device::IPhone6Plus);
        d.setDeviceName("IPhone6Plus");
        d.setScreenWidth(2208);
        d.setScreenHeight(1242);
        d.setScreenDencity(2.0);
        addDevice(d);
    }

    {
        DeviceConfig d(Device::IPad2);
        d.setDeviceName("IPad2");
        d.setScreenWidth(1024);
        d.setScreenHeight(768);
        d.setScreenDencity(1.0);
        addDevice(d);
    }

    {
        DeviceConfig d(Device::IPad4);
        d.setDeviceName("IPad4");
        d.setScreenWidth(2048);
        d.setScreenHeight(1536);
        d.setScreenDencity(2.0);
        addDevice(d);
    }

    {
        DeviceConfig d(Device::IPad4);
        d.setDeviceName("IPad4");
        d.setScreenWidth(2048);
        d.setScreenHeight(1536);
        d.setScreenDencity(2.0);
        addDevice(d);
    }

    //Android devices
    const float DENSITY_ldpi = 0.75f;
    const float DENSITY_mdpi = 1;
    const float DENSITY_hdpi = 1.5f;
    const float DENSITY_xhdpi = 2.0f;
    const float DENSITY_xxhdpi = 3.0f;

    {
        DeviceConfig d(Device::Galaxy_Ace2);
        d.setDeviceName("Galaxy_Ace2");
        d.setScreenWidth(800);
        d.setScreenHeight(480);
        d.setScreenDencity(DENSITY_mdpi);
        addDevice(d);
    }

    {
        DeviceConfig d(Device::Galaxy_S2);
        d.setDeviceName("Galaxy_S2");
        d.setScreenWidth(800);
        d.setScreenHeight(480);
        d.setScreenDencity(DENSITY_hdpi);
        addDevice(d);
    }



    {
        DeviceConfig d(Device::Galaxy_Mini2);
        d.setDeviceName("Galaxy_Mini2");
        d.setScreenWidth(480);
        d.setScreenHeight(320);
        d.setScreenDencity(DENSITY_mdpi);
        addDevice(d);
    }

    {
        DeviceConfig d(Device::HTC_One_S);
        d.setDeviceName("HTC_One_S");
        d.setScreenWidth(960);
        d.setScreenHeight(540);
        d.setScreenDencity(DENSITY_hdpi);
        addDevice(d);
    }

    {
        DeviceConfig d(Device::Motorola_Droid);
        d.setDeviceName("Motorola_Droid");
        d.setScreenWidth(854);
        d.setScreenHeight(480);
        d.setScreenDencity(DENSITY_hdpi);
        addDevice(d);
    }

    {
        DeviceConfig d(Device::Galaxy_Y);
        d.setDeviceName("Galaxy_Y");
        d.setScreenWidth(320);
        d.setScreenHeight(240);
        d.setScreenDencity(DENSITY_ldpi);
        addDevice(d);
    }

    {
        DeviceConfig d(Device::Galaxy_Tab2_7);
        d.setDeviceName("Galaxy_Tab2_7");
        d.setScreenWidth(1024);
        d.setScreenHeight(600);
        d.setScreenBottomMargin(48);
        d.setScreenDencity(DENSITY_hdpi);
        addDevice(d);
    }

    {
        DeviceConfig d(Device::Galaxy_Tab2_10);
        d.setDeviceName("Galaxy_Tab2_10");
        d.setScreenWidth(1280);
        d.setScreenHeight(800);
        d.setScreenBottomMargin(48);
        d.setScreenDencity(DENSITY_mdpi);
        addDevice(d);
    }

    {
        DeviceConfig d(Device::Google_Nexus_10);
        d.setDeviceName("Google_Nexus_10");
        d.setScreenWidth(2560);
        d.setScreenHeight(1600);
        d.setScreenBottomMargin(112);
        d.setScreenDencity(DENSITY_xhdpi);
        addDevice(d);
    }

    {
        DeviceConfig d(Device::Sony_Xperia_Z);
        d.setDeviceName("Sony_Xperia_Z");
        d.setScreenWidth(1920);
        d.setScreenHeight(1080);
        d.setScreenDencity(DENSITY_xxhdpi);
        addDevice(d);
    }

    {
        DeviceConfig d(Device::Kindle_Fire);
        d.setDeviceName("Kindle_Fire");
        d.setScreenWidth(1280);
        d.setScreenHeight(720);
        d.setScreenDencity(DENSITY_hdpi);
        addDevice(d);
    }


    {
        DeviceConfig d(Device::Microsoft_Surface_10);
        d.setDeviceName("Microsoft_Surface_10");
        d.setScreenWidth(1366);
        d.setScreenHeight(768);
        d.setScreenDencity(DENSITY_mdpi);
        addDevice(d);
    }

    {
        DeviceConfig d(Device::HD_720p);
        d.setDeviceName("HD_720p");
        d.setScreenWidth(1280);
        d.setScreenHeight(720);
        d.setScreenDencity(DENSITY_mdpi);
        addDevice(d);
    }

    {
        DeviceConfig d(Device::WindowsPhone_WXGA);
        d.setDeviceName("WindowsPhone_WXGA");
        d.setScreenWidth(1280);
        d.setScreenHeight(768);
        d.setScreenDencity(DENSITY_mdpi);
        addDevice(d);
    }
    {
        DeviceConfig d(Device::AmazonScreenShots);
        d.setDeviceName("AmazonScreenShots");
        d.setScreenWidth(1920);
        d.setScreenHeight(1080);
        d.setScreenDencity(DENSITY_mdpi);
        addDevice(d);
    }


}
void ADDeviceEmulator::setCustomDevice(const DeviceConfig& conf)
{
    getInstance()->addDevice(conf);
}

void ADDeviceEmulator::addTestCase(const TestCase& test_case)
{
    _tests.push(test_case);
}

void ADDeviceEmulator::addDevice(const DeviceConfig& device)
{
    _devices.insert(DeviceMap::value_type(device.getDeviceType(), device));
}
std::string ADDeviceEmulator::getDeviceLanguage()
{
    return _language;
}

float ADDeviceEmulator::getScreenDencity()
{
    DeviceConfig config = getDeviceConfig();
    return config.getScreenDencity();
}
#include <ADLib/Device/ADScreen.h>
void ADDeviceEmulator::createScreenShoot(
        const TestInfo& info,
        const std::string& suffix)
{
    std::stringstream ss;
    ADDeviceEmulator* em = getInstance();
    DeviceConfig config = em->getDeviceConfig();

    ss << em->_output_directory << config.getDeviceName() << '-';
    if(em->_orientation == Orientation::Landscape)
        ss << "L";
    else
        ss << "P";
    ss << '-' << em->_language << '-' << info.getTestName();

    if(suffix.size())
        ss << '-' << suffix;

    ss << ".png";
    CCSize frame = ADScreen::getFrameSize();
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    float SCALE_FACTOR = CCDirector::sharedDirector()->getContentScaleFactor();
    float scale = 1;
    while(frame.width > size.width*scale*SCALE_FACTOR ||
          frame.height > size.height*scale*SCALE_FACTOR)
        scale++;

    CCRenderTexture* texture = CCRenderTexture::createNoScale(
                ADScreen::getFrameSize().width, ADScreen::getFrameSize().height);

    CCDirector::sharedDirector()->setDisplayStats(false);
    texture->beginWithClear(0,0,0,0);
    CCDirector::sharedDirector()->setGLDefaultValues();
    CCDirector::sharedDirector()->drawScene();
    texture->end();

    //CCDirector::sharedDirector()->setGLDefaultValues();

    texture->saveToFile(ss.str().c_str(), kCCImageFormatPNG);
}

class RunLater : public CCObject
{
public:
    typedef ADDeviceEmulator::Action Action;
    static void planAction(const float interval, const Action& action)
    {
        RunLater* obj = new RunLater(action);
        obj->autorelease();
        obj->retain();

        CCDirector::sharedDirector()->getScheduler()->scheduleSelector(
                    schedule_selector(RunLater::doAction),
                    obj,
                    interval,
                    false);
    }
private:
    RunLater(const Action& action)
        : _action(action)
    {}

    void doAction(float)
    {
        CCDirector::sharedDirector()->getScheduler()->unscheduleAllForTarget(this);
        _action();
        this->release();
    }

    Action _action;
};

void ADDeviceEmulator::runLater(const float interval, const Action& action)
{
    RunLater::planAction(interval, action);
}

void ADDeviceEmulator::performNextTest()
{
    if(_tests.empty())
    {
        CCLog("Tests finished");
        exit(0);
    }
    else
    {
        std::stringstream ss;
        ss << "test" << ++_tests_performed;

        TestInfo test(ss.str());

        TestCase test_run = _tests.front();
        _tests.pop();

        CCLog("Running test: %s", ss.str().c_str());

        test_run(test);
    }
}
TestInfo::TestInfo(const std::string& test_name)
    : _test_name(test_name)
{}
void TestInfo::finish() const
{
    ADDeviceEmulator::getInstance()->performNextTest();
}
