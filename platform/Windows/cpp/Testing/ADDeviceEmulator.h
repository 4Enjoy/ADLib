#ifndef ADDEVICEEMULATOR_H
#define ADDEVICEEMULATOR_H
#include <string>
#include <map>
#include <ADLib/Generic/ADMethods.h>
#include <functional>
#include <queue>

enum class Device
{
    //IPhones
    IPhone3GS, //480x320
    IPhone4GS, //960x640
    IPhone5, //1136x640,
    IPhone6, //1334x750,
    IPhone6Plus, //2208x1242,
    //IPad
    IPad2, //1024x768
    IPad4, //2048x1536
    //Android phones
    Galaxy_Ace2, //62% 800x480
    Galaxy_S2, //800x480 xhdpi
    Galaxy_Mini2, //14% 480x320
    HTC_One_S, //6% 960x540
    Motorola_Droid, //5% 854x480
    Galaxy_Y, //5% 320x240
    //Android tablets
    Galaxy_Tab2_7, //74% 1024x600
    Galaxy_Tab2_10, //22% 1280x800
    Google_Nexus_10, //2560x1600

    Sony_Xperia_Z,

    Kindle_Fire, //1280x720px,

    Microsoft_Surface_10, //1366x768
    HD_720p, //1280x720,
    Custom,
    WindowsPhone_WXGA, //1280x768
    AmazonScreenShots //1920x1080
};

enum class Orientation
{
    Landscape,
    Portrait
};

class DeviceConfig
{
public:
    DeviceConfig(const Device type);

    AD_Selector(Device, _device, DeviceType);
    AD_SelectorModifier(const std::string&, _name, DeviceName);
    AD_SelectorModifier(unsigned int, _width, ScreenWidth);
    AD_SelectorModifier(unsigned int, _height, ScreenHeight);
    AD_SelectorModifier(unsigned int, _bottom_margin, ScreenBottomMargin);
    AD_SelectorModifier(float, _dencity, ScreenDencity);
private:
    Device _device;
    std::string _name;
    unsigned int _width;
    unsigned int _height;
    unsigned int _bottom_margin;
    float _dencity;
};

class TestInfo
{
public:
    TestInfo(const std::string& test_name);
    AD_Selector(const std::string&, _test_name, TestName);
    void finish() const;
private:
    std::string _test_name;
};

typedef std::function<void (TestInfo)> TestCase;


/**
 * @brief Class for adapting screen size to
 * specifiec devices and for auto testing
 *
 * TO RUN AUTO TESTING
 * 1. add test actions to using addTestAction
 * 2. pass to command line as argument
 * TEST device:<IPhone3GS|IPhone4GS...> orientation:<landscape|portrait> language:<uk|en|...> output:<dir>
 * 3. call run
 *
 * TO LAUNCH NORMALLY
 * 1. call run
 */
class ADDeviceEmulator
{
public:


    static ADDeviceEmulator* getInstance();

    /**
     * @brief Set the device. Each device has own
     * dencity and screen size
     * @param device
     */
    void setDevice(const Device device);

    /**
     * @brief How the device will run in vertical or in horizontal
     * @param orientation
     */
    void setOrientation(const Orientation orientation);

    /**
     * @brief Whether device will fit the screen
     * @param fit_the_screen if true than the device will exactly fit the screen.
     * if false then the device will be shown in real size
     */
    void setFitTheScreen(bool fit_the_screen);

    /**
     * @brief Sets language code of current device
     * @param lang_code
     */
    void setLanguage(const std::string& lang_code);

    /**
     * @brief If command line argument TEST given launches application
     * in testing mode.
     * Else launches application normally
     * @return
     */
    int run();

    /**
     * @brief Returns the emulator language
     * @return
     */
    std::string getDeviceLanguage();

    /**
     * @brief Returns the emulator dencity
     * @return
     */
    float getScreenDencity();

    /**
     * @brief Add test to run in test case
     * @param test_case
     */
    void addTestCase(const TestCase& test_case);

    /**
     * @brief Creates screenshoot in directory
     * <output_dir>/<device>-<orientation>-<lang>-<test_name>-<suffix>.png
     * @param info test info
     * @param suffix additional info if the multiple screenshoot are made in one test
     */
    static void createScreenShoot(const TestInfo& info,
                                  const std::string& suffix="");

    typedef std::function<void ()> Action;

    /**
     * @brief Adds action to queue and performs this action after
     * given interval.
     *
     * @param interval time in seconds after which action will be perfomed
     * @param action action to perform
     */
    static void runLater(const float interval, const Action& action);

    static void setCustomDevice(const DeviceConfig& conf);
private:
    bool _is_auto_testing;
    Device _device;
    Orientation _orientation;
    bool _fit_screen;
    std::string _language;

    std::string _output_directory;

    typedef std::map<Device, DeviceConfig> DeviceMap;
    DeviceMap _devices;

    DeviceConfig getDeviceConfig();
    void addDevice(const DeviceConfig& device);
    int launchAsDevice();

    ADDeviceEmulator();

    typedef std::queue<TestCase> TestQueue;

    unsigned int _tests_performed;
    TestQueue _tests;
    void performNextTest();
    friend class TestInfo;
};

#endif // ADDEVICEEMULATOR_H
