#include "ADHandleUrl.h"

void ADHandleUrl::addCallback(const Callback& c)
{
    _callbacks.push_back(c);
}

bool ADHandleUrl::handleUrl(NSURL* url,
                      NSString* content)
{
    bool res = false;
    
    for(Callback& c : _callbacks)
    {
        bool one_res = c(url, content);
        res = res || one_res;
    }
    
    return res;
}

std::vector<ADHandleUrl::Callback> ADHandleUrl::_callbacks;