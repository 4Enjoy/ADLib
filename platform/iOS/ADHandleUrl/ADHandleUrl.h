#ifndef ADHANDLE_URL_H
#define ADHANDLE_URL_H

#include <vector>
#include <functional>

class ADHandleUrl
{
public:
    typedef std::function<bool (NSURL*, NSString*)> Callback;
    static void addCallback(const Callback&);
    
    static bool handleUrl(NSURL* url,
                          NSString* content);
private:
    static std::vector<Callback> _callbacks;
};

#endif