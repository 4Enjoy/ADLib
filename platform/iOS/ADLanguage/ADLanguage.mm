#include <ADLib/Device/ADLanguage.h>
#import <Foundation/Foundation.h>

std::string ADLanguage::platformGetDeviceLanguage()
{
    NSString* lang = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    std::string lang_code([lang UTF8String]);
    if(lang_code.size() < 2)
        return "en";
    return lang_code.substr(0, 2);
}