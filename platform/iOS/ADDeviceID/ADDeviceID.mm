#include <ADLib/Device/ADDeviceID.h>
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

std::string ADDeviceID::getDeviceID()
{
    NSUUID *identifierForVendor =  [[UIDevice currentDevice] identifierForVendor];
    NSString *deviceId = [identifierForVendor UUIDString];
    return [deviceId UTF8String];
    
}