#include <ADLib/Device/ADAds.h>
#include <vector>
#include <map>
#import "AdMobSDK/GADBannerView.h"
#include "../ADUIThread/ADUIThread.h"
#import "AdMobSDK/GADBannerViewDelegate.h"
#import "AdMobSDK/GADInterstitial.h"
@interface ADListenter : NSObject<GADBannerViewDelegate> {
}
@end

//@interface InterstitialListener : NSObject<GADInterstitialDelegate>{
//}
//@end



class ADAdsHelper
{
public:
    static float getDensity()
    {
        static float density = [[UIScreen mainScreen] scale];
        return density;
    }
    
    static void addTestDevice(const ADAds::DeviceID& device)
    {
        NSString* str = [[NSString alloc] initWithUTF8String:device.c_str()];
        [str retain];
        
        [_test_devices addObject:str];
    }
    
    static GADAdSize getAdSize(const ADAds::BannerType& type)
    {
        if(type == "BANNER")
        {
            return kGADAdSizeBanner;
        }
        else if(type == "IAB_MRECT")
        {
            return kGADAdSizeMediumRectangle;
        }
        else if(type == "IAB_BANNER")
        {
            return kGADAdSizeFullBanner;
        }
        else if(type == "IAB_LEADERBOARD")
        {
            return kGADAdSizeLeaderboard;
        }
        return kGADAdSizeBanner;
    }
    
    static void createBanner(const ADAds::BannerID id,
                             const ADAds::BannerType& type,
                             const ADAds::UnitID &unit_id)
    {
        if(_banners.find(id) != _banners.end())
        {
            cocos2d::CCLog("ADAds: Banner with id=%id exists", id);
            return;
        }
        
        
        UIWindow* window = [[UIApplication sharedApplication] keyWindow];
        UIViewController* root_view = window.rootViewController;
        
        GADAdSize ad_size = getAdSize(type);
        GADBannerView* banner = [[GADBannerView alloc] initWithAdSize:ad_size];
        
        NSString* unit_id_obj = [NSString stringWithUTF8String:unit_id.c_str()];
        banner.adUnitID = unit_id_obj;
        banner.rootViewController = root_view;
        [root_view.view addSubview:banner];
        
        banner.hidden = TRUE;
        ADListenter* delegate =[[ADListenter alloc] init];
        [banner setDelegate:delegate];
        
        
        GADRequest* request = [GADRequest request];
        request.testDevices = _test_devices;
        
        [banner loadRequest:request];
        
        [banner retain];
        
        _banners[id] = banner;
        _loaded[id] = false;
    }
    
    static void showBanner(const ADAds::BannerID id)
    {
        auto it = _banners.find(id);
        if(it == _banners.end())
        {
            cocos2d::CCLog("ADAds: Banner with id=%id not found", id);
            return;
        }
        
        GADBannerView* banner = it->second;
        banner.hidden = FALSE;
    }
    
    static void hideBanner(const ADAds::BannerID id)
    {
        auto it = _banners.find(id);
        if(it == _banners.end())
        {
            cocos2d::CCLog("ADAds: Banner with id=%id not found", id);
            return;
        }
        
        GADBannerView* banner = it->second;
        banner.hidden = TRUE;
        
        
    }
    static void moveBanner(const ADAds::BannerID id, const cocos2d::CCPoint& position)
    {
        auto it = _banners.find(id);
        if(it == _banners.end())
        {
            cocos2d::CCLog("ADAds: Banner with id=%id not found", id);
            return;
        }
        
        GADBannerView* banner = it->second;
        CGRect frame = banner.frame;
        
        float density = getDensity();
        frame.origin.x = position.x / density;
        frame.origin.y = position.y / density;
        
        banner.frame = frame;
    }
    static bool isBannerLoaded(const ADAds::BannerID id)
    {
        bool res = false;
        auto it = _loaded.find(id);
        if(it != _loaded.end())
        {
            res = it->second;
        }
        return res;
    }
    
    static void markBannerAsLoaded(GADBannerView* banner)
    {
        for(auto& it : _banners)
        {
            if(it.second == banner)
            {
                _loaded[it.first] = true;
            }
        }
    }
    
    static void showInterstitial()
    {
        if(_interstitial != nil)
        {
            if([_interstitial isReady] == TRUE)
            {
                UIWindow* window = [[UIApplication sharedApplication] keyWindow];
                UIViewController* root_view = window.rootViewController;
                [_interstitial presentFromRootViewController: root_view];
                
                [_interstitial release];
                _interstitial = nil;
                _interstitial_shown += 1;
            }
        }
    }
    
    static void prepareInterstitial(const ADAds::UnitID & unit_id)
    {
        if(_interstitial == nil)
        {
            _interstitial = [[GADInterstitial alloc] init];
            _interstitial.adUnitID = [NSString stringWithUTF8String:unit_id.c_str()];
            GADRequest* request = [GADRequest request];
            request.testDevices = _test_devices;
            
            [_interstitial loadRequest:request];
            [_interstitial retain];
        }
    }
    
    static unsigned int getInterstialTimesShowed()
    {
        return _interstitial_shown;
    }
private:
    static NSMutableArray* _test_devices;
    static std::map<ADAds::BannerID, GADBannerView*> _banners;
    static std::map<ADAds::BannerID, bool> _loaded;
    static GADInterstitial* _interstitial;
    static unsigned int _interstitial_shown;
    
    
};

@implementation ADListenter

-(void)adViewDidReceiveAd:(GADBannerView *)view{
    ADAdsHelper::markBannerAsLoaded(view);
}

@end

NSMutableArray* ADAdsHelper::_test_devices = [[NSMutableArray alloc] initWithObjects:/*GAD_SIMULATOR_ID,*/ nil];
std::map<ADAds::BannerID, GADBannerView*> ADAdsHelper::_banners;
std::map<ADAds::BannerID, bool> ADAdsHelper::_loaded;
GADInterstitial* ADAdsHelper::_interstitial = nil;
unsigned int ADAdsHelper::_interstitial_shown = 0;

void ADAds::Platform::addTestDevice(const DeviceID& device)
{
    ADAdsHelper::addTestDevice(device);
}

cocos2d::CCSize ADAds::Platform::getBannerSize(const BannerType& type)
{
    cocos2d::CCSize base(10000,10000);
    float density = ADAdsHelper::getDensity();
    
    if(type == "BANNER")
    {
        base.width = 320;
        base.height = 50;
    }
    else if(type == "IAB_MRECT")
    {
        base.width = 300;
        base.height = 250;
    }
    else if(type == "IAB_BANNER")
    {
        base.width = 468;
        base.height = 60;
    }
    else if(type == "IAB_LEADERBOARD")
    {
        base.width = 728;
        base.height = 90;
    }
    
    base.width *= density;
    base.height *= density;
    return base;
}


bool ADAds::Platform::createBanner(const BannerID id, const BannerType& type, const UnitID &unit_id)
{
    ADUIThread::onUIThread([=](){
        ADAdsHelper::createBanner(id, type, unit_id);
    });
    return true;
}

void ADAds::Platform::showBanner(const BannerID id)
{
    ADUIThread::onUIThread([=](){
        ADAdsHelper::showBanner(id);
    });
}

void ADAds::Platform::hideBanner(const BannerID id)
{
    ADUIThread::onUIThread([=](){
        ADAdsHelper::hideBanner(id);
    });
}


void ADAds::Platform::isBannerReady(const BannerID id, const Callback& callback)
{
    callback(id, ADAdsHelper::isBannerLoaded(id));
}

void ADAds::Platform::moveBanner(const BannerID id, const cocos2d::CCPoint& position)
{
    ADUIThread::onUIThread([=](){
        ADAdsHelper::moveBanner(id, position);
    });
}

void ADAds::Platform::showInterstitial()
{
    ADUIThread::onUIThread([=](){
        ADAdsHelper::showInterstitial();
    });}

void ADAds::Platform::prepareInterstitial(const UnitID & unit)
{
    ADUIThread::onUIThread([=](){
        ADAdsHelper::prepareInterstitial(unit);
    });}

unsigned int ADAds::Platform::getInterstialTimesShowed()
{
    return ADAdsHelper::getInterstialTimesShowed();
}


