#include <ADLib/Social/ADFacebook.h>
#include "../ADUIThread/ADUIThread.h"
#include "../ADHandleUrl/ADHandleUrl.h"
#import "FacebookSDK/FacebookSDK.h"

class FacebookIOS : public ADFacebook::Platform
{
public:
    FacebookIOS()
    {
        _obj = this;
        
        auto callback = [](NSURL* url, NSString* cont) -> bool {
            return [FBAppCall handleOpenURL:url sourceApplication:cont];
        };
        
        ADHandleUrl::addCallback(callback);
    }
    
    static FacebookIOS* getInstance()
    {
        return _obj;
    }
    
    /**
     * @brief How to implement
     * 1. Start session without GUI
     * 2. Load info about Me and friends
     * @param key
     */
    void loadData()
    {
        openActiveSession(false);
    }
    
    static ADFacebookFriendPtr parseFriend(NSDictionary* f, NSString* id_name)
    {
        NSString* sid = [f objectForKey:id_name];
        NSString* slast_name = [f objectForKey:@"name"];
        //NSString* sfirst_name = [f objectForKey:@"first_name"];
        
        std::string id_([sid UTF8String]);
        std::string last_name_([slast_name UTF8String]);
        //std::string first_name_([sfirst_name UTF8String]);
        
        std::stringstream ss;
        ss << id_;
        
        uint64_t id_int = 0;
        ss >> id_int;
        
        ADFacebookFriendPtr res = std::make_shared<ADFacebookFriend>();
        res->setUserID(id_int);
        res->setFirstName(last_name_);
        //res->setLastName(last_name_);
        
        return res;
    }
    
    static void updateFriendInfo()
    {
        
        NSDictionary *queryParam = @{ };
        
        [FBRequestConnection startWithGraphPath:@"/me/friends" parameters:queryParam HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection, id result, NSError* error)
         {
             if(!error)
             {
                 ADFacebookFriendPtrArr friends;
                 
                 
                 NSArray* data = [result objectForKey:@"data"];
                 for(NSDictionary* f_data in data)
                 {
                     friends.push_back(parseFriend(f_data, @"id"));
                 }
                 
                 
                 ADUIThread::onCocos2dThread([friends](){
                     _obj->friendsLoaded(friends);
                 });
             }
         }];
    }
    
    static void updateMyInfo()
    {
        [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            if(!error)
            {
                ADFacebookFriendPtr me = parseFriend(result, @"id");
                ADUIThread::onCocos2dThread([me](){
                    _obj->myInfoLoaded(me);
                });
            }
        }];
    }
    
    static void sessionStateChanged(FBSession *session, FBSessionState state)
    {
        if(state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed)
        {
            ADUIThread::onCocos2dThread([](){
                FacebookIOS::_obj->sessionClosed();
            });
        }
        else if(state == FBSessionStateOpen)
        {
            NSString *token = [FBSession activeSession].accessTokenData.accessToken;
            std::string token_cpp([token UTF8String]);
            
            ADUIThread::onCocos2dThread([token_cpp](){
                FacebookIOS::_obj->sessionOpened(token_cpp);
            });
            
            if(!_is_new_friends_loaded)
            {
                updateFriendInfo();
                updateMyInfo();
            }
            
        }
    }
    void openActiveSession(bool show_ui)
    {
        //if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
        
        // If there's one, just open the session silently, without showing the user the login UI
        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile", @"user_friends"]
                                              allowLoginUI:show_ui
                                         completionHandler:^(FBSession *session, FBSessionState state, NSError *error)
         {
             
             if(state != FBSessionStateOpen)
             {
                 FacebookIOS::sessionStateChanged(session, state);
             }
             else
             {
                 [FBSession openActiveSessionWithPublishPermissions:@[@"publish_actions"]
                                                    defaultAudience: FBSessionDefaultAudienceEveryone
                                                       allowLoginUI:show_ui
                                                  completionHandler:^(FBSession *session, FBSessionState state, NSError *error)
                  {
                      FacebookIOS::sessionStateChanged(session, state);
                  }];
             }
         }];
        
        
        //}
    }
    
    /**
     * @brief How to implement:
     * 1. If not logged in show log in window
     */
    void logIn()
    {
        openActiveSession(true);
    }
    
    
    void logOut()
    {
        [FBSession.activeSession closeAndClearTokenInformation];
    }
    
    static NSMutableDictionary* parseURLParams(NSString *query)
    {
        NSArray *pairs = [query componentsSeparatedByString:@"&"];
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        for (NSString *pair in pairs) {
            NSArray *kv = [pair componentsSeparatedByString:@"="];
            NSString *val =
            [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            params[kv[0]] = val;
        }
        return params;
    }
    
    
    static int pushShareCallback(const ADFacebook::ShareResult &res)
    {
        auto nid = _share_next_id++;
        
        _share_map[nid] = res;
        
        return nid;
    }
    
    static void sendShareResult(const int nid, bool res)
    {
        ADUIThread::onCocos2dThread([=](){
        auto it = _share_map.find(nid);
        if(it != _share_map.end())
        {
            it->second(res);
            _share_map.erase(it);
        }
        });
    }
    
    void inviteFriends(const std::string& message)
    {
        //message:[NSString stringWithUTF8String:mess.c_str()]
        //title:nil
        std::string mess = message;
        ADUIThread::onUIThread([mess](){
            
            NSMutableDictionary *p = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                      [NSString stringWithUTF8String:mess.c_str()], @"message",
                                      
                                      nil];
            
            [FBWebDialogs
             presentDialogModallyWithSession:nil
             dialog:@"apprequests"
             parameters:p
             handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {}
             ];
        });
        
    }
    
    void share(const std::string &name,
               const std::string &caption,
               const std::string &description,
               const std::string &url,
               const std::string &picture,
               const ADFacebook::ShareResult &res)
    {
        int nid = pushShareCallback(res);
        auto share_action = [=]()
        {
            NSString* c_url = [NSString stringWithUTF8String:url.c_str()];
            NSString* c_picture = [NSString stringWithUTF8String:picture.c_str()];
            
            FBShareDialogParams *params = [[FBShareDialogParams alloc] init];
            params.link = [NSURL URLWithString:c_url];
            params.name = [NSString stringWithUTF8String:name.c_str()];
            params.caption = [NSString stringWithUTF8String:caption.c_str()];
            //params.description = [NSString stringWithUTF8String:description.c_str()];
            params.picture = [NSURL URLWithString:c_picture];
            
            
            if([FBDialogs canPresentShareDialogWithParams:params])
            {
                [FBDialogs presentShareDialogWithLink:params.link
                                                 name:params.name
                                              caption:params.caption
                                          description:params.description
                                              picture:params.picture
                                          clientState:nil
                                              handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                                  bool ok = false;
                                                  if(!error)
                                                  {
                                                      ok = true;
                                                  }
                                                  
                                                  sendShareResult(nid, ok);
                                              }];
                
            }
            else
            {
                // Put together the dialog parameters
                NSMutableDictionary *p = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                               params.name, @"name",
                                               params.caption, @"caption",
                                               params.description, @"description",
                                               c_url, @"link",
                                               c_picture, @"picture",
                                               nil];
                
                // Show the feed dialog
                [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                                       parameters:p
                                                          handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                              bool ok = false;
                                                              if (!error)
                                                              {
                                                                  if (result != FBWebDialogResultDialogNotCompleted) {
                                                                      // Handle the publish feed callback
                                                                      NSDictionary *urlParams = parseURLParams([resultURL query]);
                                                                      
                                                                      if ([urlParams valueForKey:@"post_id"])
                                                                      {
                                                                          // User clicked the Share button
                                                                          NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
                                                                          NSLog(@"result %@", result);
                                                                          ok = true;
                                                                      }
                                                                  }
                                                              }
                                                              sendShareResult(nid, ok);
                                                          }];
            }
            
        };
        ADUIThread::onUIThread(share_action);
    }
    
    
    void shareResult(int sid, bool success)
    {
        auto it = _share_map.find(sid);
        
        if(it != _share_map.end())
        {
            auto callback = it->second;
            if(callback)
                callback(success);
            _share_map.erase(it);
        }
    }
    
private:
    static bool _is_new_friends_loaded;
    static FacebookIOS* _obj;
    typedef std::map<int, ADFacebook::ShareResult> ShareMap;
    
    static ShareMap _share_map;
    static int _share_next_id;
};

FacebookIOS::ShareMap FacebookIOS::_share_map;
int FacebookIOS::_share_next_id=0;
FacebookIOS* FacebookIOS::_obj = nullptr;
bool FacebookIOS::_is_new_friends_loaded = false;

ADFacebook::PlatformPtr ADFacebook::getImplementation()
{
    return std::make_shared<FacebookIOS>();
}
