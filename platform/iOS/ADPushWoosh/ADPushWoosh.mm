#include <ADLib/Device/ADPushWoosh.h>
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <Pushwoosh/PushNotificationManager.h>
#include "../ADAppEvents/ADAppEvents.h"
class EventResponse : public HasSlots
{
public:
    void didRegisterForRemoteNotificationsWithDeviceToken(NSData * deviceToken)
    {
        [[PushNotificationManager pushManager] handlePushRegistration:deviceToken];
    }
    
    // system push notification registration error callback, delegate to pushManager
    void  didFailToRegisterForRemoteNotificationsWithError(NSError * error) {
        [[PushNotificationManager pushManager] handlePushRegistrationFailure:error];
    }
    
    // system push notifications callback, delegate to pushManager
    void didReceiveRemoteNotification(NSDictionary * userInfo) {
        [[PushNotificationManager pushManager] handlePushReceived:userInfo];
    }
};

EventResponse response;

void ADPushWoosh::inizialize(const std::string& app_id_c, const std::string& platform_parameter)
{
    CONNECT(ADAppEvents::getInstance().signalDidRegisterForRemoteNotificationsWithDeviceToken, &response, &EventResponse::didRegisterForRemoteNotificationsWithDeviceToken);
    CONNECT(ADAppEvents::getInstance().siganlDidReceiveRemoteNotification, &response, &EventResponse::didReceiveRemoteNotification);
    CONNECT(ADAppEvents::getInstance().signalDidFailToRegisterForRemoteNotificationsWithError, &response, &EventResponse::didFailToRegisterForRemoteNotificationsWithError);
    
    NSString* app_id = [NSString stringWithUTF8String:app_id_c.c_str()];
    NSString* app_name = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    
    [PushNotificationManager initializeWithAppCode:app_id appName:app_name];
    
    
    // make sure we count app open in Pushwoosh stats
    [[PushNotificationManager pushManager] sendAppOpen];
    
    // register for push notifications!
    [[PushNotificationManager pushManager] registerForPushNotifications];
}

