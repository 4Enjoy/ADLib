#include <ADLib/Device/ADAds.h>
#include <vector>
#include <map>
#include "../ADUIThread/ADUIThread.h"


#import "HeyzapAds/HeyzapAds.h"

#include "../HeyZap.h"

class ADAdsHelper
{
public:
    
    static void showInterstitial()
    {
        if ([HZInterstitialAd isAvailable]) {
            [HZInterstitialAd show];
            _interstitial_shown++;
            [HZInterstitialAd fetch];
        }
        
    }
    
    static void prepareInterstitial(const ADAds::UnitID & unit_id)
    {
        if(!_is_inited)
        {
            _is_inited = true;
            NSString* adunit = [NSString stringWithUTF8String:unit_id.c_str()];
            [HeyzapAds startWithPublisherID: adunit];
            
            emit HeyZap::getInstance()->signalHeyZapReady();
        }
        [HZInterstitialAd fetch];
        
    }
    
    static unsigned int getInterstialTimesShowed()
    {
        return _interstitial_shown;
    }
private:
    static bool _is_inited;
    static unsigned int _interstitial_shown;
    
    
};

unsigned int ADAdsHelper::_interstitial_shown = 0;
bool ADAdsHelper::_is_inited = false;

void ADAds::Platform::addTestDevice(const DeviceID& device)
{
    
}

cocos2d::CCSize ADAds::Platform::getBannerSize(const BannerType& type)
{
    cocos2d::CCSize base(10000,10000);
    return base;
}


bool ADAds::Platform::createBanner(const BannerID id, const BannerType& type, const UnitID &unit_id)
{
    
    return true;
}

void ADAds::Platform::showBanner(const BannerID id)
{
}

void ADAds::Platform::hideBanner(const BannerID id)
{
    
}


void ADAds::Platform::isBannerReady(const BannerID id, const Callback& callback)
{
    
}

void ADAds::Platform::moveBanner(const BannerID id, const cocos2d::CCPoint& position)
{
    
}

void ADAds::Platform::showInterstitial()
{
    ADUIThread::onUIThread([=](){
        ADAdsHelper::showInterstitial();
    });}

void ADAds::Platform::prepareInterstitial(const UnitID & unit)
{
    ADUIThread::onUIThread([=](){
        ADAdsHelper::prepareInterstitial(unit);
    });}

unsigned int ADAds::Platform::getInterstialTimesShowed()
{
    return ADAdsHelper::getInterstialTimesShowed();
}


