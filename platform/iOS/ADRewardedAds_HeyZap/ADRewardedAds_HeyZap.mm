#include <ADLib/Device/ADRewardedAds.h>

#include <map>

#include "../ADUIThread/ADUIThread.h"
#include "../HeyZap.h"
#import "HeyzapAds/HeyzapAds.h"
#include <ADLib/Generic/ADMethods.h>



@interface ShowDelegate : NSObject<HZIncentivizedAdDelegate, HZAdsDelegate> {
}

@end

using namespace cocos2d;
class HeyZapRewardedHelper : public HasSlots
{
public:

    static HeyZapRewardedHelper& getInstance();

    ///UI Thread Only
    void init();
    void onHeyZapReady();

    ///UI Thread Only
    void prepare();

    ///UI Thread Only
    void show();

    bool _is_avaliable;
    std::string _last_id;

    void notifyResult(bool result);
private:
    bool _is_inited;
    HeyZapRewardedHelper();
    static HeyZapRewardedHelper* _instance;

};
void HeyZapRewardedHelper::onHeyZapReady()
{
    cocos2d::CCLog("Init rewarded: Ready");
    ShowDelegate* show = [[ShowDelegate alloc] init];
    [HZIncentivizedAd setDelegate:show];
    //TODO: set HeyZap Callbacks
    _is_inited = true;
    prepare();
}

void HeyZapRewardedHelper::init()
{
    cocos2d::CCLog("Init rewarded");
    CONNECT(HeyZap::getInstance()->signalHeyZapReady, this, &HeyZapRewardedHelper::onHeyZapReady);
}

void HeyZapRewardedHelper::prepare()
{
    if(_is_inited)
    {
        [HZIncentivizedAd fetch];
    }
}


void HeyZapRewardedHelper::show()
{
    if ([HZIncentivizedAd isAvailable]) {
        [HZIncentivizedAd show];
        _is_avaliable = false;
        
        prepare();
    }
}

void HeyZapRewardedHelper::notifyResult(bool result)
{
    ADUIThread::onCocos2dThread([this, result](){
        if(result)
        {
            emit ADRewardedAds::signalAdsShownSuccess(_last_id);
        }
        else
        {
            emit ADRewardedAds::signalAdsShownFail(_last_id);
        }
    });

}


@implementation ShowDelegate
- (void) didCompleteAdWithTag: (NSString *)tag {
    HeyZapRewardedHelper::getInstance().notifyResult(true);
}

- (void) didFailToCompleteAdWithTag: (NSString *)tag {
    
    HeyZapRewardedHelper::getInstance().notifyResult(false);
}

- (void) didReceiveAdWithTag:(NSString *)tag {
    // Sent when an interstitial ad has been
    HeyZapRewardedHelper::getInstance()._is_avaliable = true;
}
@end

HeyZapRewardedHelper* HeyZapRewardedHelper::_instance = 0;

HeyZapRewardedHelper& HeyZapRewardedHelper::getInstance()
{
    if(_instance == 0)
    {
        _instance = new HeyZapRewardedHelper;
    }
    return *_instance;
}

HeyZapRewardedHelper::HeyZapRewardedHelper()
    : _is_avaliable(false), _is_inited(false)
{
}



void ADRewardedAds::show(const std::string& ad)
{
    ADUIThread::onUIThread([ad](){
        HeyZapRewardedHelper::getInstance()._last_id = ad;
        HeyZapRewardedHelper::getInstance().show();
    });
}

void ADRewardedAds::prepare()
{
    ADUIThread::onUIThread([](){
        HeyZapRewardedHelper::getInstance().prepare();
    });
}

void ADRewardedAds::init()
{
    ADUIThread::onUIThread([](){
        HeyZapRewardedHelper::getInstance().init();
    });
}

bool ADRewardedAds::isAvaliable()
{
    return HeyZapRewardedHelper::getInstance()._is_avaliable;
}
