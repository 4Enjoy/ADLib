#include <ADLib/ADSignals.h>

class ADAppEvents
{
public:
    
    Signal<NSData *> signalDidRegisterForRemoteNotificationsWithDeviceToken;
    Signal<NSError *> signalDidFailToRegisterForRemoteNotificationsWithError;
    Signal<NSDictionary *> siganlDidReceiveRemoteNotification;
    
    
    static ADAppEvents& getInstance()
    {
        static ADAppEvents obj;
        return obj;
    }
};
