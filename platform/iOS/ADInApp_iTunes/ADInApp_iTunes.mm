#include <ADLib/Device/ADInApp.h>
#import <StoreKit/StoreKit.h>
#include "../ADUIThread/ADUIThread.h"
#include <thread>
@interface ProductRequestDelegate : NSObject<SKProductsRequestDelegate> {
}
@end

@interface ProductRequestAndBuy : NSObject<SKProductsRequestDelegate> {
}
@end

@interface PurchaseObserver : NSObject<SKPaymentTransactionObserver> {
}

@end


namespace ADInAppImpl
{
    void do_restorePurchases(bool report)
    {
       
        [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
    }
    
    void do_buyProduct(SKProduct* p)
    {
        SKMutablePayment* payment = [SKMutablePayment paymentWithProduct:p];
        payment.quantity = 1;
        
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }
    
    typedef std::map<ADInApp::ProductID, SKProduct*> PMap;
    typedef std::map<std::string, ADInApp::ProductID> SKUMap;
    PMap _avaliable_products;
    
    SKUMap _product_index;
    std::mutex _avaliable_products_mutex;
    std::string _last_sku = "";
}

using namespace ADInAppImpl;

ADInApp::ErrorType getErrorType(NSError* error)
{
    ADInApp::ErrorType res = ADInApp::ErrorType::Error;
    if([error.domain isEqualToString:SKErrorDomain])
    {
        int error_code = error.code;
        
        if(error_code == SKErrorPaymentCancelled)
            res = ADInApp::ErrorType::UserCanceled;
    }
    return res;
}

void ADInApp::Platform::buyProduct(const Product& p)
{
    ADUIThread::onUIThread([p](){
        SKProduct* product = nil;
        
        _avaliable_products_mutex.lock();
        auto it = _avaliable_products.find(p.getID());
        if(it != _avaliable_products.end())
        {
            product = it->second;
        }
        _avaliable_products_mutex.unlock();
        
        _last_sku = "";
        
        if(product != nil)
            do_buyProduct(product);
        else
        {
            std::string itunes_sku = p.getID();
            if(p.hasParameter("asku"))
            {
                itunes_sku = p.getParameter("asku");
            }
            
            _product_index[itunes_sku] = p.getID();
            
            NSString* sku = [NSString stringWithUTF8String:itunes_sku.c_str()];
            //Request product info one more time
            SKProductsRequest* products_request = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObjects: sku, nil]];
            
            ProductRequestAndBuy* delegate = [[ProductRequestAndBuy alloc] init];
            products_request.delegate = delegate;
            
            [products_request start];
        }
    });
    
}
void ADInApp::Platform::loadStore(const std::string&, const Mode)
{
    ADUIThread::onUIThread([=](){
        auto& products = ADInApp::getAllProducts();
        NSMutableArray* product_list = [NSMutableArray arrayWithCapacity:products.size()];
        for(auto& it : products)
        {
            const Product& p = it.second;
            
            std::string itunes_sku = p.getID();
            if(p.hasParameter("asku"))
            {
                itunes_sku = p.getParameter("asku");
            }
            
            _product_index[itunes_sku] = p.getID();
            
            NSString* sku = [NSString stringWithUTF8String:itunes_sku.c_str()];
            [product_list addObject:sku];
        }
        
        SKProductsRequest* products_request = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithArray:product_list]];
        
        ProductRequestDelegate* delegate = [[ProductRequestDelegate alloc] init];
        products_request.delegate = delegate;
        
        [products_request start];
        
        PurchaseObserver* observer = [[PurchaseObserver alloc] init];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:observer];
        
        //do_restorePurchases(false);
    });
}
void ADInApp::Platform::restorePurchases()
{
    ADUIThread::onUIThread([](){
        do_restorePurchases(true);
    });
}

@implementation PurchaseObserver
-(void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for(SKPaymentTransaction* transaction in transactions)
    {
        std::string sku = _last_sku;
        
        bool transaction_finished = false;
        if(transaction.payment != nil && transaction.payment.productIdentifier != nil)
        {
            NSString* product_id = transaction.payment.productIdentifier;
            sku = std::string([product_id UTF8String]);
            sku = _product_index[sku];
        }
        //cocos2d::CCLog("Transaction: %s", sku.c_str());
        
        if (transaction.transactionState == SKPaymentTransactionStatePurchased)
        {
            ADUIThread::onUIThread([sku](){
                ADInApp::Platform::purchaseSuccessful(sku,
                                                      ADInApp::OperationType::Purchased);
            });
            _last_sku = "";
            transaction_finished = true;
        }
        else if(transaction.transactionState == SKPaymentTransactionStateFailed)
        {
            ADInApp::ErrorType error_type = getErrorType(transaction.error);
            ADUIThread::onUIThread([sku,error_type](){
                ADInApp::Platform::purchaseFailed(sku,
                                                  ADInApp::OperationType::Purchased,
                                                  error_type);
            });
            _last_sku = "";
            transaction_finished = true;
        }
        else if(transaction.transactionState == SKPaymentTransactionStateRestored)
        {
            ADUIThread::onUIThread([sku](){
                ADInApp::Platform::purchaseSuccessful(sku,
                                                      ADInApp::OperationType::Restored);
            });
            transaction_finished = true;
        }
        
        if(transaction_finished)
        {
            SKPaymentQueue *queue = [SKPaymentQueue defaultQueue];
            [queue finishTransaction:transaction];
        }
    }
}
-(void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    ADInApp::Platform::restorePurchaseResult(true);
}
-(void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error
{

    ADInApp::Platform::restorePurchaseResult(false);
}
@end

@implementation ProductRequestAndBuy

-(void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    if([response.products count] == 1)
    {
        do_buyProduct([response.products objectAtIndex:0]);
    }
    else
    {
        ADUIThread::onCocos2dThread([](){
            ADInApp::Platform::purchaseFailed(_last_sku,
                                              ADInApp::OperationType::Purchased,
                                              ADInApp::ErrorType::DeveloperError);
            _last_sku = "";
        });
    }
    [request release];
}

-(void)request:(SKRequest *)request didFailWithError:(NSError *)error
{
    ADInApp::ErrorType error_type = getErrorType(error);
    ADUIThread::onCocos2dThread([error_type](){
        ADInApp::Platform::purchaseFailed(_last_sku,
                                          ADInApp::OperationType::Purchased,
                                          error_type);
        _last_sku = "";
    });
    
    NSLog(@"request failed: %@, %@", request, error);
    [request release];
}

@end

@implementation ProductRequestDelegate

-(void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    typedef  std::pair<std::string, std::string> Pair;
    std::vector<Pair> sku_price;
    
    sku_price.reserve([response.products count]);
    
    for(SKProduct* p in response.products)
    {
        NSNumberFormatter* number_formatter = [[NSNumberFormatter alloc] init];
        [number_formatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
        [number_formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [number_formatter setLocale:p.priceLocale];
        
        NSString *formattedString = [number_formatter stringFromNumber:p.price];
        std::string price_with_currency([formattedString UTF8String]);
        std::string sku([p.productIdentifier UTF8String]);
        sku = _product_index[sku];
        
        [p retain];
        
        _avaliable_products_mutex.lock();
        _avaliable_products[sku] = p;
        _avaliable_products_mutex.unlock();
        
        sku_price.push_back(Pair(sku, price_with_currency));
    }
    
    ADUIThread::onCocos2dThread([sku_price](){
        for(auto& it : sku_price)
        {
            ADInApp::Platform::setRealPrice(it.first, it.second);
        }
        ADInApp::Platform::setStoreAvaliable(true);
    });
    [request release];
}

-(void)request:(SKRequest *)request didFailWithError:(NSError *)error
{
    NSLog(@"request failed: %@, %@", request, error);
    [request release];
}

@end