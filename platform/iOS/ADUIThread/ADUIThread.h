#include <functional>
#include "cocos2d.h"
class ADUIThread : public cocos2d::CCObject
{
public:
    typedef std::function<void ()> Action;
    static void onUIThread(const Action& action);
    static void onCocos2dThread(const Action& task);
private:
    void performNextCocosTask(float);

};