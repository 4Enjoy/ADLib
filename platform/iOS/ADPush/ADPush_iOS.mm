#include <ADLib/Device/ADPush.h>
#include "cocos2d.h"
#include "ADLib/Device/ADNotification.h"
#include "../ADUIThread/ADUIThread.h"
#include "../ADAppEvents/ADAppEvents.h"
void onPushResult(std::string key)
{
    ADUIThread::onCocos2dThread([key](){
        if(key.size())
        {
            cocos2d::CCLog("Push Registered: %s", key.c_str());
            emit ADPush::signalPushRegisterSuccess(key);
        }
        else
        {
            emit ADPush::signalPushRegisterFailed();
        }
    });
}

NSString* toHex(NSData* data)
{
    const unsigned char *dataBuffer = (const unsigned char *)[data bytes];
    
    if (!dataBuffer)
        return [NSString string];
    
    NSUInteger          dataLength  = [data length];
    NSMutableString     *hexString  = [NSMutableString stringWithCapacity:(dataLength * 2)];
    
    for (int i = 0; i < dataLength; ++i)
        [hexString appendString:[NSString stringWithFormat:@"%02lx", (unsigned long)dataBuffer[i]]];
    
    return [NSString stringWithString:hexString];
}

class PushResult : public HasSlots
{
public:
    static PushResult* getInstance() {
        static PushResult obj;
        return &obj;
    }
    
    void didRegisterForRemoteNotificationsWithDeviceToken(NSData * deviceToken)
    {
        NSString* hex = toHex(deviceToken);
        onPushResult([hex UTF8String]);
    }
    
    // system push notification registration error callback, delegate to pushManager
    void  didFailToRegisterForRemoteNotificationsWithError(NSError * error) {
        NSLog(@"%@",[error localizedDescription]);
        onPushResult("");
    }
    
    // system push notifications callback, delegate to pushManager
    void didReceiveRemoteNotification(NSDictionary * userInfo) {
        NSLog(@"Received notification: %@", userInfo);
    }
};

PushResult* push_res = PushResult::getInstance();

void ADPush::registerForPush(const std::string& platform_parameter)
{
    CONNECT(ADAppEvents::getInstance().signalDidRegisterForRemoteNotificationsWithDeviceToken, PushResult::getInstance(), &PushResult::didRegisterForRemoteNotificationsWithDeviceToken);
    CONNECT(ADAppEvents::getInstance().siganlDidReceiveRemoteNotification, PushResult::getInstance(), &PushResult::didReceiveRemoteNotification);
    CONNECT(ADAppEvents::getInstance().signalDidFailToRegisterForRemoteNotificationsWithError, PushResult::getInstance(), &PushResult::didFailToRegisterForRemoteNotificationsWithError);
    
    ADUIThread::onUIThread([](){
        
        
        
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        

        if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
            // use registerUserNotificationSettings
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
            
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        } else {
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
             (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
        }
        
        
    });
    
    return;
}

