#include <ADLib/Device/ADStatistics.h>
#include "../ADUIThread/ADUIThread.h"
#import "Flurry.h"

static bool _is_flurry_ever_loaded = false;
static std::string _flurry_key = "";

void ADStatistics::startSession()
{
    ADUIThread::onUIThread([](){
        if(_is_flurry_ever_loaded == false)
        {
            _is_flurry_ever_loaded = true;
            [Flurry setCrashReportingEnabled:TRUE];
            [Flurry setSessionContinueSeconds:300];
            //[Flurry setDebugLogEnabled:TRUE];
        }
        if(_flurry_key.size() == 0)
            cocos2d::CCLog("ERROR: NO FLURRY KEY GIVEN");
        
        NSString* key = [NSString stringWithUTF8String:_flurry_key.c_str()];
        [Flurry startSession:key];
    });
}

void ADStatistics::stopSession()
{
}

void ADStatistics::setApplicationKey(const std::string &application_id)
{
    _flurry_key = application_id;
}

void ADStatistics::logEvent(const std::string& name, const Params& p)
{
    ADUIThread::onUIThread([=](){
        const Params::SVec& s_keys = p.getKeysString();
        const Params::SVec& s_values = p.getValuesString();
    
        if(s_keys.size() == s_values.size())
        {
            NSMutableArray* param_keys = [NSMutableArray arrayWithCapacity:s_keys.size()];
            NSMutableArray* param_values = [NSMutableArray arrayWithCapacity:s_keys.size()];
            for(unsigned int i=0; i<s_keys.size(); ++i)
            {
                NSString* key = [NSString stringWithUTF8String:s_keys[i].c_str()];
                NSString* value = [NSString stringWithUTF8String:s_values[i].c_str()];
            
                [param_keys addObject:key];
                [param_values addObject:value];
            }
            
            NSDictionary* params = [NSDictionary dictionaryWithObject:param_values forKey: param_keys];
            NSString* even_name = [NSString stringWithUTF8String:name.c_str()];
            [Flurry logEvent:even_name withParameters:params];
        }
    });
    
}