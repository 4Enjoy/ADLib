#include "../HeyZap.h"
#include "../ADUIThread/ADUIThread.h"
#import "HeyzapAds/HeyzapAds.h"


class HeyZapTest : public HasSlots
{
public:
    HeyZapTest()
    {
        CONNECT(HeyZap::getInstance()->signalHeyZapReady, this, &HeyZapTest::onReady);
    }

    void onReady()
    {
        ADUIThread::onUIThread([=](){
            [HeyzapAds presentMediationDebugViewController];
        });

    }
};

static HeyZapTest _hey_zap_test;
