#include <ADLib/Device/ADBrowser.h>
#include "../ADUIThread/ADUIThread.h"
#include "cocos2d.h"
void ADBrowser::openWebURL(const std::string& url, const Fallback &)
{
    cocos2d::CCLog(url.c_str());
    ADUIThread::onUIThread([=](){
        NSString* url_ns = [NSString stringWithUTF8String:url.c_str()];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url_ns]];
    });
}
void ADBrowser::openOSUrl(const std::string& item_id, const Fallback&)
{
    openWebURL(item_id);
}
void ADBrowser::sendMail(const std::string& email, const std::string& subject_c, const Fallback&)
{
    ADUIThread::onUIThread([=](){
        NSString* to = [NSString stringWithUTF8String:email.c_str()];
        NSString* subject = [NSString stringWithUTF8String:subject_c.c_str()];
        
        NSString* url_ns = [NSString stringWithFormat:@"mailto:?to=%@&subject=%@",
                            [to stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding],
                            [subject stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url_ns]];
    });
}