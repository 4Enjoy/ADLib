#include <ADLib/Device/ADNotification.h>
#include "../ADUIThread/ADUIThread.h"

void ADNotification::showNotification(const std::string& text_c)
{
    ADUIThread::onUIThread([=](){
        
        NSString* text = [NSString stringWithUTF8String:text_c.c_str()];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"" message:text delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        [alert release];
    });}