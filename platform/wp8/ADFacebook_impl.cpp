#include <ADLib/Social/ADFacebook.h>
#include "WP8Adlib.h"
#include <ADLib/Common/ADCocosThread.h>

namespace PhoneDirect3DXamlAppComponent
{
	typedef Platform::Array<Platform::String^ > Arr;
	public delegate void LogInDeleagate(bool show_ui); 
	public delegate void LogOutDelegate();
	public delegate void ShareDelegate(int sid, Platform::String^ name,
		Platform::String^ caption,
		Platform::String^ description,
		Platform::String^ url,
		Platform::String^ picture);


	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class ADFacebookWP8Impl sealed
	{
	public:



		static void sessionOpened(Platform::String^ token);

		static void sessionClosed();
		static void setLogInDelegate(LogInDeleagate^ deleg)
		{
			_log_in = deleg;
		}
		static void setLogOutDelegate(LogOutDelegate^ deleg)
		{
			_log_out = deleg;
		}
		static void setShareDelegate(ShareDelegate^ deleg)
		{
			_share = deleg;
		}

		static void logIn(bool show_ui)
		{
			if(_log_in)
				_log_in->Invoke(show_ui);
		}

		static void logOut()
		{
			if(_log_out)
				_log_out->Invoke();
		}

		static void share(int sid, Platform::String^ name,
			Platform::String^ caption,
			Platform::String^ description,
			Platform::String^ url,
			Platform::String^ picture)
		{
			if(_share)
				_share->Invoke(sid, name, caption, description, url, picture);
		}

		static void sendShareResult(int nid, bool res);

		static void friendsInfo(const Arr^ ids, const Arr^ last_names, const Arr^ first_names);
		static void myInfo(Platform::String^ id, Platform::String^ last_name, Platform::String^ first_name);
	private:
		friend class FacebookWP8;
		static ADFacebookFriendPtr parseFriend(Platform::String^ id, Platform::String^ first_name, Platform::String^ last_name)
		{

			std::string id_(toStdString(id));
			std::string last_name_(toStdString(last_name));
			std::string first_name_(toStdString(first_name));

			std::stringstream ss;
			ss << id_;

			uint64_t id_int = 0;
			ss >> id_int;

			ADFacebookFriendPtr res = std::make_shared<ADFacebookFriend>();
			res->setUserID(id_int);
			res->setFirstName(first_name_);
			res->setLastName(last_name_);

			return res;
		}

		static LogInDeleagate^ _log_in;
		static LogOutDelegate^ _log_out;
		static ShareDelegate^ _share;
	};
	LogInDeleagate^ ADFacebookWP8Impl::_log_in=nullptr;
	LogOutDelegate^ ADFacebookWP8Impl::_log_out=nullptr;
	ShareDelegate^ ADFacebookWP8Impl::_share=nullptr;
}
using namespace PhoneDirect3DXamlAppComponent;


class FacebookWP8 : public ADFacebook::Platform
{
public:

	FacebookWP8()
	{
		_obj = this;
	}

	static FacebookWP8* getInstance()
	{
		return _obj;
	}

	/**
	* @brief How to implement
	* 1. Start session without GUI
	* 2. Load info about Me and friends
	* @param key
	*/
	void loadData()
	{
		openActiveSession(false);
	}

	void openActiveSession(bool show_ui)
	{
		ADFacebookWP8Impl::logIn(show_ui);
	}

	/**
	* @brief How to implement:
	* 1. If not logged in show log in window
	*/
	void logIn()
	{
		openActiveSession(true);
	}


	void logOut()
	{
		ADFacebookWP8Impl::logOut();
	}



	static int pushShareCallback(const ADFacebook::ShareResult &res)
	{
		auto nid = _share_next_id++;

		_share_map[nid] = res;

		return nid;
	}

	static void sendShareResult(const int nid, bool res)
	{
		ADUIThread::onCocos2dThread([=](){
			auto it = _share_map.find(nid);
			if(it != _share_map.end())
			{
				it->second(res);
				_share_map.erase(it);
			}
		});
	}

	void share(const std::string &name,
		const std::string &caption,
		const std::string &description,
		const std::string &url,
		const std::string &picture,
		const ADFacebook::ShareResult &res)
	{
		int nid = pushShareCallback(res);

		ADFacebookWP8Impl::share(nid, 
			toW8String(name),
			toW8String(caption),
			toW8String(description),
			toW8String(url),
			toW8String(picture));

	}


	void shareResult(int sid, bool success)
	{
		auto it = _share_map.find(sid);

		if(it != _share_map.end())
		{
			auto callback = it->second;
			if(callback)
				callback(success);
			_share_map.erase(it);
		}
	}

private:
	static bool _is_new_friends_loaded;
	static FacebookWP8* _obj;
	typedef std::map<int, ADFacebook::ShareResult> ShareMap;

	static ShareMap _share_map;
	static int _share_next_id;
};
typedef ::FacebookWP8 FWP8;
FWP8::ShareMap FWP8::_share_map;
int ::FacebookWP8::_share_next_id=0;
::FacebookWP8* ::FacebookWP8::_obj = nullptr;
bool ::FacebookWP8::_is_new_friends_loaded = false;

ADFacebook::PlatformPtr ADFacebook::getImplementation()
{
	return std::make_shared<::FacebookWP8>();
}

void ADFacebookWP8Impl::sessionOpened(Platform::String^ token)
{
	std::string token_c(toStdString(token));
	ADCocosThread::onCocos2dThread([token_c](){
		::FacebookWP8::getInstance()->sessionOpened(token_c);
	});
}

void ADFacebookWP8Impl::sessionClosed()
{
	ADCocosThread::onCocos2dThread([](){
		::FacebookWP8::getInstance()->sessionClosed();
	});
}

void ADFacebookWP8Impl::friendsInfo(const Arr^ ids, const Arr^ last_names, const Arr^ first_names)
{
	int len = ids->Length;
	ADFacebookFriendPtrArr friends; 

	for(int i=0; i<len; ++i)
	{
		friends.push_back(parseFriend(ids[i], first_names[i], last_names[i]));
	}

	ADCocosThread::onCocos2dThread([friends]()
	{
		::FacebookWP8::getInstance()->friendsLoaded(friends);
	});
}

void ADFacebookWP8Impl::myInfo(Platform::String^ id, Platform::String^ last_name, Platform::String^ first_name)
{
	ADFacebookFriendPtr me = parseFriend(id, first_name, last_name);
	ADCocosThread::onCocos2dThread([me]()
	{
		::FacebookWP8::getInstance()->myInfoLoaded(me);
	});
}

void ADFacebookWP8Impl::sendShareResult(int nid, bool res)
{
	::FacebookWP8::getInstance()->sendShareResult(nid, res);
}