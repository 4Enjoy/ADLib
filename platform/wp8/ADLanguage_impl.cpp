#include <ADLib/Device/ADLanguage.h>
#include <windows.h>
std::string ADLanguage::platformGetDeviceLanguage()
{
	ULONG numLanguages = 0;
	DWORD cchLanguagesBuffer = 0;
	BOOL hr = GetUserPreferredUILanguages(MUI_LANGUAGE_NAME, &numLanguages, NULL, &cchLanguagesBuffer);

	if (hr) 
	{
		WCHAR* pwszLanguagesBuffer = new WCHAR[cchLanguagesBuffer];
		std::string lang_code = "en";
		hr = GetUserPreferredUILanguages(MUI_LANGUAGE_NAME, &numLanguages, pwszLanguagesBuffer, &cchLanguagesBuffer);
		if (hr) 
		{
			lang_code[0] = pwszLanguagesBuffer[0];
			lang_code[1] = pwszLanguagesBuffer[1];
		}
		delete pwszLanguagesBuffer;
		return lang_code;
	}
	return "en";
}