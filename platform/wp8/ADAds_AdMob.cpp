#include <ADLib/Device/ADAds.h>
#include "WP8Adlib.h"
namespace PhoneDirect3DXamlAppComponent
{
	public delegate void AdAdsCreateBannerDelegate(int banner_id, Platform::String^ unit_id);
	public delegate void AdAdsShowBannerDelegate(int banner_id);
	public delegate void AdAdsHideBannerDelegate(int banner_id);
	public delegate void AdAdsMoveBannerDelegate(int banner_id, float x, float y);
	public delegate bool AdAdsIsBannerReadyDelegate(int banner_id);
	public delegate void AdAdsShowInterstitialDelegate();
	public delegate void AdAdsPrepareInterstitialDelegate(Platform::String^ unit_id);
	public delegate int AdAdsGetInterstialTimesShowedDelegate();
	
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class ADAdsAdMobHelper sealed
	{
	public:
		static void setDensity(float density)
		{
			_density = density;
		}
		static float getDensity()
		{
			return _density;
		}

		static void setCreateBannerDelegate(AdAdsCreateBannerDelegate^ create_banner)
		{
			_create_banner = create_banner;
		}
		static void setShowBannerDelegate(AdAdsShowBannerDelegate^ show_banner)
		{
			_show_banner = show_banner;
		}
		static void setHideBannerDelegate(AdAdsHideBannerDelegate^ hide_banner)
		{
			_hide_banner = hide_banner;
		}
		static void setMoveBannerDelegate(AdAdsMoveBannerDelegate^ move_banner)
		{
			_move_banner = move_banner;
		}
		static void setIsBannerReadyDelegate(AdAdsIsBannerReadyDelegate^ is_banner_ready)
		{
			_is_banner_ready = is_banner_ready;
		}
		static void setShowInterstitialDelegate(AdAdsShowInterstitialDelegate^ show_interstitial)
		{
			_show_interstitial = show_interstitial;
		}
		static void setPrepareInterstitialDelegate(AdAdsPrepareInterstitialDelegate^ prepare_interstitial)
		{
			_prepare_interstitial = prepare_interstitial;
		}
		static void setTimesShownDelegate(AdAdsGetInterstialTimesShowedDelegate^ times_showed)
		{
			_times_showed = times_showed;
		}

		static void createBanner(int banner_id, Platform::String^ unit_id)
		{
			if(_create_banner)
			{
				_create_banner->Invoke(banner_id, unit_id);
			}
		}
		static void showBanner(int banner_id)
		{
			if(_show_banner)
			{
				_show_banner->Invoke(banner_id);
			}
		}
		static void hideBanner(int banner_id)
		{
			if(_hide_banner)
				_hide_banner->Invoke(banner_id);
		}
		static void moveBanner(int banner_id, float x, float y)
		{
			if(_move_banner)
				_move_banner->Invoke(banner_id, x, y);
		}
		static bool isBannerReady(int banner_id)
		{
			if(_is_banner_ready)
				return _is_banner_ready->Invoke(banner_id);
			return false;
		}
		static void showInterstitial()
		{
			if(_show_interstitial)
				_show_interstitial->Invoke();

		}
		static void prepareIntertitial(Platform::String^ unit_id)
		{
			if(_prepare_interstitial)
				_prepare_interstitial->Invoke(unit_id);
		}
		static int getInterstitialTimesShown()
		{
			if(_times_showed)
				return _times_showed->Invoke();
			return 0;
		}
		
	private:
		static AdAdsCreateBannerDelegate^ _create_banner;
		static AdAdsShowBannerDelegate^ _show_banner;
		static AdAdsHideBannerDelegate^ _hide_banner;
		static AdAdsMoveBannerDelegate^ _move_banner;
		static AdAdsIsBannerReadyDelegate^ _is_banner_ready;
		static AdAdsShowInterstitialDelegate^ _show_interstitial;
		static AdAdsPrepareInterstitialDelegate^ _prepare_interstitial;
		static AdAdsGetInterstialTimesShowedDelegate^ _times_showed;

		static float _density;
	};
	float ADAdsAdMobHelper::_density = 1.0f;
	AdAdsCreateBannerDelegate^ ADAdsAdMobHelper::_create_banner = nullptr;
	AdAdsShowBannerDelegate^ ADAdsAdMobHelper::_show_banner = nullptr;
	AdAdsHideBannerDelegate^ ADAdsAdMobHelper::_hide_banner = nullptr;
	AdAdsMoveBannerDelegate^ ADAdsAdMobHelper::_move_banner = nullptr;
	AdAdsIsBannerReadyDelegate^ ADAdsAdMobHelper::_is_banner_ready = nullptr;
	AdAdsShowInterstitialDelegate^ ADAdsAdMobHelper::_show_interstitial = nullptr;
	AdAdsPrepareInterstitialDelegate^ ADAdsAdMobHelper::_prepare_interstitial = nullptr;
	AdAdsGetInterstialTimesShowedDelegate^ ADAdsAdMobHelper::_times_showed = nullptr;

}
using namespace PhoneDirect3DXamlAppComponent;

void ADAds::Platform::addTestDevice(const DeviceID& device)
{
}

cocos2d::CCSize ADAds::Platform::getBannerSize(const BannerType&)
{
	//Only one size is supported
    return cocos2d::CCSize(320, 50) * 1.5f;
}

bool ADAds::Platform::createBanner(const BannerID id, const BannerType& type, const UnitID &unit_id)
{
	ADAdsAdMobHelper::createBanner(id, toW8String(unit_id));
    return true;
}

void ADAds::Platform::showBanner(const BannerID id)
{
	ADAdsAdMobHelper::showBanner(id);
}

void ADAds::Platform::hideBanner(const BannerID id)
{
	ADAdsAdMobHelper::hideBanner(id);
}


void ADAds::Platform::isBannerReady(const BannerID id, const Callback& callback)
{
	bool res = ADAdsAdMobHelper::isBannerReady(id);
	callback(res, id);
}

void ADAds::Platform::moveBanner(const BannerID id, const cocos2d::CCPoint& position)
{
	ADAdsAdMobHelper::moveBanner(id, position.x, position.y);
}

void ADAds::Platform::showInterstitial()
{
	ADAdsAdMobHelper::showInterstitial();
}

void ADAds::Platform::prepareInterstitial(const UnitID & unit_id)
{
	ADAdsAdMobHelper::prepareIntertitial(toW8String(unit_id));
}

unsigned int ADAds::Platform::getInterstialTimesShowed()
{
    return ADAdsAdMobHelper::getInterstitialTimesShown();
}


