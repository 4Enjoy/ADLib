#include <ADLib/Device/ADBrowser.h>
#include "cocos2d.h"
#include "WP8Adlib.h"

using namespace cocos2d;

namespace PhoneDirect3DXamlAppComponent
{
	public delegate void OpenUrlDelegate(Platform::String^ url);  
	public delegate void SendEmailDelegate(Platform::String^ email, Platform::String^ subject);

	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class BrowserHelper sealed
	{
	public:
		static void setOpenUrlDelegate(OpenUrlDelegate^ deleg)
		{
			open_url_delegate = deleg;
		}
		static void setSendEmailDelegate(SendEmailDelegate^ deleg)
		{
			send_email_delegate = deleg;
		}
		static void openUrl(Platform::String^ url)
		{
			if(open_url_delegate)
				open_url_delegate->Invoke(url);
		}
		static void sendEmail(Platform::String^ email, Platform::String^ subject)
		{
			if(send_email_delegate)
				send_email_delegate->Invoke(email, subject);
		}
	private:
		static OpenUrlDelegate^ open_url_delegate;
		static SendEmailDelegate^ send_email_delegate;
	};
	OpenUrlDelegate^ BrowserHelper::open_url_delegate = nullptr;
	SendEmailDelegate^ BrowserHelper::send_email_delegate = nullptr;
}


void ADBrowser::openWebURL(const std::string& url, const Fallback &)
{
	PhoneDirect3DXamlAppComponent::BrowserHelper::openUrl(toW8String(url));
}
void ADBrowser::openOSUrl(const std::string& item_id, const Fallback&)
{
	openWebURL(item_id);
}
void ADBrowser::sendMail(const std::string& email, const std::string& subject, const Fallback&)
{
	PhoneDirect3DXamlAppComponent::BrowserHelper::sendEmail(toW8String(email),
		toW8String(subject));
}
