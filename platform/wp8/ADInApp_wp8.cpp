#include <ADLib/Device/ADInApp.h>
#include "WP8Adlib.h"
namespace PhoneDirect3DXamlAppComponent
{
	typedef Platform::Array<Platform::String^ > Arr;
	public delegate void LoadStoreDelegate(const Arr^ product_ids);  
	public delegate void RestorePurchasesDelegate();
	public delegate void BuyProductDelegate(Platform::String^ product_id);

	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class ADInAppWP8 sealed
	{
	public:
		static void setLoadStoreDelegate(LoadStoreDelegate^ deleg)
		{
			_load_store = deleg;
		}
		static void setRestorePurchasesDelegate(RestorePurchasesDelegate^ deleg)
		{
			_restore_purchases = deleg;
		}
		static void setBuyProductDelegate(BuyProductDelegate^ deleg)
		{
			_buy_product = deleg;
		}
		static void loadStore(const Arr^ products)
		{
			if(_load_store)
				_load_store->Invoke(products);
		}
		static void buyProduct(Platform::String^ product)
		{
			if(_buy_product)
				_buy_product->Invoke(product);
		}
		static void restorePurchases()
		{
			if(_restore_purchases)
				_restore_purchases->Invoke();
		}

		static void notifyStoreAvaliable(bool val)
		{
			ADUIThread::onCocos2dThread([=](){
				ADInApp::Platform::setStoreAvaliable(val);
			});
		}

		static void notifyRestorePurchases(bool val)
		{
			ADUIThread::onCocos2dThread([=](){
				ADInApp::Platform::restorePurchaseResult(val);
			});
		}

		static void notifyPurchaseResult(Platform::String^ product,
			bool success, bool restored, int error_code)
		{
			std::string key = toStdString(product);
			ADInApp::OperationType type = ADInApp::OperationType::Restored;
			if(!restored)
				type = ADInApp::OperationType::Purchased;
			ADInApp::ErrorType error = static_cast<ADInApp::ErrorType>(error_code);
			ADUIThread::onCocos2dThread([key, success, type, error](){
				if(success)
					ADInApp::Platform::purchaseSuccessful(key, type);
				else
					ADInApp::Platform::purchaseFailed(key, type, error);
			});
		}

		static void notifySetPrice(Platform::String^ product,
			Platform::String^ price)
		{
			std::string p_id(toStdString(product));
			std::string p_price(toStdString(price));

			ADUIThread::onCocos2dThread([p_id,p_price](){
				ADInApp::Platform::setRealPrice(p_id, p_price);
			});
		}
		
	private:
		static LoadStoreDelegate^ _load_store;
		static RestorePurchasesDelegate^ _restore_purchases;
		static BuyProductDelegate^ _buy_product;
	};
	LoadStoreDelegate^ ADInAppWP8::_load_store = nullptr;
	RestorePurchasesDelegate^ ADInAppWP8::_restore_purchases = nullptr;
	BuyProductDelegate^ ADInAppWP8::_buy_product = nullptr;
}
using namespace PhoneDirect3DXamlAppComponent;


void ADInApp::Platform::buyProduct(const Product& p)
{
	ADInAppWP8::buyProduct(toW8String(p.getID()));
}
void ADInApp::Platform::loadStore(const std::string&, const Mode)
{
	auto& products = ADInApp::getAllProducts();
	Arr^ ids = ref new Arr(products.size());
	int i = 0;
	for(auto& it : products)
    {
		const Product& p = it.second;
		ids[i++] = toW8String(p.getID());
	}
	ADInAppWP8::loadStore(ids);
}
void ADInApp::Platform::restorePurchases()
{
	ADInAppWP8::restorePurchases();
}