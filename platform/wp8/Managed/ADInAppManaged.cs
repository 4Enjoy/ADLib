using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Store;
using System.Windows;

namespace PhoneDirect3DXamlAppComponent
{
    public partial class ADInAppManaged
    {

        public ADInAppManaged()
        {
            ADInAppWP8.setBuyProductDelegate(buyProduct);
            ADInAppWP8.setLoadStoreDelegate(loadStore);
            ADInAppWP8.setRestorePurchasesDelegate(restorePurchases);
        }
        void buyProduct(string prod_id)
        {
			try{
				Deployment.Current.Dispatcher.BeginInvoke(async () =>
				{
					try
					{
						await CurrentApp.RequestProductPurchaseAsync(prod_id, false);

						doRestorePurchases(true);
					}
					catch (Exception)
					{
						ADInAppWP8.notifyPurchaseResult(prod_id, false, false, 1);
					}
				});
			} catch (Exception e){}
        }

        void doRestorePurchases(bool is_purchase)
        {
            var lic = CurrentApp.LicenseInformation.ProductLicenses;
            foreach(var p in lic)
            {
                ProductLicense prod_lic = p.Value;
                string prod_id = prod_lic.ProductId;

                if (prod_lic.IsActive)
                {
                    if (prod_lic.IsConsumable)
                        CurrentApp.ReportProductFulfillment(prod_id);

                    ADInAppWP8.notifyPurchaseResult(prod_id, true, !is_purchase, 0);
                }
            }
        }

        void loadStore(string[] prod_ids)
        {
			try{
				Deployment.Current.Dispatcher.BeginInvoke(async () =>
				{
					try
					{

						ListingInformation listings = await CurrentApp.LoadListingInformationByProductIdsAsync(prod_ids);

						foreach (KeyValuePair<string, ProductListing> p in listings.ProductListings)
						{
							string price = p.Value.FormattedPrice;
							string id = p.Key;

							ADInAppWP8.notifySetPrice(id, price);
						}

						doRestorePurchases(false);
						ADInAppWP8.notifyStoreAvaliable(true);
					}
					catch (Exception ex)
					{
						ex.ToString();
					}
				});
			} catch (Exception e){}
        }

        void restorePurchases()
        {
			try{
				Deployment.Current.Dispatcher.BeginInvoke(() =>
				{
					doRestorePurchases(false);
					ADInAppWP8.notifyRestorePurchases(true);
				});
			} catch (Exception e){}
        }

        

        public static void init()
        {
            _obj = new ADInAppManaged();
        }
        static ADInAppManaged _obj = null;
    }
}