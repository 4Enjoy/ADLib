using System;
using Microsoft.Phone.Tasks;
using GoogleAds;
using PhoneDirect3DXamlAppInterop;
using System.Windows.Controls;
using System.Windows;
using System.Collections.Generic;
namespace PhoneDirect3DXamlAppComponent
{
    public partial class ADAdsManaged
    {
        Dictionary<int, AdView> _banners = new Dictionary<int, AdView>();
        bool _banner_ready = false;
        InterstitialAd _interstitial = null;
        bool _interstitial_loaded = false;
        int _interstitial_shown = 0;
        Object _interstitial_lock = new Object();
        public ADAdsManaged()
        {
            float density = PhoneDirect3DXamlAppInterop.App.Current.Host.Content.ScaleFactor;
            density /= 100;
            ADAdsAdMobHelper.setDensity(density);

            ADAdsAdMobHelper.setCreateBannerDelegate(createBanner);
            ADAdsAdMobHelper.setHideBannerDelegate(hideBanner);
            ADAdsAdMobHelper.setIsBannerReadyDelegate(isBannerReady);
            ADAdsAdMobHelper.setMoveBannerDelegate(moveBanner);
            ADAdsAdMobHelper.setPrepareInterstitialDelegate(prepareInterstitial);
            ADAdsAdMobHelper.setShowBannerDelegate(showBanner);
            ADAdsAdMobHelper.setShowInterstitialDelegate(showInterstitial);
            ADAdsAdMobHelper.setTimesShownDelegate(interstitialTimesShown);
        }

        
        void createBanner(int id, string unit_id)
        {
			try
			{
				Canvas layout = MainPage.getMainLayout();
				if (layout != null)
				{
					Deployment.Current.Dispatcher.BeginInvoke(() =>
						{
							try
							{
								AdView bannerAd = new AdView
								{
									Format = AdFormats.Banner,
									AdUnitID = unit_id
								};

								AdRequest adRequest = new AdRequest();
								layout.Children.Add(bannerAd);
								bannerAd.ReceivedAd += ReceivedAd;
								bannerAd.LoadAd(adRequest);

								bannerAd.Visibility = Visibility.Collapsed;
								_banners.Add(id, bannerAd);
							} catch (Exception e){}
							

							
						});

				}
			} catch (Exception e){}
        }

        private void ReceivedAd(object sender, AdEventArgs e)
        {
            _banner_ready = true;
        }

        void showBanner(int id)
        {
			try
			{
				Deployment.Current.Dispatcher.BeginInvoke(() =>
					{
						try
						{
							if (_banners.ContainsKey(id))
							{
								AdView banner = _banners[id];
								banner.Visibility = Visibility.Visible;
							}
						} catch (Exception e){}
					});
			} catch (Exception e){}
        }

        void hideBanner(int id)
        {
			try
			{
				Deployment.Current.Dispatcher.BeginInvoke(() =>
					{
						try
						{
							if (_banners.ContainsKey(id))
							{
								AdView banner = _banners[id];
								banner.Visibility = Visibility.Collapsed;
							}
						} catch (Exception e){}
					});
			} catch (Exception e){}
        }

        void moveBanner(int id, float x, float y)
        {
			try {
				Deployment.Current.Dispatcher.BeginInvoke(() =>
				{
					try{
						if (_banners.ContainsKey(id))
						{
							AdView banner = _banners[id];
							Canvas.SetLeft(banner, x);
							Canvas.SetTop(banner, y);
						}
					} catch (Exception e){}
				});
			} catch (Exception e){}
        }

        bool isBannerReady(int id)
        {
            return _banner_ready;
        }

        void showInterstitial()
        {
			try{
				Deployment.Current.Dispatcher.BeginInvoke(() =>
				{
					try{
						lock (_interstitial_lock)
						{
							if (_interstitial_loaded && _interstitial != null)
							{
								_interstitial.ShowAd();
								_interstitial_shown = 0;
								_interstitial = null;
								_interstitial_loaded = false;
								_interstitial_shown += 1;
							}
						}
					} catch (Exception e){}
				});
			} catch (Exception e){}
        }

        void prepareInterstitial(string unit_id)
        {
			try{
				Deployment.Current.Dispatcher.BeginInvoke(() =>
				{
					try{
						lock (_interstitial_lock)
						{
							if (_interstitial == null)
							{
								_interstitial = new InterstitialAd(unit_id);
								AdRequest adRequest = new AdRequest();
								_interstitial_loaded = false;
								_interstitial.ReceivedAd += OnAdReceived;
								_interstitial.LoadAd(adRequest);

							}
						}
					} catch (Exception e){}
				});
			} catch (Exception e){}
        }

        private void OnAdReceived(object sender, AdEventArgs e)
        {
            lock (_interstitial_lock)
            {
                _interstitial_loaded = true;
            }
        }


        int interstitialTimesShown()
        {
            return _interstitial_shown;
        }

        public static void init()
        {
            _obj = new ADAdsManaged();
        }
        static ADAdsManaged _obj = null;
    }
}