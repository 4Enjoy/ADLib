using System;
using Microsoft.Phone.Tasks;
namespace PhoneDirect3DXamlAppComponent
{
    public partial class ADBrowserManaged
    {

        public ADBrowserManaged()
        {
            BrowserHelper.setOpenUrlDelegate(openUrl);
            BrowserHelper.setSendEmailDelegate(sendEmail);
        }

        void openUrl(string url)
        {
			try{
				WebBrowserTask webBrowserTask = new WebBrowserTask();
				webBrowserTask.Uri = new Uri(url, UriKind.Absolute);
				webBrowserTask.Show();
			} catch (Exception e){}
        }

        void sendEmail(string email, string subject)
        {
			try{
				EmailComposeTask emailComposeTask = new EmailComposeTask();
				emailComposeTask.Subject = subject;
				emailComposeTask.To = email;

				emailComposeTask.Show();
			} catch (Exception e){}
        }

        public static void init()
        {
            _obj = new ADBrowserManaged();
        }
        static ADBrowserManaged _obj = null;
    }
}