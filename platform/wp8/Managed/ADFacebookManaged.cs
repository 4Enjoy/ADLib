using System;
using System.Collections.Generic;
using System.Windows;
using Facebook.Client;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;
using System.IO;
using System.Text;
using System.Linq;

namespace PhoneDirect3DXamlAppComponent
{
    public partial class ADFacebookManaged
    {
        private FacebookSession session = null;
        private FacebookSessionClient FacebookSessionClient = null;
        public ADFacebookManaged()
        {
            ADFacebookWP8Impl.setLogInDelegate(logIn);
            ADFacebookWP8Impl.setLogOutDelegate(logOut);
            ADFacebookWP8Impl.setShareDelegate(share);
        }

        void setAppID(string app_id)
        {
            FacebookSessionClient = new FacebookSessionClient(app_id);
        }

        void logIn(bool show_ui)
        {	
			try{
				Deployment.Current.Dispatcher.BeginInvoke(async () =>
				{
					bool is_opened = false;
					string access_token = null;
					try
					{
						if (!show_ui)
						{
							session = FacebookSessionIsolatedStorageCacheProvider.Current.GetSessionData();
							if (session.AccessToken.Length > 0)
							{
								is_opened = true;
								access_token = session.AccessToken;
							}
						}
						else
						{

							session = await FacebookSessionClient.LoginAsync("publish_stream");
							FacebookSessionIsolatedStorageCacheProvider.Current.SaveSessionData(session);

							is_opened = true;
							access_token = session.AccessToken;
						}
					}
					catch (Exception)
					{

					}

					if (is_opened && access_token != null)
					{
						ADFacebookWP8Impl.sessionOpened(session.AccessToken);
						await updateMyInfo();
						await updateFriendsInfo();
					}
					else
					{
						ADFacebookWP8Impl.sessionClosed();
					}
				});
			} catch (Exception e)
			{
				ADFacebookWP8Impl.sessionClosed();
			}
			
        }

        void logOut()
        {
			try
			{
				Deployment.Current.Dispatcher.BeginInvoke(async () =>
				{
					try
					{
						FacebookSessionClient.Logout();
					}
					catch (Exception)
					{

					}
					ADFacebookWP8Impl.sessionClosed();
				});
			} catch (Exception e){}
        }

        void share(int sid, string name, string caption, string description, string url, string picture)
        {
			try{
				Deployment.Current.Dispatcher.BeginInvoke(async () =>
				{
					bool shared_ok = false;
					try
					{
						var fb = new Facebook.FacebookClient(session.AccessToken);
						var postParams = new
						{
							name = name,
							caption = caption,
							description = description,
							link = url,
							picture = picture
						};

						dynamic fbPostTaskResult = await fb.PostTaskAsync("/me/feed", postParams);
						var result = (IDictionary<string, object>)fbPostTaskResult;

						shared_ok = true;
					}
					catch (Exception)
					{
						
					}
					ADFacebookWP8Impl.sendShareResult(sid, shared_ok);
				});
			} catch (Exception e){}
        }

        public class User
        {
            public string uid { get; set; }
            public string first_name { get; set; }
            public string last_name { get; set; }
        }

        public class RootObject
        {
            public List<User> data { get; set; }
        }

        async System.Threading.Tasks.Task updateFriendsInfo()
        {

            try
            {
                var fb = new Facebook.FacebookClient(session.AccessToken);
                string query = "select uid, first_name, last_name from user where uid in (select uid2 from friend where uid1 = me()) and is_app_user";

                var result = await fb.GetTaskAsync("fql",
                    new
                    {
                        q = query
                    });

                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(result.ToString()));

                RootObject obj = new RootObject();
                DataContractJsonSerializer ser = new DataContractJsonSerializer(obj.GetType());

                obj = ser.ReadObject(ms) as RootObject;

                List<string> ids = new List<string>();
                List<string> last_names = new List<string>();
                List<string> first_names = new List<string>();

                foreach (User u in obj.data)
                {
                    ids.Add(u.uid);
                    last_names.Add(u.last_name);
                    first_names.Add(u.first_name);
                    //System.Diagnostics.Debug.WriteLine("UID: " + u.uid + u.first_name + u.last_name);
                }

                ADFacebookWP8Impl.friendsInfo(ids.ToArray(), last_names.ToArray(), first_names.ToArray());
                //System.Diagnostics.Debug.WriteLine("Result: " + result);
            }
            catch (Exception)
            {
                System.Diagnostics.Debug.WriteLine("updateFriendsInfo failed ");
            }

        }

        async System.Threading.Tasks.Task updateMyInfo()
        {
            try
            {
                var client = new Facebook.FacebookClient(session.AccessToken);
                dynamic result = await client.GetTaskAsync("me");
                var currentUser = new Facebook.Client.GraphUser(result);

                ADFacebookWP8Impl.myInfo(currentUser.Id, currentUser.LastName, currentUser.FirstName);
            }
            catch (Exception)
            { }
        }


        public static void init(string app_id)
        {
            _obj = new ADFacebookManaged();
            _obj.setAppID(app_id);
        }
        static ADFacebookManaged _obj = null;
    }
}