using System;
using System.Collections.Generic;
using FlurryWP8SDK;
using FlurryWP8SDK.Models;
namespace PhoneDirect3DXamlAppComponent
{
    public partial class ADStatisticsManaged
    {

        public ADStatisticsManaged()
        {
            ADStatisticsFlurry.setStartSessionDelegate(startSession);
            ADStatisticsFlurry.setLogEventDelegate(logEvent);
        }

        void startSession(string app_id)
        {
            FlurryWP8SDK.Api.StartSession(app_id);
        }

        void logEvent(string event_id, string[] keys, string[] values)
        {
			try{
				if (keys == null || keys.Length == 0)
					FlurryWP8SDK.Api.LogEvent(event_id);
				else
				{
					List<Parameter> param = new List<Parameter>();
					for (int i = 0; i < keys.Length; ++i)
					{
						param.Add(new Parameter(keys[i], values[i]));
					}
					FlurryWP8SDK.Api.LogEvent(event_id, param);
				}
			} catch (Exception e){}
        }

        public static void init()
        {
            _obj = new ADStatisticsManaged();
        }
        static ADStatisticsManaged _obj = null;
    }
}