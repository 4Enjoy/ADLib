#include "WP8Adlib.h"
#include <queue>
#include <mutex>
#include "cocos2d.h"

namespace ADUIThreadImpl {
    static ADUIThread _ui_thread;
    static std::queue<ADUIThread::Action> _cocos_tasks;
    static std::mutex _cocos_tasks_mutex;
}

using namespace ADUIThreadImpl;
using namespace cocos2d;

void ADUIThread::performNextCocosTask(float)
{
    bool repeat = false;
    Action task(nullptr);
    _cocos_tasks_mutex.lock();
    
    if(_cocos_tasks.size() > 0)
    {
        task = _cocos_tasks.front();
        _cocos_tasks.pop();
        
        if(_cocos_tasks.size() > 0)
            repeat = true;
    }
    _cocos_tasks_mutex.unlock();
    
    if(task)
    {
        task();
    }
    if(!repeat)
        CCDirector::sharedDirector()->getScheduler()->unscheduleSelector(schedule_selector(ADUIThread::performNextCocosTask), &_ui_thread);
}

void ADUIThread::onCocos2dThread(const Action& task)
{
    _cocos_tasks_mutex.lock();
    _cocos_tasks.push(task);
    _cocos_tasks_mutex.unlock();
    
    CCDirector::sharedDirector()->getScheduler()->scheduleSelector(schedule_selector(ADUIThread::performNextCocosTask), &_ui_thread, 0, false);
}