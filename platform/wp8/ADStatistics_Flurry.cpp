#include <ADLib/Device/ADStatistics.h>
#include "cocos2d.h"

#include "WP8Adlib.h"
#include <collection.h>

using namespace cocos2d;

namespace PhoneDirect3DXamlAppComponent
{
	typedef Platform::Array<Platform::String^ > Arr;
	public delegate void StartSessionDelegate(Platform::String^ app_id);  
	public delegate void LogEventDelegate(Platform::String^ event_name, 
		const Arr^ keys, const Arr^ values);

	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class ADStatisticsFlurry sealed
	{
	public:
		static void setStartSessionDelegate(StartSessionDelegate^ deleg)
		{
			_start_session = deleg;
		}
		static void setLogEventDelegate(LogEventDelegate^ deleg)
		{
			_log_event = deleg;
		}
		static void startSession(Platform::String^ app_id)
		{
			if(_start_session)
				_start_session->Invoke(app_id);
		}
		static void logEvent(Platform::String^ event_name, 
		const Arr^ keys, const Arr^ values)
		{
			if(_log_event)
				_log_event->Invoke(event_name, keys, values);
		}
	private:
		static StartSessionDelegate^ _start_session;
		static LogEventDelegate^ _log_event;
	};
	StartSessionDelegate^ ADStatisticsFlurry::_start_session = nullptr;
	LogEventDelegate^ ADStatisticsFlurry::_log_event = nullptr;
}
using namespace PhoneDirect3DXamlAppComponent;

void ADStatistics::startSession()
{
	//No need for wp8
}

void ADStatistics::stopSession()
{
    //No need for wp8
}

void ADStatistics::setApplicationKey(const std::string &application_id)
{
    //cocos2d::CCLog("ADStatistics: Application key: %s", application_id.c_str());
	ADStatisticsFlurry::startSession(toW8String(application_id));
}

void ADStatistics::logEvent(const std::string& name, const Params& p)
{
    cocos2d::CCLog("<Event '%s'", name.c_str());
    const Params::SVec& s_keys = p.getKeysString();
    const Params::SVec& s_values = p.getValuesString();

    if(s_keys.size() == s_values.size())
    {
		Arr^ keys = ref new Arr(s_keys.size());
		Arr^ values = ref new Arr(s_keys.size());
		
        for(unsigned int i=0; i<s_keys.size(); ++i)
        {
			keys[i] = toW8String(s_keys[i]);
			values[i] = toW8String(s_values[i]);
			
            cocos2d::CCLog("-Param '%s'='%s'", s_keys[i].c_str(), s_values[i].c_str());
        }
		ADStatisticsFlurry::logEvent(toW8String(name), keys, values);
    }

    cocos2d::CCLog("Event End>");
}