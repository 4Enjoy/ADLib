#pragma once
#include "CCWinRTUtils.h"
#include <string>
inline std::string toStdString(Platform::String^ str)
{
	std::wstring wstr = str->Data();
	return cocos2d::CCUnicodeToUtf8(wstr.c_str());
}
inline Platform::String^ toW8String(const std::string& str)
{
	std::wstring wstr = cocos2d::CCUtf8ToUnicode(str.c_str(), -1);
	Platform::String^ str_w8 = ref new Platform::String(wstr.c_str());
	return str_w8;
}

#include <functional>
#include "cocos2d.h"
class ADUIThread : public cocos2d::CCObject
{
public:
    typedef std::function<void ()> Action;
    static void onCocos2dThread(const Action& task);
private:
    void performNextCocosTask(float);

};