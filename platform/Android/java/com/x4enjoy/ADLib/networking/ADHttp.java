package com.x4enjoy.ADLib.networking;

import android.app.Activity;
import android.util.Log;
import java.util.Hashtable;
import java.lang.String;
import android.os.Bundle;
import java.util.ArrayList;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.NameValuePair;
import java.util.List;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.client.methods.HttpRequestBase;
import android.os.AsyncTask;

public class ADHttp
{
    public static final int TYPE_POST = 1;
    public static final int TYPE_GET = 2;

	static private class DownloadTask extends AsyncTask<Integer, Integer, Integer> 
	{
		protected Integer doInBackground(Integer... urls) 
		{
			if(_type == TYPE_POST)
				sendPostRequest(_id, _url, _params);
			else
				sendGetRequest(_id, _url, _params);
			return 0;
		}
		
		public DownloadTask(int id, int type, String url, List<NameValuePair> params)
		{
			_id = id;
			_type = type;
			_url = url;
			_params = params;
		}
		private int _id = 0;
		private int _type = 0;
		private String _url = "";
		private List<NameValuePair> _params;
	}

	
    public static void makeRequest(int id, int type, String url, ArrayList<String> keys, ArrayList<String> values)
    {
        List<NameValuePair> params = formatParams(keys, values);
        new DownloadTask(id, type, url, params).execute();
    }

    private static List<NameValuePair> formatParams(ArrayList<String> keys, ArrayList<String> values)
    {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        if(keys.size() == values.size())
        {
            for(int i=0; i<keys.size(); ++i)
            {
                params.add(new BasicNameValuePair(keys.get(i), values.get(i)));
            }
        }
        return params;
    }

    private static void sendGetRequest(int id, String url, List<NameValuePair> params)
    {
        String paramString = URLEncodedUtils.format(params, "utf-8");
		if(paramString.length() > 0)
		{
			url += "?" + paramString;
		}
        // Prepare a request object
        HttpGet httpget = new HttpGet(url);
        readRequest(id, httpget);
		
		Log.d("ADHttp", url);

    }

    private static void sendPostRequest(int id, String url, List<NameValuePair> params)
    {
        HttpPost httppost = new HttpPost(url);
        try
        {
            httppost.setEntity(new UrlEncodedFormEntity(params));
        }
        catch (Exception e)
        {
            requestResult(id, -104, false, null, 0);
        }

        readRequest(id, httppost);
        
    }

    private static void readRequest(int id, HttpRequestBase req)
    {
        HttpClient client = new DefaultHttpClient();
        try
        {
            HttpResponse response = client.execute(req);
            int code = response.getStatusLine().getStatusCode();



            byte[] content = EntityUtils.toByteArray(response.getEntity());


            boolean is_success = ((code / 100) == 2);
			//Log.d("ADHttp", content.toString());
            requestResult(id, code, is_success, content, content.length);
        }
        catch (Exception e)
        {
            requestResult(id, -100, false, null, 0);
        }
    }

    private static native void requestResult(int id, int code, boolean success, byte[] data, int data_size);
}
