package com.x4enjoy.ADLib;
import android.app.Activity;
import android.provider.Settings.Secure;
public class ADDeviceID
{
	static String getDeviceID(Activity act)
	{
		return Secure.getString(act.getContentResolver(), Secure.ANDROID_ID); 
	}
}