package com.x4enjoy.ADLib.messanger;
import android.app.Activity;
import com.arellomobile.android.push.BasePushMessageReceiver;
import com.arellomobile.android.push.PushManager;
import com.arellomobile.android.push.PushManager.GetTagsListener;
import com.arellomobile.android.push.utils.RegisterBroadcastReceiver;
import android.util.Log;

public class ADPushWoosh
{
	private final static String TAG="ADPushWoosh";
	
	static void onCreate(Activity context,java.lang.String appId,java.lang.String projectId)
	{
		
		Log.d("flag",appId+"; "+ projectId);
		PushManager.initializePushManager(context,appId,projectId);
		PushManager pushManager=PushManager.getInstance(context);
		
		//Start push manager, this will count app open for Pushwoosh stats as well
		try {
            pushManager.onStartup(context);
		}
		catch(Exception e)
		{
		
             //push notifications are not available or AndroidManifest.xml is not configured properly
			 Log.e("flag", e.toString());
		}
       
		//Register for push!
		pushManager.registerForPushNotifications();
	}
}