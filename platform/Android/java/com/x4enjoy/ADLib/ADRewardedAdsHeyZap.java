package com.x4enjoy.ADLib;
import android.app.Activity;
import android.provider.Settings.Secure;
import android.view.WindowManager;
import android.util.Log;
import com.heyzap.sdk.ads.HeyzapAds.OnStatusListener;
import com.heyzap.sdk.ads.HeyzapAds.OnIncentiveResultListener;
import com.heyzap.sdk.ads.IncentivizedAd;

public class ADRewardedAdsHeyZap
{
	public static void init()
	{
		Log.d("REWARDED", "Init");
		IncentivizedAd.setOnStatusListener(new OnStatusListener() {
			@Override
			public void onShow(String tag) {
				// Ad is now showing
			}

			@Override
			public void onClick(String tag) {
				// Ad was clicked on. You can expect the user to leave your application temporarily.
			}

			@Override
			public void onHide(String tag) {
				// Ad was closed. The user has returned to your application.
			}

			@Override
			public void onFailedToShow(String tag) {
				// Display was called but there was no ad to show
			}
			
			@Override
			public void onFailedToFetch(String tag) {
				// Display was called but there was no ad to show
			}

			@Override
			public void onAvailable(String tag) {
				onAdsAvaliable();
				
				Log.d("REWARDED", "avaliable");
			}

			
			@Override
			public void onAudioStarted() {
				
				// The ad being shown no longer requires audio. Any background audio can be resumed.
			}
		
			@Override
			public void onAudioFinished() {
				
				// The ad being shown no longer requires audio. Any background audio can be resumed.
			}
		});
		
		IncentivizedAd.setOnIncentiveResultListener(new OnIncentiveResultListener() {
			@Override
			public void onComplete(String tag) {
				onVideoResult(true);
			}

			@Override
			public void onIncomplete(String tag) {
				onVideoResult(false);
			}
		});
	}
	
	public native static void onAdsAvaliable();
	public native static void onVideoResult(boolean value);
}