package com.x4enjoy.ADLib.inapp.amazon;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.HashMap;
import java.util.Map;
import java.util.HashSet;
import android.app.Activity;
import android.util.Log;
import android.content.Intent;
import com.amazon.inapp.purchasing.PurchasingManager;
import com.amazon.inapp.purchasing.Item;
import com.amazon.inapp.purchasing.PurchasingObserver;
import com.amazon.inapp.purchasing.BasePurchasingObserver;
import com.amazon.inapp.purchasing.PurchaseUpdatesResponse;
import com.amazon.inapp.purchasing.PurchaseUpdatesResponse.PurchaseUpdatesRequestStatus;
import com.amazon.inapp.purchasing.PurchaseResponse.PurchaseRequestStatus;
import com.amazon.inapp.purchasing.PurchaseResponse;
import com.amazon.inapp.purchasing.ItemDataResponse;
import com.amazon.inapp.purchasing.ItemDataResponse.ItemDataRequestStatus;
import com.amazon.inapp.purchasing.Offset;
import com.amazon.inapp.purchasing.Receipt;
import com.amazon.inapp.purchasing.Item.ItemType;
import com.amazon.inapp.purchasing.GetUserIdResponse;
import com.amazon.inapp.purchasing.GetUserIdResponse.GetUserIdRequestStatus;
import android.os.Bundle;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;

public class ADInApp 
{
	//private static IabHelper _iab = null;
	private static Activity _activity = null;
	
	private final static String TAG = "ADInApp";
	
	static HashMap<String, String> _sku_to_id = new HashMap<String, String>();
	static HashMap<String, String> _id_to_sku = new HashMap<String, String>();
	
	static final boolean RES_OK = true;
	static final boolean RES_FAILED = false;
	
	static final boolean TYPE_RESTORE = true;
	static final boolean TYPE_PURCHASE = false;
	
	static final int ERROR_NONE = 0;
	static final int ERROR_USER_CANCEL = 1;
	static final int ERROR_BILLING_UNAVALIABLE = 3;
	static final int ERROR_DEVELOPER_ERROR = 5;
	static final int ERROR_ERROR = 6;
	
	static boolean _iap_connected = false;
	
	static int getErrorCode(int iap_error)
	{
		/*if(iap_error == SamsungIapHelper.IAP_PAYMENT_IS_CANCELED)
			return ERROR_USER_CANCEL;
		if(iap_error == SamsungIapHelper.IAP_ERROR_INITIALIZATION)
			return ERROR_BILLING_UNAVALIABLE;
		if(iap_error == SamsungIapHelper.IAP_ERROR_NEED_APP_UPGRADE)
			return ERROR_NONE;*/
		
		return ERROR_ERROR;
	}
	
	static final int MODE_PRODUCTION = 0;
	static final int MODE_TEST_SUCCESS = 1;
	static final int MODE_TEST_FAIL = 2;
	
	
	
	static private String _last_sku = "";
	static private String _user_id = null;
	static boolean _notify_about_restore = false;
	static HashSet<String> _products = new HashSet<String>();
	public static void restorePurchases()
	{
		_notify_about_restore = true;
		
		if(_user_id != null)
		{
			PurchasingManager.initiatePurchaseUpdatesRequest(Offset.BEGINNING);
		}
		else
		{
			PurchasingManager.initiateGetUserIdRequest();
			if(_notify_about_restore)
			{
				_notify_about_restore = false;
				notifyRestorePurchases(RES_FAILED, ERROR_BILLING_UNAVALIABLE);
			}
		}
		
		
	}
	
	static boolean _sandbox_mode = false;
	
	public static class MyPurchasingObserver extends BasePurchasingObserver {
        private static final String TAG = "IAPPurchasingObserver";
 
			public MyPurchasingObserver(Activity iapActivity) {
				super(iapActivity);
			}

			public void onSdkAvailable(final boolean isSandboxMode) 
			{
				_sandbox_mode = isSandboxMode;
				PurchasingManager.initiateGetUserIdRequest();
			}
			public void onGetUserIdResponse(final GetUserIdResponse response) 
			{
				if (response.getUserIdRequestStatus() ==
					GetUserIdResponse.GetUserIdRequestStatus.SUCCESSFUL) 
				{
					_user_id = response.getUserId();
					setStoreState(true);
					
					PurchasingManager.initiatePurchaseUpdatesRequest(Offset.BEGINNING);
					PurchasingManager.initiateItemDataRequest(_products);
				}
			}
			public void onItemDataResponse(final ItemDataResponse response) 
			{
				Log.w(TAG, "Data responce " + response.getItemDataRequestStatus().toString());
				if(response.getItemDataRequestStatus() == ItemDataRequestStatus.SUCCESSFUL_WITH_UNAVAILABLE_SKUS ||
					response.getItemDataRequestStatus() == ItemDataRequestStatus.SUCCESSFUL)
				{
				
					final Map<String, Item> items = response.getItemData();
					for (final String key : items.keySet()) 
					{
						Item i = items.get(key);
						String sku = i.getSku();
						String price = i.getPrice();
						setProductPrice(sku, price);
					}
				}
				
			
			}
			public void onPurchaseResponse(final PurchaseResponse response) 
			{
				final PurchaseRequestStatus status = response.getPurchaseRequestStatus();
				Receipt receipt = response.getReceipt();
				
				if(receipt == null)
				{
					if(_last_sku != null)
						notifyPurchaseSuccessful(_last_sku, RES_FAILED, TYPE_PURCHASE, ERROR_USER_CANCEL);
					return;
				}
				String sku = receipt.getSku();
				if (status == PurchaseRequestStatus.SUCCESSFUL) 
				{
					notifyPurchaseSuccessful(sku, RES_OK, TYPE_PURCHASE, ERROR_NONE);
				}
				else if(status == PurchaseRequestStatus.ALREADY_ENTITLED)
				{
					notifyPurchaseSuccessful(sku, RES_OK, TYPE_RESTORE, ERROR_NONE);
				}
			}
			
			public void onPurchaseUpdatesResponse(final PurchaseUpdatesResponse response) 
			{
				switch (response.getPurchaseUpdatesRequestStatus()) 
				{
				case SUCCESSFUL:
					 
					// Process receipts
					for (final Receipt receipt : response.getReceipts()) {
						switch (receipt.getItemType()) {
							case ENTITLED: // Re-entitle the customer
								String sku = receipt.getSku();
								notifyPurchaseSuccessful(sku, RES_OK, TYPE_RESTORE, ERROR_NONE);
							break;
						}
					}
					if(_notify_about_restore)
					{
						_notify_about_restore = false;
						notifyRestorePurchases(RES_OK, ERROR_NONE);
					}
					
					break;
					 
			   case FAILED:
					if(_notify_about_restore)
					{
						_notify_about_restore = false;
						notifyRestorePurchases(RES_FAILED, ERROR_BILLING_UNAVALIABLE);
					}
					break;
				}
			}
    };
	
	
	public static void makePurchase(String prod_id)
	{
		_last_sku = prod_id;
		String requestId = PurchasingManager.initiatePurchaseRequest(prod_id);
	}
	public static void loadStore(Activity parent, ArrayList<String> products)
	{
		//Save activity
		_activity = parent;	
		_products = new HashSet(products);
		PurchasingManager.registerObserver(new MyPurchasingObserver(_activity));
	}


	
	public static native void setProductPrice(String prod_id, String price);
	public static native void notifyPurchaseSuccessful(String prod_id, boolean success, boolean restored, int error_id);
	public static native void setStoreState(boolean supported);
	public static native void notifyRestorePurchases(boolean success, int error_id);
}