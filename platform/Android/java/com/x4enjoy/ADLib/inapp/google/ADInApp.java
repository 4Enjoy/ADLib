package com.x4enjoy.ADLib.inapp.google;
import java.util.ArrayList;
import android.app.Activity;
import android.util.Log;
import android.content.Intent;
public class ADInApp 
{
	private static IabHelper _iab = null;
	private static Activity _activity = null;
	private static ArrayList<String> _consumable_products = null;
	private static ArrayList<String> _nonconsumable_products = null;
	private final static String TAG = "ADInApp";
	static final int RC_REQUEST = 10001;
	static String last_sku = "";
	
	static final boolean PURCHASE_OK = true;
	static final boolean PURCHASE_FAILED = false;
	
	static final boolean TYPE_RESTORE = true;
	static final boolean TYPE_PURCHASE = false;
	
	static final int ERROR_NONE = 0;
	static final int ERROR_USER_CANCEL = 1;
	static final int ERROR_BILLING_UNAVALIABLE = 3;
	static final int ERROR_DEVELOPER_ERROR = 5;
	static final int ERROR_ERROR = 6;
	
	static int getErrorCode(int iap_error)
	{
		if(iap_error == IabHelper.IABHELPER_USER_CANCELLED)
			return ERROR_USER_CANCEL;
		if(iap_error == IabHelper.IABHELPER_REMOTE_EXCEPTION ||
			iap_error == IabHelper.IABHELPER_SEND_INTENT_FAILED)
			return ERROR_BILLING_UNAVALIABLE;
		
		return ERROR_ERROR;
	}
	static boolean _notify_restore = false;
	public static void restorePurchases()
	{
		
		if(_iab != null)
		{
			_notify_restore = true;
			_iab.queryInventoryAsync(false, null, mGotInventoryListener);
		}
		else
		{
			notifyRestorePurchases(PURCHASE_FAILED, ERROR_BILLING_UNAVALIABLE);
		}
	}
	
	public static void makePurchase(String prod_id)
	{
		if(_iab != null)
		{
			last_sku = prod_id;
			_iab.launchPurchaseFlow(_activity, prod_id, RC_REQUEST, mPurchaseFinishedListener);
		}
	}
	public static void loadStore(Activity parent, String key, 
	ArrayList<String> consumable_products,
	ArrayList<String> nonconsumable_products)
	{
		_activity = parent;
		_consumable_products = consumable_products;
		_nonconsumable_products = nonconsumable_products;
		_iab = new IabHelper(_activity, key);
		_iab.enableDebugLogging(false);
		
		_iab.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                Log.d(TAG, "Setup finished.");

                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    Log.w(TAG, "Setup failed.");
                    return;
                }

                // Have we been disposed of in the meantime? If so, quit.
                if (_iab == null) return;

                // IAB is fully set up. Now, let's get an inventory of stuff we own.
                Log.d(TAG, "Setup successful. Querying inventory.");
				ArrayList<String> products = new ArrayList<String>();
				products.addAll(_consumable_products);
				products.addAll(_nonconsumable_products);
                _iab.queryInventoryAsync(true, products, mGotInventoryListener);
            }
        });
	}
	private static IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            Log.d(TAG, "Query inventory finished.");

            // Have we been disposed of in the meantime? If so, quit.
            if (_iab == null) return;

            // Is it a failure?
            if (result.isFailure()) {
                Log.w(TAG, "Get inventory failed");
				if(_notify_restore)
				{
					_notify_restore = false;
					notifyRestorePurchases(PURCHASE_FAILED, getErrorCode(result.getResponse()));
				}
				
                return;
            }

            Log.d(TAG, "Query inventory was successful.");

            /*
             * Check for items we own. Notice that for each purchase, we check
             * the developer payload to see if it's correct! See
             * verifyDeveloperPayload().
             */

			
			for(String sku : _consumable_products )
			{
				Purchase consumable = inventory.getPurchase(sku);
				if(consumable != null && verifyDeveloperPayload(consumable))
				{
					_iab.consumeAsync(consumable, mConsumeFinishedListener);
					
				}
				
				SkuDetails details = inventory.getSkuDetails(sku);
				if(details  != null)
				{
					setProductPrice(sku, details.getPrice());
				}
			} 
			
			for(String sku : _nonconsumable_products)
			{
				Purchase nonconsumable = inventory.getPurchase(sku);
				if(nonconsumable != null && verifyDeveloperPayload(nonconsumable))
				{
					notifyPurchaseSuccessful(sku, PURCHASE_OK, TYPE_RESTORE, ERROR_NONE);
				}
				
				SkuDetails details = inventory.getSkuDetails(sku);
				if(details != null)
				{
					setProductPrice(sku, details.getPrice());
				}
			}
			setStoreState(true);
			
			if(_notify_restore)
			{
				_notify_restore = false;
				notifyRestorePurchases(PURCHASE_OK, ERROR_NONE);
			}
        }
    };
	
	
	
	static IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            Log.d(TAG, "Consumption finished. Purchase: " + purchase + ", result: " + result);

            // if we were disposed of in the meantime, quit.
            if (_iab == null) return;

			if(purchase == null)
			{
				Log.e(TAG, "Consume finish but no Purchase");
				return;
			}
            // We know this is the "gas" sku because it's the only one we consume,
            // so we don't check which sku was consumed. If you have more than one
            // sku, you probably should check...
            if (result.isSuccess()) 
			{
				notifyPurchaseSuccessful(purchase.getSku(), PURCHASE_OK, TYPE_PURCHASE, ERROR_NONE);
            }
            else 
			{
                notifyPurchaseSuccessful(purchase.getSku(), PURCHASE_FAILED, TYPE_PURCHASE, getErrorCode(result.getResponse()));
            }
            
            Log.d(TAG, "End consumption flow.");
        }
    };
	
	static IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);

            // if we were disposed of in the meantime, quit.
            if (_iab == null) return;
			
			String sku = last_sku;
			if(purchase != null) 
			{
				sku = purchase.getSku();
			}
			
            if (result.isFailure() && result.getResponse() != IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED) {
                notifyPurchaseSuccessful(sku, PURCHASE_FAILED, TYPE_PURCHASE, getErrorCode(result.getResponse()));
                return;
            }
            if (!verifyDeveloperPayload(purchase)) {
                notifyPurchaseSuccessful(sku, PURCHASE_FAILED, TYPE_PURCHASE, ERROR_ERROR);
                return;
            }

            Log.d(TAG, "Purchase successful.");
			
			if(_consumable_products.contains(sku))
			{
				_iab.consumeAsync(purchase, mConsumeFinishedListener);
			}
			else
			{
				notifyPurchaseSuccessful(sku, PURCHASE_OK, TYPE_PURCHASE, ERROR_NONE);
			}
        }
    };
	
	static public boolean handleActivityResult(int requestCode, int resultCode, Intent data)
	{
		if(_iab == null)
			return false;
		return _iab.handleActivityResult(requestCode, resultCode, data);
	}
	
	static boolean verifyDeveloperPayload(Purchase p) {
        String payload = p.getDeveloperPayload();
        return true;
    }
	
	public static native void setProductPrice(String prod_id, String price);
	public static native void notifyPurchaseSuccessful(String prod_id, boolean success, boolean restored, int error_id);
	public static native void setStoreState(boolean supported);
	public static native void notifyRestorePurchases(boolean success, int error_id);
}