package com.x4enjoy.ADLib.inapp.samsung;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.HashMap;
import android.app.Activity;
import android.util.Log;
import android.content.Intent;
import com.sec.android.iap.lib.helper.SamsungIapHelper;
import com.sec.android.iap.lib.vo.ErrorVo;
import com.sec.android.iap.lib.vo.PurchaseVo;
import com.sec.android.iap.lib.vo.InboxVo;
import com.sec.android.iap.lib.vo.ItemVo;

import com.sec.android.iap.lib.listener.OnPaymentListener;
import com.sec.android.iap.lib.listener.OnGetItemListener;
import com.sec.android.iap.lib.listener.OnGetInboxListener;
import com.sec.android.iap.lib.listener.OnIapBindListener;
import android.os.Bundle;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;

public class ADInApp 
{
	//private static IabHelper _iab = null;
	private static Activity _activity = null;
	
	private final static String TAG = "ADInApp";
	
	static HashMap<String, String> _sku_to_id = new HashMap<String, String>();
	static HashMap<String, String> _id_to_sku = new HashMap<String, String>();
	
	static final boolean RES_OK = true;
	static final boolean RES_FAILED = false;
	
	static final boolean TYPE_RESTORE = true;
	static final boolean TYPE_PURCHASE = false;
	
	static final int ERROR_NONE = 0;
	static final int ERROR_USER_CANCEL = 1;
	static final int ERROR_BILLING_UNAVALIABLE = 3;
	static final int ERROR_DEVELOPER_ERROR = 5;
	static final int ERROR_ERROR = 6;
	
	static boolean _iap_connected = false;
	
	static int getErrorCode(int iap_error)
	{
		if(iap_error == SamsungIapHelper.IAP_PAYMENT_IS_CANCELED)
			return ERROR_USER_CANCEL;
		if(iap_error == SamsungIapHelper.IAP_ERROR_INITIALIZATION)
			return ERROR_BILLING_UNAVALIABLE;
		if(iap_error == SamsungIapHelper.IAP_ERROR_NEED_APP_UPGRADE)
			return ERROR_NONE;
		
		return ERROR_ERROR;
	}
	
	static final int MODE_PRODUCTION = 0;
	static final int MODE_TEST_SUCCESS = 1;
	static final int MODE_TEST_FAIL = 2;
	
	static int SAMSUNG_MODE = SamsungIapHelper.IAP_MODE_COMMERCIAL;
	static private SamsungIapHelper _iap = null;
	static private String GROUP_ID = "";
	
	static private String _last_sku = "";
	static String getIdBySku(String sku)
	{
		if(!_sku_to_id.containsKey(sku))
		{
			Log.e(TAG, "Product not found "+sku);
			return "";
		}
		return _sku_to_id.get(sku);
	}
	
	static boolean _notify_about_restore = false;
	
	public static void restorePurchases()
	{
		_notify_about_restore = true;
		
		if(_iap != null)
		{
			Date d = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd",Locale.getDefault() );
			String today = sdf.format( d );
			
			_iap.getItemInboxList(GROUP_ID, 1, 100, "20130101", today, new OnGetInboxListener(){
				public void onGetItemInbox( ErrorVo _errorVO, ArrayList<InboxVo> _inboxList )
				{
					ADInApp.onGetItemInbox(_errorVO, _inboxList);
				}
			});
		}
		else
		{
			if(_notify_about_restore)
			{
				_notify_about_restore = false;
				notifyRestorePurchases(RES_FAILED, ERROR_BILLING_UNAVALIABLE);
			}
		}
		
	}
	static void do_restorePurchases()
	{
		try
		{
			ErrorVo             mErrorVo        = new ErrorVo();
			ArrayList<InboxVo>  mInbox          = new ArrayList<InboxVo>();
		
			Date d = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd",Locale.getDefault() );
			String today = sdf.format( d );
			
			//_iap.getItemInboxList(GROUP_ID, 1, 100, "20130101", today, mRestorePurchases);
		
			// 1. Call getItemsInbox() method of IAPService
			// ============================================================
			Bundle bundle = _iap.getItemsInbox( GROUP_ID,
										   1,
										   100,
										   "20130101",
										   today );
			// ============================================================
			
			// 2. save status code, error string
			// ============================================================
			mErrorVo.setError( bundle.getInt( SamsungIapHelper.KEY_NAME_STATUS_CODE ),
							   bundle.getString( SamsungIapHelper.KEY_NAME_ERROR_STRING ) );
			// ============================================================

			// 3. If inbox(list of purchased item) is loaded successfully,
			//    make inbox by Bundle data
			// ============================================================
			if( SamsungIapHelper.IAP_ERROR_NONE == mErrorVo.getErrorCode() )
			{
				ArrayList<String> purchaseItemStringList = 
						 bundle.getStringArrayList( SamsungIapHelper.KEY_NAME_RESULT_LIST );
			
				if( purchaseItemStringList != null )
				{
					for( String itemString : purchaseItemStringList )
					{
						InboxVo inboxVo = new InboxVo( itemString );
						mInbox.add( inboxVo );
					}
				}
				else
				{
					Log.d( TAG, "Bundle Value 'RESULT_LIST' is null." );
				}
			}
			// ============================================================
			// 4. If error occurred, print log.
			// ============================================================
			else
			{
				Log.d( TAG, mErrorVo.getErrorString() );
			}
			onGetItemInbox(mErrorVo, mInbox);
			
			// ============================================================
		}
		catch( Exception e )
		{
			if(_notify_about_restore)
			{
				_notify_about_restore = false;
				notifyRestorePurchases(RES_FAILED, ERROR_ERROR);
			}
			e.printStackTrace();
			
		}
	}
	static void do_getPurchasesList()
	{
		try
		{
			// 1) call getItemList() method of IAPService
			// ============================================================
			Bundle bundle = _iap.getItemList( GROUP_ID,
										 1,
										 100,
										 SamsungIapHelper.ITEM_TYPE_ALL );
			// ============================================================
			
			// 2) save status code, error string and extra String.
			// ============================================================
			ErrorVo mErrorVo = new ErrorVo();
			ArrayList<ItemVo> mItemList  = new ArrayList<ItemVo>();
			
			mErrorVo.setError( bundle.getInt( SamsungIapHelper.KEY_NAME_STATUS_CODE ),
							   bundle.getString( SamsungIapHelper.KEY_NAME_ERROR_STRING ) );
			
			mErrorVo.setExtraString( bundle.getString( 
											  SamsungIapHelper.KEY_NAME_IAP_UPGRADE_URL ) );
			// ============================================================
			
			// 3) If item list is loaded successfully,
			//    make item list by Bundle data
			// ============================================================
			if( mErrorVo.getErrorCode() == SamsungIapHelper.IAP_ERROR_NONE )
			{
				ArrayList<String> itemStringList = 
						 bundle.getStringArrayList( SamsungIapHelper.KEY_NAME_RESULT_LIST );
				
				if( itemStringList != null )
				{
					for( String itemString : itemStringList )
					{
						ItemVo itemVo = new ItemVo( itemString );
						mItemList.add( itemVo );
					}
				}
				else
				{
					Log.d( TAG, "Bundle Value 'RESULT_LIST' is null." );
				}
			}
			// ============================================================
			// 4) If failed, print log.
			// ============================================================
			else
			{
				Log.d( TAG, mErrorVo.getErrorString() );
			}
			onGetItem(mErrorVo, mItemList);
			// ============================================================
		}
		catch( Exception e )
		{   
		   e.printStackTrace();
		}
	}
	static MyTask mQueuedTask = null;
	static void queueTask(boolean restore_purchases, boolean load_purchase_list)
	{
		if(_iap != null && _iap_connected == true)
		{	
			try
			{
				if( mQueuedTask != null &&
					mQueuedTask.getStatus() != Status.FINISHED )
				{
					mQueuedTask.cancel( true );
				}

				MyTask task = new MyTask(restore_purchases, load_purchase_list);
				mQueuedTask = task;								
				mQueuedTask.execute();
			}
			catch( Exception e )
			{
				e.printStackTrace();
			}
		}
	}
	
	private static class MyTask extends AsyncTask<String, Object, Boolean>
    {
        boolean _restore_purchases = false;
		boolean _load_purchase_list = false;
		
        public MyTask
        (
            boolean          restore_purchases,
            boolean          load_purchase_list
        )
        {
			_restore_purchases = restore_purchases;
            _load_purchase_list    = load_purchase_list;
        }
        
        @Override
        protected Boolean doInBackground( String... params )
        {
            if(_load_purchase_list)
				do_getPurchasesList();
            if(_restore_purchases)
				do_restorePurchases();
            return true;
        }

        @Override
        protected void onPostExecute( Boolean _result )
        {
        }
    }
	
	
	static String getSkuById(String id)
	{
		if(!_id_to_sku.containsKey(id))
		{
			Log.e(TAG, "Product not found "+id);
			return "";
		}
		return _id_to_sku.get(id);
	}
	
	public static void makePurchase(String prod_id)
	{
		_last_sku = prod_id;
		String samsung_id = getIdBySku(prod_id);
		Log.d(TAG, "Purchase started: " + prod_id + " (" + samsung_id + ")");
		_iap.startPayment(GROUP_ID, samsung_id, true, mPurchaseFinishedListener);
	}
	public static void loadStore(Activity parent, String group_id, 
				ArrayList<String> products_skus,
				ArrayList<String> products_ids,
				int running_mode)
	{
		//Definde running mode
		if(running_mode == MODE_TEST_SUCCESS)
			SAMSUNG_MODE = SamsungIapHelper.IAP_MODE_TEST_SUCCESS;
		else if(running_mode == MODE_TEST_FAIL)
			SAMSUNG_MODE = SamsungIapHelper.IAP_MODE_TEST_FAIL;
		
		//Save activity
		_activity = parent;
		
		//save group id
		GROUP_ID = group_id;
		
		//Create products lists
		int products_num = products_skus.size();
		if(products_ids.size() < products_num)
			products_num = products_ids.size();
			
		for(int i = 0; i<products_num; ++i)
		{
			String sku = products_skus.get(i);
			String id = products_ids.get(i);
			
			_sku_to_id.put(sku, id);
			_id_to_sku.put(id, sku);
		}
		
		_iap = SamsungIapHelper.getInstance(_activity, SAMSUNG_MODE);
		
		
		_iap.bindIapService(new OnIapBindListener()
		{
			public void onBindIapFinished( int result )
			{
				if(result == SamsungIapHelper.IAP_RESPONSE_RESULT_OK)
				{
					_iap_connected = true;
					queueTask(true, true);
				}
			}
		});
	}
	
	static OnPaymentListener mPurchaseFinishedListener = new OnPaymentListener() {
        public void onPayment( ErrorVo errorVO, PurchaseVo _purchaseVO ) 
		{
			String sku = _last_sku;
			
			if(_purchaseVO != null)
				sku = getSkuById(_purchaseVO.getItemId());
            Log.d(TAG, "Purchase finished: purchase: " + sku);

            if(errorVO.getErrorCode() == SamsungIapHelper.IAP_ERROR_NONE)
			{
				notifyPurchaseSuccessful(sku, RES_OK, TYPE_PURCHASE, ERROR_NONE);
			}
			else if(errorVO.getErrorCode() == SamsungIapHelper.IAP_ERROR_ALREADY_PURCHASED)
			{
				notifyPurchaseSuccessful(sku, RES_OK, TYPE_RESTORE, ERROR_NONE);
			}
			else
			{
				Log.e(TAG, "Samsung error: "+errorVO.getErrorCode() + " "+errorVO.dump());
				notifyPurchaseSuccessful(sku, RES_FAILED, TYPE_PURCHASE, getErrorCode(errorVO.getErrorCode()));
			}
        }
    };
	

	static void onGetItem( ErrorVo errorVO, ArrayList<ItemVo> _itemList )
	{
		if(errorVO.getErrorCode() == SamsungIapHelper.IAP_ERROR_NONE)
		{
			if(_itemList != null)
			{
				for(ItemVo item : _itemList)
				{
					String sku = getSkuById(item.getItemId());
					String price = item.getItemPriceString();
					Log.d(TAG, "Price loaded: " + sku + " = " + price);
					setProductPrice(sku, price);
				}
			}
			
			setStoreState(true);
		}
		
	}

	

	static void onGetItemInbox( ErrorVo errorVO, ArrayList<InboxVo> _inboxList )
	{
		Log.w(TAG, "Purchase restore completed!!!");
		if(errorVO.getErrorCode() == SamsungIapHelper.IAP_ERROR_NONE)
		{
			if(_inboxList != null)
			{
				for(InboxVo item : _inboxList)
				{
					if(item.getType().equals(SamsungIapHelper.ITEM_TYPE_NON_CONSUMABLE))
					{
						
						String sku = getSkuById(item.getItemId());
						Log.d(TAG, "Purchase restored: " + sku);
						notifyPurchaseSuccessful(sku, RES_OK, TYPE_RESTORE, ERROR_NONE);
					}
				}
			}
			
			if(_notify_about_restore)
			{
				_notify_about_restore = false;
				notifyRestorePurchases(RES_OK, ERROR_NONE);
			}
		}
		else
		{
			Log.e(TAG, "Samsung error: "+errorVO.getErrorCode() + " "+errorVO.dump());
			if(_notify_about_restore)
			{
				_notify_about_restore = false;
				notifyRestorePurchases(RES_FAILED, getErrorCode(errorVO.getErrorCode()));
			}
		}
	}

	
	public static native void setProductPrice(String prod_id, String price);
	public static native void notifyPurchaseSuccessful(String prod_id, boolean success, boolean restored, int error_id);
	public static native void setStoreState(boolean supported);
	public static native void notifyRestorePurchases(boolean success, int error_id);
}