package com.x4enjoy.ADLib;
import org.cocos2dx.lib.Cocos2dxActivity;

import android.util.Log;
import android.widget.FrameLayout;
import android.content.res.Configuration;
import android.widget.Toast;
import android.content.pm.ActivityInfo;
import android.view.OrientationEventListener;
import android.hardware.SensorManager;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.Surface;
import android.view.WindowManager;
import android.view.Display;
import android.graphics.Point;
import android.util.DisplayMetrics;

public class ADActivity extends Cocos2dxActivity{

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		orientationListener = new OrientationEventListener(this, SensorManager.SENSOR_DELAY_NORMAL) {
			public void onOrientationChanged(int orientation) {
				if(_is_flip_enabled) {
					handleOrientationChange(orientation);
				}
			}
		};
	}
	
	OrientationEventListener orientationListener = null;
	
	static final int LANDSCAPE = 1;
	static final int LANDSCAPE_REVERSED = 2;
	static final int PORTRAIT = 3;
	static final int PORTRAIT_REVERSED = 4;
	
	int _last_orientation = LANDSCAPE;
	int _next_orientation = LANDSCAPE;
	int _last_orientation_repeated = 0;
	
	boolean _is_running = false;
	boolean _is_default_landscape = true;
	boolean _is_flip_enabled = false; 
	
	int _orientation_diff = 0;
	
	void enableOrientationListener()
	{
		/*if(_is_flip_enabled && orientationListener != null && !_is_running)
		{
			_orientation_diff = gerOrientationDiff();
			_is_running = true;
			orientationListener.enable();
		}*/
	}
	
	//Returns the initial device angle to make detection work normally
	int gerOrientationDiff() 
	{
		WindowManager windowManager =  (WindowManager) getSystemService(WINDOW_SERVICE);

		Configuration config = getResources().getConfiguration();

		int rotation = windowManager.getDefaultDisplay().getRotation();
		int angle = 0;
		if(rotation == Surface.ROTATION_180)
			angle = 180;
		if(rotation == Surface.ROTATION_90)
			angle = 90;
		if(rotation == Surface.ROTATION_270)
			angle = 270;
		return angle - 90;
	}
	
	void disableOrientationListener()
	{
		if(_is_flip_enabled && orientationListener != null && _is_running)
		{
			_is_running = false;
			orientationListener.disable();
		}
	}
	
	int getCurrentOrientation(int angle)
	{
		angle -= _orientation_diff;
		if(angle < 0) {
			angle += 360;
		}
		
		if(angle >= 0+45 && angle <= 90 + 45)
		{
			return PORTRAIT;
		}
		else if(angle >= 90+45 && angle <= 180 + 45)
		{
			return LANDSCAPE;//_REVERSED;
		}
		else if(angle >= 180+45 && angle <= 270 +45)
		{
			return PORTRAIT;//_REVERSED;
		}
		return LANDSCAPE;
	}
	

	void handleOrientationChange(int ori)
	{
		/*int orientationx = getResources().getConfiguration().orientation;
		if(orientationx == Configuration.ORIENTATION_LANDSCAPE)
		{
			Log.d("ORIENT", "LANDSCAPE");
			orientationChanged(true);
		}
		else if(orientationx == Configuration.ORIENTATION_PORTRAIT)
		{
			Log.d("ORIENT", "PORTRAIT");
			orientationChanged(false);
		}*/
		
		/*Display display = ((WindowManager)
          getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		int screenOrientation = display.getRotation();
		
		
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getRealMetrics(dm);
		int width = dm.widthPixels;
		int height = dm.heightPixels;
		
		Log.d("ORIENT", "W:"+width+" H:"+height+" R:"+screenOrientation);*/
		
	
		//int rotate_after = 1;
		/*int orientation = getCurrentOrientation(ori);
		if(_last_orientation != orientation)
		{
			if(_next_orientation != orientation)
			{
				_next_orientation = orientation;
				_last_orientation_repeated = 0;
				return;
			}
			else
			{
				if(_last_orientation_repeated < 2)
				{
					_last_orientation_repeated++;
					return;
				}
			}
			
			_last_orientation = orientation;
			
			int rotate_to = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
			boolean use_landscape = true;
			
			//Perform rotation
			if(_is_default_landscape)
			{
				//For landscaped
			}
			else
			{
				rotate_to = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
				//For portrait
				if(orientation == PORTRAIT)
				{
					use_landscape = false;
				}
				else if(orientation == PORTRAIT_REVERSED)
				{
					rotate_to = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT ;
					use_landscape = false;
				}
				else if(orientation == LANDSCAPE)
				{
					use_landscape = true;
				}
				else if(orientation == LANDSCAPE_REVERSED)
				{
					rotate_to = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
					use_landscape = true;
				}
			}
			
			setRequestedOrientation(rotate_to);
			orientationChanged(use_landscape);
		}
		//Log.d("ORIENT", "Value: "+getCurrentOrientation(orientation));*/
	}
	
	public void enableOrientationHandler(boolean default_is_landscape)
	{
		Log.d("ORIENT", "default enabled");
		_is_flip_enabled = true;
		_is_default_landscape = default_is_landscape;
		
		enableOrientationListener();
		//
	}
	
	private native void orientationChanged(boolean is_landscape);
	
	@Override
	protected void onResume() {
		super.onResume();
		
		enableOrientationListener();
	}

	@Override
	protected void onPause() {
		super.onPause();
		
		disableOrientationListener();
	}
}