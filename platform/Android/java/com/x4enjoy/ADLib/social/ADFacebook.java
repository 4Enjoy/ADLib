package com.x4enjoy.ADLib.social;

import android.app.Activity;
import android.util.Log;
import com.facebook.Session;
import com.facebook.SessionState;
import java.util.Hashtable;
import java.lang.String;
import com.facebook.Request;
import android.os.Bundle;
import com.facebook.HttpMethod;
import com.facebook.Response;
import com.facebook.model.GraphObject;
import org.json.JSONObject;
import org.json.JSONException;
import org.json.JSONArray;
import java.util.ArrayList;
import com.facebook.model.GraphUser;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.FacebookException;
import java.util.Arrays;
import com.facebook.Session.OpenRequest;
import com.facebook.Session.StatusCallback;
import com.facebook.Session.Builder;
import java.util.*;


public class ADFacebook
{
	public static class User
	{
		User(String s_id, String s_fir_name, String s_sec_name)
		{
			first_name = s_fir_name;
			last_name = s_sec_name;
			id = s_id;
		}
		public String first_name = "";
		public String last_name = "";
		public String id = "";
	}

	static final String TAG = "ADFacebook";
	public static void loadData(Activity activity)
	{
		m_activity = activity;
		openActiveSession(false);
	}
	
	public static void logIn()
	{
		openActiveSession(true);
	}
	
	public static void logOut()
	{
		Session session = Session.getActiveSession();
		if(session != null)
		{
			session.closeAndClearTokenInformation();
		}
	}
	
        public static void invite(String message)
        {
            try
            {
                Session session = Session.getActiveSession();
                if(session != null && session.isOpened())
                {
                    Bundle params = new Bundle();
                    params.putString("message", message);

                    WebDialog.Builder builder = new WebDialog.Builder(m_activity,
                                Session.getActiveSession(), "apprequests", params);
                    builder.build().show();
                }
            }
            catch(Throwable e)
            {}
        }


	private static void updateMyInfo()
	{
		try 
		{
			Session session = Session.getActiveSession();
			if(session != null && session.isOpened())
			{
				// make request to the /me API
				Request.executeMeRequestAsync(session, new Request.GraphUserCallback() 
				{
					// callback after Graph API response with user object
					@Override
					public void onCompleted(GraphUser user, Response response) 
					{
						if (user != null) 
						{
							parseMyInfo(user);
						}
					}
				});
			}
		}
		catch(Throwable e)
		{}
	}
	
	private static void shareAction(final int share_id,
									String name,
									String caption,
									String description,
									String link,
									String picture)
	{
		try{
			Session session = Session.getActiveSession();
			if(session != null && session.isOpened())
			{
				Log.d(TAG, "share");
				Bundle params = new Bundle();
				params.putString("name", name);
				params.putString("caption", caption);
				params.putString("description", description);
				params.putString("link", link);
				params.putString("picture", picture);

				WebDialog feedDialog = (
					new WebDialog.FeedDialogBuilder(m_activity,
						session,
						params))
					.setOnCompleteListener(new OnCompleteListener() 
					{

						@Override
						public void onComplete(Bundle values,
							FacebookException error) 
						{
							if (error == null) 
							{
								// When the story is posted, echo the success
								// and the post Id.
								final String postId = values.getString("post_id");
								if (postId != null) 
								{
									shareResult(share_id, true);
									//Log.e(TAG, "Posted story, id: "+postId);
								} 
								else 
								{
									// User clicked the Cancel button
									shareResult(share_id, false);
									//Log.e(TAG, "Publish cancelled");
								}
							} 
							else if (error instanceof FacebookOperationCanceledException) 
							{
								// User clicked the "x" button
								//Log.e(TAG, "Publish cancelled");
								shareResult(share_id, false);
							} 
							else 
							{
								// Generic, ex: network error
								//Log.e(TAG, "Error posting story");
								shareResult(share_id, false);
							}
						}

					})
				.build();
				feedDialog.show();
			}
		}
		catch(Throwable e)
		{
			shareResult(share_id, false);
		}
			
	}
	
	private static boolean updateFriendList()
	{
            Log.d("Facebook", "Friends list update started");
		try 
		{
			//TODO: filter is_app_user = true
                        //String fqlQuery = "select uid, first_name, last_name from user where uid in (select uid2 from friend where uid1 = me()) and is_app_user";
			Bundle params = new Bundle();
                        //params.putString("q", fqlQuery);

			Session session = Session.getActiveSession();
			if(session != null && session.isOpened())
			{
				//Log.d(TAG, "Friend request sent");
				Request request = new Request(session, 
                                        "/me/friends",
					params, 
					HttpMethod.GET, 
					new Request.Callback()
					{ 
						public void onCompleted(Response response) 
						{
							//Log.d(TAG, "Friend response");
							parseFriends(response);
							m_new_friends_loaded = true;
						}	
					});
				Request.executeBatchAsync(request);
				return true;
			}
		}
		catch(Throwable e)
                {
                    Log.e("Facebook", "Get friends exception", e);
                }
                Log.d("Facebook", "Get friends list");
		return false;
	}
	private static void parseMyInfo(GraphUser user)
	{
		String id = user.getId();
		String first_name = user.getFirstName();
		String last_name = user.getLastName();
		
		User aduser = new User(id, first_name, last_name);
		myInfoLoaded(aduser);

	}
	private static void parseFriends(Response response)
	{
            //Log.d("Facebook", response.toString());
		GraphObject graphObject = response.getGraphObject();
		if (graphObject != null) 
		{
			JSONObject jsonObject = graphObject.getInnerJSONObject();
			try 
			{
				ArrayList<User> list = new ArrayList<User>();
				
				JSONArray array = jsonObject.getJSONArray("data");
                                //Log.d("Facebook", array.toString());
				for (int i = 0; i < array.length(); i++) 
				{
					JSONObject object = (JSONObject) array.get(i);
                                        String id = object.get("id").toString();
                                        String first_name = object.get("name").toString();
                                        //String last_name = object.get("last_name").toString();
					
                                        User user = new User(id, first_name, "");
					list.add(user);
				}
				
				friendsLoaded(list);
			} 
			catch (JSONException e) 
			{

				e.printStackTrace();
			}
		}
	}
	
	private static Session openActiveSession(Activity context, boolean allowLoginUI, OpenRequest openRequest) {
        Session session = new Builder(context).build();
        if (SessionState.CREATED_TOKEN_LOADED.equals(session.getState()) || allowLoginUI) {
            Session.setActiveSession(session);
            session.openForPublish(openRequest);
            return session;
        }
        return null;
    }
	
	public static Session openActiveSession(Activity activity, boolean allowLoginUI,
            List<String> permissions, StatusCallback callback) {
        return openActiveSession(
                activity, 
                allowLoginUI, 
                new OpenRequest(activity).setCallback(callback).setPermissions(permissions));
    }
	
	private static void openActiveSession(boolean with_ui)
	{
		if(m_activity != null && m_token == null)
		{
                        openActiveSession(m_activity, with_ui, Arrays.asList("publish_actions", "user_friends"), m_sessionCallback);
		}
	}
	
	static Session.StatusCallback m_sessionCallback = new Session.StatusCallback()
	{
		@Override
		public void call(Session session, SessionState state, Exception exception) 
		{

                    Log.d("Facebook", "Session callback");
			if(exception != null) {
				Log.w(TAG, "FB exception");
				Log.w(TAG, exception.toString()); 
			}
			if(state == SessionState.CLOSED || state == SessionState.CLOSED_LOGIN_FAILED)
			{
				Log.d(TAG, "Session closed");
				
				sessionClosed();
				m_token = null;
			}
			else if(state == SessionState.OPENED || state == SessionState.OPENED_TOKEN_UPDATED)
			{
				Log.d(TAG, "Session opened");
				if(session != null)
				{
					m_token = session.getAccessToken();
					sessionOpened(m_token);
                                        Log.d("Facebook", "Update friends call");
					if(!m_new_friends_loaded)
					{
						updateFriendList();
						updateMyInfo();
						
					}
				}
			}
		}
	};
	
	static boolean m_new_friends_loaded = false;
	static Activity m_activity = null;
	static String m_token = null;
	native static void sessionClosed();
	native static void sessionOpened(String token); 
	native static void friendsLoaded(ArrayList<User> friends);
	native static void myInfoLoaded(User user);
	native static void shareResult(int share_id, boolean is_success);
}
