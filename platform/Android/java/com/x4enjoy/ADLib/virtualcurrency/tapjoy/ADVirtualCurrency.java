package com.x4enjoy.ADLib.virtualcurrency.tapjoy;

import android.app.Activity;
import android.util.Log;
import com.tapjoy.TapjoyConnect;
import com.tapjoy.TapjoyConnectNotifier;
import com.tapjoy.TapjoyNotifier;
import com.tapjoy.TapjoySpendPointsNotifier;
import java.util.Hashtable;
import java.lang.String;

public class ADVirtualCurrency
{
	public static void init(Activity parent, String appID, String secretKey)
	{
		TapjoyConnect.enableLogging(false);
		Hashtable<String, String> params = new Hashtable<String, String>();
		TapjoyConnect.requestTapjoyConnect(parent, appID, secretKey, params, d_connect_notifier);
	}
	
	public static void onPause()
	{
		TapjoyConnect.getTapjoyConnectInstance().appPause();
	}
	
	public static void onResume()
	{
		TapjoyConnect.getTapjoyConnectInstance().appResume();
		queueCurrencyValue();
	}
	
	public static void showOffers()
	{
		TapjoyConnect.getTapjoyConnectInstance().showOffers();
	}
	
	public static void queueCurrencyValue()
	{
		TapjoyConnect.getTapjoyConnectInstance().getTapPoints(d_currnecy_notifier);
	}
	
	static TapjoyConnectNotifier d_connect_notifier = new TapjoyConnectNotifier() {
        public void connectFail()
		{
			Log.w("TapJoy", "Connect Failed");
		}
		public void connectSuccess()
		{
			Log.d("TapJoy", "Connect Success");
			queueCurrencyValue();
			nativeConnectSuccess();
		}
    };
	
	static TapjoyNotifier d_currnecy_notifier = new TapjoyNotifier() {
		public void getUpdatePoints(java.lang.String currencyName, int pointTotal)
		{
			Log.d("TapJoy", "Update points success: " + pointTotal);
			if(pointTotal > 0)
			{
				Log.d("TapJoy", "Spending points...");
				TapjoyConnect.getTapjoyConnectInstance().spendTapPoints(pointTotal, d_spend_notifier);
				addCurrencyPoints(pointTotal);
			}
		}
		
		public void getUpdatePointsFailed(java.lang.String error) 
		{
			Log.w("TapJoy", "Update points failed");
		}
	};
	
	static TapjoySpendPointsNotifier d_spend_notifier = new TapjoySpendPointsNotifier() 
	{
		public void getSpendPointsResponse(java.lang.String currencyName, int pointTotal)
		{
			Log.d("TapJoy", "Spend points success" + pointTotal);
			if(pointTotal > 0)
			{
				queueCurrencyValue();
			}
			
		}
		public void getSpendPointsResponseFailed(java.lang.String error)
		{
			Log.w("TapJoy", "Spend points failed");
		}
	};
	
	native static void addCurrencyPoints(int points);
	native static void nativeConnectSuccess(); 
}