#include "ADJNI.h"
#include "platform/android/jni/JniHelper.h"
inline bool getEnvH(JNIEnv **env)
{
    bool bRet = false;

    do
    {
        if (cocos2d::JniHelper::getJavaVM()->GetEnv((void**)env, JNI_VERSION_1_4) != JNI_OK)
        {
            LOGD("Failed to get the environment using GetEnv()");
            break;
        }

        if (cocos2d::JniHelper::getJavaVM()->AttachCurrentThread(env, 0) < 0)
        {
            LOGD("Failed to get the environment using AttachCurrentThread()");
            break;
        }

        bRet = true;
    } while (0);

    return bRet;
}

JNIEnv* ADJNI::getEnv()
{
    JNIEnv* env = 0;
    if(getEnvH(&env))
        return (env);
    return (nullptr);
}



#define CocosActivity "org/cocos2dx/lib/Cocos2dxActivity"

jobject ADJNI::getActivity(JNIEnv* env)
{
    jclass cocos_activity = env->FindClass(CocosActivity);
    jmethodID get_activity = env->GetStaticMethodID(
                cocos_activity, "getContext", F(J(cContext), None));
    jobject activity = env->CallStaticObjectMethod(cocos_activity, get_activity);
    return activity;
}
