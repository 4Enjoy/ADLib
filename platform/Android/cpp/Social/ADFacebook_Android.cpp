#include <ADLib/Social/ADFacebook.h>
#include "../ADOnActivityResult.h"
#include "../ADJNI.h"
#include "../Device/ADUIThread.h"

#include <ADLib/Common/ADCocosThread.h>

#define cSession "com/facebook/Session"
#define cADFacebook "com/x4enjoy/ADLib/social/ADFacebook"
#define cArrayList "java/util/ArrayList"
#define cObject "java/lang/Object"
#define cUser "com/x4enjoy/ADLib/social/ADFacebook$User"

bool facebookActivityResult(jint requestCode,
                            jint resultCode,
                            jobject data)
{
    JNIEnv* env = ADJNI::getEnv();
    static jclass cl_Session = ADJNI::GRef(env, env->FindClass(cSession));
    static jmethodID m_Session_getActiveSession = env->GetStaticMethodID(
                cl_Session, "getActiveSession", F(J(cSession), None));
    jobject session = env->CallStaticObjectMethod(cl_Session, m_Session_getActiveSession);

    bool res = false;
    if(session)
    {
        jobject activity = ADJNI::getActivity(env);
        static jmethodID m_Session_onActivityResult = env->GetMethodID(
                    cl_Session,
                    "onActivityResult",
                    F(Bool, J(cActivity) Int Int J(cIntent)));

        res = env->CallBooleanMethod(session, m_Session_onActivityResult,
                                     activity, requestCode, resultCode, data);

        env->DeleteLocalRef(activity);
    }
    env->DeleteLocalRef(session);
    return res;
}

class FacebookAndroid : public ADFacebook::Platform
{
public:
    FacebookAndroid()
        : _share_next_id(0)
    {
        _obj = this;
        //Setup on activity result
        ADOnActivityResult::addCallback(&facebookActivityResult);
    }

    static FacebookAndroid* getInstance()
    {
        return _obj;
    }

    /**
     * @brief How to implement
     * 1. Start session without GUI
     * 2. Load info about Me and friends
     * @param key
     */
    void loadData()
    {
        JNIEnv* env = ADJNI::getEnv();
        jobject activity = ADJNI::getActivity(env);

        static jclass cl_ADFacebook = ADJNI::GRef(env, env->FindClass(cADFacebook));
        static jmethodID m_ADFacebook_loadData = env->GetStaticMethodID(
                    cl_ADFacebook, "loadData", F(Void, J(cActivity)));

        env->CallStaticVoidMethod(cl_ADFacebook,
                                  m_ADFacebook_loadData,
                                  activity);
        env->DeleteLocalRef(activity);
    }

    /**
     * @brief How to implement:
     * 1. If not logged in show log in window
     */
    void logIn()
    {
        JNIEnv* env = ADJNI::getEnv();
        static jclass cl_ADFacebook = ADJNI::GRef(env, env->FindClass(cADFacebook));
        static jmethodID m_ADFacebook_logIn = env->GetStaticMethodID(
                    cl_ADFacebook, "logIn", F(Void, None));

        env->CallStaticVoidMethod(cl_ADFacebook,
                                  m_ADFacebook_logIn);
    }

    void inviteFriends(const std::string& message)
    {
        ADUIThread::getInstance().runInUIThread(ADRunnable([=]()
        {

            JNIEnv* env = ADJNI::getEnv();
            static jclass cl_ADFacebook = ADJNI::GRef(env, env->FindClass(cADFacebook));
            static jmethodID m_ADFacebook_invite = env->GetStaticMethodID(
                        cl_ADFacebook, "invite", F(Void, J(cString)));

            jstring jmessage = env->NewStringUTF(message.c_str());


            env->CallStaticVoidMethod(cl_ADFacebook, m_ADFacebook_invite,
                                      jmessage);

        }));
    }


    void logOut()
    {
        JNIEnv* env = ADJNI::getEnv();
        static jclass cl_ADFacebook = ADJNI::GRef(env, env->FindClass(cADFacebook));
        static jmethodID m_ADFacebook_logOut = env->GetStaticMethodID(
                    cl_ADFacebook, "logOut", F(Void, None));

        env->CallStaticVoidMethod(cl_ADFacebook,
                                  m_ADFacebook_logOut);
    }

    void share(const std::string &name,
               const std::string &caption,
               const std::string &description,
               const std::string &link,
               const std::string &url,
               const ADFacebook::ShareResult &res)
    {
        ADUIThread::getInstance().runInUIThread(ADRunnable([=]()
        {


            int id = _share_next_id++;
            _share_map[id] = res;

            JNIEnv* env = ADJNI::getEnv();
            static jclass cl_ADFacebook = ADJNI::GRef(env, env->FindClass(cADFacebook));
            static jmethodID m_ADFacebook_shareAction = env->GetStaticMethodID(
                        cl_ADFacebook, "shareAction", F(Void, Int J(cString) J(cString) J(cString) J(cString) J(cString)));

            jstring jname = env->NewStringUTF(name.c_str());
            jstring jcaption = env->NewStringUTF(caption.c_str());
            jstring jdescription = env->NewStringUTF(description.c_str());
            jstring jlink = env->NewStringUTF(link.c_str());
            jstring jurl = env->NewStringUTF(url.c_str());

            env->CallStaticVoidMethod(cl_ADFacebook, m_ADFacebook_shareAction,
                                      id, jname, jcaption, jdescription, jlink, jurl);

        }));
    }


    void shareResult(int id, bool success)
    {
        auto it = _share_map.find(id);

        if(it != _share_map.end())
        {
            auto callback = it->second;
            if(callback)
                callback(success);
            _share_map.erase(it);
        }
    }

private:
    static FacebookAndroid* _obj;
    typedef std::map<int, ADFacebook::ShareResult> ShareMap;

    ShareMap _share_map;
    int _share_next_id;
};
FacebookAndroid* FacebookAndroid::_obj = nullptr;
ADFacebook::PlatformPtr ADFacebook::getImplementation()
{
    return std::make_shared<FacebookAndroid>();
}

ADFacebookFriendPtr facebookReadUser(JNIEnv* env, jobject user)
{
    static jclass cl_User = ADJNI::GRef(env, env->FindClass(cUser));
    static jfieldID f_User_first_name = env->GetFieldID(
                cl_User, "first_name", J(cString));
    static jfieldID f_User_last_name = env->GetFieldID(
                cl_User, "last_name", J(cString));
    static jfieldID f_User_id = env->GetFieldID(
                cl_User, "id", J(cString));

    jstring j_first_name = (jstring)env->GetObjectField(user, f_User_first_name);
    jstring j_last_name = (jstring)env->GetObjectField(user, f_User_last_name);
    jstring j_id = (jstring)env->GetObjectField(user, f_User_id);

    std::string first_name = ADJNI::toString(env, j_first_name);
    std::string last_name = ADJNI::toString(env, j_last_name);
    std::string id = ADJNI::toString(env, j_id);

    std::stringstream ss;
    ss << id;

    uint64_t id_int = 0;
    ss >> id_int;

    ADFacebookFriendPtr f = std::make_shared<ADFacebookFriend>();
    f->setUserID(id_int);
    f->setFirstName(first_name);
    f->setLastName(last_name);


    env->DeleteLocalRef(j_first_name);
    env->DeleteLocalRef(j_last_name);
    env->DeleteLocalRef(j_id);

    return f;
}

extern "C"
{


void Java_com_x4enjoy_ADLib_social_ADFacebook_sessionOpened(JNIEnv*  env, jclass, jstring token)
{
	cocos2d::CCLog("FB: session opened");
    std::string ctoken = ADJNI::toString(env, token);

    ADUIThread::getInstance().runInCocos2dThread(ADRunnable([ctoken](){
        FacebookAndroid::getInstance()->sessionOpened(ctoken);
    }));
}

void Java_com_x4enjoy_ADLib_social_ADFacebook_sessionClosed(JNIEnv*  env, jclass)
{

	cocos2d::CCLog("FB: session closed");
    ADUIThread::getInstance().runInCocos2dThread(ADRunnable([](){
        FacebookAndroid::getInstance()->sessionClosed();
    }));
}

void Java_com_x4enjoy_ADLib_social_ADFacebook_myInfoLoaded(JNIEnv*  env, jclass, jobject user)
{
    ADFacebookFriendPtr f = facebookReadUser(env, user);
    if(f->getUserID() > 0)
    {
        ADUIThread::getInstance().runInCocos2dThread(ADRunnable([f](){
            FacebookAndroid::getInstance()->myInfoLoaded(f);
        }));
    }
}

void Java_com_x4enjoy_ADLib_social_ADFacebook_shareResult(JNIEnv*  env, jclass, jint id, jboolean success)
{
    bool is_success = success;
    int cid = id;
    ADCocosThread::onCocos2dThread([is_success, cid](){
        FacebookAndroid::getInstance()->shareResult(cid, is_success);
    });
}

void Java_com_x4enjoy_ADLib_social_ADFacebook_friendsLoaded(JNIEnv*  env, jclass, jobject list)
{
    static jclass cl_ArrayList = ADJNI::GRef(env, env->FindClass(cArrayList));
    static jmethodID m_ArrayList_size = env->GetMethodID(
                cl_ArrayList, "size", F(Int, None));
    static jmethodID m_ArrayList_get = env->GetMethodID(
                cl_ArrayList, "get", F(J(cObject), Int));

    //cocos2d::CCLog("ArrayList %d, size %d, list %d", cl_ArrayList, m_ArrayList_size, list);

    int length = env->CallIntMethod(list, m_ArrayList_size);

    ADFacebookFriendPtrArr friends;
    friends.reserve(length);

    for(int i = 0; i < length; ++i)
    {
        jobject user = env->CallObjectMethod(list, m_ArrayList_get, i);
        ADFacebookFriendPtr f = facebookReadUser(env, user);
        env->DeleteLocalRef(user);

        friends.push_back(f);
    }

    ADUIThread::getInstance().runInCocos2dThread(ADRunnable([friends](){
        FacebookAndroid::getInstance()->friendsLoaded(friends);
    }));
}

}

