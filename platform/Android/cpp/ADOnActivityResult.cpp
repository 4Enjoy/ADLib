#include "ADOnActivityResult.h"
#define cCocos2dxActivity "org/cocos2dx/lib/Cocos2dxActivity"
#define cIntent "android/content/Intent"
void ADOnActivityResult::handleResult(JNIEnv*  env,
                  jobject thiz,
                  jint requestCode,
                  jint resultCode,
                  jobject data)
{

    for(auto call : _callbacks)
    {
        bool res = call(requestCode, resultCode, data);
        if(res)
		{
            LOGW("ADOnActivityResult: Callback found");
            return;
		}
    }

    static jclass cl_Cocos2dxActivity = ADJNI::GRef(env, env->FindClass(cCocos2dxActivity));
    static jmethodID m_Cocos2dxActivity_onActivityResult = env->GetMethodID(
                cl_Cocos2dxActivity, "onActivityResult",
                F(Void, Int Int J(cIntent)));

    env->CallNonvirtualVoidMethod(thiz, cl_Cocos2dxActivity, m_Cocos2dxActivity_onActivityResult,
                                  requestCode, resultCode, data);
    LOGW("ADOnActivityResult: default callback");
}


void ADOnActivityResult::addCallback(const ActivityResultCallback& callback)
{
    _callbacks.push_back(callback);
}

std::vector<ADOnActivityResult::ActivityResultCallback> ADOnActivityResult::_callbacks;
