#include <ADLib/Device/ADStatistics.h>
#include "../ADJNI.h"
#define cFlurryAgent "com/flurry/android/FlurryAgent"
#define cHashMap "java/util/HashMap"
#define cMap "java/util/Map"
#define cObject "java/lang/Object"
class ADStatisticsHelper
{
public:
    ADStatisticsHelper()
        : _application_key(nullptr),
          _activity(nullptr),
          _session_started(false)
    {

    }

    void setApplicationKey(const std::string& key)
    {
        JNIEnv* env = ADJNI::getEnv();
        if(_application_key)
        {
            env->DeleteGlobalRef(_application_key);
            _application_key = nullptr;
        }

        _application_key = ADJNI::GRef(env, env->NewStringUTF(key.c_str()));
    }

    void startSession()
    {
        if(!_application_key)
        {
            LOGE("No flurry key given");
        }
        else if(!_session_started)
        {
            _session_started = true;
            JNIEnv* env = ADJNI::getEnv();
            prepareActivity();

            static jclass cl_FlurryAgent = ADJNI::GRef(env, env->FindClass(cFlurryAgent));

            static jmethodID m_FlurryAgent_setContinueSessionMillis =
                    env->GetStaticMethodID(cl_FlurryAgent, "setContinueSessionMillis", F(Void, Long));

            static jmethodID m_FlurryAgent_onStartSession =
                    env->GetStaticMethodID(cl_FlurryAgent, "onStartSession", F(Void, J(cContext) J(cString)));
            jlong time_to_next_session_milis = 300*1000;
            env->CallStaticVoidMethod(cl_FlurryAgent, m_FlurryAgent_setContinueSessionMillis, time_to_next_session_milis);
            env->CallStaticVoidMethod(cl_FlurryAgent, m_FlurryAgent_onStartSession, _activity, _application_key);
            LOGD("Flurry: Session started");
        }
        else
        {
            LOGW("Flurry: session already started");
        }


    }

    void stopSession()
    {
        if(_session_started)
        {
            _session_started = false;
            JNIEnv* env = ADJNI::getEnv();
            prepareActivity();

            static jclass cl_FlurryAgent = ADJNI::GRef(env, env->FindClass(cFlurryAgent));
            static jmethodID m_FlurryAgent_onEndSession =
                    env->GetStaticMethodID(cl_FlurryAgent, "onEndSession", F(Void, J(cContext)));
            env->CallStaticVoidMethod(cl_FlurryAgent, m_FlurryAgent_onEndSession, _activity);
            LOGD("Flurry: Session stopped");
        }
        else
        {
            LOGW("Flurry: session not started");
        }
    }


    void logEvent(const std::string& name, const ADStatistics::Params& p)
    {
        typedef ADStatistics::Params Params;
        if(!_session_started)
        {
            LOGE("Flurry session not started yet");
        }

        LOGD("<Event '%s'", name.c_str());

        JNIEnv* env = ADJNI::getEnv();

        const Params::SVec& s_keys = p.getKeysString();
        const Params::SVec& s_values = p.getValuesString();

        static jclass cl_FlurryAgent = ADJNI::GRef(env, env->FindClass(cFlurryAgent));
        jstring name_j = env->NewStringUTF(name.c_str());

        if(s_keys.size() == 0)
        {
            //No params event
            static jmethodID m_FlurryAgent_logEvent1 = env->GetStaticMethodID(
                        cl_FlurryAgent, "logEvent", F(Void, J(cString)));
            env->CallStaticVoidMethod(cl_FlurryAgent, m_FlurryAgent_logEvent1, name_j);
        }
        else
        {
            //Even with params
            static jclass cl_HashMap = ADJNI::GRef(env, env->FindClass(cHashMap));
            static jmethodID m_HashMap_Constructor = env->GetMethodID(
                        cl_HashMap, "<init>", F(Void, Int));

            jobject map = env->NewObject(cl_HashMap,
                                         m_HashMap_Constructor,
                                         s_keys.size());

            static jmethodID m_HashMap_put = env->GetMethodID(
                        cl_HashMap, "put", F(J(cObject), J(cObject) J(cObject)));


            for(unsigned int i=0; i<s_keys.size(); ++i)
            {
                jstring key = env->NewStringUTF(s_keys[i].c_str());
                jstring value = env->NewStringUTF(s_values[i].c_str());

                LOGD("-Param '%s'='%s'", s_keys[i].c_str(), s_values[i].c_str());

                env->CallObjectMethod(map,
                                      m_HashMap_put,
                                      key, value);

                env->DeleteLocalRef(key);
                env->DeleteLocalRef(value);
            }

            static jmethodID m_FlurryAgent_logEvent2 = env->GetStaticMethodID(
                        cl_FlurryAgent, "logEvent", F(Void, J(cString) J(cMap)));

            env->CallStaticVoidMethod(cl_FlurryAgent,
                                      m_FlurryAgent_logEvent2,
                                      name_j,
                                      map);

            env->DeleteLocalRef(map);
        }

        env->DeleteLocalRef(name_j);
        LOGD("Event End>");
    }


    static ADStatisticsHelper& getInstance()
    {
        static ADStatisticsHelper obj;
        return obj;
    }
private:
    void prepareActivity()
    {
        if(_activity == nullptr)
        {
            JNIEnv* env = ADJNI::getEnv();
            _activity = ADJNI::GRef(env, ADJNI::getActivity(env));
        }
    }

    jstring _application_key;
    jobject _activity;
    bool _session_started;
};

void ADStatistics::startSession()
{
    ADStatisticsHelper::getInstance().startSession();
}

void ADStatistics::stopSession()
{
    ADStatisticsHelper::getInstance().stopSession();
}

void ADStatistics::setApplicationKey(const std::string &application_id)
{
    ADStatisticsHelper::getInstance().setApplicationKey(application_id);
}

void ADStatistics::logEvent(const std::string& name, const Params& p)
{
    ADStatisticsHelper::getInstance().logEvent(name, p);
}
