#include <ADLib/Device/ADAds.h>
#include <ADLib/Device/ADDeviceEvents.h>
#include <ADLib/Device/ADInfo.h>
#include "../ADJNI.h"
#include "ADUIThread.h"



#define cAdColony "com/jirbo/adcolony/AdColony"

using namespace cocos2d;
class AndroidAdColonyAdapter : public HasSlots
{
public:
    AndroidAdColonyAdapter()
    {
        CONNECT(ADAds::signalBeforeAdsInit,this, &AndroidAdColonyAdapter::init);
        CONNECT(ADDeviceEvents::signalOnPause, this, &AndroidAdColonyAdapter::onPause);
        CONNECT(ADDeviceEvents::signalOnResume, this, &AndroidAdColonyAdapter::onResume);
    }

    void init()
    {
        std::string zone_id = ADAds::getArgument("AdColony:zone_id");
        std::string app_id = ADAds::getArgument("AdColony:app_id");
        std::string store = "google";

        if(ADInfo::getStore() == ADStore::AmazonStore)
        {
            store = "amazon";
        }
        std::string options = "version:1.1,store:" + store;


//        CCLog("AdColony_init");
//        CCLog("Zone_id %s", zone_id.c_str());
//        CCLog("app_id %s", app_id.c_str());

//        CCLog("options %s", options.c_str());

        auto task = [=](){
            JNIEnv* env = ADJNI::getEnv();
            static jclass cl_AdColony = ADJNI::GRef(env, env->FindClass(cAdColony));

            static jmethodID m_AdColony_Configure = env->GetStaticMethodID(
                        cl_AdColony, "configure", F(Void , J(cActivity) J(cString) J(cString) Arr(J(cString)) ) );

            jobject activity = ADJNI::getActivity(env);

            jstring zone_id_j = env->NewStringUTF(zone_id.c_str());
            jstring app_id_j = env->NewStringUTF(app_id.c_str());
            jstring options_id_j = env->NewStringUTF(options.c_str());

            jobject zones = env->NewObjectArray(1,env->FindClass(cString),zone_id_j);

            env->CallStaticVoidMethod(cl_AdColony, m_AdColony_Configure, activity, options_id_j, app_id_j, zones);

            env->DeleteLocalRef(zones);
            env->DeleteLocalRef(zone_id_j);
            env->DeleteLocalRef(app_id_j);
            env->DeleteLocalRef(options_id_j);
            env->DeleteLocalRef(activity);
        };

        ADUIThread::getInstance().runInUIThread(ADRunnable(task));

    }

    void onResume()
    {
        CCLog("AdColony_onResume");

        auto task = [=](){
            JNIEnv* env = ADJNI::getEnv();
            static jclass cl_AdColony = ADJNI::GRef(env, env->FindClass(cAdColony));

            static jmethodID m_AdColony_onResume = env->GetStaticMethodID(
                        cl_AdColony, "resume", F(Void , J(cActivity) ) );

            static jobject activity = ADJNI::GRef(env, ADJNI::getActivity(env));


            env->CallStaticVoidMethod(cl_AdColony, m_AdColony_onResume, activity);
        };

        ADUIThread::getInstance().runInUIThread(ADRunnable(task));

        //static jobject activity = ADJNI::GRef(env, ADJNI::getActivity(env));

    }

    void onPause()
    {
        CCLog("AdColony_onPause");
        auto task = [=](){
            JNIEnv* env = ADJNI::getEnv();
            static jclass cl_AdColony = ADJNI::GRef(env, env->FindClass(cAdColony));

            static jmethodID m_AdColony_onPause = env->GetStaticMethodID(
                        cl_AdColony, "pause", F(Void , None ) );



            env->CallStaticVoidMethod(cl_AdColony, m_AdColony_onPause);
        };

        ADUIThread::getInstance().runInUIThread(ADRunnable(task));

    }


};

static AndroidAdColonyAdapter adapter;
