#include <ADLib/Device/ADVirtualCurrency.h>
#include "../ADJNI.h"
#include "ADUIThread.h"

#define cADVirtualCurrency "com/x4enjoy/ADLib/virtualcurrency/tapjoy/ADVirtualCurrency"

typedef ADVirtualCurrency::PlatformPtr PlatformPtr;

class TapJoyAndroid : public ADVirtualCurrency::Platform
{
public:
    void init(const std::string& key)
    {
        ADUIThread::getInstance().runInUIThread(ADRunnable([=](){
            do_init(key);
        }));
    }

    void showVirtualCurrencyShop()
    {
        ADUIThread::getInstance().runInUIThread(ADRunnable([=](){
            do_showVirtualCurrencyShop();
        }));
    }

    void onPause()
    {
        ADUIThread::getInstance().runInUIThread(ADRunnable([=](){
            do_onPause();
        }));
    }

    void onResume()
    {
        ADUIThread::getInstance().runInUIThread(ADRunnable([=](){
            do_onResume();
        }));
    }

    static PlatformPtr getInstance();
private:
    static PlatformPtr createInstance();
    static PlatformPtr _instance;
    void do_init(const std::string& key);
    void do_showVirtualCurrencyShop();
    void do_onPause();
    void do_onResume();
};

PlatformPtr ADVirtualCurrency::getImplementation()
{
    return TapJoyAndroid::getInstance();
}

PlatformPtr TapJoyAndroid::getInstance()
{
    return _instance;
}

PlatformPtr TapJoyAndroid::createInstance()
{
    PlatformPtr obj = std::make_shared<TapJoyAndroid>();
    return obj;
}

PlatformPtr TapJoyAndroid::_instance = createInstance();

void TapJoyAndroid::do_init(const std::string& key)
{
    std::string::size_type loc = key.find( "|", 0 );

    if( loc != std::string::npos )
    {
        std::string appID = key.substr(0, loc);
        std::string secret = key.substr(loc+1);

        cocos2d::CCLog(appID.c_str());
        cocos2d::CCLog(secret.c_str());

        JNIEnv* env = ADJNI::getEnv();
        jobject activity = ADJNI::getActivity(env);

        static jclass cl_ADVirtualCurrency = ADJNI::GRef(env, env->FindClass(cADVirtualCurrency));
        static jmethodID m_ADVirtualCurrency_init = env->GetStaticMethodID(
                    cl_ADVirtualCurrency, "init", F(Void, J(cActivity) J(cString) J(cString)));

        jstring appID_j = env->NewStringUTF(appID.c_str());
        jstring secret_j = env->NewStringUTF(secret.c_str());

        env->CallStaticVoidMethod(cl_ADVirtualCurrency, m_ADVirtualCurrency_init,
                                  activity, appID_j, secret_j);

        env->DeleteLocalRef(secret_j);
        env->DeleteLocalRef(appID_j);
        env->DeleteLocalRef(activity);
    }
}

void TapJoyAndroid::do_showVirtualCurrencyShop()
{
    JNIEnv* env = ADJNI::getEnv();

    static jclass cl_ADVirtualCurrency = ADJNI::GRef(env, env->FindClass(cADVirtualCurrency));
    static jmethodID m_ADVirtualCurrency_showOffers = env->GetStaticMethodID(
                cl_ADVirtualCurrency, "showOffers", F(Void, None));

    env->CallStaticVoidMethod(cl_ADVirtualCurrency, m_ADVirtualCurrency_showOffers);

}

void TapJoyAndroid::do_onPause()
{
    JNIEnv* env = ADJNI::getEnv();

    static jclass cl_ADVirtualCurrency = ADJNI::GRef(env, env->FindClass(cADVirtualCurrency));
    static jmethodID m_ADVirtualCurrency_onPause = env->GetStaticMethodID(
                cl_ADVirtualCurrency, "onPause", F(Void, None));

    env->CallStaticVoidMethod(cl_ADVirtualCurrency, m_ADVirtualCurrency_onPause);

}

void TapJoyAndroid::do_onResume()
{
    JNIEnv* env = ADJNI::getEnv();
    static jclass cl_ADVirtualCurrency = ADJNI::GRef(env, env->FindClass(cADVirtualCurrency));
    static jmethodID m_ADVirtualCurrency_onResume = env->GetStaticMethodID(
                cl_ADVirtualCurrency, "onResume", F(Void, None));


    env->CallStaticVoidMethod(cl_ADVirtualCurrency, m_ADVirtualCurrency_onResume);

}

extern "C"
{

void Java_com_x4enjoy_ADLib_virtualcurrency_tapjoy_ADVirtualCurrency_addCurrencyPoints(JNIEnv*  env, jclass, jint points)
{
    int p = points;
    ADUIThread::getInstance().runInCocos2dThread(ADRunnable([p](){
        TapJoyAndroid::getInstance()->addCurrency(p);
    }));
}

void Java_com_x4enjoy_ADLib_virtualcurrency_tapjoy_ADVirtualCurrency_nativeConnectSuccess(JNIEnv*  env, jclass)
{
    ADUIThread::getInstance().runInCocos2dThread(ADRunnable([](){
        TapJoyAndroid::getInstance()->setModuleIsSupported();
    }));
}
}

