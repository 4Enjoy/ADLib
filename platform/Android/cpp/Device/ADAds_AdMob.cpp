#include <ADLib/Device/ADAds.h>

#include <map>

#include "../ADJNI.h"
#include "ADUIThread.h"

#define cAdSize "com/google/android/gms/ads/AdSize"
#define cAdView "com/google/android/gms/ads/AdView"
#define cFrameLayout "android/widget/FrameLayout"
#define cView "android/view/View"
#define cAdRequest "com/google/android/gms/ads/AdRequest"
#define cAdRequestBuilder "com/google/android/gms/ads/AdRequest$Builder"
#define cInterstitialAd "com/google/android/gms/ads/InterstitialAd"
#define cLayoutParams "android/view/ViewGroup$LayoutParams"
#define RID "android/R$id"
#define cResources "android/content/res/Resources"
#define cDisplayMetrics "android/util/DisplayMetrics"
#define Build_Version "android/os/Build$VERSION"

#include <ADLib/Generic/ADMethods.h>
class AdMobHelper
{
public:
    typedef jobject AdSize;
    typedef jobject AdView;
    typedef jobject AdRequest;
    typedef jobject Activity;
    typedef jobject InterstitialAd;
    typedef jobject AdRequestBuilder;

    static AdMobHelper& getInstance();
    float getDensity();

    void addTestDevice(const ADAds::DeviceID& id);
    bool isBannerSupported();

    ///UI Thread Only
    AdSize getBannerSize(const ADAds::BannerType& type);
    ///UI Thread Only
    jstring getUnitID(const ADAds::UnitID& unit_id);
    ///UI Thread Only
    AdRequest createAdRequest();

    ///UI Thread Only
    void createBanner(const ADAds::BannerID id,
                      const ADAds::BannerType& type,
                      const ADAds::UnitID& unit_id);
    ///UI Thread Only
    void hideBanner(const ADAds::BannerID id);
    ///UI Thread Only
    void showBanner(const ADAds::BannerID id);
    ///UI Thread Only
    void isBannerReady(const ADAds::BannerID, const ADAds::Callback&);
    ///UI Thread Only
    void moveBanner(const ADAds::BannerID, const cocos2d::CCPoint& position);

    ///UI Thread Only
    void prepareInterstitial(const ADAds::UnitID& unit_id);

    ///UI Thread Only
    void showInterstitial();

    cocos2d::CCSize getBannerRealSize(const ADAds::BannerType& type);

    AD_Selector(unsigned int, _interstitial_showed, InterstitialShowed);
private:
    AdMobHelper();

    float _density;
    std::vector<jstring> _test_devices;


    std::map<ADAds::BannerType, AdSize> _banner_sizes;
    std::map<ADAds::UnitID, jstring> _unit_ids;


    static AdMobHelper* _instance;

    typedef std::map<ADAds::BannerID, jobject> ObjectMap;
    ObjectMap _banners_map;

    int _SDK_Version;
    static const int _Translation_SDKMin = 11;



    InterstitialAd _next_interstitial;
    unsigned int _interstitial_showed;

};
bool AdMobHelper::isBannerSupported()
{
    if(!_SDK_Version)
    {
        JNIEnv* env = ADJNI::getEnv();
        jclass cl_Build_Version = env->FindClass(Build_Version);
        jfieldID f_SDK_INT = env->GetStaticFieldID(cl_Build_Version, "SDK_INT", Int);
        _SDK_Version = env->GetStaticIntField(cl_Build_Version, f_SDK_INT);
        env->DeleteLocalRef(cl_Build_Version);
    }
    return _SDK_Version >= _Translation_SDKMin;
}
void AdMobHelper::prepareInterstitial(const ADAds::UnitID& unit_id)
{
    if(!_next_interstitial)
    {
        JNIEnv* env = ADJNI::getEnv();

        bool success = false;

        //Activity activity = this;
        static Activity activity = ADJNI::GRef(env, ADJNI::getActivity(env));



        static jclass cl_InterstitialAd = ADJNI::GRef(env, env->FindClass(cInterstitialAd));

        static jmethodID m_InterstitialAd_Contructor = env->GetMethodID(
                    cl_InterstitialAd, "<init>", F(Void , J(cContext) ) );
        static jmethodID m_InterstitialAd_setAdUnitId = env->GetMethodID(
                    cl_InterstitialAd, "setAdUnitId", F(Void , J(cString) ) );



        jstring ad_unit_id = getUnitID(unit_id);

        _next_interstitial = ADJNI::GRef(
                    env, env->NewObject(
                        cl_InterstitialAd,
                        m_InterstitialAd_Contructor,
                        activity));

        env->CallVoidMethod(_next_interstitial, m_InterstitialAd_setAdUnitId, ad_unit_id);
        ADJNI::clearExceptions(env);

        AdRequest request = createAdRequest();
        if(request)
        {

            static jmethodID m_InterstitialAd_loadAd = env->GetMethodID(
                        cl_InterstitialAd, "loadAd", F(Void, J(cAdRequest)));
            env->CallVoidMethod(_next_interstitial, m_InterstitialAd_loadAd, request);

            //Check whether everything is ok
            if(env->ExceptionOccurred())
            {
                env->ExceptionClear();
            }
            else
            {
                success = true;
            }

            env->DeleteLocalRef(request);
        }

        if(!success)
        {
            if(_next_interstitial)
                env->DeleteGlobalRef(_next_interstitial);
            _next_interstitial = nullptr;
        }
    }
}

void AdMobHelper::showInterstitial()
{
    if(_next_interstitial)
    {
        JNIEnv* env = ADJNI::getEnv();

        static jmethodID m_InterstitialAd_isLoaded = nullptr;
        static jmethodID m_InterstitialAd_show = nullptr;

        if(m_InterstitialAd_isLoaded == nullptr ||
                m_InterstitialAd_show == nullptr)
        {
            jclass cl_InterstitialAd = env->FindClass(cInterstitialAd);

            m_InterstitialAd_isLoaded = env->GetMethodID(cl_InterstitialAd,
                                                        "isLoaded",
                                                        F(Bool, None));
            m_InterstitialAd_show = env->GetMethodID(cl_InterstitialAd,
                                                     "show",
                                                     F(Void, None));
            env->DeleteLocalRef(cl_InterstitialAd);
        }

        bool is_ready = env->CallBooleanMethod(_next_interstitial,
                                               m_InterstitialAd_isLoaded);

        if(env->ExceptionOccurred())
            env->ExceptionClear();
        else
        {
            if(is_ready)
            {
                //Show
                env->CallVoidMethod(_next_interstitial, m_InterstitialAd_show);
                ADJNI::clearExceptions(env);

                _interstitial_showed++;
                //Remove ptr
                env->DeleteGlobalRef(_next_interstitial);
                _next_interstitial = nullptr;

                ADJNI::clearExceptions(env);
            }
        }
    }
}

void AdMobHelper::createBanner(const ADAds::BannerID id,
                               const ADAds::BannerType &type,
                               const ADAds::UnitID &unit_id)
{
    if(_banners_map.find(id) != _banners_map.end())
    {
        LOGD("ADAds Admob: Banner with id=%d exists", id);
        return;
    }

    JNIEnv* env = ADJNI::getEnv();

    //Activity activity = this;
    static Activity activity = ADJNI::GRef(env, ADJNI::getActivity(env));

    //import AdView;
    static jclass cl_AdView = ADJNI::GRef(env, env->FindClass(cAdView));

    //AdView banner = new AdView(activity, ad_size, ad_unit_id);
    AdView banner = nullptr;
    {
        static jmethodID m_AdView_Contructor = env->GetMethodID(cl_AdView, "<init>", F(Void , J(cContext)) );
        static jmethodID m_AdView_setAdUnitId = env->GetMethodID(cl_AdView, "setAdUnitId", F(Void,  J(cString)));
        static jmethodID m_AdView_setAdSize = env->GetMethodID(cl_AdView, "setAdSize", F(Void,  J(cAdSize)));


        //
        jstring ad_unit_id = getUnitID(unit_id);
        AdSize ad_size = getBannerSize(type);
        banner = ADJNI::GRef(env,
                             env->NewObject(
                                 cl_AdView, m_AdView_Contructor,
                                 activity));
        ADJNI::clearExceptions(env);
        env->CallVoidMethod(banner, m_AdView_setAdUnitId, ad_unit_id);
        ADJNI::clearExceptions(env);
        env->CallVoidMethod(banner, m_AdView_setAdSize, ad_size);
        ADJNI::clearExceptions(env);


        //env->DeleteLocalRef(ad_unit_id);
        //env->DeleteLocalRef(ad_size);
    }

    typedef jobject ViewGroup_LayoutParams;

    //ViewGroup.LayoutParams ad_layout_params = new ViewGroup.LayoutParams( ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    static ViewGroup_LayoutParams ad_layout_params = nullptr;
    if(!ad_layout_params)
    {
        jclass cl_LayoutParams = env->FindClass(cLayoutParams);

        jmethodID cl_LayoutParams_Constructor = env->GetMethodID(
                    cl_LayoutParams, "<init>", F(Void, Int Int));
        ADJNI::clearExceptions(env);

        jfieldID f_LayoutParams_FILL_PARENT = env->GetStaticFieldID(
                    cl_LayoutParams, "FILL_PARENT", Int);
        jfieldID f_LayoutParams_WRAP_CONTENT = env->GetStaticFieldID(
                    cl_LayoutParams, "WRAP_CONTENT", Int);

        ADJNI::clearExceptions(env);

        jint FILL_PARENT = env->GetStaticIntField(
                    cl_LayoutParams, f_LayoutParams_FILL_PARENT);
        jint WRAP_CONTENT = env->GetStaticIntField(
                    cl_LayoutParams, f_LayoutParams_WRAP_CONTENT);

        ad_layout_params = ADJNI::GRef(env, env->NewObject(
                                           cl_LayoutParams,
                                           cl_LayoutParams_Constructor,
                                           FILL_PARENT,
                                           WRAP_CONTENT));

        ADJNI::clearExceptions(env);
        env->DeleteLocalRef(cl_LayoutParams);
    }

    //banner.setLayoutParams(ad_layout_params);
    static jmethodID m_AdView_setLayoutParams = env->GetMethodID(
                cl_AdView, "setLayoutParams", F(Void, J(cLayoutParams)));
    env->CallVoidMethod(banner, m_AdView_setLayoutParams, ad_layout_params);

    ADJNI::clearExceptions(env);

    //banner.loadAd(ad_request);
    AdRequest ad_request = createAdRequest();
    static jmethodID m_AdView_loadAd = env->GetMethodID(
                cl_AdView, "loadAd", F(Void, J(cAdRequest)));
    env->CallVoidMethod(banner, m_AdView_loadAd, ad_request);

    ADJNI::clearExceptions(env);

    env->DeleteLocalRef(ad_request);

    typedef jobject FrameLayout;
    //FrameLayout main_layout = activity.findViewById(R.id.content);
    static FrameLayout main_layout = nullptr;
    if(!main_layout)
    {
        jclass cl_R_id = env->FindClass(RID);
        jfieldID f_R_id_content = env->GetStaticFieldID(cl_R_id, "content", Int);
        jint R_id_content = env->GetStaticIntField(cl_R_id, f_R_id_content);
        env->DeleteLocalRef(cl_R_id);

        jclass cl_Activity = env->FindClass(cActivity);
        jmethodID m_Activity_findViewById = env->GetMethodID(
                    cl_Activity, "findViewById", F(J(cView), Int));
        main_layout = ADJNI::GRef(env, env->CallObjectMethod(
                                      activity,
                                      m_Activity_findViewById,
                                      R_id_content));
        env->DeleteLocalRef(cl_Activity);

        ADJNI::clearExceptions(env);
    }

    cocos2d::CCSize banner_size = getBannerRealSize(type);
    int banner_width = banner_size.width + 0.5f;
    int banner_height = banner_size.height + 0.5f;
    //framelayout.addView(banner, banner_width, banner_height);
    static jmethodID m_FrameLayout_addView = nullptr;
    if(!m_FrameLayout_addView)
    {
        jclass cl_FrameLayout = env->FindClass(cFrameLayout);
        m_FrameLayout_addView = env->GetMethodID(cl_FrameLayout, "addView", F(Void, J(cView) Int Int));
        env->DeleteLocalRef(cl_FrameLayout);
    }
    env->CallVoidMethod(main_layout, m_FrameLayout_addView,
                        banner,
                        banner_width,
                        banner_height);
    ADJNI::clearExceptions(env);

    LOGD("ADAds Admob: Banner added id=%d", id);
    _banners_map[id] = banner;
    this->hideBanner(id);

}
jstring AdMobHelper::getUnitID(const ADAds::UnitID& unit_id)
{
    auto it = _unit_ids.find(unit_id);

    //Read from cache
    if(it != _unit_ids.end())
        return it->second;

    JNIEnv* env = ADJNI::getEnv();
    jstring id = ADJNI::GRef(env, env->NewStringUTF(unit_id.c_str()));

    _unit_ids[unit_id] = id;
    return id;
}
AdMobHelper::AdRequest AdMobHelper::createAdRequest()
{
    JNIEnv* env = ADJNI::getEnv();

    //static jclass cl_AdRequest = ADJNI::GRef(env, env->FindClass(cAdRequest));
    static jclass cl_AdRequestBuilder = ADJNI::GRef(env, env->FindClass(cAdRequestBuilder));

    static jmethodID m_AdRequestBuilder_Construct = env->GetMethodID(
                cl_AdRequestBuilder, "<init>", F(Void, None));
    static jmethodID m_AdRequestBuilder_addTestDevice = env->GetMethodID(
                cl_AdRequestBuilder, "addTestDevice", F(J(cAdRequestBuilder), J(cString)));
    static jmethodID m_AdRequestBuilder_build = env->GetMethodID(
                cl_AdRequestBuilder, "build", F(J(cAdRequest), None));


    AdRequestBuilder request = env->NewObject(cl_AdRequestBuilder, m_AdRequestBuilder_Construct);

    for(unsigned int i=0; i<_test_devices.size(); ++i)
    {
        env->CallObjectMethod(request, m_AdRequestBuilder_addTestDevice, _test_devices[i]);
        ADJNI::clearExceptions(env);
    }

    AdRequest result = env->CallObjectMethod(request, m_AdRequestBuilder_build);
    env->DeleteLocalRef(request);
    return result;
}

AdMobHelper::AdSize AdMobHelper::getBannerSize(const ADAds::BannerType& typet)
{
    ADAds::BannerType type = typet;
    auto it = _banner_sizes.find(type);

    //Read from cache
    if(it != _banner_sizes.end())
        return it->second;

    JNIEnv* env = ADJNI::getEnv();
    jclass cl_AdSize = env->FindClass(cAdSize);

    if(type == "IAB_BANNER")
    {
        type = "FULL_BANNER";
    }
    else if(type == "IAB_MRECT")
    {
        type = "MEDIUM_RECTANGLE";
    }
    else if(type == "IAP_LEADERBOARD")
    {
        type = "LEADERBOARD";
    }

    jfieldID f_AdSize_type = env->GetStaticFieldID(cl_AdSize, type.c_str(), J(cAdSize));

    AdSize res = ADJNI::GRef(env, env->GetStaticObjectField(cl_AdSize, f_AdSize_type));
    env->DeleteLocalRef(cl_AdSize);

    _banner_sizes[type] = res;
    return res;
}

void AdMobHelper::hideBanner(const ADAds::BannerID id)
{
    ObjectMap::iterator it = _banners_map.find(id);
    if(it == _banners_map.end())
    {
        LOGD("ADAds Admob: Banner with id=%d not found", id);
        return;
    }

    AdView banner = it->second;
    JNIEnv* env = ADJNI::getEnv();

    //banner.setVisibility(View.GONE);
    static int View_GONE = -1;
    static jmethodID m_AdView_setVisibility = nullptr;

    if(View_GONE < 0 || m_AdView_setVisibility == nullptr)
    {
        jclass cl_AdView = env->FindClass(cAdView);

        m_AdView_setVisibility = env->GetMethodID(cl_AdView, "setVisibility", F(Void, Int));

        jclass cl_View = env->FindClass(cView);
        jfieldID f_View_GONE = env->GetStaticFieldID(cl_View, "GONE", Int);
        View_GONE =  env->GetStaticIntField(cl_View, f_View_GONE);

        env->DeleteLocalRef(cl_AdView);
        env->DeleteLocalRef(cl_View);
    }
    env->CallVoidMethod(banner, m_AdView_setVisibility, View_GONE);
    ADJNI::clearExceptions(env);
}

void AdMobHelper::showBanner(const ADAds::BannerID id)
{
    ObjectMap::iterator it = _banners_map.find(id);
    if(it == _banners_map.end())
    {
        LOGD("ADAds Admob: Banner with id=%d not found", id);
        return;
    }

    AdView banner = it->second;
    JNIEnv* env = ADJNI::getEnv();

    //banner.setVisibility(View.VISIBLE);
    static int View_VISIBLE = -1;
    static jmethodID m_AdView_setVisibility = nullptr;

    if(View_VISIBLE < 0 || m_AdView_setVisibility == nullptr)
    {
        jclass cl_AdView = env->FindClass(cAdView);

        m_AdView_setVisibility = env->GetMethodID(cl_AdView, "setVisibility", F(Void, Int));

        jclass cl_View = env->FindClass(cView);
        jfieldID f_adView_VISIBLE = env->GetStaticFieldID(cl_View, "VISIBLE", Int);
        View_VISIBLE =  env->GetStaticIntField(cl_View, f_adView_VISIBLE);

        env->DeleteLocalRef(cl_AdView);
        env->DeleteLocalRef(cl_View);
    }

    env->CallVoidMethod(banner, m_AdView_setVisibility, View_VISIBLE);
    ADJNI::clearExceptions(env);
}

void AdMobHelper::moveBanner(const ADAds::BannerID id, const cocos2d::CCPoint& position)
{
    ObjectMap::iterator it = _banners_map.find(id);
    if(it == _banners_map.end())
    {
        LOGD("ADAds Admob: Banner with id=%d not found", id);
        return;
    }

    AdView banner = it->second;
    JNIEnv* env = ADJNI::getEnv();

    static jmethodID m_AdView_setTranslationX = nullptr;
    static jmethodID m_AdView_setTranslationY = nullptr;

    if(m_AdView_setTranslationX == nullptr ||
            m_AdView_setTranslationY == nullptr)
    {
        jclass cl_AdView = env->FindClass(cAdView);
        m_AdView_setTranslationX = env->GetMethodID(
                    cl_AdView, "setTranslationX", F(Void, Float));
        m_AdView_setTranslationY = env->GetMethodID(
                    cl_AdView, "setTranslationY", F(Void, Float));
        env->DeleteLocalRef(cl_AdView);
    }

    //banner.setTranslationX(position.x);
    env->CallVoidMethod(banner, m_AdView_setTranslationX, position.x);
    ADJNI::clearExceptions(env);

    //banner.setTranslationY(position.y);
    env->CallVoidMethod(banner, m_AdView_setTranslationY, position.y);
    ADJNI::clearExceptions(env);
}

void AdMobHelper::isBannerReady(const ADAds::BannerID id, const ADAds::Callback& callback)
{
    bool result = true;
//TODO: implement for new ADMob SDK
//    ObjectMap::iterator it = _banners_map.find(id);
//    if(it != _banners_map.end())
//    {
//        AdView banner = it->second;
//        JNIEnv* env = ADJNI::getEnv();

//        static jmethodID m_AdView_isReady = nullptr;
//        if(!m_AdView_isReady)
//        {
//            jclass cl_AdView = env->FindClass(cAdView);
//            m_AdView_isReady = env->GetMethodID(cl_AdView, "isReady", F(Bool, None));
//            env->DeleteLocalRef(cl_AdView);
//        }


//        result = env->CallBooleanMethod(banner, m_AdView_isReady);
//        ADJNI::clearExceptions(env);
//    }

    ADUIThread::getInstance().runInCocos2dThread(ADRunnable([callback, result, id]()
    {
        callback(result, id);
    }));

}

float AdMobHelper::getDensity()
{
    if(_density == 0)
    {
        JNIEnv* env = ADJNI::getEnv();

        //Activity activity = getActivity();
        jobject activity = ADJNI::getActivity(env);

        //Resources res = activity.getResources();
        jobject res = nullptr;
        {
            jclass cl_Activity = env->FindClass(cActivity);
            jmethodID m_getResources = env->GetMethodID(
                        cl_Activity, "getResources", F(J(cResources), None));

            res = env->CallObjectMethod(activity, m_getResources);
            env->DeleteLocalRef(cl_Activity);
        }

        //DisplayMetrics metrics = res.getDisplayMetrics();
        jobject metrics = nullptr;
        {
            jclass cl_Resources = env->FindClass(cResources);
            jmethodID m_getDisplayMetrics = env->GetMethodID(
                        cl_Resources, "getDisplayMetrics", F(J(cDisplayMetrics), None));

            metrics = env->CallObjectMethod(res, m_getDisplayMetrics);

            env->DeleteLocalRef(cl_Resources);
        }

        //float density = metrics.density;
        float density = 0;
        {
            jclass cl_DisplayMetrics = env->FindClass(cDisplayMetrics);
            jfieldID f_density = env->GetFieldID(cl_DisplayMetrics, "density", Float);
            density = env->GetFloatField(metrics, f_density);

            env->DeleteLocalRef(cl_DisplayMetrics);
        }

        env->DeleteLocalRef(activity);
        env->DeleteLocalRef(res);
        env->DeleteLocalRef(metrics);

        _density = density;
    }
    return _density;
}

cocos2d::CCSize ADAds::Platform::getBannerSize(const BannerType& type)
{
    return AdMobHelper::getInstance().getBannerRealSize(type);
}

cocos2d::CCSize AdMobHelper::getBannerRealSize(const ADAds::BannerType& type)
{
    cocos2d::CCSize base(10000,10000);
    float density = getDensity();

    if(type == "BANNER")
    {
        base.width = 320;
        base.height = 50;
    }
    else if(type == "IAB_MRECT")
    {
        base.width = 300;
        base.height = 250;
    }
    else if(type == "IAB_BANNER")
    {
        base.width = 468;
        base.height = 60;
    }
    else if(type == "IAB_LEADERBOARD")
    {
        base.width = 728;
        base.height = 90;
    }

    base.width *= density;
    base.height *= density;
    return base;
}

void ADAds::Platform::addTestDevice(const DeviceID& device)
{
    AdMobHelper::getInstance().addTestDevice(device);
}

void AdMobHelper::addTestDevice(const ADAds::DeviceID& id)
{
    JNIEnv* env = ADJNI::getEnv();
    jstring global_str = ADJNI::GRef(env, env->NewStringUTF(id.c_str()));
    _test_devices.push_back(global_str);
}

bool ADAds::Platform::createBanner(const BannerID id,
                                   const BannerType& type,
                                   const UnitID& unit_id)
{
    if(!AdMobHelper::getInstance().isBannerSupported())
        return false;

    ADUIThread::getInstance().runInUIThread(ADRunnable([id, type, unit_id](){
        AdMobHelper::getInstance().createBanner(id, type, unit_id);
    }));
}

AdMobHelper* AdMobHelper::_instance = 0;

AdMobHelper& AdMobHelper::getInstance()
{
    if(_instance == 0)
    {
        _instance = new AdMobHelper;
    }
    return *_instance;
}

AdMobHelper::AdMobHelper()
    : _density(0), _SDK_Version(0), _next_interstitial(nullptr), _interstitial_showed(0)
{
}


void ADAds::Platform::showBanner(const BannerID id)
{
    ADUIThread::getInstance().runInUIThread(ADRunnable([id](){
        AdMobHelper::getInstance().showBanner(id);
    }));
}

void ADAds::Platform::hideBanner(const BannerID id)
{
    ADUIThread::getInstance().runInUIThread(ADRunnable([id](){
        AdMobHelper::getInstance().hideBanner(id);
    }));
}


void ADAds::Platform::isBannerReady(const BannerID id, const Callback& callback)
{
    ADUIThread::getInstance().runInUIThread(ADRunnable([id, callback](){
        AdMobHelper::getInstance().isBannerReady(id, callback);
    }));
}

void ADAds::Platform::moveBanner(const BannerID id, const cocos2d::CCPoint& position)
{
    ADUIThread::getInstance().runInUIThread(ADRunnable([id, position](){
        AdMobHelper::getInstance().moveBanner(id, position);
    }));
}

void ADAds::Platform::showInterstitial()
{
    ADUIThread::getInstance().runInUIThread(ADRunnable([](){
        AdMobHelper::getInstance().showInterstitial();
    }));
}

void ADAds::Platform::prepareInterstitial(const UnitID & id)
{
    ADUIThread::getInstance().runInUIThread(ADRunnable([id](){
        AdMobHelper::getInstance().prepareInterstitial(id);
    }));
}

unsigned int ADAds::Platform::getInterstialTimesShowed()
{
    return AdMobHelper::getInstance().getInterstitialShowed();
}
