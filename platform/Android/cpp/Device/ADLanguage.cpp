#include <ADLib/Device/ADLanguage.h>
#include "../ADJNI.h"

#define cLocale "java/util/Locale"

std::string ADLanguage::platformGetDeviceLanguage()
{
    JNIEnv* env = ADJNI::getEnv();

    jclass cl_Locale = env->FindClass(cLocale);
    jmethodID m_Locale_getDefault = env->GetStaticMethodID(
                cl_Locale, "getDefault", F(J(cLocale), None));

    jobject locale = env->CallStaticObjectMethod(cl_Locale,
                                                 m_Locale_getDefault);

    jmethodID m_Locale_getLanguage = env->GetMethodID(
                cl_Locale, "getLanguage", F(J(cString), None));
    jstring lang = (jstring)env->CallObjectMethod(locale, m_Locale_getLanguage);
    //String lang = Locale.getDefault().getLanguage();

    std::string res = ADJNI::toString(env, lang);
    env->DeleteLocalRef(cl_Locale);
    env->DeleteLocalRef(locale);
    env->DeleteLocalRef(lang);

    if(res.size() < 2)
        res = ADLanguage::getDefaultLanguage();
    if(res.size() > 2)
        res = res.substr(0, 2);
    return res;
}

