#include <ADLib/Device/ADBrowser.h>
#include "../ADJNI.h"

#define cIntent "android/content/Intent"
#define cUri "android/net/Uri"
class ADBrowserHelper
{
public:
    typedef ADBrowser::Fallback Fallback;
    ADBrowserHelper()
        : _activity(nullptr)
    {

    }
    static ADBrowserHelper& getInstance()
    {
        static ADBrowserHelper obj;
        return obj;
    }

    void openWebURL(const std::string& url, const Fallback& fallback)
    {
        JNIEnv* env = ADJNI::getEnv();

        static jclass cl_Intent = ADJNI::GRef(env, env->FindClass(cIntent));
        static jmethodID m_Intent_Constructor = env->GetMethodID(cl_Intent, "<init>", F(Void, J(cString) J(cUri)));
        static jmethodID m_Intent_addFlags = env->GetMethodID(cl_Intent, "addFlags", F(J(cIntent), Int));

        static jclass cl_Uri = ADJNI::GRef(env, env->FindClass(cUri));
        static jmethodID m_Uri_parse = env->GetStaticMethodID(cl_Uri, "parse", F(J(cUri), J(cString)));

        static int Intent_FLAG_ACTIVITY_NEW_TASK = get_Intent_FLAG_ACTIVITY_NEW_TASK(env, cl_Intent);

        static jobject Intent_ACTION_VIEW = ADJNI::GRef(
                    env,
                    getIntentStaticStringField(env, cl_Intent, "ACTION_VIEW"));

        jstring url_j = env->NewStringUTF(url.c_str());
        jobject uri = env->CallStaticObjectMethod(cl_Uri, m_Uri_parse, url_j);


        jobject intent = env->NewObject(
                    cl_Intent, m_Intent_Constructor,
                    Intent_ACTION_VIEW, uri);


        env->CallObjectMethod(intent,
                              m_Intent_addFlags,
                              Intent_FLAG_ACTIVITY_NEW_TASK);

        startActivity(env, intent, fallback);

        env->DeleteLocalRef(url_j);
        env->DeleteLocalRef(uri);
        env->DeleteLocalRef(intent);
    }
    void sendMail(const std::string& email, const std::string &subject, const Fallback& fallback)
    {
        JNIEnv* env = ADJNI::getEnv();

        static jclass cl_Intent = ADJNI::GRef(env, env->FindClass(cIntent));
        static jmethodID m_Intent_Constructor = env->GetMethodID(cl_Intent, "<init>", F(Void, J(cString)));
        static jobject Intent_ACTION_SEND = ADJNI::GRef(
                    env,
                    getIntentStaticStringField(env, cl_Intent, "ACTION_SEND"));

        //    Intent i = new Intent(Intent.ACTION_SEND);
        jobject intent = env->NewObject(
                    cl_Intent, m_Intent_Constructor, Intent_ACTION_SEND);

        static jmethodID m_Intent_addFlags = env->GetMethodID(cl_Intent, "addFlags", F(J(cIntent), Int));
        static int Intent_FLAG_ACTIVITY_NEW_TASK = get_Intent_FLAG_ACTIVITY_NEW_TASK(env, cl_Intent);

        //    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        env->CallObjectMethod(intent,
                              m_Intent_addFlags,
                              Intent_FLAG_ACTIVITY_NEW_TASK);

        //    i.setType("message/rfc822");
        static jobject message_type = ADJNI::GRef(env, env->NewStringUTF("message/rfc822"));
        static jmethodID m_Intent_setType = env->GetMethodID(
                    cl_Intent, "setType", F(J(cIntent), J(cString)));

        env->CallObjectMethod(intent, m_Intent_setType, message_type);


        //    i.putExtra(Intent.EXTRA_EMAIL  , new String[]{mailTo});
        static jobject Intent_EXTRA_EMAIL = ADJNI::GRef(
                    env,
                    getIntentStaticStringField(env, cl_Intent, "EXTRA_EMAIL"));
        static jmethodID m_Intent_putExtra_StringArr = env->GetMethodID(
                    cl_Intent, "putExtra", F(J(cIntent), J(cString) Arr(J(cString))));

        jstring email_j = env->NewStringUTF(email.c_str());
        static jclass cl_String = ADJNI::GRef(env, env->FindClass(cString));
        jobjectArray email_array = env->NewObjectArray(1, cl_String, email_j);

        env->CallObjectMethod(intent, m_Intent_putExtra_StringArr,
                              Intent_EXTRA_EMAIL,
                              email_array);


        //    i.putExtra(Intent.EXTRA_SUBJECT, "");
        static jobject Intent_EXTRA_SUBJECT = ADJNI::GRef(
                    env,
                    getIntentStaticStringField(env, cl_Intent, "EXTRA_SUBJECT"));
        static jmethodID m_Intent_putExtra_String = env->GetMethodID(
                    cl_Intent, "putExtra", F(J(cIntent), J(cString) J(cString)));
        jstring subject_j = env->NewStringUTF(subject.c_str());
        env->CallObjectMethod(intent, m_Intent_putExtra_String,
                              Intent_EXTRA_SUBJECT, subject_j);

        startActivity(env, intent, fallback);

        env->DeleteLocalRef(intent);
        env->DeleteLocalRef(email_j);
        env->DeleteLocalRef(email_array);
        env->DeleteLocalRef(subject_j);
    }

private:
    typedef jobject Intent;

    int get_Intent_FLAG_ACTIVITY_NEW_TASK(JNIEnv* env, jclass cl_Intent)
    {
        static int result = -1;
        if(result == -1)
        {
            jfieldID f_Intent_FLAG_ACTIVITY_NEW_TASK = env->GetStaticFieldID(
                        cl_Intent, "FLAG_ACTIVITY_NEW_TASK", Int);
            result = env->GetStaticIntField(cl_Intent, f_Intent_FLAG_ACTIVITY_NEW_TASK);
        }
        return result;
    }

    jobject getIntentStaticStringField(JNIEnv* env, jclass cl_Intent, const char* field_name)
    {
        jfieldID f_Intent_FIELD = env->GetStaticFieldID(
                    cl_Intent, field_name, J(cString));

        return env->GetStaticObjectField(cl_Intent, f_Intent_FIELD);
    }

    void prepareActivity(JNIEnv* env)
    {
        if(_activity == nullptr)
        {
            _activity = ADJNI::GRef(env, ADJNI::getActivity(env));
        }
    }

    void startActivity(JNIEnv* env,
                       Intent intent,
                       const Fallback& fallback)
    {
        static jmethodID m_Context_startActivity = nullptr;
        if(!m_Context_startActivity)
        {
            jclass cl_Context = env->FindClass(cContext);
            m_Context_startActivity = env->GetMethodID(
                        cl_Context, "startActivity", F(Void, J(cIntent))
                        );
            env->DeleteLocalRef(cl_Context);
        }

        prepareActivity(env);
        env->CallVoidMethod(_activity, m_Context_startActivity, intent);

        if(env->ExceptionCheck())
        {
            env->ExceptionDescribe();
            env->ExceptionClear();

            if(fallback)
                fallback();
        }
    }

    jobject _activity;
};

#include "ADUIThread.h"

void ADBrowser::openWebURL(const std::string& url, const Fallback & fallback)
{
    ADUIThread::getInstance().runInUIThread(ADRunnable([=](){
        ADBrowserHelper::getInstance().openWebURL(url, fallback);
    }));
}
void ADBrowser::openOSUrl(const std::string& url, const Fallback& fallback)
{
    ADUIThread::getInstance().runInUIThread(ADRunnable([=](){
        ADBrowserHelper::getInstance().openWebURL(url, fallback);
    }));
}
void ADBrowser::sendMail(const std::string& email, const std::string& subject, const Fallback& fallback)
{
    ADUIThread::getInstance().runInUIThread(ADRunnable([=](){
        ADBrowserHelper::getInstance().sendMail(email, subject, fallback);
    }));
}
