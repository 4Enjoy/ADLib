#include <ADLib/Device/ADAds.h>

#include <map>

#include "../ADJNI.h"
#include "ADUIThread.h"
#include "HeyZap.h"

#include <ADLib/Generic/ADMethods.h>

#define cHeyzapAds "com/heyzap/sdk/ads/HeyzapAds"
#define cInterstitialAd "com/heyzap/sdk/ads/InterstitialAd"
#define cBoolean "java/lang/Boolean"
using namespace cocos2d;
class HeyZapHelper
{
public:

    bool readBoolean(JNIEnv* env, jobject obj);
    static HeyZapHelper& getInstance();

    ///UI Thread Only
    void init(const ADAds::UnitID& unit_id);

    ///UI Thread Only
    void prepareInterstitial(const ADAds::UnitID& unit_id);

    ///UI Thread Only
    void showInterstitial();

    AD_Selector(unsigned int, _interstitial_showed, InterstitialShowed);
private:
    HeyZapHelper();
    static HeyZapHelper* _instance;

    bool _is_inited;
    unsigned int _interstitial_showed;

};
void HeyZapHelper::init(const ADAds::UnitID& unit_id)
{
    if(!_is_inited)
    {
        _is_inited = true;
        JNIEnv* env = ADJNI::getEnv();

        //Activity activity = this;
        jobject activity = ADJNI::getActivity(env);

        jclass HeyzapAds = env->FindClass(cHeyzapAds);
        jmethodID HeyzapAds_start = env->GetStaticMethodID(HeyzapAds, "start", F(Void , J(cString) J(cActivity)));

        jstring appid = env->NewStringUTF(unit_id.c_str());

        env->CallStaticVoidMethod(HeyzapAds, HeyzapAds_start, appid, activity);
        ADJNI::clearExceptions(env);

        env->DeleteLocalRef(appid);
        env->DeleteLocalRef(HeyzapAds);
        env->DeleteLocalRef(activity);

        emit HeyZap::getInstance()->signalHeyZapReady();

    }
}

void HeyZapHelper::prepareInterstitial(const ADAds::UnitID& unit_id)
{
    init(unit_id);

    JNIEnv* env = ADJNI::getEnv();

    static jclass InterstitialAd = ADJNI::GRef(env, env->FindClass(cInterstitialAd));

    static jmethodID InterstitialAd_fetch = env->GetStaticMethodID(
                InterstitialAd, "fetch", F(Void , None ) );

    env->CallStaticVoidMethod(InterstitialAd, InterstitialAd_fetch);
    ADJNI::clearExceptions(env);
}

bool HeyZapHelper::readBoolean(JNIEnv* env, jobject obj)
{
    static jclass Boolean = ADJNI::GRef(env, env->FindClass(cBoolean));
    static jmethodID booleanValueMID   = env->GetMethodID(Boolean, "booleanValue", "()Z");
    bool booleanValue           = (bool) env->CallBooleanMethod(obj, booleanValueMID);
    return booleanValue;
}

void HeyZapHelper::showInterstitial()
{
    JNIEnv* env = ADJNI::getEnv();

    static jclass InterstitialAd = ADJNI::GRef(env, env->FindClass(cInterstitialAd));

    static jmethodID InterstitialAd_display = env->GetStaticMethodID(
                InterstitialAd, "display", F(Void , J(cActivity) ) );
    static jmethodID InterstitialAd_isAvailable = env->GetStaticMethodID(
                InterstitialAd, "isAvailable", F(J(cBoolean) , None ) );

    jobject boolean = env->CallStaticObjectMethod(InterstitialAd, InterstitialAd_isAvailable);
    if(readBoolean(env, boolean))
    {
        jobject activity = ADJNI::getActivity(env);
        env->CallStaticVoidMethod(InterstitialAd, InterstitialAd_display, activity);
        ADJNI::clearExceptions(env);
        env->DeleteLocalRef(activity);
        _interstitial_showed++;

        prepareInterstitial("");
    }
    env->DeleteLocalRef(boolean);

}


cocos2d::CCSize ADAds::Platform::getBannerSize(const BannerType& type)
{
    return cocos2d::CCSize(1,1);
}


void ADAds::Platform::addTestDevice(const DeviceID& device)
{
    CCLog("Test devices not supported");
}



bool ADAds::Platform::createBanner(const BannerID id,
                                   const BannerType& type,
                                   const UnitID& unit_id)
{
    return false;

}

HeyZapHelper* HeyZapHelper::_instance = 0;

HeyZapHelper& HeyZapHelper::getInstance()
{
    if(_instance == 0)
    {
        _instance = new HeyZapHelper;
    }
    return *_instance;
}

HeyZapHelper::HeyZapHelper()
    : _interstitial_showed(0), _is_inited(false)
{
}


void ADAds::Platform::showBanner(const BannerID id)
{
    CCLog("Banners not supported");
}

void ADAds::Platform::hideBanner(const BannerID id)
{
    CCLog("Banners not supported");
}


void ADAds::Platform::isBannerReady(const BannerID id, const Callback& callback)
{
    CCLog("Banners not supported");
}

void ADAds::Platform::moveBanner(const BannerID id, const cocos2d::CCPoint& position)
{

    CCLog("Banners not supported");
}

void ADAds::Platform::showInterstitial()
{
    ADUIThread::getInstance().runInUIThread(ADRunnable([](){
        HeyZapHelper::getInstance().showInterstitial();
    }));
}

void ADAds::Platform::prepareInterstitial(const UnitID & id)
{
    ADUIThread::getInstance().runInUIThread(ADRunnable([id](){
        HeyZapHelper::getInstance().prepareInterstitial(id);
    }));
}

unsigned int ADAds::Platform::getInterstialTimesShowed()
{
    return HeyZapHelper::getInstance().getInterstitialShowed();
}
