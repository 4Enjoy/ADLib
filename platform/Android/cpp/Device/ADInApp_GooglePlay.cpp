#include <ADLib/Device/ADInApp.h>
#include "../ADJNI.h"
#include "../ADOnActivityResult.h"
#include "ADLib/Device/ADNotification.h"
#include "ADUIThread.h"

#define cADInApp "com/x4enjoy/ADLib/inapp/google/ADInApp"
#define cIntent "android/content/Intent"
#define cArrayList "java/util/ArrayList"
#define cObject "java/lang/Object"

void do_buyProduct(const ADInApp::Product& p)
{
    JNIEnv* env = ADJNI::getEnv();
    static jclass cl_ADInApp = ADJNI::GRef(env, env->FindClass(cADInApp));
    static jmethodID m_ADInApp_makePurchase = env->GetStaticMethodID(
                cl_ADInApp, "makePurchase", F(Void, J(cString)));

    jstring sku = env->NewStringUTF(p.getID().c_str());
    env->CallStaticVoidMethod(cl_ADInApp, m_ADInApp_makePurchase, sku);
    env->DeleteLocalRef(sku);

    if(env->ExceptionCheck())
    {
        env->ExceptionDescribe();
        env->ExceptionClear();

        ADUIThread::getInstance().runInCocos2dThread(ADRunnable([p](){
                ADInApp::Platform::purchaseFailed(p.getID(),
                                                  ADInApp::OperationType::Purchased,
                                                  ADInApp::ErrorType::BillingUnavaliable);
        }));
    }
}

void do_restorePurchases()
{
    JNIEnv* env = ADJNI::getEnv();
    static jclass cl_ADInApp = ADJNI::GRef(env, env->FindClass(cADInApp));
    static jmethodID m_ADInApp_restorePurchases = env->GetStaticMethodID(
                cl_ADInApp, "restorePurchases", F(Void, None));

    env->CallStaticVoidMethod(cl_ADInApp, m_ADInApp_restorePurchases);

    if(env->ExceptionCheck())
    {
        env->ExceptionDescribe();
        env->ExceptionClear();

        ADUIThread::getInstance().runInCocos2dThread(ADRunnable([](){
                ADInApp::Platform::restorePurchaseResult(false);
        }));
    }
}


bool handleActivityResult(jint requestCode,
                          jint resultCode,
                          jobject data)
{
    JNIEnv* env = ADJNI::getEnv();
    static jclass cl_ADInApp = ADJNI::GRef(env, env->FindClass(cADInApp));
    static jmethodID m_ADInApp_handleActivityResult = env->GetStaticMethodID(
                cl_ADInApp, "handleActivityResult", F(Bool, Int Int J(cIntent)));

    return env->CallStaticBooleanMethod(cl_ADInApp, m_ADInApp_handleActivityResult,
                                        requestCode, resultCode, data);
}

void do_loadStore(const std::string& store_key)
{
    //Setup on activity result
    ADOnActivityResult::addCallback(&handleActivityResult);

    JNIEnv* env = ADJNI::getEnv();
    //Create to lists with consumables and non-consumables
    jclass cl_ArrayList = env->FindClass(cArrayList);

    jmethodID m_ArrayList_Constructor = env->GetMethodID(cl_ArrayList, "<init>",
                                                         F(Void, None));
    jmethodID m_ArrayList_add = env->GetMethodID(cl_ArrayList, "add", F(Bool, J(cObject)));

    jobject consumable = env->NewObject(cl_ArrayList, m_ArrayList_Constructor);
    jobject non_consumable = env->NewObject(cl_ArrayList, m_ArrayList_Constructor);

    const ADInApp::ProductMap& products = ADInApp::getAllProducts();
    const std::string TYPE = "type";
    for(auto it : products)
    {
        const ADInApp::Product& p = it.second;
        const std::string& type = p.getParameter(TYPE);

        jstring sku = env->NewStringUTF(p.getID().c_str());

        if(type == "consumable")
            env->CallBooleanMethod(consumable, m_ArrayList_add, sku);
        else if(type == "non-consumable")
            env->CallBooleanMethod(non_consumable, m_ArrayList_add, sku);
        else
        {
            LOGE("Purchase without type: %s", p.getID().c_str());
        }

        env->DeleteLocalRef(sku);
    }

    //Call loadStore
    jobject activity = ADJNI::getActivity(env);
    jstring store_key_j = env->NewStringUTF(store_key.c_str());

    jclass cl_ADInApp = env->FindClass(cADInApp);
    jmethodID cl_ADInApp_loadStore = env->GetStaticMethodID(
                cl_ADInApp, "loadStore",
                F(Void, J(cActivity) J(cString) J(cArrayList) J(cArrayList)));

    env->CallStaticVoidMethod(cl_ADInApp, cl_ADInApp_loadStore,
                              activity, store_key_j,
                              consumable, non_consumable);

    if(env->ExceptionCheck())
    {
        env->ExceptionDescribe();
        env->ExceptionClear();

        ADNotification::showNotification("Google Play Error");
    }

    env->DeleteLocalRef(cl_ADInApp);
    env->DeleteLocalRef(store_key_j);
    env->DeleteLocalRef(activity);
    env->DeleteLocalRef(consumable);
    env->DeleteLocalRef(non_consumable);
    env->DeleteLocalRef(cl_ArrayList);
}

void ADInApp::Platform::buyProduct(const Product& p)
{
    ADUIThread::getInstance().runInUIThread(ADRunnable([=](){
        do_buyProduct(p);
    }));

}
void ADInApp::Platform::loadStore(const std::string& key, const Mode)
{
    ADUIThread::getInstance().runInUIThread(ADRunnable([=](){
        do_loadStore(key);
    }));
}


void ADInApp::Platform::restorePurchases()
{
    ADUIThread::getInstance().runInUIThread(ADRunnable([=](){
        do_restorePurchases();
    }));
}

extern "C"
{


void Java_com_x4enjoy_ADLib_inapp_google_ADInApp_setProductPrice(JNIEnv*  env, jclass, jstring sku, jstring price)
{
    std::string key = ADJNI::toString(env, sku);
    std::string new_price = ADJNI::toString(env, price);

    ADUIThread::getInstance().runInCocos2dThread(ADRunnable([key, new_price](){
        ADInApp::Platform::setRealPrice(key, new_price);
    }));
}

void Java_com_x4enjoy_ADLib_inapp_google_ADInApp_notifyPurchaseSuccessful(JNIEnv*  env, jclass, jstring sku, jboolean success, jboolean restored, jint error_code)
{
    std::string key = ADJNI::toString(env, sku);
    bool vsuccess = success;
    ADInApp::OperationType type = ADInApp::OperationType::Restored;
    if(!restored)
        type = ADInApp::OperationType::Purchased;

    ADInApp::ErrorType error = static_cast<ADInApp::ErrorType>(error_code);
    ADUIThread::getInstance().runInCocos2dThread(ADRunnable([key, vsuccess, type, error](){
        if(vsuccess)
            ADInApp::Platform::purchaseSuccessful(key, type);
        else
            ADInApp::Platform::purchaseFailed(key, type, error);
    }));
}

void Java_com_x4enjoy_ADLib_inapp_google_ADInApp_setStoreState(JNIEnv*, jclass, jboolean success)
{
    bool vsuccess = success;

    ADUIThread::getInstance().runInCocos2dThread(ADRunnable([vsuccess](){
        ADInApp::Platform::setStoreAvaliable(vsuccess);
    }));
}

void Java_com_x4enjoy_ADLib_inapp_google_ADInApp_notifyRestorePurchases(JNIEnv*, jclass, jboolean success)
{
    bool vsuccess = success;

    ADUIThread::getInstance().runInCocos2dThread(ADRunnable([vsuccess](){
        ADInApp::Platform::restorePurchaseResult(vsuccess);
    }));
}

}

