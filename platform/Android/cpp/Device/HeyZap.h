#include <ADLib/ADSignals.h>

class HeyZap
{
public:
    static HeyZap* getInstance()
    {
        static HeyZap obj;
        return &obj;
    }

    Signal<> signalHeyZapReady;
};
