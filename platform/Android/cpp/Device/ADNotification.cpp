#include <ADLib/Device/ADNotification.h>
#include "../ADJNI.h"
#include "ADUIThread.h"

#define cToast "android/widget/Toast"
#define cCharSequence "java/lang/CharSequence"
void showNotificationHelper(const std::string& text)
{
    JNIEnv* env = ADJNI::getEnv();

    static jclass cl_Toast = ADJNI::GRef(env, env->FindClass(cToast));
    static jmethodID m_Toast_makeText = env->GetStaticMethodID(
                cl_Toast, "makeText", F(J(cToast), J(cContext) J(cCharSequence) Int));

    static jobject activity = ADJNI::GRef(env, ADJNI::getActivity(env));
    jstring text_j = env->NewStringUTF(text.c_str());

    static int Toast_LENGTH_LONG = -1;
    if(Toast_LENGTH_LONG < 0)
    {
        jfieldID f_Toast_LENGTH_LONG = env->GetStaticFieldID(
                    cl_Toast, "LENGTH_LONG", Int);
        Toast_LENGTH_LONG = env->GetStaticIntField(cl_Toast, f_Toast_LENGTH_LONG);
    }

    jobject toast = env->CallStaticObjectMethod(
                cl_Toast, m_Toast_makeText,
                activity, text_j, Toast_LENGTH_LONG);

    static jmethodID m_Toast_show = env->GetMethodID(cl_Toast, "show", F(Void, None));

    env->CallVoidMethod(toast, m_Toast_show);
    //Toast.makeText(App.getAppContext(), text, Toast.LENGTH_LONG).show();

    env->DeleteLocalRef(text_j);
    env->DeleteLocalRef(toast);
}

void ADNotification::showNotification(const std::string& text)
{
    ADUIThread::getInstance().runInUIThread(ADRunnable([=](){
        showNotificationHelper(text);
    }));
}
