#include <ADJNI.h>
#include <ADLib/Device/ADOrientation.h>
#include "ADUIThread.h"
#define cMainActivity "com/x4enjoy/ADLib/ADActivity"

static DeviceOrientation _last_reported_orientation = DeviceOrientation::Landscape;

inline bool orientationToBool(const DeviceOrientation orientation)
{
    return orientation == DeviceOrientation::Landscape;
}

inline DeviceOrientation boolToOrientation(bool value)
{
    if(value)
    {
        return DeviceOrientation::Landscape;
    }
    return DeviceOrientation::Portrait;
}

void ADOrientation::enableRuntimeOrientation(const DeviceOrientation orientation)
{
	_last_reported_orientation = orientation;

    JNIEnv* env = ADJNI::getEnv();
    jobject activity = ADJNI::getActivity(env);

    jclass cl_Activity = env->FindClass(cMainActivity);
    jmethodID m_Activity_enableOrientationHandler = env->GetMethodID(
                    cl_Activity, "enableOrientationHandler", F(Void, Bool));

    env->CallVoidMethod(activity, m_Activity_enableOrientationHandler, orientationToBool(orientation));
}

extern "C"
{


void Java_com_x4enjoy_ADLib_ADActivity_orientationChanged(JNIEnv*  env, jobject, jboolean orientation)
{
    DeviceOrientation orient = boolToOrientation(orientation);
	static DeviceOrientation to_report = orient;
    to_report = orient;
	
    ADUIThread::getInstance().runInCocos2dThread(ADRunnable([](){
        if(_last_reported_orientation != to_report)
        {
            _last_reported_orientation = to_report;
            emit ADOrientation::signalOrientationChanged(to_report);
        }
    }));

}

}
