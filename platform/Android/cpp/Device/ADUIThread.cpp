#include "ADUIThread.h"

#include "cocos2d.h"
#include <pthread.h>
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
using namespace cocos2d;
#define cRunnable "java/lang/Runnable"
#define cADUIThread "com/x4enjoy/ADLib/ADUIThread"


extern "C"
{
void Java_com_x4enjoy_ADLib_ADUIThread_run(JNIEnv*  env, jobject thiz)
{
    ADUIThread::getInstance().performNextTask();
}

}

ADUIThread& ADUIThread::getInstance()
{
    if(_instance == 0)
    {
        _instance = new ADUIThread;
    }
    return *_instance;
}

void ADUIThread::runInUIThread(const ADRunnable &task)
{
    mutexLock();
    _task_queue.push(task);
    mutexUnlock();


    JNIEnv* env = ADJNI::getEnv();

    jobject java_task = env->NewObject(_UIHelper, _UIHelper_Construct);
    env->CallVoidMethod(_main_activity, _runOnUiThread, java_task);
    env->DeleteLocalRef(java_task);
}
void ADUIThread::performNextCocosTask(float)
{
    bool repeat = false;
    ADRunnable task(nullptr);
    mutexLock();
    if(_cocos_queue.size() > 0)
    {
        task = _cocos_queue.front();
        _cocos_queue.pop();

        if(_cocos_queue.size() > 0)
            repeat = true;
    }
    mutexUnlock();

    if(!task.isNull())
    {
        task.run();
    }
    if(!repeat)
        CCDirector::sharedDirector()->getScheduler()->unscheduleSelector(schedule_selector(ADUIThread::performNextCocosTask), this);
}

void ADUIThread::runInCocos2dThread(const ADRunnable &task)
{

    mutexLock();
    _cocos_queue.push(task);
    mutexUnlock();

    CCDirector::sharedDirector()->getScheduler()->scheduleSelector(schedule_selector(ADUIThread::performNextCocosTask), this, 0, false);
}

void ADUIThread::performNextTask()
{
    ADRunnable task(nullptr);
    mutexLock();
    if(_task_queue.size() > 0)
    {
        task = _task_queue.front();
        _task_queue.pop();
    }
    mutexUnlock();

    if(!task.isNull())
    {
        task.run();
    }
}
void ADUIThread::initMethods()
{
    JNIEnv* env = ADJNI::getEnv();
    _main_activity = ADJNI::GRef(env, ADJNI::getActivity(env));

    jclass activity_class = env->FindClass(cActivity);
    _runOnUiThread = env->GetMethodID(activity_class, "runOnUiThread", F(Void, J(cRunnable)));

    _UIHelper = ADJNI::GRef(env, env->FindClass(cADUIThread));
    _UIHelper_Construct = env->GetMethodID(_UIHelper, "<init>", F(Void, None));

    env->DeleteLocalRef(activity_class);
}
ADUIThread::ADUIThread()
{
    initMethods();
}

void ADUIThread::mutexLock()
{
    pthread_mutex_lock(&mutex);

}
void ADUIThread::mutexUnlock()
{
    pthread_mutex_unlock(&mutex);

}

ADUIThread* ADUIThread::_instance = 0;
