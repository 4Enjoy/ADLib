#include <ADLib/Device/ADRewardedAds.h>

#include <map>

#include "../ADJNI.h"
#include "ADUIThread.h"
#include "HeyZap.h"

#include <ADLib/Generic/ADMethods.h>

#define cHeyzapAds "com/heyzap/sdk/ads/HeyzapAds"
#define cIncentivizedAd "com/heyzap/sdk/ads/IncentivizedAd"
#define cADRewardedAdsHeyZap "com/x4enjoy/ADLib/ADRewardedAdsHeyZap"
#define cBoolean "java/lang/Boolean"
using namespace cocos2d;
class HeyZapRewardedHelper : public HasSlots
{
public:

    bool readBoolean(JNIEnv* env, jobject obj);
    static HeyZapRewardedHelper& getInstance();

    ///UI Thread Only
    void init();
    void onHeyZapReady();

    ///UI Thread Only
    void prepare();

    ///UI Thread Only
    void show();

    bool _is_avaliable;
    std::string _last_id;

    void notifyResult(bool result);
private:
    bool _is_inited;
    HeyZapRewardedHelper();
    static HeyZapRewardedHelper* _instance;

};
void HeyZapRewardedHelper::onHeyZapReady()
{
    cocos2d::CCLog("Init rewarded: Ready");
    JNIEnv* env = ADJNI::getEnv();

    static jclass ADRewardedAdsHeyZap = ADJNI::GRef(env, env->FindClass(cADRewardedAdsHeyZap));

    static jmethodID ADRewardedAdsHeyZap_init = env->GetStaticMethodID(
                ADRewardedAdsHeyZap, "init", F(Void , None ) );

    env->CallStaticVoidMethod(ADRewardedAdsHeyZap, ADRewardedAdsHeyZap_init);
    ADJNI::clearExceptions(env);
    _is_inited = true;
    prepare();
}

void HeyZapRewardedHelper::init()
{
    cocos2d::CCLog("Init rewarded");
    CONNECT(HeyZap::getInstance()->signalHeyZapReady, this, &HeyZapRewardedHelper::onHeyZapReady);
}

void HeyZapRewardedHelper::prepare()
{
    if(_is_inited)
    {
        JNIEnv* env = ADJNI::getEnv();

        static jclass IncentivizedAd = ADJNI::GRef(env, env->FindClass(cIncentivizedAd));

        static jmethodID IncentivizedAd_fetch = env->GetStaticMethodID(
                    IncentivizedAd, "fetch", F(Void , None ) );

        env->CallStaticVoidMethod(IncentivizedAd, IncentivizedAd_fetch);
        ADJNI::clearExceptions(env);
    }
}

bool HeyZapRewardedHelper::readBoolean(JNIEnv* env, jobject obj)
{
    static jclass Boolean = ADJNI::GRef(env, env->FindClass(cBoolean));
    static jmethodID booleanValueMID   = env->GetMethodID(Boolean, "booleanValue", "()Z");
    bool booleanValue           = (bool) env->CallBooleanMethod(obj, booleanValueMID);
    return booleanValue;
}

void HeyZapRewardedHelper::show()
{
    JNIEnv* env = ADJNI::getEnv();

    static jclass IncentivizedAd = ADJNI::GRef(env, env->FindClass(cIncentivizedAd));

    static jmethodID IncentivizedAd_display = env->GetStaticMethodID(
                IncentivizedAd, "display", F(Void , J(cActivity) ) );
    static jmethodID IncentivizedAd_isAvailable = env->GetStaticMethodID(
                IncentivizedAd, "isAvailable", F(J(cBoolean) , None ) );

    jobject boolean = env->CallStaticObjectMethod(IncentivizedAd, IncentivizedAd_isAvailable);
    if(readBoolean(env, boolean))
    {
        jobject activity = ADJNI::getActivity(env);
        env->CallStaticVoidMethod(IncentivizedAd, IncentivizedAd_display, activity);
        ADJNI::clearExceptions(env);
        env->DeleteLocalRef(activity);
        _is_avaliable = false;

        prepare();
    }
    env->DeleteLocalRef(boolean);

}

void HeyZapRewardedHelper::notifyResult(bool result)
{
    ADUIThread::getInstance().runInCocos2dThread(ADRunnable([this, result](){
        if(result)
        {
            emit ADRewardedAds::signalAdsShownSuccess(_last_id);
        }
        else
        {
            emit ADRewardedAds::signalAdsShownFail(_last_id);
        }
    }));

}

HeyZapRewardedHelper* HeyZapRewardedHelper::_instance = 0;

HeyZapRewardedHelper& HeyZapRewardedHelper::getInstance()
{
    if(_instance == 0)
    {
        _instance = new HeyZapRewardedHelper;
    }
    return *_instance;
}

HeyZapRewardedHelper::HeyZapRewardedHelper()
    : _is_avaliable(false), _is_inited(false)
{
}



void ADRewardedAds::show(const std::string& ad)
{
    ADUIThread::getInstance().runInUIThread(ADRunnable([ad](){
        HeyZapRewardedHelper::getInstance()._last_id = ad;
        HeyZapRewardedHelper::getInstance().show();
    }));
}

void ADRewardedAds::prepare()
{
    ADUIThread::getInstance().runInUIThread(ADRunnable([](){
        HeyZapRewardedHelper::getInstance().prepare();
    }));
}

void ADRewardedAds::init()
{
    ADUIThread::getInstance().runInUIThread(ADRunnable([](){
        HeyZapRewardedHelper::getInstance().init();
    }));
}

bool ADRewardedAds::isAvaliable()
{
    return HeyZapRewardedHelper::getInstance()._is_avaliable;
}
extern "C"
{
void Java_com_x4enjoy_ADLib_ADRewardedAdsHeyZap_onAdsAvaliable(JNIEnv*  env, jclass)
{
    HeyZapRewardedHelper::getInstance()._is_avaliable = true;
}

void Java_com_x4enjoy_ADLib_ADRewardedAdsHeyZap_onVideoResult(JNIEnv*  env, jclass, jboolean res)
{
    HeyZapRewardedHelper::getInstance().notifyResult(res);
}
}
