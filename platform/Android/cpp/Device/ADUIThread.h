#ifndef ADUIThread_H
#define ADUIThread_H

#include <queue>
#include <memory>
#include "../ADJNI.h"
#include "cocos2d.h"
#include <functional>
class ADRunnable
{
public:
    typedef std::function<void()> Function;
    ADRunnable(const Function& f):
        _f(f)
    {}
    bool isNull()
    {
        return _f == nullptr;
    }

    void run()
    {
        _f();
    }

private:
    Function _f;
};


class ADUIThread : cocos2d::CCObject
{
public:
    static ADUIThread& getInstance();

    void runInUIThread(const ADRunnable& task);
    void runInCocos2dThread(const ADRunnable& task);
    void performNextTask();
private:
    ADUIThread();
    void initMethods();
    void mutexLock();
    void mutexUnlock();

    void performNextCocosTask(float);

    static ADUIThread* _instance;
    typedef std::queue<ADRunnable> TaskQueue;
    TaskQueue _task_queue;
    TaskQueue _cocos_queue;

    jclass _UIHelper;
    jobject _main_activity;
    jmethodID _runOnUiThread;
    jmethodID _UIHelper_Construct;

};

#endif
