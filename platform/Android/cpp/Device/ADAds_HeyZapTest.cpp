#include "HeyZap.h"
#include "../ADJNI.h"
#include "ADUIThread.h"
#define cHeyzapAds "com/heyzap/sdk/ads/HeyzapAds"
class HeyZapTest : public HasSlots
{
public:
    HeyZapTest()
    {
        CONNECT(HeyZap::getInstance()->signalHeyZapReady, this, &HeyZapTest::onReady);
    }

    void onReady()
    {
        ADUIThread::getInstance().runInUIThread(ADRunnable([](){
            JNIEnv* env = ADJNI::getEnv();

            //Activity activity = this;
            jobject activity = ADJNI::getContext(env);

            jclass HeyzapAds = env->FindClass(cHeyzapAds);
            jmethodID HeyzapAds_start = env->GetStaticMethodID(HeyzapAds, "startTestActivity", F(Void , J(cContext)));

            cocos2d::CCLog("HeyZapTest: %d", HeyzapAds_start);
            env->CallStaticVoidMethod(HeyzapAds, HeyzapAds_start, activity);
        }));

    }
};

static HeyZapTest _hey_zap_test;
