#include <ADLib/Device/ADDeviceID.h>
#include "cocos2d.h"
#include "../ADJNI.h"

#define cADDeviceID "com/x4enjoy/ADLib/ADDeviceID"

std::string ADDeviceID::getDeviceID()
{
    JNIEnv* env = ADJNI::getEnv();

    jclass cl_ADDeviceID = env->FindClass(cADDeviceID);

    jmethodID m_ADDeviceID_getDeviceID = env->GetStaticMethodID(
                    cl_ADDeviceID, "getDeviceID", F(J(cString), J(cActivity)));

    //cocos2d::CCLog("%d %d", cl_ADDeviceID, m_ADDeviceID_getDeviceID);
    jobject activity = ADJNI::getActivity(env);
    jstring id = (jstring)env->CallStaticObjectMethod(cl_ADDeviceID,
                                                      m_ADDeviceID_getDeviceID,
                                                      activity);

    std::string res = ADJNI::toString(env, id);
    env->DeleteLocalRef(activity);
        env->DeleteLocalRef(cl_ADDeviceID);
        env->DeleteLocalRef(id);
    return res;
}
