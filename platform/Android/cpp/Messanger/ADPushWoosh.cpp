#include <ADLib/Device/ADPushWoosh.h>
#include "cocos2d.h"
#include "../ADJNI.h"
#include "ADLib/Device/ADNotification.h"

#define cPushWoosh "com/x4enjoy/ADLib/messanger/ADPushWoosh"



void ADPushWoosh::inizialize(const std::string& app_id, const std::string& platform_parameter)
{
    cocos2d::CCLog("PushWoosh starting inizialization");
    JNIEnv* env = ADJNI::getEnv();
    static jclass cl_Push = ADJNI::GRef(env, env->FindClass(cPushWoosh));
    static jmethodID m_Push=env->GetStaticMethodID(cl_Push,"onCreate",
                                                   F(Void, J(cActivity) J(cString) J(cString)));
    jstring appId=env->NewStringUTF(app_id.c_str());
    jstring projectId=env->NewStringUTF(platform_parameter.c_str());
    env->CallStaticVoidMethod(cl_Push, m_Push, ADJNI::getActivity(env), appId, projectId);
    if(env->ExceptionCheck())
    {
        env->ExceptionDescribe();
        env->ExceptionClear();

        cocos2d::CCLog("PushWoosh Error");
    }
    env->DeleteLocalRef(appId);
    env->DeleteLocalRef(projectId);
    return;
}
