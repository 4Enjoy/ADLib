#include <ADLib/Device/ADPush.h>
#include "cocos2d.h"
#include "../ADJNI.h"
#include "ADLib/Device/ADNotification.h"
#include "../Device/ADUIThread.h"
#define cPush "com/x4enjoy/ADLib/messanger/ADPush"



void ADPush::registerForPush(const std::string& platform_parameter)
{
    cocos2d::CCLog("Push starting inizialization");
    JNIEnv* env = ADJNI::getEnv();
    static jclass cl_Push = ADJNI::GRef(env, env->FindClass(cPush));
    static jmethodID m_Push=env->GetStaticMethodID(cl_Push,"onCreate",
                                                   F(Void, J(cActivity) J(cString)));

    jstring projectId=env->NewStringUTF(platform_parameter.c_str());
    env->CallStaticVoidMethod(cl_Push, m_Push, ADJNI::getActivity(env), projectId);
    if(env->ExceptionCheck())
    {
        env->ExceptionDescribe();
        env->ExceptionClear();

        cocos2d::CCLog("Push Error");
    }
    env->DeleteLocalRef(projectId);
    return;
}
extern "C"
{
void Java_com_x4enjoy_ADLib_messanger_ADPush_saveResult(JNIEnv*  env, jclass, jstring regId)
{
    std::string key = ADJNI::toString(env, regId);

    ADUIThread::getInstance().runInCocos2dThread(ADRunnable([key](){
        if(key.size())
        {
            emit ADPush::signalPushRegisterSuccess(key);
        }
        else
        {
            emit ADPush::signalPushRegisterFailed();
        }
    }));
}
}
