#include "cocos2d.h"
#include "ADJNI.h"
#include <ADLib/Device/ADDeviceEvents.h>
#define cScreenLock "com/x4enjoy/ADLib/ADScreenLock"
#include "Device/ADUIThread.h"
#include <ADLib/Device/ADScreenLock.h>

void ADScreenLock::lockTheScreen()
{

        ADUIThread::getInstance().runInUIThread(ADRunnable([](){
            JNIEnv* env = ADJNI::getEnv();
            static jclass cl_ADScreenLock = ADJNI::GRef(env, env->FindClass(cScreenLock));
            static jmethodID m_lock=env->GetStaticMethodID(cl_ADScreenLock,"enableScreenLock",
                                                           F(Void, J(cActivity)));

            env->CallStaticVoidMethod(cl_ADScreenLock, m_lock, ADJNI::getActivity(env));


        }));

}


