#include <ADLib/Networking/ADHttp.h>
#include "../ADJNI.h"
#include "cocos2d.h"
#define cArrayList "java/util/ArrayList"
#define cObject "java/lang/Object"
#define cADHttp "com/x4enjoy/ADLib/networking/ADHttp"
#include <ADLib/Common/ADCocosThread.h>

namespace ADHttp_impl
{
typedef std::map<int, ADHttp::Callback> CallbacksMap;
static CallbacksMap _callbacks;
static int _next_id = 0;
}

using namespace ADHttp_impl;

void ADHttp::makeRequest(const RequestType& type,
                         const std::string& url,
                         const Params& params,
                         const Callback& callback)
{
    JNIEnv* env = ADJNI::getEnv();

    static jclass cl_ArrayList = ADJNI::GRef(env, env->FindClass(cArrayList));

    static jmethodID m_ArrayList_Constructor = env->GetMethodID(cl_ArrayList, "<init>",
                                                         F(Void, None));
    static jmethodID m_ArrayList_add = env->GetMethodID(cl_ArrayList, "add", F(Bool, J(cObject)));

    jobject keys = env->NewObject(cl_ArrayList, m_ArrayList_Constructor);
    jobject values = env->NewObject(cl_ArrayList, m_ArrayList_Constructor);

    const ADHttp::Params::SVec& keys_arr = params.getKeysString();
    const ADHttp::Params::SVec& values_arr = params.getValuesString();


    for(auto s : keys_arr)
    {
        jstring str = env->NewStringUTF(s.c_str());
        env->CallBooleanMethod(keys, m_ArrayList_add, str);
        env->DeleteLocalRef(str);
    }

    for(auto s : values_arr)
    {
        jstring str = env->NewStringUTF(s.c_str());
        env->CallBooleanMethod(values, m_ArrayList_add, str);
        env->DeleteLocalRef(str);
    }

    static jclass cl_ADHttp = ADJNI::GRef(env, env->FindClass(cADHttp));

    static jmethodID m_ADHttp_makeRequest = env->GetStaticMethodID(
                cl_ADHttp, "makeRequest", F(Void, Int Int J(cString) J(cArrayList) J(cArrayList)));

    jstring jurl = env->NewStringUTF(url.c_str());

    int req_id = _next_id++;
    _callbacks[req_id] = callback;

    env->CallStaticVoidMethod(cl_ADHttp, m_ADHttp_makeRequest,
                              req_id, static_cast<int>(type), jurl, keys, values);

    env->DeleteLocalRef(keys);
    env->DeleteLocalRef(values);
}

extern "C"
{


void Java_com_x4enjoy_ADLib_networking_ADHttp_requestResult(JNIEnv*  env, jclass, jint id, jint code, jboolean is_ok, jbyteArray data, jint data_size)
{
    ADHttp::ResponsePtr response(new ADHttp::Response);

    response->setIsOk(is_ok);
    response->setCode(code);

    if(data_size > 0)
    {
        jbyte* bytes = env->GetByteArrayElements(data, 0 );
        ADHttp::Response::Data& d = response->getData();

        char* b_ptr = (char*)bytes;
        //cocos2d::CCLog("Ptr: %d", b_ptr);

        d.resize(data_size);

        std::copy(b_ptr, b_ptr+data_size, &d[0]);
        env->ReleaseByteArrayElements(data, bytes, 0);
    }

    ADCocosThread::onCocos2dThread([response, id](){
        auto it = _callbacks.find(id);
        if(it != _callbacks.end())
        {
            it->second(response);
            _callbacks.erase(it);
        }
    });

}

}
