#ifndef AD_ON_ACTIVITY_RESULT_H
#define AD_ON_ACTIVITY_RESULT_H
#include "ADJNI.h"
#include <functional>
#include <vector>
class ADOnActivityResult
{
public:
    typedef std::function<bool (jint, jint, jobject)> ActivityResultCallback;

    static void handleResult(JNIEnv*  env,
                             jobject thiz,
                             jint requestCode,
                             jint resultCode,
                             jobject data);

    static void addCallback(const ActivityResultCallback&);
private:
    static std::vector<ActivityResultCallback> _callbacks;
};

#endif
