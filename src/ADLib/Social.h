#ifndef SOCIAL_H
#define SOCIAL_H
#include <ADLib/Social/ADFacebook.h>
#include <ADLib/Social/ADFacebookPhoto.h>
#include <ADLib/Social/ADFacebookFriend.h>
#include <ADLib/Social/ADHighscoreServer.h>
#endif // SOCIAL_H
