#ifndef ADHTTP_CURL_HPP
#define ADHTTP_CURL_HPP
#include <curl/curl.h>
#include <ADLib/Networking/ADHttp.h>
#include <ADLib/Common/ADCocosThread.h>
#include <thread>
#include <fstream>
#include <chrono>


class ADCurlReader
{
public:
    static const int REPORT_PROGRESS_EVERY_MS = 500;
    typedef std::chrono::high_resolution_clock Timer;
    typedef ADHttp::ProgressCallback ProgressCallback;

    ADCurlReader(std::string file_dir="",
                 const ProgressCallback* callback=nullptr)
        : _response(new ADHttp::Response),
          _file(nullptr),
          _progress_callback(callback)
    {
        cocos2d::CCLog("ADCurlReader created");
        _timer = Timer::now();
        if(file_dir.size())
        {
            _file = std::make_shared<std::ofstream>(file_dir, std::ios::binary);
        }
    }

    ~ADCurlReader()
    {
        cocos2d::CCLog("ADCurlReader destroyed");
    }

    ADHttp::ResponsePtr getResponse() const
    {
        return _response;
    }

    void closeFile() const
    {
        if(_file)
        {
            _file->close();
        }
    }
    ADHttp::ResponsePtr _response;
    static size_t handle(char * data, size_t size, size_t nmemb, void * p);
    static int handleProgress(void *clientp,    double  dltotal,  double  dlnow,  double ,  double );
    static size_t handleFile(char * data, size_t size, size_t nmemb, void * p);


    size_t handle_impl(char * data, size_t size, size_t nmemb);
    size_t handleFile_impl(char * data, size_t size, size_t nmemb);
    void progress(uint64_t downloaded, uint64_t total);
private:
    std::shared_ptr<std::ofstream> _file;
    const ProgressCallback* _progress_callback;
    Timer::time_point _timer;
};

size_t ADCurlReader::handle(char * data, size_t size, size_t nmemb, void * p)
{
    return static_cast<ADCurlReader*>(p)->handle_impl(data, size, nmemb);
}

size_t ADCurlReader::handleFile(char * data, size_t size, size_t nmemb, void * p)
{
    return static_cast<ADCurlReader*>(p)->handleFile_impl(data, size, nmemb);
}

int ADCurlReader::handleProgress(void *p,    double  dltotal,  double  dlnow,  double ,  double )
{
    static_cast<ADCurlReader*>(p)->progress(dlnow, dltotal);
    return 0;
}

size_t ADCurlReader::handle_impl(char* data, size_t size, size_t nmemb)
{
    cocos2d::CCLog("Reader read data %d", size*nmemb);
    size_t chunk = size * nmemb;

    ADHttp::Response::Data& output = _response->getData();

    size_t old_size = output.size();
    output.resize(old_size + chunk);

    std::copy(data, data+chunk, &output[0] + old_size);

    return chunk;
}

size_t ADCurlReader::handleFile_impl(char * data, size_t size, size_t nmemb)
{
    if(!_file)
    {
        return 0;
    }

    size_t chunk = size * nmemb;
    _file->write(data, chunk);

    return chunk;
}

void ADCurlReader::progress(uint64_t downloaded, uint64_t total)
{
    if(_progress_callback)
    {
        bool report = false;
        Timer::time_point now = Timer::now();

        int interval_in_ms = std::chrono::duration_cast<std::chrono::milliseconds>(
                    now-_timer).count();
        if(interval_in_ms > REPORT_PROGRESS_EVERY_MS)
        {
            report = true;
            _timer = now;
        }

        if(report)
        {
            (*_progress_callback)(downloaded, total);
        }

    }
}

inline std::string urlEncode(CURL* curl, const std::string text)
{
    char* enc = curl_easy_escape(curl, text.c_str(), text.size());
    std::string res(enc);
    curl_free(enc);
    return res;
}

inline std::string paramsToUrl(CURL* curl, const ADHttp::Params& params)
{
    std::stringstream res;
    const ADHttp::Params::SVec& keys = params.getKeysString();
    const ADHttp::Params::SVec& values = params.getValuesString();

    for(size_t i = 0; i<keys.size(); ++i)
    {
        if(i)
            res << "&";

        res << urlEncode(curl, keys[i]) << "=" << urlEncode(curl, values[i]);
    }
    return res.str();
}



#include <thread>
#include <mutex>
#include <stack>
class CurlHandles
{
public:
    static CurlHandles* getInstance() {
        static CurlHandles obj;
        return &obj;
    }
    CURL* getHandle()
    {
        static int HANDLES = 0;
        CURL* res = nullptr;
        {
            std::unique_lock<std::mutex> lock(_lock);
            if(_free.size() > 0)
            {
                res = _free.top();
                _free.pop();
            }
            else
            {
                HANDLES++;
                res = curl_easy_init();
                cocos2d::CCLog("CURL Handle Opened: %d", HANDLES);
            }
        }

        return res;
    }
    void freeHandle(CURL* curl)
    {
        {
            std::unique_lock<std::mutex> lock(_lock);
            _free.push(curl);
            curl_easy_reset(curl);
        }
    }

private:
    CurlHandles() {
        curl_global_init(CURL_GLOBAL_ALL);
    }

    std::mutex _lock;
    std::stack<CURL*> _free;
};


class CurlReq;
typedef std::shared_ptr<ADCurlReader> ADCurlReaderPtr;
typedef std::shared_ptr<CurlReq> CurlReqPtr;
class CurlReq
{
public:
    static CurlReqPtr create(const ADHttp::Callback& callback, ADCurlReaderPtr reader)
    {
        return CurlReqPtr(new CurlReq(callback, reader));
    }

    CURL* curl;
    ADCurlReaderPtr reader;
    std::map<CURLoption, std::string> curlStrings;
    void notifyResults(CURLcode res)
    {
        long http_code = 0;
        curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);

        ADHttp::ResponsePtr response = reader->getResponse();
        response->setCode(http_code);

        if(res == CURLE_OK)
        {
            cocos2d::CCLog("Req Success: %d", http_code);
            response->setIsOk(true);
        }
        else
        {

            cocos2d::CCLog("Req Failed: %d", http_code);
            response->setIsOk(false);
        }


        reader->closeFile();
        ADHttp::Callback callback_copy = callback;
        ADCocosThread::onCocos2dThread([response, callback_copy](){
            callback_copy(response);
        });
    }

    ~CurlReq() {
        CurlHandles::getInstance()->freeHandle(curl);
    }
private:
    CurlReq(const ADHttp::Callback& _callback, ADCurlReaderPtr _reader)
        : callback(_callback),
          curl(CurlHandles::getInstance()->getHandle()),
          reader(_reader)
    {
    }

    ADHttp::Callback callback;

};
#include <queue>
#include <condition_variable>
class CurlReqQueue
{
public:
    static CurlReqQueue* getInstance()
    {
        static CurlReqQueue* obj = new CurlReqQueue;
        return obj;
    }
    void run(CurlReqPtr req)
    {
        {
            std::unique_lock<std::mutex> lock(_lock);
            _requests.push(req);
            _navigation[req->curl] = req;

            cocos2d::CCLog("==HTTP notify");
            _cond.notify_one();
        }
    }

private:
    CurlReqQueue()
        : running(0),
          max_running(25),
          stop(false),
          cm(curl_multi_init()),
          _thread([this](){
        this->processTasks();
    })
    {

    }
    ~CurlReqQueue()
    {
        stop = true;
        _cond.notify_one();
        _thread.join();
        cocos2d::CCLog("Joined");
    }
    bool stop;

    CURLM* cm;
    int running;
    int max_running;


    void startTasks()
    {
        std::vector<CurlReqPtr> reqs;

        {
            std::unique_lock<std::mutex> lock(_lock);
            while(_requests.size()>0 && running<max_running)
            {
                CurlReqPtr req = _requests.front();
                _requests.pop();
                reqs.push_back(req);
                running++;
            }
        }

        for(CurlReqPtr req : reqs)
        {
            perform(req);
        }

    }

    int getRunning()
    {
        int res = 0;
        {
            std::unique_lock<std::mutex> lock(_lock);
            res = running;
        }
        return res;
    }

    void processTasks()
    {
        cocos2d::CCLog("==HTTP Started");
        std::unique_lock<std::mutex>
                lock(_cond_mutex);

        while(true)
        {
            startTasks();
            int still_running = 0;
            curl_multi_perform(cm, &still_running);

            int msgs_left = 0;
            CURLMsg *msg=NULL;
            while ((msg = curl_multi_info_read(cm, &msgs_left))) {
                if (msg->msg == CURLMSG_DONE) {
                    CURL* curl = msg->easy_handle;

                    CURLcode return_code = msg->data.result;
                    onEnd(curl, return_code);
                }
                else {
                    fprintf(stderr, "error: after curl_multi_info_read(), CURLMsg=%d\n", msg->msg);
                }
            }

            int old_running = getRunning();
            startTasks();

            int new_running = getRunning();
            if(!still_running && new_running == 0)
            {
                cocos2d::CCLog("==HTTP sleep");
                _cond.wait(lock);
                cocos2d::CCLog("==HTTP wakeup");
                if(stop)
                    return;
            }

            if(still_running && old_running == new_running)
            {
                int numfds=0;

                curl_multi_wait(cm, NULL, 0, 1000, &numfds);
            }
        }
    }

    void perform(CurlReqPtr req)
    {
        CURL* curl = req->curl;
//        for(auto it: req->curlStrings)
//        {
//            curl_easy_setopt(curl, it.first, it.second.c_str());
//            cocos2d::CCLog("Add param: %s", it.second.c_str());
//        }
        curl_multi_add_handle(cm, curl);

    }

    void onEnd(CURL* curl, CURLcode res)
    {
        curl_multi_remove_handle(cm, curl);

        CurlReqPtr req = nullptr;
        {
            std::unique_lock<std::mutex> lock(_lock);
            auto it = _navigation.find(curl);
            if(it != _navigation.end())
            {
                req = it->second;
                _navigation.erase(it);
            }
            running--;
            if(running < 0)
                running = 0;
        }

        if(!req)
        {
            cocos2d::CCLog("Lost CURL Handle: %d", curl);
        }
        else
        {
//            for(auto it: req->curlStrings)
//            {
//                //curl_easy_setopt(curl, it.first, it.second.c_str());
//                cocos2d::CCLog("param after: %s", it.second.c_str());
//            }
            req->notifyResults(res);
        }

    }


    std::mutex _lock;
    std::mutex _cond_mutex;
    std::condition_variable _cond;


    std::map<CURL*, CurlReqPtr> _navigation;
    std::queue<CurlReqPtr> _requests;
    std::thread _thread;
};

/**
 * @brief Raw post request
 * @param url_in
 * @param params_in
 * @param callback
 */
void makeHttpRequestAsync(const std::string& url_in,
                          const std::string& params_in,
                          const ADHttp::Callback& callback)
{
    CurlReqPtr req = CurlReq::create(callback, std::make_shared<ADCurlReader>());

    //req->curl = curl_easy_init();
    CURL* curl = req->curl;
    ADCurlReader* reader = req->reader.get();

    curl_easy_reset(curl);

    curl_easy_setopt(curl, CURLOPT_POST, true);
    req->curlStrings[CURLOPT_POSTFIELDS] = params_in;
    req->curlStrings[CURLOPT_URL] = url_in;

    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, req->curlStrings[CURLOPT_POSTFIELDS].c_str());
    curl_easy_setopt(curl, CURLOPT_URL, req->curlStrings[CURLOPT_URL].c_str());
    //cocos2d::CCLog("URL: %s", req->curlStrings[CURLOPT_URL].c_str());
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, reader);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &ADCurlReader::handle);
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, true);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_easy_setopt(curl, CURLOPT_ACCEPT_ENCODING, "gzip");

    CurlReqQueue::getInstance()->run(req);
        //CURLcode res = curl_easy_perform(curl);

        //req->notifyResults(res);
}

void makeHttpRequestAsync(const ADHttp::RequestType& type,
                          const std::string& url_in,
                          const ADHttp::Params& params_in,
                          const ADHttp::Callback& callback)
{
    CurlReqPtr req = CurlReq::create(callback, std::make_shared<ADCurlReader>());

    CURL* curl = req->curl;
    ADCurlReader* reader = req->reader.get();

    curl_easy_reset(curl);

    std::string params = paramsToUrl(curl, params_in);
    std::string url = url_in;

    req->curlStrings[CURLOPT_URL] = url;

    if(type == ADHttp::RequestType::POST)
    {
        curl_easy_setopt(curl, CURLOPT_POST, true);
        req->curlStrings[CURLOPT_POSTFIELDS] = params;
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, req->curlStrings[CURLOPT_POSTFIELDS].c_str());
    }
    else
    {
        if(params.size() > 0)
        {
            url += "?";
            url += params;
        }

    }



    curl_easy_setopt(curl, CURLOPT_URL, req->curlStrings[CURLOPT_URL].c_str());
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, reader);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &ADCurlReader::handle);
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, true);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, false);

    CurlReqQueue::getInstance()->run(req);
}

void doDownloadFile(const std::string& url,
                    const std::string& file_dir,
                    const ADHttp::Callback& callback,
                    const ADHttp::ProgressCallback& progress_callback)
{

    ;

    CurlReqPtr req = CurlReq::create(callback, std::make_shared<ADCurlReader>(file_dir, &progress_callback));

    CURL* curl = req->curl;
    ADCurlReader* reader = req->reader.get();

    curl_easy_reset(curl);

    req->curlStrings[CURLOPT_URL] = url;
    curl_easy_setopt(curl, CURLOPT_URL, req->curlStrings[CURLOPT_URL].c_str());
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, reader);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &ADCurlReader::handleFile);

    curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L);
    curl_easy_setopt(curl, CURLOPT_PROGRESSDATA, reader);
    curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, &ADCurlReader::handleProgress);


    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, true);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, false);

    CurlReqQueue::getInstance()->run(req);
}

void ADHttp::makeRequest(const RequestType& type,
                         const std::string& url,
                         const Params& params,
                         const Callback& callback)
{
    makeHttpRequestAsync(type, url, params, callback);
}

void ADHttp::makeRequestRawPost(const std::string& url,
                                const std::string& body,
                                const Callback& callback)
{
    makeHttpRequestAsync(url, body, callback);
}


void ADHttp::platformDownloadFile(const std::string& url,
                                  const std::string& file_dir,
                                  const Callback& callback,
                                  const ProgressCallback& progress_callback)
{
    doDownloadFile(url, file_dir, callback, progress_callback);
}

#endif // ADHTTP_CURL_HPP
