#ifndef ADREWARDEDADS_EMULATOR_HPP
#define ADREWARDEDADS_EMULATOR_HPP
#include <ADLib/Device/ADRewardedAds.h>

bool _ad_rewarded_avaliable = true;
void ADRewardedAds::init()
{

}

void ADRewardedAds::prepare()
{

}

void ADRewardedAds::show(const std::string& id)
{
    if(rand()%2)
    {
        _ad_rewarded_avaliable = false;
        emit signalAdsShownSuccess(id);
    }
    else
    {
        emit signalAdsShownFail(id);
    }
}

bool ADRewardedAds::isAvaliable()
{
    return _ad_rewarded_avaliable;
}

#endif // ADREWARDEDADS_EMULATOR_HPP
