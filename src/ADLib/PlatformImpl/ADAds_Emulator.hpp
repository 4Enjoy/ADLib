#include <ADLib/Device/ADAds.h>
#include <ADLib/Device/ADNotification.h>
#include <ADLib/Common/ADCocosThread.h>
void ADAds::Platform::addTestDevice(const DeviceID& device)
{
    cocos2d::CCLog("[ADADS] Added device: %s", device.c_str());
}
#ifndef AD_ADS_NOSIZE
cocos2d::CCSize ADAds::Platform::getBannerSize(const BannerType&)
{
    return cocos2d::CCSize(100000, 100000);
}
#endif

bool ADAds::Platform::createBanner(const BannerID id, const BannerType& type, const UnitID &unit_id)
{
    cocos2d::CCLog("[ADADS] Create banner: %s,ID=%d UID=%s", type.c_str(),id, unit_id.c_str());
    return true;
}

void ADAds::Platform::showBanner(const BannerID id)
{
    cocos2d::CCLog("[ADADS] Show banner: ID=%d", id);
}

void ADAds::Platform::hideBanner(const BannerID id)
{
    cocos2d::CCLog("[ADADS] Hide banner: ID=%d", id);
}


void ADAds::Platform::isBannerReady(const BannerID id, const Callback& callback)
{
    callback(true, id);
}

void ADAds::Platform::moveBanner(const BannerID id, const cocos2d::CCPoint& position)
{
    cocos2d::CCLog("[ADADS] Banner moved: ID=%d, (%f, %f)", id, position.x, position.y);
}

namespace ADAdsEmulator
{
    static int times_shown = 0;

    bool showNext()
    {
        return rand() % 5 != 0;
    }
}
using namespace ADAdsEmulator;

void ADAds::Platform::showInterstitial()
{
    cocos2d::CCLog("Show interstitial");
    if(showNext())
    {
        ADNotification::showNotification("Full Screen Ads");
        ADCocosThread::onCocos2dThread([](){
            times_shown++;
        });
    }
}

void ADAds::Platform::prepareInterstitial(const UnitID &)
{
    cocos2d::CCLog("Prepare interstitial");
}

unsigned int ADAds::Platform::getInterstialTimesShowed()
{
    return times_shown;
}
