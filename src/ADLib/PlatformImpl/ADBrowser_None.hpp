#ifndef ADBROWSER_NONE_HPP
#define ADBROWSER_NONE_HPP
#include <ADLib/Device/ADBrowser.h>
#include "cocos2d.h"
void ADBrowser::openWebURL(const std::string& url, const Fallback &)
{
    cocos2d::CCLog("Open Web url: %s", url.c_str());
}
void ADBrowser::openOSUrl(const std::string& item_id, const Fallback&)
{
    cocos2d::CCLog("Open Os url: %s", item_id.c_str());
}
void ADBrowser::sendMail(const std::string& email, const std::string& subject, const Fallback&)
{
    cocos2d::CCLog("Send letter: %s (%s)", email.c_str(), subject.c_str());
}


#endif // ADBROWSER_NONE_HPP
