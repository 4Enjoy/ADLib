#ifndef ADORIENTATION_KEYBOARD_HPP
#define ADORIENTATION_KEYBOARD_HPP
#include <ADLib/Device/ADOrientation.h>
#include "cocos2d.h"

class OrientationKeyboard : public cocos2d::CCObject, public cocos2d::CCIMEDelegate
{
public:
    OrientationKeyboard(const DeviceOrientation orient)
    {
        using namespace cocos2d;
        bool bRet = CCIMEDelegate::attachWithIME();
        if (bRet)
        {
            // open keyboard
            CCEGLView * pGlView = CCDirector::sharedDirector()->getOpenGLView();
            if (pGlView)
            {
                pGlView->setIMEKeyboardState(true);
            }
        }
    }
    bool canAttachWithIME()
    {
        return true;
    }

    void insertText(const char * text, int len)
    {
        std::string sInsert(text, len);
        if(sInsert == "l")
        {
            ADOrientation::signalOrientationChanged(DeviceOrientation::Landscape);
        }
        else
        {
            ADOrientation::signalOrientationChanged(DeviceOrientation::Portrait);
        }
    }

    static void init(const DeviceOrientation orient)
    {
        _obj = new OrientationKeyboard(orient);
    }

    static OrientationKeyboard* _obj;
};
OrientationKeyboard* OrientationKeyboard::_obj = nullptr;
void ADOrientation::enableRuntimeOrientation(const DeviceOrientation o)
{
    OrientationKeyboard::init(o);
}


#endif // ADORIENTATION_KEYBOARD_HPP
