#ifndef ADPLATFORM_DEFAULT_HPP
#define ADPLATFORM_DEFAULT_HPP
#include <ADLib/Device/ADInfo.h>

ADPlatform ADInfo::platformGetPlatform()
{
#ifdef AD_PLATFORM_CURRENT
    return AD_PLATFORM_CURRENT;
#else
    return ADPlatform::Undefined;
#endif
}

ADStore ADInfo::platformGetStore()
{
#ifdef AD_STORE_CURRENT
    return AD_STORE_CURRENT;
#else
    return ADStore::Undefined;
#endif
}
#endif // ADPLATFORM_DEFAULT_HPP
