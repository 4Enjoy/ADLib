#ifndef ADINAPP_EMULATOR_HPP
#define ADINAPP_EMULATOR_HPP
#include <ADLib/Device/ADInApp.h>
#include <ADLib/ADRandom.h>
void ADInApp::Platform::buyProduct(const Product& p)
{
    ADRandom rand;
    if(rand.getNumber(0,4))
    {
        Platform::purchaseSuccessful(p.getID(), OperationType::Purchased);
    }
    else
    {
        Platform::purchaseFailed(p.getID());
    }

}
void ADInApp::Platform::loadStore(const std::string&, const Mode)
{
    Platform::setStoreAvaliable(true);
}
void ADInApp::Platform::restorePurchases()
{
    Platform::restorePurchaseResult(true);
}

#endif // ADINAPP_EMULATOR_HPP
