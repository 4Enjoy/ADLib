#include <ADLib/Device/ADAds.h>

void ADAds::Platform::addTestDevice(const DeviceID& device)
{
#ifdef _DEBUG
    cocos2d::CCLog("Added device: %s", device.c_str());
#endif
}
#ifndef AD_ADS_NOSIZE
cocos2d::CCSize ADAds::Platform::getBannerSize(const BannerType&)
{
    return cocos2d::CCSize(100000, 100000);
}
#endif

bool ADAds::Platform::createBanner(const BannerID id, const BannerType& type, const UnitID &unit_id)
{
#ifdef _DEBUG
    cocos2d::CCLog("Create banner: %s,ID=%d UID=%s", type.c_str(),id, unit_id.c_str());
#endif
    return true;
}

void ADAds::Platform::showBanner(const BannerID id)
{
#ifdef _DEBUG
    cocos2d::CCLog("Show banner: ID=%d", id);
#endif
}

void ADAds::Platform::hideBanner(const BannerID id)
{
#ifdef _DEBUG
    cocos2d::CCLog("Hide banner: ID=%d", id);
#endif
}


void ADAds::Platform::isBannerReady(const BannerID id, const Callback& callback)
{
#ifdef _DEBUG
    callback(true, id);
#else
    callback(false, id);
#endif
}

void ADAds::Platform::moveBanner(const BannerID id, const cocos2d::CCPoint& position)
{
#ifdef _DEBUG
    cocos2d::CCLog("Banner moved: ID=%d, (%f, %f)", id, position.x, position.y);
#endif
}

void ADAds::Platform::showInterstitial()
{
#ifdef _DEBUG
    cocos2d::CCLog("Show interstitial");
#endif
}

void ADAds::Platform::prepareInterstitial(const UnitID &)
{
#ifdef _DEBUG
    cocos2d::CCLog("Prepare interstitial");
#endif
}

unsigned int ADAds::Platform::getInterstialTimesShowed()
{
    return 0;
}


