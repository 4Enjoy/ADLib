#ifndef ADSTATISTICS_NONE_HPP
#define ADSTATISTICS_NONE_HPP
#include <ADLib/Device/ADStatistics.h>
#include "cocos2d.h"

void ADStatistics::startSession()
{
    cocos2d::CCLog("ADStatistics: Session started");
}

void ADStatistics::stopSession()
{
    cocos2d::CCLog("ADStatistics: Session ended");
}

void ADStatistics::setApplicationKey(const std::string &application_id)
{
    cocos2d::CCLog("ADStatistics: Application key: %s", application_id.c_str());
}

void ADStatistics::logEvent(const std::string& name, const Params& p)
{
    cocos2d::CCLog("<Event '%s'", name.c_str());
    const Params::SVec& s_keys = p.getKeysString();
    const Params::SVec& s_values = p.getValuesString();

    if(s_keys.size() == s_values.size())
    {
        for(unsigned int i=0; i<s_keys.size(); ++i)
        {
            cocos2d::CCLog("-Param '%s'='%s'", s_keys[i].c_str(), s_values[i].c_str());
        }
    }

    cocos2d::CCLog("Event End>");
}
#endif // ADSTATISTICS_NONE_HPP
