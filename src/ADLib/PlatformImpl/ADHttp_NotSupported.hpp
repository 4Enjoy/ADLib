#ifndef ADHTTP_NOTSUPPORTED_HPP
#define ADHTTP_NOTSUPPORTED_HPP
#include <ADLib/Networking/ADHttp.h>

#include "cocos2d.h"
void ADHttp::makeRequest(const RequestType&,
                            const std::string& url,
                            const Params&,
                            const Callback& callback)
{
    cocos2d::CCLog("HTTP is not supported on this platform: %s",
                   url.c_str());

    ResponsePtr res = std::make_shared<Response>();
    res->setCode(-100);
    res->setIsOk(false);

    callback(res);
}

void ADHttp::platformDownloadFile(const std::string& url,
                                  const std::string& ,
                                  const Callback& callback,
                                  const ProgressCallback& )
{
    cocos2d::CCLog("HTTP is not supported on this platform: %s",
                   url.c_str());

    ResponsePtr res = std::make_shared<Response>();
    res->setCode(-100);
    res->setIsOk(false);

    callback(res);
}

#endif // ADHTTP_NOTSUPPORTED_HPP
