#ifndef ADFACEBOOK_EMULATOR_HPP
#define ADFACEBOOK_EMULATOR_HPP
#include <ADLib/Social/ADFacebook.h>
#ifdef FACEBOOK_INIT
class FacebookEmulator;
void FACEBOOK_INIT(FacebookEmulator*);
#endif
class FacebookEmulator : public ADFacebook::Platform
{
public:
    FacebookEmulator()
    {
        #ifdef FACEBOOK_INIT
        FACEBOOK_INIT(this);
        #endif
    }

    static std::shared_ptr<FacebookEmulator> getInstance()
    {
        if(_obj == nullptr)
            _obj = std::make_shared<FacebookEmulator>();
        return _obj;
    }

    /**
     * @brief How to implement
     * 1. Start session without GUI
     * 2. Load info about Me and friends
     * @param key
     */
    void loadData()
    {
        this->sessionOpened(token);
        if(me)
            this->myInfoLoaded(me);
        this->friendsLoaded(friends);
    }

    /**
     * @brief How to implement:
     * 1. If not logged in show log in window
     */
    void logIn()
    {

    }


    void logOut()
    {
        this->sessionClosed();
    }


    std::string token;
    ADFacebookFriendPtr me;
    ADFacebookFriendPtrArr friends;
    static ADFacebookFriendPtr createFriend(FacebookID id,
                                            const std::string& first_name,
                                            const std::string& last_name)
    {
        ADFacebookFriendPtr user = std::make_shared<ADFacebookFriend>();
        user->setUserID(id);
        user->setFirstName(first_name);
        user->setLastName(last_name);
        return user;
    }

    void share(const std::string &name,
               const std::string &caption,
               const std::string &description,
               const std::string &url,
               const std::string &picture,
               const ADFacebook::ShareResult &res)
    {
        res(true);
    }

private:

    static std::shared_ptr<FacebookEmulator> _obj;
};
std::shared_ptr<FacebookEmulator> FacebookEmulator::_obj = nullptr;
ADFacebook::PlatformPtr ADFacebook::getImplementation()
{
    return FacebookEmulator::getInstance();
}



#endif // ADFACEBOOK_EMULATOR_HPP
