#ifndef ADLANGUAGE_DEFAULT_HPP
#define ADLANGUAGE_DEFAULT_HPP
#include <ADLib/Device/ADLanguage.h>

std::string ADLanguage::platformGetDeviceLanguage()
{
#ifdef AD_LANGUAGE_CURRENT
    return AD_LANGUAGE_CURRENT;
#else
    return "en";
#endif
}

#endif // ADLANGUAGE_DEFAULT_HPP
