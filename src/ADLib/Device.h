#ifndef DEVICE_H
#define DEVICE_H
#include <ADLib/Device/ADAds.h>
#include <ADLib/Device/ADBrowser.h>
#include <ADLib/Device/ADInApp.h>
#include <ADLib/Device/ADInfo.h>
#include <ADLib/Device/ADLanguage.h>
#include <ADLib/Device/ADNotification.h>
#include <ADLib/Device/ADScreen.h>
#include <ADLib/Device/ADStatistics.h>
#include <ADLib/Device/ADVirtualCurrency.h>
#include <ADLib/Device/ADDeviceEvents.h>
#include <ADLib/Device/ADSoundManager.h>
#include <ADLib/Device/ADOrientation.h>
#include <ADLib/Device/ADFullScreenAds.h>
#include <ADLib/Device/ADMonetization.h>
#endif // DEVICE_H
