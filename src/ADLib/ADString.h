#ifndef ADSTRING_H
#define ADSTRING_H
#include <string>
#include <sstream>
template <class T>
std::string AD_to_string(const T& val)
{
    std::stringstream ss;
    ss << val;
    return ss.str();
}

#endif // ADSTRING_H
