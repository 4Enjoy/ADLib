#ifndef ADFULLSCREENADS_H
#define ADFULLSCREENADS_H
#include <ctime>

class ADFullScreenAds
{
public:
    /**
     * @brief How oftet full screen ads is shown
     * @param seconds
     */
    static void setInterval(int seconds);

    /**
     * @brief How much should grow interval
     * @param grow_each after how much banners should grow rate
     * @param rate how much should grow rate
     */
    static void setIntervalGrowthRate(float rate);

    /**
     * @brief Prepares the ads
     */
    static void prepare();

    /**
     * @brief Place in app where ads should be shown
     */
    static void showHere();
private:
    static void onWasShown();
    static bool shouldShow();
    static void checkReallyShownAds();

    static float _interval;
    static float _growth_rate;
    static time_t _last_shown;

    static time_t _last_try;
    static int _last_try_ads_shown;
};

#endif // ADFULLSCREENADS_H
