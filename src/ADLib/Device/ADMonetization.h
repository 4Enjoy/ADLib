#ifndef ADMONETIZATION_H
#define ADMONETIZATION_H
#include <string>
#include <ADLib/ADSignals.h>
enum class ADMonetizationPolicy
{
    NoAdsNoPurchase,
    AdsNoPurchase,
    AdsPurchase
};

class ADMonetization : public HasSlots
{
public:
    static const int BLOCK_PURCHASE_MADE = 0xAD00EF00;

    static StaticSignal<> signalPurchaseMade;

    static void configureAds(const std::string zone_key);
    static void configurePurchase(const std::string& purchase_id,
                                  const std::string& price,
                                  const std::string& store_key);

    static void usePolicy(const ADMonetizationPolicy);
    static bool isPurchaseMade();
    static bool canMakePurchase();
    static bool canShowAds();

    static void makePurchase();
    static void restorePurchase();
    static std::string getPrice();

    static void init();
private:
    static ADMonetization _instanse;

    ADMonetization();

    std::string _ads_zone_key;

    std::string _purchase_id;
    std::string _purchase_price;
    std::string _purchase_store_key;

    bool _ads_enabled;
    bool _purchase_enabled;

    void createBlocks();
    void setDefaultValues();
    void onPurchaseMade(const std::string purchase_id);
};

#endif // ADMONETIZATION_H
