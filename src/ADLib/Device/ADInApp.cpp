#include "ADInApp.h"
#include <ADLib/Device/ADStatistics.h>
#include <cassert>
#include "cocos2d.h"
ADInApp::Product::Product(
        const ProductID& id,
        const Price& base_price)
    : _id(id),
      _base_price(base_price)
{
}

const ADInApp::Price& ADInApp::Product::getPrice() const
{
    if(_real_price.size() != 0)
        return _real_price;
    else
        return _base_price;
}

void ADInApp::Product::setParameter(const std::string& key, const std::string& value)
{
    _params[key] = value;
}

bool ADInApp::Product::hasParameter(const std::string& key) const
{
    auto it = _params.find(key);

    if (it == _params.end())
        return false;
    if(it->second.size() != 0)
        return true;
    return false;
}
const std::string& ADInApp::Product::getParameter(const std::string& key) const
{
    return _params[key];
}

void ADInApp::setDelegate(DelegatePtr delegate)
{
    _delegate = delegate;
}

//Must be after all products are added
void ADInApp::loadStore(const Mode mode)
{
    assert(!_store_loaded);
    Platform::loadStore(_store_key, mode);
    _store_loaded = true;
}
void ADInApp::setStoreKey(const std::string& key)
{
    _store_key = key;
}

void ADInApp::addProduct(const Product& product)
{
    assert(!_store_loaded);
    _products.insert(ProductMap::value_type(product.getID(), product));
}

bool ADInApp::isStoreAvaliable()
{
    return _store_supported;
}

//Returns false if the product is not in store
bool ADInApp::buyProduct(const ProductID& id)
{
    if(_store_loaded)
    {
        const Product* p = getProduct(id);
        if(p == nullptr)
            return false;
        Platform::buyProduct(*p);
        return true;
    }
    return false;
}

void ADInApp::restorePurchases()
{
    Platform::restorePurchases();
}

const ADInApp::Product* ADInApp::getProduct(const ProductID& id)
{
    auto it = _products.find(id);
    if(it != _products.end())
        return &(it->second);
    return nullptr;
}
const ADInApp::ProductMap& ADInApp::getAllProducts()
{
    return _products;
}

ADInApp::DelegatePtr ADInApp::_delegate(nullptr);
ADInApp::ProductMap ADInApp::_products;

bool ADInApp::_store_loaded = false;
bool ADInApp::_store_supported = false;

std::string ADInApp::_store_key="";

void ADInApp::Platform::setRealPrice(const ProductID& id, const Price& price)
{
    auto it = _products.find(id);
    if(it != _products.end())
    {
        Product* p = &(it->second);
        p->setRealPrice(price);
        //cocos2d::CCLog("Price for %s is %s", id.c_str(), price.c_str());
    }
}

void ADInApp::Platform::purchaseSuccessful(const ProductID& id, const OperationType type)
{
    if(type == OperationType::Purchased)
        ADStatistics::logEvent("Purchase Successful", ADStatistics::Params().add("product", id));

    if(_delegate)
        _delegate->purchaseSuccessful(id);
    emit signalPurchaseSuccessful(id);
}

void ADInApp::Platform::purchaseFailed(const ProductID& id,
                                       const OperationType type,
                                       const ErrorType error_type)
{
    ADStatistics::Params params;
    params.add("product", id);
    params.add("operation", static_cast<int>(type));
    params.add("error", static_cast<int>(error_type));
    ADStatistics::logEvent("Purchase Failed", params);

    if(_delegate)
        _delegate->purchaseFailed(id, error_type);
    emit signalPurchaseFailed(id, error_type);
}

void ADInApp::Platform::restorePurchaseResult(bool res)
{
    if(res)
    {
        if(_delegate)
            _delegate->restorePurchasesSuccessfully();
        emit signalRestorePurchasesSuccessfully();
    }
    else
    {
        if(_delegate)
            _delegate->restorePurchasesFailed();
        emit signalRestorePurchasesFailed();
    }

}

void ADInApp::Platform::setStoreAvaliable(bool b)
{
    _store_supported = b;
//    if(b)
//        ADStatistics::logEvent("InApp", ADStatistics::Params().add("supported", "yes"));
//    else
//        ADStatistics::logEvent("InApp", ADStatistics::Params().add("supported", "no"));
}

StaticSignal<ADInApp::ProductID> ADInApp::signalPurchaseSuccessful;
StaticSignal<ADInApp::ProductID, ADInApp::ErrorType> ADInApp::signalPurchaseFailed;
StaticSignal<> ADInApp::signalRestorePurchasesSuccessfully;
StaticSignal<> ADInApp::signalRestorePurchasesFailed;



