#ifndef VERSION_ADSNOPURCHASE_H
#define VERSION_ADSNOPURCHASE_H
#include "../ADMonetizationVersionsSelector.hpp"

ADMonetizationPolicy ADMonetizationVersion::VERSION = ADMonetizationPolicy::AdsPurchase;

#endif // VERSION_ADSNOPURCHASE_H
