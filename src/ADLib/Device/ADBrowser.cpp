#include "ADBrowser.h"

const std::string ADBrowser::MAIL_PREFIX = "mailto:";
const std::string ADBrowser::OS_PERFIX = "os:";

bool hasPrefix(const std::string& s, const std::string& prefix)
{
    std::string real_prefix = s.substr(0, prefix.size());
    return real_prefix == prefix;
}

std::string removePrefix(const std::string& s, const std::string& prefix)
{
    return s.substr(prefix.size(), s.size() - prefix.size());
}

void ADBrowser::open(const std::string& url, const Fallback& f)
{
    if(hasPrefix(url, MAIL_PREFIX))
    {
        sendMail(removePrefix(url, MAIL_PREFIX), "", f);
    }
    else if(hasPrefix(url, OS_PERFIX))
    {
        openOSUrl(removePrefix(url, OS_PERFIX), f);
    }
    else
    {
        openWebURL(url, f);
    }
}
#include <ADLib/Device/ADStatistics.h>
#include <ADLib/Device/ADInfo.h>

void ADBrowser::openApplicationPage(const ADPackageName& package_name)
{
    ADPlatform platform = ADInfo::getPlatform();
    if(platform == ADPlatform::Android)
        openApplicationPage(package_name.getAndroidName());
    else if(platform == ADPlatform::iOS)
        openApplicationPage(package_name.getIOSName());
	else if (platform == ADPlatform::WindowsPhone)
		openApplicationPage(package_name.getWindowsPhoneName());
	else if (platform == ADPlatform::Windows8)
		openApplicationPage(package_name.getWindows8Name());
}

void ADBrowser::openApplicationPage(const std::string& package_id)
{
    ADStatistics::Params params;
    params.add("package", package_id);
    static const std::string event_name = "Open Application Page";
    static const std::string fail_name = "Open Application Page Failed";
    static const std::string no_support_name = "Application Page Not Supported";

    ADPlatform platform = ADInfo::getPlatform();
    ADStore store = ADInfo::getStore();

    std::string os_url = "";
    std::string web_url = "";

    if(platform == ADPlatform::Android)
    {
        if(store == ADStore::GooglePlay)
        {
            os_url = "market://details?id=" + package_id;
            web_url = "http://play.google.com/store/apps/details?id=" + package_id;
        }
        else if(store == ADStore::SamsungStore)
        {
            os_url = "samsungapps://ProductDetail/" + package_id;
            web_url = "http://www.samsungapps.com/appquery/appDetail.as?appId=" + package_id;
        }
        else if(store == ADStore::AmazonStore)
        {
            os_url = "amzn://apps/android?p=" + package_id;
            web_url = "http://www.amazon.com/gp/mas/dl/android?p=" + package_id;
        }
    }
    else if(platform == ADPlatform::iOS)
    {
        os_url = "https://itunes.apple.com/app/" + package_id;
    }
	else if(platform == ADPlatform::WindowsPhone)
	{
		os_url = "http://windowsphone.com/s?appId=" + package_id;
	}
	else if (platform == ADPlatform::Windows8)
	{
		os_url = "http://apps.microsoft.com/windows/app/" + package_id;
	}

    if(os_url.size() != 0)
    {
        ADStatistics::logEvent(event_name, params);
        //Try to open os URL
        ADBrowser::openOSUrl(os_url, [=](){
            //If failed try to open Web URL
            if(web_url.size() != 0)
                ADBrowser::openWebURL(web_url, [=](){
                    //Is still failed log this event
                    ADStatistics::logEvent(fail_name, params);
                });
        });
    }
    else
    {
        ADStatistics::logEvent(no_support_name, params);
    }


}

