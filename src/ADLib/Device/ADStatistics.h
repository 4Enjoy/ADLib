#ifndef ADSTATISTICS_H
#define ADSTATISTICS_H
#include <vector>
#include <string>
#include <sstream>
#include <ADLib/ADSignals.h>
#include <ADLib/Common/ADParams.h>

class ADStatistics : public HasSlots
{
public:
    typedef ADParams Params;

    static void setApplicationKey(const std::string& application_id);
    static void startSession();
    static void stopSession();
    static void logEvent(const std::string& name, const Params& p=Params());

private:
    ADStatistics();
    static ADStatistics _obj;
    void onPause();
    void onResume();
};
#endif // ADSTATISTICS_H
