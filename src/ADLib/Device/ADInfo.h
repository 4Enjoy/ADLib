#ifndef ADINFO_H
#define ADINFO_H
enum class ADPlatform
{
    Undefined=0,
    Android=1,
    iOS=2,
    Windows8=3,
    WindowsPhone=4
};

enum class ADStore
{
    Undefined=0,
    GooglePlay=1,
    SamsungStore=2,
    AmazonStore=3,
    iTunes=4,
    WindowsStore=5,
    WindowsPhoneStore=6,
    StoreLess=7
};

class ADInfo
{
public:
    static ADPlatform getPlatform()
    {
        static ADPlatform res = doGetPlatform();
        return res;
    }

    static ADStore getStore()
    {
        static ADStore res = doGetStore();
        return res;
    }

private:
    static ADPlatform doGetPlatform();
    static ADStore doGetStore();

    //Platform dependent
    static ADPlatform platformGetPlatform();
    static ADStore platformGetStore();
};

#endif // ADINFO_H
