#ifndef ADDEVICEID_H
#define ADDEVICEID_H
#include <string>
class ADDeviceID
{
public:
    /**
     * @brief Returns the unique device ID
     * @return
     */
    static std::string getDeviceID();
};

#endif // ADDEVICEID_H
