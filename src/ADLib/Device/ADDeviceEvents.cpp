#include <ADLib/Device/ADDeviceEvents.h>
#include "cocos2d.h"


void ADDeviceEvents::closeApp()
{
    emit signalOnClose();
    cocos2d::CCDirector::sharedDirector()->end();
}

void ADDeviceEvents::onPause()
{
    emit signalOnPause();
}

void ADDeviceEvents::onResume()
{
    emit signalOnResume();
}
void ADDeviceEvents::clickBack()
{
    emit signalOnBackClicked();
}

StaticSignal<> ADDeviceEvents::signalOnClose;
StaticSignal<> ADDeviceEvents::signalOnPause;
StaticSignal<> ADDeviceEvents::signalOnResume;
StaticSignal<> ADDeviceEvents::signalOnBackClicked;
