#include "ADMonetization.h"
#include <ADLib/Storage.h>
#include <ADLib.h>
ADMonetization::ADMonetization()
    : _ads_enabled(false),
      _purchase_enabled(false)
{
}

void ADMonetization::configureAds(const std::string zone_key)
{
    _instanse._ads_zone_key = zone_key;
}

void ADMonetization::configurePurchase(
        const std::string& purchase_id,
        const std::string& price,
        const std::string& store_key)
{
    _instanse._purchase_id = purchase_id;
    _instanse._purchase_price = price;
    _instanse._purchase_store_key = store_key;
}

void ADMonetization::usePolicy(const ADMonetizationPolicy policy)
{
    if(policy == ADMonetizationPolicy::AdsNoPurchase)
    {
        _instanse._ads_enabled = true;
        _instanse._purchase_enabled = false;
    }
    else if(policy == ADMonetizationPolicy::AdsPurchase)
    {
        _instanse._ads_enabled = true;
        _instanse._purchase_enabled = true;
    }
    else if(policy == ADMonetizationPolicy::NoAdsNoPurchase)
    {
        _instanse._ads_enabled = false;
        _instanse._purchase_enabled = false;
    }
}

void ADMonetization::init()
{
    //Add blocks to storage
    CONNECT(ADStorage::signalCreateBlocks, &_instanse,
                   &ADMonetization::createBlocks);

    //Check if first launch and add bonuses
    CONNECT(ADStorage::signalInitialDataLoaded, &_instanse,
                   &ADMonetization::setDefaultValues);

    CONNECT(ADInApp::signalPurchaseSuccessful, &_instanse,
                   &ADMonetization::onPurchaseMade);

    //Init purchases
    typedef ADInApp::Product Product;
    Product disable_ads(_instanse._purchase_id, _instanse._purchase_price);
    ADInApp::setStoreKey(_instanse._purchase_store_key);
    ADStore store = ADInfo::getStore();
    if(store == ADStore::GooglePlay)
    {
        disable_ads.setParameter("type","non-consumable");
    }
    ADInApp::addProduct(disable_ads);

}

void ADMonetization::createBlocks()
{
    ADStorage::createValueBlock<uint16_t>(BLOCK_PURCHASE_MADE);
}

void ADMonetization::setDefaultValues()
{
    //Init ads
    if(canShowAds())
    {
        ADAds::registerInterstitialType(_instanse._ads_zone_key);
        ADFullScreenAds::prepare();
    }
    else
    {
        ADAds::disableAds();
    }

    //Load store
    ADInApp::loadStore(ADInApp::Mode::Production);
}


bool ADMonetization::isPurchaseMade()
{
    return ADStorage::getValue<uint16_t>(BLOCK_PURCHASE_MADE, 0) != 0;
}

bool ADMonetization::canMakePurchase()
{
    return _instanse._purchase_enabled && !isPurchaseMade();
}

bool ADMonetization::canShowAds()
{
    return _instanse._ads_enabled && !isPurchaseMade();
}

void ADMonetization::makePurchase()
{
    ADInApp::buyProduct(_instanse._purchase_id);
}

void ADMonetization::onPurchaseMade(const std::string purchase_id)
{
    if(purchase_id == _instanse._purchase_id)
    {
        if(!isPurchaseMade())
        {
            if(canShowAds())
            {
                ADAds::disableAds();
            }
            ADStorage::setValue<uint16_t>(BLOCK_PURCHASE_MADE, 1);
            emit signalPurchaseMade();
        }
    }
}

std::string ADMonetization::getPrice()
{
    return ADInApp::getProduct(_instanse._purchase_id)->getPrice();
}

void ADMonetization::restorePurchase()
{
    ADInApp::restorePurchases();
}

ADMonetization ADMonetization::_instanse;
StaticSignal<> ADMonetization::signalPurchaseMade;
