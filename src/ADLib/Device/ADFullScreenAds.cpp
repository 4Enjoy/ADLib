#include "ADFullScreenAds.h"
#include <ADLib/Device/ADAds.h>



void ADFullScreenAds::setInterval(int seconds)
{
    _interval = seconds;
}


void ADFullScreenAds::setIntervalGrowthRate(float rate)
{
    _growth_rate = rate;
}


void ADFullScreenAds::prepare()
{
    ADAds::prepareInterstitial();
}


void ADFullScreenAds::checkReallyShownAds()
{
    int shown = ADAds::getInterstialTimesShowed();

    //Check if number of shown ads changed
    if(shown > _last_try_ads_shown)
    {
        _last_try_ads_shown = shown;
        _last_shown = _last_try;
    }
}

void ADFullScreenAds::showHere()
{
    prepare();
    checkReallyShownAds();

    if(shouldShow())
    {
        _last_try = time(0);
        ADAds::showInterstitial();
    }
}

void ADFullScreenAds::onWasShown()
{
    _last_shown = _last_try;
    _interval *= _growth_rate;
}

bool ADFullScreenAds::shouldShow()
{
    return time(0) - _last_shown > _interval;
}

float ADFullScreenAds::_interval = 30;

float ADFullScreenAds::_growth_rate = 1.08f;

time_t ADFullScreenAds::_last_shown = 0;

time_t ADFullScreenAds::_last_try = 0;
int ADFullScreenAds::_last_try_ads_shown = 0;

