#include "ADStatistics.h"
#include <ADLib/Device/ADDeviceEvents.h>
ADStatistics ADStatistics::_obj;

ADStatistics::ADStatistics()
{
    CONNECT(ADDeviceEvents::signalOnClose, this,
            &ADStatistics::onPause);
    CONNECT(ADDeviceEvents::signalOnPause, this,
            &ADStatistics::onPause);
    CONNECT(ADDeviceEvents::signalOnResume, this,
            &ADStatistics::onResume);
}

void ADStatistics::onPause()
{
    stopSession();
}

void ADStatistics::onResume()
{
    startSession();
}
