#ifndef ADMONETIZATIONVERSIONSSELECTOR_HPP
#define ADMONETIZATIONVERSIONSSELECTOR_HPP
#include "ADMonetization.h"

class ADMonetizationVersion
{
public:
    static ADMonetizationPolicy VERSION;
};


#endif // ADMONETIZATIONVERSIONSSELECTOR_HPP
