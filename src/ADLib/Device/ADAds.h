#ifndef ADADS_H
#define ADADS_H
#include <string>
#include "cocos2d.h"

#include "ADLib/Generic/ADCreate.h"
#include "ADLib/Generic/ADMethods.h"
#include "ADLib/ADSignals.h"
#include <functional>
class ADAds
{
public:
    typedef std::string UnitID;
    typedef std::string DeviceID;
    typedef std::string BannerType;
    typedef std::string InterstitialType;
    typedef int ADAdsBannerID;
    typedef ADAdsBannerID BannerID;

    /**
     * @brief Type for action which will be preformed
     * when custom banner is clicked
     */
    typedef std::function<void ()> BannerClickAction;

    typedef std::function<void(bool, BannerID)> Callback;


    class Banner: public ADCreate<cocos2d::CCNode, Banner>
    {
    public:
        Banner();

        AD_SelectorModifier(ADAdsBannerID, _id, ID);
        bool isUsed();
        void onEnter();
        void onExit();

        void showAds();
        void hideAds();
    private:

        void updatePosition();
        ADAdsBannerID _id;
    };


    static void addTestDevice(const DeviceID& device);

    static void registerBannerType(const BannerType& type,
                            const UnitID& unit_id);

    static void registerInterstitialType(const UnitID& unit_id);

    static Banner* getBanner(const cocos2d::CCSize& empty_space);

    static void prepareInterstitial();
    static void showInterstitial();
    static unsigned int getInterstialTimesShowed();

    static void disableAds();
    static void hideAllAds();

    static void setArgument(const std::string& key, const std::string& value);
    static std::string getArgument(const std::string& key);

    static StaticSignal<> signalBeforeAdsInit;


    /**
     * @brief Add banner for home ads
     * The banner will be displayed in two cases:
     * - when the ads provider banner is not yet loaded
     * - when the ads provider do not work now
     *
     * You can add as many banners as you want.
     * If there are multiple banners then each request another banner will be shown
     *
     * If there are no custom banner then in described cases no banner will be shown at all
     * @param banner CCNode which contains content which should be used as a banner
     * it can be CCSprite or any other reach content.
     * If the content do not fit the banner zone size then it will be scaled to fit this zone
     *
     * @param action the action which will performed when user clicks on banner
     * @param banner_id if not empty then when user click on banner
     * new ADStatistics action with name "Banner Click" will logged and
     * this banner_id will be added as parameter
     */
    static void addCustomBanner(cocos2d::CCNode* banner,
                                const BannerClickAction& action,
                                const std::string& banner_id="");

private:
    static bool _is_inited;
    static void init();
    /**
     * @brief Class which store info needed to show home banner
     */
    class CustomBanner : public cocos2d::CCObject
    {
    public:
        /**
         * @brief @see ADAds::addCustomBanner
         * @param banner will be retained
         * @param action
         * @param banner_id
         */
        CustomBanner(cocos2d::CCNode* banner,
                     const BannerClickAction& action,
                     const std::string& banner_id);
        ~CustomBanner();

        AD_Selector(cocos2d::CCNode*, _banner, Banner);
        AD_Selector(const BannerClickAction&, _action, Action);
        AD_Selector(const std::string&, _id, ID);

        /**
         * @brief This method will be called when user clicks on the banner
         */
        void onClick(cocos2d::CCObject*);
    private:
        cocos2d::CCNode* _banner;
        BannerClickAction _action;
        std::string _id;
    };

    /**
     * @brief List of Home banners to display if normal are not avaliable
     */
    static std::vector<CustomBanner*> _home_banners;

    /**
     * @brief Finds the first unused banner
     * @return If there is no such banners the nullptr is returned
     */
    static CustomBanner *getCustomBanner();

    static ADAds _instance;
    ADAds();
    void do_addTestDevice(const DeviceID& device);

    void do_registerBannerType(const BannerType& type,
                            const UnitID& unit_id);

    void do_registerInterstitialType(const UnitID& unit_id);

    Banner* do_getBanner(const cocos2d::CCSize& empty_space);

    void do_prepareInterstitial();
    void do_showInterstitial();
    unsigned int do_getInterstialTimesShowed();

    void do_disableAds();
    void do_hideAllAds();


    class BannerInfo
    {
    public:
        BannerInfo(BannerID id,
                   Banner* banner,
                   const cocos2d::CCSize& size,
                   const UnitID&,
                   const BannerType&);

        AD_Selector(BannerID, _id, ID);
        AD_Selector(const cocos2d::CCSize, _size, Size);
        AD_SelectorModifier(Banner*, _banner, Banner);
        AD_Selector(const UnitID&, _unit_id, UnitID);
        AD_Selector(const BannerType&, _banner_type, BannerType);
        AD_SelectorModifier(bool, _is_ready, IsReady);
        AD_SelectorModifier(bool, _is_loaded, IsLoaded);
    private:
        BannerID _id;
        cocos2d::CCSize _size;
        bool _is_ready;
        bool _is_loaded;
        Banner* _banner;
        UnitID _unit_id;
        BannerType _banner_type;
    };

    class InterstitialInfo
    {
    public:
        InterstitialInfo(const UnitID&);

        AD_Selector(const UnitID&, _unit_id, UnitID);
    private:

        UnitID _unit_id;
    };

    std::vector<BannerInfo> _banners;
    std::vector<InterstitialInfo> _interstitials;
    bool _ads_enabled;


    void loadBanners();
    void fillBanner(Banner*);
    void isBannerReadyCallback(bool, BannerID);

    class Platform
    {
    public:
        static void addTestDevice(const DeviceID& device);
        static cocos2d::CCSize getBannerSize(const BannerType& type);
        static bool createBanner(const BannerID, const BannerType& type, const UnitID& id);
        static void showBanner(const BannerID);
        static void hideBanner(const BannerID);


        static void isBannerReady(const BannerID, const Callback&);
        static void moveBanner(const BannerID, const cocos2d::CCPoint& position);

        static void showInterstitial();
        static void prepareInterstitial(const UnitID&);
        static unsigned int getInterstialTimesShowed();

    };
};

#endif // ADADS_H
