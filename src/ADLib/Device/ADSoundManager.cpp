#include "ADSoundManager.h"
#include "SimpleAudioEngine.h"
#include <ADLib/Device/ADDeviceEvents.h>

ADSoundManager::ADSoundManager()
{
    CONNECT(ADDeviceEvents::signalOnPause,
            this, &ADSoundManager::onPause);
    CONNECT(ADDeviceEvents::signalOnResume,
            this, &ADSoundManager::onResume);
}




void ADSoundManager::turnOnSound()
{
    _sound_on = true;
}

void ADSoundManager::turnOffSound()
{
    _sound_on = false;
}

void ADSoundManager::turnOnMusic()
{
    _music_on = true;
}

void ADSoundManager::turnOffMusic()
{
    if(_music_on)
    {
        _music_on = false;
        CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
    }
}

bool ADSoundManager::isMusicTurnedOn()
{
    return _music_on;
}

bool ADSoundManager::isSoundTurnedOn()
{
    return _sound_on;
}

void ADSoundManager::pauseMusic()
{
    if(_music_on)
    {
        CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
    }
}

void ADSoundManager::playMusic(const std::string& name_in)
{
    if(_music_on)
    {
		std::string name = name_in;
		makeWavFileExtensionOnWP8(name);
        if(_background_music_file != name ||
                CocosDenshion::SimpleAudioEngine::sharedEngine()->isBackgroundMusicPlaying()==false)
        {
            _background_music_file = name;
            CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic
                                    (_background_music_file.c_str(), true);
        }
    }
}

void ADSoundManager::playSoundEffect(const std::string& name_in)
{
    if(_sound_on)
    {
		std::string name = name_in;
		makeWavFileExtensionOnWP8(name);
        CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect(name.c_str());
    }
}
ADSoundManager ADSoundManager::_obj;
bool ADSoundManager::_sound_on = true;
bool ADSoundManager::_music_on = true;
std::string ADSoundManager::_background_music_file="";

void ADSoundManager::onPause()
{
    CocosDenshion::SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
}

void ADSoundManager::onResume()
{
    CocosDenshion::SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
}
