#include "ADAds.h"
#include "ADLib/Device/ADScreen.h"
#include "ADLib/ADRandom.h"
#include <algorithm>
#include "ADStatistics.h"
using namespace cocos2d;
/**
 * @brief All home banners will be tagged with this tag
 */
static int HOME_ADS_NODE_TAG = 0x05600;


void ADAds::addTestDevice(const DeviceID& device)
{
    _instance.do_addTestDevice(device);
}

void ADAds::registerBannerType(const BannerType& type,
                               const UnitID& unit_id)
{
    _instance.do_registerBannerType(type, unit_id);
}

void ADAds::registerInterstitialType(const UnitID& unit_id)
{
    _instance.do_registerInterstitialType(unit_id);
}

ADAds::Banner* ADAds::getBanner(const cocos2d::CCSize& empty_space)
{
    init();
    return _instance.do_getBanner(empty_space);
}

void ADAds::prepareInterstitial()
{
    _instance.do_prepareInterstitial();
}

void ADAds::showInterstitial()
{
    _instance.do_showInterstitial();
}

unsigned int ADAds::getInterstialTimesShowed()
{
    return _instance.do_getInterstialTimesShowed();
}

void ADAds::disableAds()
{
    _instance.do_disableAds();
}

void ADAds::hideAllAds()
{
    _instance.do_hideAllAds();
}

ADAds ADAds::_instance;

ADAds::Banner::Banner()
    : _id(0)
{}

bool ADAds::Banner::isUsed()
{
    return this->getParent() != nullptr;
}

void ADAds::Banner::onEnter()
{
    CCNode::onEnter();
    showAds();
}

void ADAds::Banner::updatePosition()
{
    CCPoint position = this->getPosition();
    CCSize size = this->getContentSize();
    CCPoint anchor = this->getAnchorPoint();

    CCPoint origin = ADScreen::getOrigin();
    CCSize frameSize = CCEGLView::sharedOpenGLView()->getFrameSize();
    CCSize visibleSize = ADScreen::getVisibleSize();

    float scale = frameSize.width / visibleSize.width;

    position.x -= size.width * anchor.x + origin.x;
    position.y += size.height * (1 - anchor.y) - origin.y;

    CCPoint screen_point(position.x * scale,
                         (visibleSize.height - position.y) * scale);

    ADAds::Platform::moveBanner(_id, screen_point);
}

void ADAds::Banner::onExit()
{
    CCNode::onExit();
    hideAds();
}

void ADAds::Banner::showAds()
{
    updatePosition();
    ADAds::Platform::showBanner(_id);
    this->setVisible(true);
}

void ADAds::Banner::hideAds()
{

    ADAds::Platform::hideBanner(_id);
    this->setVisible(false);
}


ADAds::ADAds()
    : _ads_enabled(true)
{
}



void ADAds::do_addTestDevice(const DeviceID& device)
{
    ADAds::Platform::addTestDevice(device);
}

void ADAds::do_registerBannerType(const BannerType& type,
                                  const UnitID& unit_id)
{

    CCSize frameSize = CCEGLView::sharedOpenGLView()->getFrameSize();
    CCSize visibleSize = ADScreen::getVisibleSize();

    float scale =  visibleSize.width / frameSize.width;

    CCSize banner_size = ADAds::Platform::getBannerSize(type);
    BannerInfo info(_banners.size()+1,
                    0,
                    CCSize(banner_size.width*scale,
                           banner_size.height*scale),
                    unit_id,
                    type);
    _banners.push_back(info);
}

void ADAds::do_registerInterstitialType(const UnitID& unit_id)
{
    _interstitials.push_back(InterstitialInfo(unit_id));
}

ADAds::Banner* ADAds::do_getBanner(const cocos2d::CCSize& empty_space)
{
    Banner* res = nullptr;
    if(_ads_enabled)
    {
        float max_area = 0;
        BannerInfo* max = nullptr;
        for(unsigned int i=0; i<_banners.size(); ++i)
        {
            BannerInfo& info = _banners[i];

            float area = info.getSize().width * info.getSize().height;
            if(info.getSize().width <= empty_space.width &&
                    info.getSize().height <= empty_space.height &&
                    area > max_area)
            {
                max_area = area;
                max = &info;
            }
        }

        if(max)
        {
            if(max->getIsReady())
            {
                res = max->getBanner();

                //Cleare home ads
                if(res)
                {
                   CCNode* home_ads = res->getChildByTag(HOME_ADS_NODE_TAG);
                   if(home_ads && home_ads->getParent())
                       home_ads->removeFromParent();
                }
            }
            else
            {
                if(!max->getIsLoaded())
                {
                    bool created = Platform::createBanner(max->getID(),
                                                          max->getBannerType(),
                                                          max->getUnitID());
                    if(created)
                    {
                        Banner* banner = Banner::create();
                        banner->setID(max->getID());
                        banner->retain();
                        max->setBanner(banner);

                        banner->setContentSize(max->getSize());

                        fillBanner(banner);
                        max->setIsLoaded(true);
                    }
                }
                if(max->getIsLoaded())
                    Platform::isBannerReady(
                                max->getID(),
                                [this](bool b, BannerID id){
                        this->isBannerReadyCallback(b, id);
                    });

                //Show home ads
                if(_home_banners.size() > 0)
                {

                    if(max->getBanner())
                    {
                        Banner* banner = max->getBanner();
                        if(banner->getChildByTag(HOME_ADS_NODE_TAG))
                        {
                            //The ads is attached
                            res = banner;
                        }
                    }
                }
            }
        }
    }
    if(res)
    {
        if(res->getParent() != nullptr)
            res->removeFromParent();
    }
    return res;
}
void ADAds::fillBanner(Banner* banner)
{
#if defined(_DEBUG) && CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
    CCRenderTexture* texture = CCRenderTexture::create(100, 100);
    texture->beginWithClear(0.5f, 0.5f, 0.5f, 1);

    texture->end();

    CCSprite* s = CCSprite::createWithTexture(texture->getSprite()->getTexture());

    s->setScaleX(banner->getContentSize().width/s->getContentSize().width);
    s->setScaleY(banner->getContentSize().height/s->getContentSize().width);
    banner->addChild(s);
    s->setAnchorPoint(ccp(0,0));
    s->setPosition(ccp(0,0));
#endif

    if(_home_banners.size())
    {
        //std::random_shuffle(_home_banners.begin(), _home_banners.end());
        CustomBanner* home_ads = getCustomBanner();

        CCMenuItem* item = CCMenuItem::create(
                    home_ads, menu_selector(CustomBanner::onClick));


        CCMenu* menu = CCMenu::create();
        menu->addChild(item);

        banner->addChild(menu, 0, HOME_ADS_NODE_TAG);
        menu->setAnchorPoint(ccp(0,0));

        menu->setPosition(ccp(0,0));

        CCNode* banner_content = home_ads->getBanner();
        CCSize content_size = banner_content->getContentSize();
        CCSize zone_size = banner->getContentSize();

        float scale = MIN(zone_size.width/content_size.width,
                          zone_size.height/content_size.height);
        banner_content->setScale(scale);
        banner_content->setAnchorPoint(ccp(0, 0));
        banner_content->setPosition(ccp(0,0));

        item->setContentSize(content_size*scale);
        item->setAnchorPoint(ccp(0.5f,0.5f));
        item->setPosition(zone_size*0.5f);
        item->addChild(banner_content);
    }
}

void ADAds::do_prepareInterstitial()
{
    if(_ads_enabled)
    {
        init();
        if(_interstitials.size() > 0)
        {
            Platform::prepareInterstitial(_interstitials[0].getUnitID());
        }
    }
}

StaticSignal<> ADAds::signalBeforeAdsInit;
bool ADAds::_is_inited = false;
static std::map<std::string, std::string> _ads_map;

void ADAds::setArgument(const std::string& key, const std::string& value)
{
    _ads_map[key] = value;
}

std::string ADAds::getArgument(const std::string& key)
{
    return _ads_map[key];
}

void ADAds::init()
{
    if(!_is_inited)
    {
        _is_inited = true;
        emit signalBeforeAdsInit();
    }
}

void ADAds::do_showInterstitial()
{
    if(_ads_enabled)
    {
        init();
        Platform::showInterstitial();
    }
}
void ADAds::do_disableAds()
{
    _ads_enabled = false;
    CCLog("Ads disabled");
    do_hideAllAds();
}

unsigned int ADAds::do_getInterstialTimesShowed()
{
    return Platform::getInterstialTimesShowed();
}

void ADAds::do_hideAllAds()
{
    for(unsigned int i=0; i<_banners.size(); ++i)
    {
        BannerInfo& info = _banners[i];
        if(info.getBanner())
        {
            if(info.getBanner()->getParent())
                info.getBanner()->removeFromParent();
        }
    }
}

ADAds::BannerInfo::BannerInfo(BannerID id,
                              Banner* banner,
                              const cocos2d::CCSize& size,
                              const UnitID& unit, const BannerType & type)
    : _id(id),
      _banner(banner),
      _size(size),
      _is_ready(false),
      _is_loaded(false),
      _unit_id(unit),
      _banner_type(type)
{}

ADAds::InterstitialInfo::InterstitialInfo(const UnitID& id)
    : _unit_id(id)
{}

void ADAds::isBannerReadyCallback(bool val, BannerID id)
{
    if(val)
    {
        for(unsigned int i=0; i<_banners.size(); ++i)
        {
            BannerInfo& info = _banners[i];
            if(info.getID() == id)
            {
                info.setIsReady(true);
            }
        }
    }
}
std::vector<ADAds::CustomBanner*> ADAds::_home_banners;

ADAds::CustomBanner::CustomBanner(cocos2d::CCNode* banner,
                                  const BannerClickAction& action,
                                  const std::string& banner_id)
    : _banner(banner),
      _action(action),
      _id(banner_id)
{
    _banner->retain();
}

ADAds::CustomBanner::~CustomBanner()
{
    if(_banner)
        _banner->release();
}

ADAds::CustomBanner* ADAds::getCustomBanner()
{
    CustomBanner* res = nullptr;

    ADRandom r;
    int num = r.getNumber(0, _home_banners.size()-1);

    CustomBanner* banner = _home_banners[num];

    if(banner->getBanner()->getParent() == nullptr)
    {
        res = banner;
    }


    return res;
}

void ADAds::addCustomBanner(cocos2d::CCNode* banner,
                            const BannerClickAction& action,
                            const std::string& banner_id)
{
    //The custom banner info will be never deleted (it lives all the time)
    CustomBanner* home_ads = new CustomBanner(banner, action, banner_id);

    _home_banners.push_back(home_ads);
}

void ADAds::CustomBanner::onClick(cocos2d::CCObject*)
{
    if(_id.size() != 0)
    {
        ADStatistics::Params params;
        params.add("id", _id);

        ADStatistics::logEvent("Banner Clicked", params);
    }
    if(_action)
        _action();
}
