#ifndef ADDEVICEEVENTS_H
#define ADDEVICEEVENTS_H
#include <ADLib/ADSignals.h>
class ADDeviceEvents
{
public:
    static StaticSignal<> signalOnClose;
    static StaticSignal<> signalOnPause;
    static StaticSignal<> signalOnResume;
    static StaticSignal<> signalOnBackClicked;

    static void closeApp();
    static void clickBack();
    static void onPause();
    static void onResume();
};

#endif // ADDEVICEEVENTS_H
