#ifndef ADPACKAGENAME_H
#define ADPACKAGENAME_H
#include <string>
#include <ADLib/Generic/ADMethods.h>

/**
 * @brief Class which stores package names for each platform
 * Android: com.x4enjoy.mathisfun
 * iOS: id657096722
 * Windows Phone: 4a78dd6e-321f-4b21-829c-5b35490671c9
 */
class ADPackageName
{
public:
    ADPackageName();

    static ADPackageName create(const std::string& android_name="",
                                const std::string& ios_name="",
                                const std::string& windows_phone_name="",
								const std::string& windows_8_name="");

    AD_SelectorModifier(const std::string&, _android, AndroidName);
    AD_SelectorModifier(const std::string&, _ios, IOSName);
    AD_SelectorModifier(const std::string&, _windows_phone, WindowsPhoneName);
	AD_SelectorModifier(const std::string&, _windows8_name, Windows8Name);
private:
    std::string _android;
    std::string _ios;
    std::string _windows_phone;
	std::string _windows8_name;
};

#endif // ADPACKAGENAME_H
