#ifndef ADSOUNDMANAGER_H
#define ADSOUNDMANAGER_H
#include <ADLib/ADSignals.h>
#include <ADLib/Device/ADInfo.h>
#include <string>
class ADSoundManager : public HasSlots
{
public:
    static void turnOnSound();
    static void turnOffSound();

    static void turnOnMusic();
    static void turnOffMusic();

    static bool isMusicTurnedOn();
    static bool isSoundTurnedOn();

    static void playMusic(const std::string& name);
    static void pauseMusic();
    static void playSoundEffect(const std::string& name);
private:
    static bool _sound_on;
    static bool _music_on;
    static std::string _background_music_file;
    ADSoundManager();
    void onPause();
    void onResume();

    static ADSoundManager _obj;
};

inline void makeWavFileExtensionOnWP8(std::string& file)
{
	static bool res = ADInfo::getPlatform() == ADPlatform::WindowsPhone;
	if (res)
	{
		size_t length = file.size();
		file[length - 3] = 'w';
		file[length - 2] = 'a';
		file[length - 1] = 'v';
	}
}

#include "cocos2d.h"

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
#define MUSIC_EXT ".mp3"
#endif

#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
#define MUSIC_EXT ".ogg"
#endif

#if CC_TARGET_PLATFORM == CC_PLATFORM_WINRT
#define MUSIC_EXT ".mp3"
#endif

#ifndef MUSIC_EXT
#define MUSIC_EXT ".wav"
#endif

#endif // ADSOUNDMANAGER_H
