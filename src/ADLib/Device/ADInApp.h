#ifndef ADINAPP_H
#define ADINAPP_H
#include <string>
#include <map>
#include <ADLib/Generic/ADMethods.h>
#include <memory>
#include <map>
#include <ADLib/ADSignals.h>
class ADInApp
{
public:
    typedef std::string ProductID;
    typedef std::string Price;

    enum class OperationType
    {
        Undefined,
        Purchased,
        Restored
    };

    enum class ErrorType
    {
        None=0,
        UserCanceled=1,
        BillingUnavaliable=3,
        DeveloperError=5,
        Error=6
    };

    enum class Mode
    {
        Production=0,
        TestSuccess=1,
        TestFailed=2
    };

    class Product
    {
    public:

        Product(const ProductID& id,
                const Price& base_price);

        AD_SelectorModifier(const Price&, _base_price, BasePrice);
        AD_Modifier(const Price&, _real_price, RealPrice);
        AD_Selector(const ProductID&, _id, ID);

        const Price& getPrice() const;

        void setParameter(const std::string& key, const std::string& value);
        bool hasParameter(const std::string& key) const;
        const std::string& getParameter(const std::string& key) const;
    private:
        typedef std::map<std::string, std::string> Params;
        Price _base_price;
        Price _real_price;
        ProductID _id;
        mutable Params _params;

    };
    typedef std::map<ProductID, Product> ProductMap;


    class Delegate
    {
    public:
        virtual ~Delegate()
        {}
        virtual void purchaseSuccessful(const ProductID&)=0;
        virtual void purchaseFailed(const ProductID&, const ErrorType)=0;
        virtual void restorePurchasesSuccessfully()=0;
        virtual void restorePurchasesFailed()=0;
    };

    static StaticSignal<ProductID> signalPurchaseSuccessful;
    static StaticSignal<ProductID, ErrorType> signalPurchaseFailed;
    static StaticSignal<> signalRestorePurchasesSuccessfully;
    static StaticSignal<> signalRestorePurchasesFailed;


    typedef std::shared_ptr<Delegate> DelegatePtr;

    //Must be called first
    static void setDelegate(DelegatePtr delegate);
    static void setStoreKey(const std::string& key);

    //Must be after all products are added
    static void loadStore(const Mode = Mode::Production);

    static void addProduct(const Product& product);
    static bool isStoreAvaliable();

    static void restorePurchases();


    //Returns false if the product is not in store
    static bool buyProduct(const ProductID& id);

    static const Product* getProduct(const ProductID& id);
    static const ProductMap& getAllProducts();

    class Platform
    {
    public:
        //Can be used in platform implemenation
        static void setRealPrice(const ProductID&, const Price&);
        static void purchaseSuccessful(const ProductID&, const OperationType = OperationType::Undefined);
        static void purchaseFailed(const ProductID&, const OperationType = OperationType::Undefined, const ErrorType = ErrorType::None);
        static void setStoreAvaliable(bool);
        static void restorePurchaseResult(bool);

        //Must be implemented
        static void buyProduct(const Product& p);
        static void loadStore(const std::string& store_key, const Mode);
        static void restorePurchases();
    };
private:
    ADInApp();


    static DelegatePtr _delegate;
    static ProductMap _products;

    static bool _store_loaded;
    static bool _store_supported;
    static std::string _store_key;
};

#endif // ADINAPP_H
