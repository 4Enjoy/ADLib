#include "ADVirtualCurrency.h"
#include "cocos2d.h"
#include <ADLib/Device/ADDeviceEvents.h>
ADVirtualCurrency::DelegatePtr ADVirtualCurrency::_delegate = nullptr;
ADVirtualCurrency::PlatformPtr ADVirtualCurrency::_implementation = nullptr;
bool ADVirtualCurrency::_is_init = false;
bool ADVirtualCurrency::_is_supported = false;

void ADVirtualCurrency::initProvider(const std::string key)
{
    if(!_is_init)
    {

        _implementation = getImplementation();
        if(_implementation)
        {
            CONNECT(ADDeviceEvents::signalOnPause,
                    &_obj, &ADVirtualCurrency::_onPause);
            CONNECT(ADDeviceEvents::signalOnClose,
                    &_obj, &ADVirtualCurrency::_onPause);
            CONNECT(ADDeviceEvents::signalOnResume,
                    &_obj, &ADVirtualCurrency::_onResume);
            cocos2d::CCLog("ADVirtualCurrency init");
            _is_init = true;
            _implementation->init(key);
        }
        else
        {
            cocos2d::CCLog("ADVirtualCurrency platform implementation is not set, %d", _implementation.get());
        }
    }
}
ADVirtualCurrency ADVirtualCurrency::_obj;
void ADVirtualCurrency::_onPause()
{
    onPause();
}

void ADVirtualCurrency::_onResume()
{
    onResume();
}

void ADVirtualCurrency::setDelegate(DelegatePtr delegate)
{
    _delegate = delegate;
}

void ADVirtualCurrency::showVirtualCurrencyShop()
{
    if(_is_init && _implementation)
    {
        _implementation->showVirtualCurrencyShop();
    }
}

void ADVirtualCurrency::onPause()
{
    if(_is_init && _implementation)
    {
        _implementation->onPause();
    }
}

void ADVirtualCurrency::onResume()
{
    if(_is_init && _implementation)
    {
        _implementation->onResume();
    }
}

bool ADVirtualCurrency::isSupported()
{
    return _is_supported;
}


ADVirtualCurrency::Platform::~Platform()
{}
void ADVirtualCurrency::Platform::setModuleIsSupported() const
{
    _is_supported = true;
}
void ADVirtualCurrency::Platform::addCurrency(unsigned int number) const
{
    if(_delegate)
    {
        _delegate->onCurrencyAdded(number);
    }
    else
    {
        cocos2d::CCLog("ADVirtualCurrency without delegate. User will lose currency");
    }
}
void ADVirtualCurrency::Platform::onPause()
{}
void ADVirtualCurrency::Platform::onResume()
{}
