#include "ADPackageName.h"

ADPackageName::ADPackageName()
{
}

ADPackageName ADPackageName::create(const std::string& android_name,
                                const std::string& ios_name,
                                const std::string& windows_phone_name,
								const std::string& windows8_name)
{
    ADPackageName res;
    res.setAndroidName(android_name);
    res.setIOSName(ios_name);
    res.setWindowsPhoneName(windows_phone_name);
	res.setWindows8Name(windows8_name);

    return res;
}
