#ifndef ADREWARDEDADS_H
#define ADREWARDEDADS_H
#include <string>
#include <ADLib/ADSignals.h>

class ADRewardedAds
{
public:
    static Signal<std::string> signalAdsShownSuccess;
    static Signal<std::string> signalAdsShownFail;
    static void init();
    static void prepare();
    static void show(const std::string& id);
    static bool isAvaliable();
private:

};

#endif // ADREWARDEDADS_H
