#include "ADRewardedAds.h"


Signal<std::string> ADRewardedAds::signalAdsShownSuccess;
Signal<std::string> ADRewardedAds::signalAdsShownFail;
