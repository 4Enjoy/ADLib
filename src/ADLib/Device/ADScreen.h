#ifndef ADSCREEN_H
#define ADSCREEN_H
#include "cocos2d.h"

class ADScreen
{
public:
    static float getScaleFactor()
    {
        static float s = cocos2d::CCDirector::sharedDirector()->getContentScaleFactor();
        return _design_scale * s;
    }

    static const cocos2d::CCPoint& getOrigin()
    {
        static cocos2d::CCPoint p = cocos2d::CCDirector::sharedDirector()->getVisibleOrigin();
        return p;
    }

    static const cocos2d::CCSize& getVisibleSize()
    {
        static cocos2d::CCSize s = cocos2d::CCDirector::sharedDirector()->getVisibleSize();
        return s;
    }

    static const cocos2d::CCSize& getFrameSize()
    {
        static cocos2d::CCSize s = cocos2d::CCEGLView::sharedOpenGLView()->getFrameSize();
        return s;
    }

    static float getDesignResourceScale()
    {
        return 1.0f / _design_scale;
    }

    static void setDesignScale(float scale);
private:
    static float _design_scale;
};

#endif // ADSCREEN_H
