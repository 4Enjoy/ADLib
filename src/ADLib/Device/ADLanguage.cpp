#include <ADLib/Device/ADLanguage.h>
#include <ADLib/Device/ADStatistics.h>

std::string ADLanguage::localizeFileName(const std::string& file_name)
{

    unsigned int last_dot=0;
    for(unsigned int i=0; i<file_name.size(); ++i)
    {
        if(file_name[i] == '.')
            last_dot = i;
    }
    std::string res(file_name.begin(), file_name.begin()+last_dot+1);
    res.reserve(file_name.size() + 3);
    res += getLanguage();
    res += ".";
    res += &file_name[last_dot+1];
    return res;
}

std::string ADLanguage::getDeviceLanguage()
{
    std::string lang = platformGetDeviceLanguage();
    if(_supported_languages.find(lang) == _supported_languages.end())
    {
//        ADStatistics::logEvent(
//                    "Unsupported Language", ADStatistics::Params().add("code", lang));
        lang = _default_language;
    }
//    else
//        ADStatistics::logEvent("Supported Language", ADStatistics::Params().add("code", lang));
    return lang;
}

void ADLanguage::setDefaultLanguage(const std::string& s)
{
    _default_language = s;
}

const std::string& ADLanguage::getDefaultLanguage()
{
    return _default_language;
}

const std::string& ADLanguage::getTranslationFileTemplate()
{
    return _translation_file_template;
}

void ADLanguage::setTranslationFileTemplate(const std::string& file)
{
    _translation_file_template = file;
}
#include "cocos2d.h"
const char* ADLanguage::getFontName()
{
    static const char* def = "Arial";

    auto it = _fonts.find(getLanguage());
    if(it != _fonts.end())
    {
        return (it->second).c_str();
    }

    CCAssert(false, "No font for language");
    return def;
}

const char* ADLanguage::getBMFontName()
{
    static const char* def = "Arial";

    auto it = _bm_fonts.find(getLanguage());
    if(it != _bm_fonts.end())
    {
        return (it->second).c_str();
    }

    CCAssert(false, "No bmfont for language");
    return def;
}

std::string ADLanguage::_default_language = "en";
std::set<std::string> ADLanguage::_supported_languages;
std::string ADLanguage::_translation_file_template = "translations/Translation.mo";

ADLanguage::StringMap ADLanguage::_fonts;
ADLanguage::StringMap ADLanguage::_bm_fonts;

#define LE_MAGIC 0x950412de
#define BE_MAGIC 0xde120495
#include <map>
#include <memory>
#include "cocos2d.h"
using namespace std;
void parse_data(const char *data, size_t length);
static map<std::string,std::string> localizedStrings;

inline uint32_t swap_le(const char *buf, size_t ofs)
{
    uint32_t* res = (uint32_t*)(buf + ofs);
    return *res;
}

inline uint32_t swap_be(const char *buf, size_t ofs)
{
    uint32_t* res = (uint32_t*)(buf + ofs);
    return *res;
}

void parse_file(const char *filename)
{
    unsigned char * fileContents = NULL;
    unsigned long fileSize = 0;
    fileContents = cocos2d::CCFileUtils::sharedFileUtils()->getFileData( filename,
                                                                "rb",
                                                                &fileSize);
    if(fileContents != nullptr)
    {
        parse_data(reinterpret_cast<const char*>(fileContents),fileSize);
        delete[] fileContents;
    }
}



void parse_data(const char *data, size_t length)
{

    uint32_t (*swap)(const char *, size_t);
    if (((uint32_t *)data)[0] == LE_MAGIC)
    {
        swap = &swap_le;
    }
    else if (((uint32_t *)data)[0] == BE_MAGIC)
    {
        swap = &swap_be;
    }
    else
    {
        cocos2d::CCLog("Tranlation file reading error");
        return;
    }

    uint32_t number_of_strings = swap(data, 8);
    uint32_t ofs_original = swap(data, 12);
    uint32_t ofs_translated = swap(data, 16);

    for (unsigned i = 0; i < number_of_strings; i++)
    {
        uint32_t original_len = swap(data, ofs_original + i * 8);
        uint32_t original_ofs = swap(data, ofs_original + 4 + i * 8);
        uint32_t translated_len = swap(data, ofs_translated + i * 8);
        uint32_t translated_ofs = swap(data, ofs_translated + 4 + i * 8);

        std::string original(&(data[original_ofs]), original_len);
        std::string translated(&(data[translated_ofs]), translated_len);
        localizedStrings[original] = translated;
    }
}

const char* _(const string& key)
{
    static bool loaded = false;
    if(!loaded)
    {
        loaded = true;
        parse_file(ADLanguage::localizeFileName(ADLanguage::getTranslationFileTemplate()).c_str());
    }
    auto it = localizedStrings.find(key);
    if(it == localizedStrings.end())
        return key.c_str();
    else
        return (it->second).c_str();
}
