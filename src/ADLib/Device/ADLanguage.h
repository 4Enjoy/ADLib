#ifndef ADLANGUAGE_H
#define ADLANGUAGE_H
#include <string>
#include <set>
#include <map>
class ADLanguage
{
public:
    static std::string localizeFileName(const std::string& file_name);

    static const std::string& getLanguage()
    {
        static std::string lang_code = getDeviceLanguage();
        return lang_code;
    }

    static void setDefaultLanguage(const std::string& s);
    static void addSupportedLanguage(const std::string& lang,
                                     const std::string& font_name="",
                                     const std::string& bm_font_name="")
    {
        _supported_languages.insert(lang);
        if(font_name.size())
            _fonts[lang] = font_name;
        else if(bm_font_name.size())
            _bm_fonts[lang] = bm_font_name;
    }

    static const std::string& getDefaultLanguage();

    /**
     * @brief Return the translation file template
     * @return
     */
    static const std::string& getTranslationFileTemplate();

    /**
     * @brief Sets the translation file template
     * if translation files are named trans.en.mo, trans.de.mo
     * then template will be trans.mo
     * @param file
     */
    static void setTranslationFileTemplate(const std::string& file);

    /**
     * @brief returns the font name for current language
     * @return
     */
    static const char* getFontName();
    /**
     * @brief returns the bmfont name for current language
     * @return
     */
    static const char* getBMFontName();
private:
    typedef std::map<std::string, std::string> StringMap;

    static StringMap _fonts;
    static StringMap _bm_fonts;

    static std::set<std::string> _supported_languages;
    static std::string getDeviceLanguage();
    static std::string platformGetDeviceLanguage();
    static std::string _default_language;

    static std::string _translation_file_template;
};

/**
 * @brief localise given text
 * @param key
 * @return
 */
const char *_(const std::string &key);

#endif // ADLANGUAGE_H
