#ifndef ADNOTIFICATION_H
#define ADNOTIFICATION_H
#include <string>
class ADNotification
{
public:
    static void showNotification(const std::string& text);
};

#endif // ADNOTIFICATION_H
