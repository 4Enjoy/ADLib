#ifndef ADPUSH_H
#define ADPUSH_H
#include "ADLib/ADSignals.h"
class ADPush
{
public:
    static void registerForPush(const std::string& parameter);

    static StaticSignal<std::string> signalPushRegisterSuccess;
    static StaticSignal<> signalPushRegisterFailed;
};

#endif // ADPUSH_H
