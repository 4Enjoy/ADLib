#include "ADInfo.h"
#include <ADLib/Device/ADStatistics.h>

ADPlatform ADInfo::doGetPlatform()
{
    ADPlatform res = platformGetPlatform();
//    ADStatistics::logEvent("Platform",
//                           ADStatistics::Params().add(
//                               "platform",
//                               static_cast<int>(res)));
    return res;
}

ADStore ADInfo::doGetStore()
{
    ADStore res = platformGetStore();
//    ADStatistics::logEvent("Store",
//                           ADStatistics::Params().add(
//                               "store",
//                               static_cast<int>(res)));
    return res;
}
