#ifndef ADVIRTUALCURRENCY_H
#define ADVIRTUALCURRENCY_H
#include <string>
#include <memory>
#include <ADLib/ADSignals.h>
/**
 * @brief Class with API to work with virtual currency shop
 * with rewards
 *
 * 1. Use ADVirtualCurrency::setDelegate to setup delegate
 * 2. Call ADVirtualCurrency::initProvider first
 * 3. Call ADVirtualCurrency::showVirtualCurrencyShop to show proposals
 *
 * DO NOT FORGET
 * to call ADVirtualCurrency::onPause when the application is paused
 * and ADVirtualCurrency::onResume when the application is resumed
 */
class ADVirtualCurrency : public HasSlots
{
public:

    /**
     * @brief The actions which the client should implement
     */
    class Delegate
    {
    public:
        virtual ~Delegate()
        {}

        /**
         * @brief Called when the info about new currency is recieved
         * @param number
         */
        virtual void onCurrencyAdded(const unsigned int number)=0;
    };

    typedef std::shared_ptr<Delegate> DelegatePtr;

    /**
     * @brief Make initialization needed for provider.
     * Must be called after setDelegate
     *
     * @param key key for provider
     *
     * - TapJoy string in format "{App ID}|{App Secret Key}"
     * Example: "f1041413-201e-463d-b03a-83b0bbb991|TmZdV7bTpyvZiGxiwBDw4"
     */
    static void initProvider(const std::string key);

    /**
     * @brief Sets delegate to respond for currency added
     * @param delegate response for actions
     */
    static void setDelegate(DelegatePtr delegate);

    /**
     * @brief Call to show virtual currency shop
     */
    static void showVirtualCurrencyShop();

    /**
     * @brief MUST be called when the application is going to background
     */
    static void onPause();

    /**
     * @brief MUST be called when application is resumed
     */
    static void onResume();

    /**
     * @brief Returns true only if the store is supported
     * @return
     */
    static bool isSupported();

    /**
     * @brief Class to provide platform dependent implementation
     */
    class Platform
    {
    public:
        virtual ~Platform();

        /**
         * @brief Marks module as supported.
         * if module is not supported this method should not be called
         */
        void setModuleIsSupported() const;

        /**
         * @brief If currency is recieved this method should be called
         * @param number the amount of currence recieved
         */
        void addCurrency(unsigned int number) const;

        /**
         * @brief How to implement:
         * 1. Init module with given key
         * 2. If init is succes call setModuleIsSupported();
         * 3. Ask for virtual currency update
         * 4. If currency is recieved call addCurrency
         * @param key
         */
        virtual void init(const std::string& key) = 0;

        /**
         * @brief How to implement:
         * 1. Open store to user
         */
        virtual void showVirtualCurrencyShop() = 0;

        /**
         * @brief How to implement:
         * 1. Send pause event to module
         */
        virtual void onPause();

        /**
         * @brief How to implement:
         * 1. Send resume even to module
         * 2. Ask for virtual currency update
         */
        virtual void onResume();
    };
    typedef std::shared_ptr<Platform> PlatformPtr;


private:
    static PlatformPtr getImplementation();
    static DelegatePtr _delegate;
    static PlatformPtr _implementation;
    static bool _is_init;
    static bool _is_supported;

    static ADVirtualCurrency _obj;
    void _onPause();
    void _onResume();
};

#endif // ADVIRTUALCURRENCY_H
