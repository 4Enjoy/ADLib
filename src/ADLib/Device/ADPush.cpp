#include "ADPush.h"

StaticSignal<std::string> ADPush::signalPushRegisterSuccess;
StaticSignal<> ADPush::signalPushRegisterFailed;
