#ifndef ADPUSHWOOSH_H
#define ADPUSHWOOSH_H
#include <string>

class ADPushWoosh
{
public:
    /**
     * @brief Turn on push notifications
     * @param app_id the id of the application in PushWoosh
     * @param platform_parameter for Android - project_id, nothing for iOS
     */
    static void inizialize(const std::string& app_id, const std::string& platform_parameter);
};

#endif // PUSHWOOSH_H
