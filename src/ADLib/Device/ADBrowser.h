#ifndef ADBrowser_H
#define ADBrowser_H
#include <string>
#include <functional>
#include <ADLib/Device/ADPackageName.h>

class ADBrowser
{
public:
    typedef std::function<void()> Fallback;

    static void open(const std::string& url, const Fallback& = [](){});
    static void openWebURL(const std::string& url, const Fallback& = [](){});
    static void openOSUrl(const std::string& os_url, const Fallback& = [](){});
    static void sendMail(const std::string& email, const std::string &subject="", const Fallback& = [](){});

    static void openApplicationPage(const ADPackageName& package_name);
    static void openApplicationPage(const std::string& package_id);

    static const std::string MAIL_PREFIX;
    static const std::string OS_PERFIX;
};

#endif // ADBrowser_H
