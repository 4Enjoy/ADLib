#ifndef ADORIENTATION_H
#define ADORIENTATION_H
#include <ADLib/ADSignals.h>
enum class DeviceOrientation
{
    Landscape=0,
    Portrait=1
};

class ADOrientation
{
public:
    static void enableRuntimeOrientation(const DeviceOrientation default_orientation);
    static StaticSignal<DeviceOrientation> signalOrientationChanged;
};

#endif // ADORIENTATION_H
