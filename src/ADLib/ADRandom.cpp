#include "ADRandom.h"

ADRandom::ADRandom()
    : _generator()
{
    std::random_device rd;
    _generator.seed(rd());
}

ADRandom::ADRandom(const SeedValue seed)
    : _generator(seed)
{
}
