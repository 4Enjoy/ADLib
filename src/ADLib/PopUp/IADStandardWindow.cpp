#include "IADStandardWindow.h"
#include <ADLib/Device/ADScreen.h>
using namespace cocos2d;

namespace IADStandardWindow_impl
{
typedef std::map<WindowSize, cocos2d::CCSize> SizeMap;
SizeMap getDefaultMap()
{
    SizeMap map;
    CCSize VISIBLE_SIZE =ADScreen::getVisibleSize();
    map[WindowSize::Big] = CCSize(VISIBLE_SIZE.width*0.8f,
                                  VISIBLE_SIZE.width*1.0f);
    map[WindowSize::Medium] = CCSize(VISIBLE_SIZE.width*0.9,
                                     VISIBLE_SIZE.width*0.6);
    return map;
}
CCPoint getDefaultWindowPosition()
{
    CCSize VISIBLE_SIZE = ADScreen::getVisibleSize();
    CCPoint ORIGIN = ADScreen::getOrigin();
    return VISIBLE_SIZE*0.5f + ORIGIN;
}
}
using namespace IADStandardWindow_impl;

void IADStandardWindow::setDesignSize(const Size id, const cocos2d::CCSize& size)
{
    getSizeMap()[id] = size;
}


IADStandardWindow::IADStandardWindow(
        const Size size_id,
        const cocos2d::ccColor3B& background_color,
        const Animation animation)
    : _size(getSizeMap()[size_id]),
      _color(background_color),
      _animation(animation),
      _window_position()
{
    setWindowPosition(getDefaultWindowPosition());
}

IADStandardWindow::IADStandardWindow(
        const cocos2d::CCSize size,
        const cocos2d::ccColor3B& background_color,
        const Animation animation)
    : _size(size),
      _color(background_color),
      _animation(animation),
      _window_position()
{
    setWindowPosition(getDefaultWindowPosition());
}

IADStandardWindow::SizeMap& IADStandardWindow::getSizeMap()
{
    static SizeMap map = IADStandardWindow_impl::getDefaultMap();
    return map;
}


void IADStandardWindow::initDesing(cocos2d::CCNode* window_node)
{
    CCPoint window_target_position =  _window_position;

    window_node->setZOrder(10000);
    window_node->setPosition(window_target_position);
    window_node->setContentSize(_size);
    window_node->setAnchorPoint(ccp(0.5f,0.5f));

    //Create background
    CCDrawNode* background = CCDrawNode::create();
    CCPoint rect[4] = {ccp(0,0),
                       ccp(0, _size.height),
                       _size,
                       ccp(_size.width, 0)};

    ccColor4F color = ccc4FFromccc3B(_color);
    background->drawPolygon(rect, 4, color, 0, color);
    background->setAnchorPoint(ccp(0,0));
    background->setPosition(ccp(0,0));
    window_node->addChild(background);
}

void IADStandardWindow::setWindowPosition(const cocos2d::CCPoint& position)
{
    _window_position = position;
    _window_position = _window_position - _size*0.5f;
}

const cocos2d::CCSize& IADStandardWindow::getWindowSize() const
{
    return _size;
}

float IADStandardWindow::moveInAnimation(cocos2d::CCNode* window_node)
{
    const float time = 0.2f;
    window_node->stopAllActions();

    CCPoint target_position = _window_position;
    CCPoint start_position = target_position;

    CCSize VISIBLE_SIZE = ADScreen::getVisibleSize();

    if(_animation == Animation::DownToTop)
        start_position.y -= VISIBLE_SIZE.height;
    else if(_animation == Animation::TopToDown)
        start_position.y += VISIBLE_SIZE.height;
    else if(_animation == Animation::LeftToRight)
        start_position.x -= VISIBLE_SIZE.width;
    else if(_animation == Animation::RightToLeft)
        start_position.x += VISIBLE_SIZE.width;

    window_node->setPosition(start_position);
    window_node->runAction(CCMoveTo::create(time, target_position));
    return time;
}

float IADStandardWindow::moveOutAnimation(cocos2d::CCNode* window_node)
{
    const float time = 0.2f;
    window_node->stopAllActions();

    CCPoint target_position = _window_position;

    CCSize VISIBLE_SIZE = ADScreen::getVisibleSize();

    if(_animation == Animation::DownToTop)
        target_position.y += VISIBLE_SIZE.height;
    else if(_animation == Animation::TopToDown)
        target_position.y -= VISIBLE_SIZE.height;
    else if(_animation == Animation::LeftToRight)
        target_position.x += VISIBLE_SIZE.width;
    else if(_animation == Animation::RightToLeft)
        target_position.x -= VISIBLE_SIZE.width;

    window_node->runAction(CCMoveTo::create(time, target_position));
    return time;
}
