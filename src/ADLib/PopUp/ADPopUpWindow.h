#ifndef ADPOPUPWINDOW_H
#define ADPOPUPWINDOW_H
#include "cocos2d.h"
#include <functional>
#include <ADLib/ADSignals.h>
class ADPopUpWindow : public cocos2d::CCLayer
{
public:
    class Content;

    void closeWindow(CCObject* obj_callback = 0,
                     cocos2d::SEL_CallFunc callfunc_callback = 0);

    static ADPopUpWindow* create(
            Content* content,
            CCObject* obj_close_callback = 0,
            cocos2d::SEL_CallFunc callfunc_close_callback = 0);

    void showWindow();
    ~ADPopUpWindow();
private:
    void initWindow();

    virtual bool ccTouchBegan(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent);
    virtual void ccTouchMoved(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent);
    virtual void ccTouchEnded(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent);
    virtual void ccTouchCancelled(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent);

    CCObject* _close_callback_obj;
    cocos2d::SEL_CallFunc _close_callback_fun;
    Content* _content;

    CCNode* _sheet_menu;
    cocos2d::CCPoint _sheet_target_position;

    bool _closed;
    void cleanUp();

    ADPopUpWindow(Content* content,
                CCObject* obj_close_callback = 0,
                cocos2d::SEL_CallFunc callfunc_close_callback = 0);
};

class ADPopUpWindowManager : public cocos2d::CCObject
{
public:
    ADPopUpWindowManager(cocos2d::CCNode* parent);
    void closeWindow(cocos2d::CCObject* obj_callback = 0,
                     cocos2d::SEL_CallFunc callfunc_callback = 0);
    ADPopUpWindow* openWindow(ADPopUpWindow::Content* content);

    bool backAction();
    void addMenuToAutoDisable(cocos2d::CCMenu* menu);
    void addTouchZoneToAutoDisable(cocos2d::CCLayer* layer);


    /**
     * @brief Type for planned actions
     */
    typedef std::function<void ()> Action;

    /**
     * @brief The given action will be performed just before first window is showed
     * @param action functor with action to perform
     */
    void addOnShowWindowAction(const Action& action);
    /**
     * @brief The action will be performed just after the last window was hidden
     * @param action functor with action to perform
     */
    void addOnHideWindowAction(const Action& action);
private:
    /**
     * @brief Runs all actions added by addOnShowWindowAction in the same
     * order as they were added
     */
    void runOnShowWindowActions();
    /**
     * @brief Runs all actions added by addOnHideWindowAction in the same
     * order as they were added
     */
    void runOnHideWindowActions();

    void onLastClosed();
    void setMenusAvaliablitity(bool enabled);
    void do_openWindow(ADPopUpWindow* window);
    void disableMenus();
    void enableMenus();

    unsigned int _windows_active;
    ADPopUpWindow* _opened_window;
    ADPopUpWindow* _pending_window;

    std::vector<cocos2d::CCMenu*> _menus;
    std::vector<cocos2d::CCLayer*> _scroll_views;
    cocos2d::CCNode* _parent;

    std::vector<Action> _show_window_action;
    std::vector<Action> _hide_window_action;
};

class ADPopUpWindow::Content : public CCObject, public HasSlots
{
public:
    Content();

    void closeWindow(CCObject* obj_callback = 0, cocos2d::SEL_CallFunc callfunc_callback = 0);

    virtual ~Content()
    {
    }
private:
    friend class ADPopUpWindow;
    ADPopUpWindow* _parent_window;

    void perform_init(cocos2d::CCNode* parent,
                      ADPopUpWindow* window);
    void perform_close();
protected:
    /**
     * @brief In this method child class should attach window content
     * @param parent
     */
    virtual void onCreate(cocos2d::CCNode* parent)=0;

    /**
     * @brief This action is performed when window is being closed
     */
    virtual void onClose()
    {}

    /**
     * @brief In this action child class should define window size
     * and its initial position
     * @param window_node
     */
    virtual void initDesing(cocos2d::CCNode* window_node)=0;

    /**
     * @brief In this action child class should define animation of window
     * to appear on the screen
     * @param window_node
     * @return the length of animation
     */
    virtual float moveInAnimation(cocos2d::CCNode* window_node)=0;

    /**
     * @brief In this action child class should define animation of window
     * to go out of the screen
     * @param window_node
     * @return the length of animation
     */
    virtual float moveOutAnimation(cocos2d::CCNode* window_node)=0;
};

typedef ADPopUpWindow::Content ADPopUpWindowContent;
#endif // ADPOPUPWINDOW_H
