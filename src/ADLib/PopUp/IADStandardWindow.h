#ifndef IADSTANDARDWINDOW_H
#define IADSTANDARDWINDOW_H
#include <ADLib/PopUp/ADPopUpWindow.h>
#include <map>

/**
 * @brief The Size of window.
 * Each size has predefined value.
 * You can change it by using IADStandardWindow::setDesignSize
 */
enum class WindowSize
{
    Medium,
    Big
};

class IADStandardWindow : public ADPopUpWindowContent
{
public:
    typedef WindowSize Size;

    /**
     * @brief Defines how window will appear, and
     * how window will hide itself
     */
    enum class Animation
    {
        DownToTop,
        TopToDown,
        LeftToRight,
        RightToLeft
    };

    /**
     * @brief Changes size of predefined Size
     * must be called before first window is created
     * @param id size id
     * @param size the size of window
     */
    static void setDesignSize(const Size id, const cocos2d::CCSize& size);


    /**
     * @brief Sets the position of the window
     * PLEASE note that windows anchor point is (0.5f, 0.5f)
     *
     * by default position = VISIBLE_SIZE*0.5f + ORIGIN
     * @param position
     */
    void setWindowPosition(const cocos2d::CCPoint& position);

    /**
     * @brief returns the size of the window
     * @return
     */
    const cocos2d::CCSize& getWindowSize() const;

    /**
     * @brief Creates window
     * @param size_id predefined size of window
     * @param background_color color
     * @param animation how window will apear and hide itself
     */
    IADStandardWindow(const Size size_id,
                      const cocos2d::ccColor3B& background_color=cocos2d::ccc3(255,255,255),
                      const Animation animation=Animation::DownToTop);

    /**
     * @brief Creates window
     * @param size custom size of the window
     * @param background_color color
     * @param animation how window will apear and hide itself
     */
    IADStandardWindow(const cocos2d::CCSize size,
                      const cocos2d::ccColor3B& background_color=cocos2d::ccc3(255,255,255),
                      const Animation animation=Animation::DownToTop);

private:
    cocos2d::CCSize _size;
    cocos2d::ccColor3B _color;
    Animation _animation;
    cocos2d::CCPoint _window_position;

    typedef std::map<Size, cocos2d::CCSize> SizeMap;
    static SizeMap& getSizeMap();

protected:
    virtual void initDesing(cocos2d::CCNode* window_node);
    virtual float moveInAnimation(cocos2d::CCNode* window_node);
    virtual float moveOutAnimation(cocos2d::CCNode* window_node);
};

#endif // IADSTANDARDWINDOW_H
