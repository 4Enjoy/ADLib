#include "ADPopUpWindow.h"
#include <ADLib/Device/ADScreen.h>

using namespace cocos2d;

void ADPopUpWindow::closeWindow(CCObject* obj_callback,
                 cocos2d::SEL_CallFunc callfunc_callback)
{
    if(!_closed)
    {
        _closed = true;
        float time = _content->moveOutAnimation(this);
        //CCLog("Window Closed");
        _content->perform_close();
        this->runAction(
                    CCSequence::create(
                        CCCallFunc::create(_close_callback_obj,
                                           _close_callback_fun),
                        CCDelayTime::create(time),
                        CCCallFunc::create(obj_callback,
                                           callfunc_callback),
                        CCCallFunc::create(this,
                                           callfunc_selector(ADPopUpWindow::cleanUp)),
                        NULL));
    }

}
void ADPopUpWindow::cleanUp()
{
    this->removeFromParent();
}

ADPopUpWindow* ADPopUpWindow::create(
        Content* content,
        CCObject* obj_close_callback,
        cocos2d::SEL_CallFunc callfunc_close_callback)
{
    ADPopUpWindow* obj = new ADPopUpWindow(content, obj_close_callback, callfunc_close_callback);
    if(obj)
    {
        obj->autorelease();
    }
    return obj;
}
ADPopUpWindow::~ADPopUpWindow()
{
    if(_content)
        _content->release();
}


ADPopUpWindow::ADPopUpWindow(Content* content,
            CCObject* obj_close_callback,
            cocos2d::SEL_CallFunc callfunc_close_callback)
    : _content(content),
      _close_callback_obj(obj_close_callback),
      _close_callback_fun(callfunc_close_callback),
      _closed(false)
{
    _content->autorelease();
    _content->retain();


}

void ADPopUpWindow::showWindow()
{
    initWindow();
}

bool ADPopUpWindow::ccTouchBegan(cocos2d::CCTouch *pTouch, cocos2d::CCEvent * /* pEvent */)
{
    CCPoint touchLocation = pTouch->getLocation();
    CCPoint local_cords = this->convertToNodeSpace(touchLocation);

    CCRect r = this->boundingBox();
    r.origin = CCPointZero;


    if (!r.containsPoint(local_cords))
    {
        return true;
    }
    return false;
}

void ADPopUpWindow::ccTouchMoved(cocos2d::CCTouch *, cocos2d::CCEvent *)
{}
void ADPopUpWindow::ccTouchEnded(cocos2d::CCTouch *, cocos2d::CCEvent *)
{
    closeWindow();
}

void ADPopUpWindow::ccTouchCancelled(cocos2d::CCTouch *, cocos2d::CCEvent *)
{}

void ADPopUpWindow::initWindow()
{
    this->setTouchEnabled(true);
    CCDirector* pDirector = CCDirector::sharedDirector();
    pDirector->getTouchDispatcher()->addTargetedDelegate(this, kCCMenuHandlerPriority, false);

    _sheet_menu = this;

    _content->initDesing(_sheet_menu);
    _content->moveInAnimation(_sheet_menu);

    _content->perform_init(_sheet_menu, this);
}

ADPopUpWindow::Content::Content()
    : _parent_window(0)
{

}

void ADPopUpWindow::Content::closeWindow(
        CCObject* obj_callback, cocos2d::SEL_CallFunc callfunc_callback)
{
    if(_parent_window)
        _parent_window->closeWindow(obj_callback, callfunc_callback);
}


void ADPopUpWindow::Content::perform_init(cocos2d::CCNode* parent,
                  ADPopUpWindow* window)
{
    _parent_window = window;
    onCreate(parent);
}

void ADPopUpWindow::Content::perform_close()
{
    onClose();
}

ADPopUpWindowManager::ADPopUpWindowManager(cocos2d::CCNode* parent)
    : _parent(parent),
      _opened_window(0),
      _pending_window(0),
      _windows_active(0)
{

}

void ADPopUpWindowManager::closeWindow(cocos2d::CCObject* obj_callback,
                 cocos2d::SEL_CallFunc callfunc_callback)
{
    if(_opened_window)
    {
        _opened_window->closeWindow(obj_callback, callfunc_callback);

        _opened_window = 0;
    }


}

ADPopUpWindow* ADPopUpWindowManager::openWindow(ADPopUpWindow::Content* content)
{
    ADPopUpWindow* window = ADPopUpWindow::create(
                content,
                this,
                callfunc_selector(ADPopUpWindowManager::onLastClosed));
    window->retain();

    disableMenus();
    if(_opened_window)
    {
        closeWindow();
        _pending_window = _opened_window;
        _opened_window = 0;

    }


    do_openWindow(window);
    return window;
}

 void ADPopUpWindowManager::do_openWindow(ADPopUpWindow* window)
 {
     if(_opened_window == nullptr)
         runOnShowWindowActions();

     _opened_window = window;
     window->showWindow();
     _parent->addChild(window);
     window->release();
 }
void ADPopUpWindowManager::disableMenus()
{
    _windows_active++;
    setMenusAvaliablitity(false);
    //CCLog("+Active windows: %d", _windows_active);
}

void ADPopUpWindowManager::enableMenus()
{
    //CCLog("-Active windows: %d", _windows_active);
    if(_windows_active > 0)
    {
        _windows_active--;
        if(_windows_active == 0)
        {
            setMenusAvaliablitity(true);
        }
    }
}

void ADPopUpWindowManager::onLastClosed()
{
    enableMenus();
    if(_pending_window == 0 && _windows_active == 0)
    {
        _opened_window = 0;
        runOnHideWindowActions();
    }
    _pending_window = 0;
}
void ADPopUpWindowManager::addMenuToAutoDisable(cocos2d::CCMenu* menu)
{
    _menus.push_back(menu);
}
void ADPopUpWindowManager::addTouchZoneToAutoDisable(cocos2d::CCLayer* scroll_view)
{
    _scroll_views.push_back(scroll_view);
}

void ADPopUpWindowManager::setMenusAvaliablitity(bool enabled)
{
    if(enabled)
    {
        //CCLog("Enabled Menu");
    }
    else
    {
        //CCLog("Disabled Menu");
    }
    for(unsigned int i=0; i<_menus.size(); ++i)
    {
        CCMenu* m = _menus[i];
        if(m)
        {
            m->setEnabled(enabled);
        }
    }
    for(unsigned int i=0; i<_scroll_views.size(); ++i)
    {
        CCLayer* m = _scroll_views[i];
        if(m)
        {
            m->setTouchEnabled(enabled);
        }
        
    }

}

bool ADPopUpWindowManager::backAction()
{
    if(_opened_window)
    {
        closeWindow();
        return true;
    }
    return false;
}

void ADPopUpWindowManager::addOnShowWindowAction(const Action& action)
{
    _show_window_action.push_back(action);
}

void ADPopUpWindowManager::addOnHideWindowAction(const Action& action)
{
    _hide_window_action.push_back(action);
}

void ADPopUpWindowManager::runOnShowWindowActions()
{
    for(auto& it : _show_window_action)
        it();
}

void ADPopUpWindowManager::runOnHideWindowActions()
{
    for(auto& it : _hide_window_action)
        it();
}
