#ifndef ADCREATOR_H
#define ADCREATOR_H

template <class C1, class C2=void, class C3=void, class C4=void, class C5=void>
class ADInherit;

template <class C1, class C2, class C3, class C4>
class ADInherit<C1, C2, C3, C4> : public C4, public ADInherit<C1, C2, C3>
{};

template <class C1, class C2, class C3>
class ADInherit<C1, C2, C3> : public C3, public ADInherit<C1, C2>
{};

template <class C1, class C2>
class ADInherit<C1, C2> : public C2, public ADInherit<C1>
{};

template <class C1>
class ADInherit<C1> : public C1
{};

template <class Base_Class, class Child_Class>
class ADCreate : public Base_Class
{
public:
    static Child_Class* create()
    {
        Child_Class* pRet = new Child_Class();
        pRet->autorelease();
        return pRet;
    }
};

template <class Base_Class, class Child_Class, class Par1=void, class Par2=void, class Par3=void, class Par4=void, class Par5=void>
class ADCreateInit;

template <class Base_Class, class Child_Class>
class ADCreateInit<Base_Class, Child_Class> : public Base_Class
{
public:
    static Child_Class* create()
    {
        Child_Class* pRet = new Child_Class();
        if(pRet->init())
        {
            pRet->autorelease();
        }
        else
        {
            delete pRet;
            pRet = nullptr;
        }
        return pRet;
    }
};


template <class Base_Class, class Child_Class, class Par1>
class ADCreateInit<Base_Class, Child_Class, Par1> : public Base_Class
{
public:
    static Child_Class* create(Par1 p1)
    {
        Child_Class* pRet = new Child_Class();
        if(pRet->init(p1))
        {
            pRet->autorelease();
        }
        else
        {
            delete pRet;
            pRet = nullptr;
        }
        return pRet;
    }
};

template <class Base_Class, class Child_Class, class Par1, class Par2>
class ADCreateInit<Base_Class, Child_Class, Par1, Par2> : public Base_Class
{
public:
    static Child_Class* create(Par1 p1, Par2 p2)
    {
        Child_Class* pRet = new Child_Class();
        if(pRet->init(p1, p2))
        {
            pRet->autorelease();
        }
        else
        {
            delete pRet;
            pRet = nullptr;
        }
        return pRet;
    }
};

#endif // ADCREATOR_H
