#ifndef ADCLASS_H
#define ADCLASS_H

#define p_AD_DoConcat(A, B) A ## B
#define p_AD_Concat(A, B) p_AD_DoConcat(A, B)


#define AD_Selector(Type, Variable, Name) \
    Type p_AD_Concat(get, Name) () const \
    { \
        return Variable; \
    } //


#define AD_Modifier(Type, Variable, Name) \
    void p_AD_Concat(set, Name)(Type obj) \
    { \
        Variable = obj; \
    } //

#define AD_SelectorModifier(Type, Variable, Name) \
    AD_Selector(Type, Variable, Name) \
    AD_Modifier(Type, Variable, Name) //


#endif // ADCLASS_H
