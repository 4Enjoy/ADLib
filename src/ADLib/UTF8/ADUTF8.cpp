#include "ADUTF8.h"
#include "utf8.h"
#include <clocale>
namespace ADUTF8_Impl {
    std::wstring utf8ToUtf16(const std::string& s)
    {
        std::wstring result;
        result.reserve(s.size());
        utf8::utf8to16(s.c_str(), s.c_str() + s.size(), back_inserter(result));
        return result;
    }

    std::string utf16ToUtf8(const std::wstring& s)
    {
        std::string result;
        result.reserve(s.size()*2);
        utf8::utf16to8(s.c_str(), s.c_str() + s.size(), back_inserter(result));
        return result;
    }
}
using namespace ADUTF8_Impl;

std::string ADUTF8::toUpper(const std::string& s)
{
    std::wstring str = utf8ToUtf16(s);
    for(unsigned int i=0; i<str.size(); ++i)
    {
        str[i] = towupper(str[i]);
    }
    return utf16ToUtf8(str);
}

std::string ADUTF8::toLower(const std::string& s)
{
    std::wstring str = utf8ToUtf16(s);
    for(unsigned int i=0; i<str.size(); ++i)
    {
        str[i] = towlower(str[i]);
    }
    return utf16ToUtf8(str);
}

std::vector<std::string> ADUTF8::splitToChars(const std::string& s)
{
    std::wstring str = utf8ToUtf16(s);
    std::vector<std::string> res;
    res.reserve(s.size()*2);

    for(unsigned int i=0; i<str.size(); ++i)
    {
        std::wstring ws(1, str[i]);
        res.push_back(utf16ToUtf8(ws));
    }
    return res;
}
