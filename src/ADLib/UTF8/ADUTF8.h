#ifndef ADUTF8_H
#define ADUTF8_H
#include <string>
#include <vector>
class ADUTF8
{
public:
    static std::string toUpper(const std::string& s);
    static std::string toLower(const std::string& s);
    static std::vector<std::string> splitToChars(const std::string& s);

};

#endif // ADUTF8_H
