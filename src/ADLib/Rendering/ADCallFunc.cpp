#include "ADCallFunc.h"
using namespace cocos2d;
//
// CallFunc
//
ADCallFunc * ADCallFunc::create(const Action& action)
{
    ADCallFunc *pRet = new ADCallFunc(action);

    pRet->autorelease();

    return pRet;
}



ADCallFunc::~ADCallFunc(void)
{

}

CCObject * ADCallFunc::copyWithZone(CCZone *pZone) {
    CCZone* pNewZone = NULL;
    ADCallFunc* pRet = NULL;

    if (pZone && pZone->m_pCopyObject) {
        //in case of being called at sub class
        pRet = (ADCallFunc*) (pZone->m_pCopyObject);
    } else {
        pRet = new ADCallFunc(_action);
        pZone = pNewZone = new CCZone(pRet);
    }

    CCActionInstant::copyWithZone(pZone);
    pRet->_action = _action;

    CC_SAFE_DELETE(pNewZone);
    return pRet;
}

void ADCallFunc::update(float time) {
    CC_UNUSED_PARAM(time);
    this->execute();
}

void ADCallFunc::execute() {
    if (_action) {
        _action();
    }
}
