#ifndef ADBLURSPRITEHORIZONTAL_H
#define ADBLURSPRITEHORIZONTAL_H
#include "ADShaderSprite.h"
class ADBlurSpriteHorizontal : public ADShaderSpriteCreator<ADBlurSpriteHorizontal, ADShaderSprite>
{
public:
    ADBlurSpriteHorizontal();
    void setBlur(float value);
    float getBlur();
protected:
    void buildCustomUniforms();
    void setCustomUniforms();
    float _blur;
    GLuint _blurLocation;
};

class ADBlurSpriteVertical : public ADShaderSpriteCreator<ADBlurSpriteVertical, ADShaderSprite>
{
public:
    ADBlurSpriteVertical();
    void setBlur(float value);
    float getBlur();
protected:
    void buildCustomUniforms();
    void setCustomUniforms();
    float _blur;
    GLuint _blurLocation;
};

class ADBlurSprite
{
public:
    static cocos2d::CCSprite* applyBlur(cocos2d::CCSprite* sprite, const cocos2d::CCPoint& blur);
private:
    ADBlurSprite();
};

#endif // ADBLURSPRITEHORIZONTAL_H
