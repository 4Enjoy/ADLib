#ifndef ADSHADERSPRITE_H
#define ADSHADERSPRITE_H
#include "cocos2d.h"
/**
 * @brief Renders CCSprite and helps to apply fragment shader to it
 */
class ADShaderSprite : public cocos2d::CCSprite
{
public:
    ADShaderSprite();
    ~ADShaderSprite();

    virtual void initShader();
    void draw();

protected:
    virtual void buildCustomUniforms() = 0;
    virtual void setCustomUniforms() = 0;
    const GLchar* _fragShader;
};


template <class T, class Base>
class ADShaderSpriteCreator : public Base
{
public:
    static T* createWithTexture(cocos2d::CCTexture2D *pTexture)
    {
        T *pobSprite = new T();
        if (pobSprite && pobSprite->initWithTexture(pTexture))
        {
            pobSprite->initShader();
            pobSprite->autorelease();
            return pobSprite;
        }
        CC_SAFE_DELETE(pobSprite);
        return NULL;
    }

    static T* create(const char *pszFileName)
    {
        T* pobSprite = new T();
        if (pobSprite && pobSprite->initWithFile(pszFileName))
        {
            pobSprite->initShader();
            pobSprite->autorelease();
            return pobSprite;
        }
        CC_SAFE_DELETE(pobSprite);
        return NULL;
    }
};

#endif // ADSHADERSPRITE_H
