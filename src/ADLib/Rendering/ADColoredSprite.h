#ifndef ADCOLOREDSPRITE_H
#define ADCOLOREDSPRITE_H
#include "ADShaderSprite.h"
class ADColoredSprite : public ADShaderSpriteCreator<ADColoredSprite, ADShaderSprite>
{
public:
    ADColoredSprite();
    void setTintColor(const cocos2d::ccColor4B &color);
    const cocos2d::ccColor4B& getTintColor();
protected:
    void buildCustomUniforms();
    void setCustomUniforms();
    cocos2d::ccColor4B _color;
    GLuint _color_location;
};

#endif // ADCOLOREDSPRITE_H
