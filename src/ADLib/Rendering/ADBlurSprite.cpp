#include "ADBlurSprite.h"
#include "ADBakeNode.h"
using namespace cocos2d;
//http://www.gamerendering.com/2008/10/11/gaussian-blur-filter-shader/
#define GAUSS_X0 "0.265962"
#define GAUSS_X1 "0.212965"
#define GAUSS_X2 "0.109340"
#define GAUSS_X3 "0.035994"
#define GAUSS_X4 "0.007597"

const GLchar * blurHorizontal_frag = " \n\
#ifdef GL_ES                        \n\
precision mediump float;            \n\
#endif                              \n\
                                    \n\
varying vec4 v_fragmentColor;       \n\
varying vec2 v_texCoord;            \n\
                                    \n\
uniform sampler2D CC_Texture0;      \n\
                                    \n\
uniform float blurSize;              \n\
                                    \n\
void main() {                       \n\
    vec4 sum = vec4(0.0);           \n\
            sum += texture2D(CC_Texture0, vec2(v_texCoord.x - 4.0*blurSize, v_texCoord.y)) * " GAUSS_X4 "; \n\
    sum += texture2D(CC_Texture0, vec2(v_texCoord.x - 3.0*blurSize, v_texCoord.y)) * " GAUSS_X3 "; \n\
    sum += texture2D(CC_Texture0, vec2(v_texCoord.x - 2.0*blurSize, v_texCoord.y)) * " GAUSS_X2 "; \n\
    sum += texture2D(CC_Texture0, vec2(v_texCoord.x - blurSize, v_texCoord.y)) * " GAUSS_X1 ";    \n\
    sum += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y)) * " GAUSS_X0 ";   \n\
    sum += texture2D(CC_Texture0, vec2(v_texCoord.x + blurSize, v_texCoord.y)) * " GAUSS_X1 ";    \n\
    sum += texture2D(CC_Texture0, vec2(v_texCoord.x + 2.0*blurSize, v_texCoord.y)) * " GAUSS_X2 ";    \n\
    sum += texture2D(CC_Texture0, vec2(v_texCoord.x + 3.0*blurSize, v_texCoord.y)) * " GAUSS_X3 ";    \n\
    sum += texture2D(CC_Texture0, vec2(v_texCoord.x + 4.0*blurSize, v_texCoord.y)) * " GAUSS_X4 ";    \n\
    gl_FragColor = (sum); \n\
}       \n\
";

const GLchar * blurVertical_frag = " \n\
#ifdef GL_ES                        \n\
precision mediump float;            \n\
#endif                              \n\
                                    \n\
varying vec4 v_fragmentColor;       \n\
varying vec2 v_texCoord;            \n\
                                    \n\
uniform sampler2D CC_Texture0;      \n\
                                    \n\
uniform float blurSize;              \n\
                                    \n\
void main() {                       \n\
    vec4 sum = vec4(0.0);           \n\
    sum += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y - 4.0*blurSize)) * " GAUSS_X4 "; \n\
    sum += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y - 3.0*blurSize)) * " GAUSS_X3 "; \n\
    sum += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y - 2.0*blurSize)) * " GAUSS_X2 "; \n\
    sum += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y - blurSize)) * " GAUSS_X1 ";    \n\
    sum += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y)) * " GAUSS_X0 ";   \n\
    sum += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y + blurSize)) * " GAUSS_X1 ";    \n\
    sum += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y + 2.0*blurSize)) * " GAUSS_X2 ";    \n\
    sum += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y + 3.0*blurSize)) * " GAUSS_X3 ";    \n\
    sum += texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y + 4.0*blurSize)) * " GAUSS_X4 ";    \n\
    gl_FragColor = (sum); \n\
}       \n\
";

ADBlurSpriteHorizontal::ADBlurSpriteHorizontal()
    : _blur(1.0f)
{
    _fragShader = blurHorizontal_frag;
}

void ADBlurSpriteHorizontal::buildCustomUniforms()
{
    _blurLocation = glGetUniformLocation( getShaderProgram()->getProgram(), "blurSize");
}

void ADBlurSpriteHorizontal::setCustomUniforms()
{
    getShaderProgram()->setUniformLocationWith1f(_blurLocation, _blur);
}

void ADBlurSpriteHorizontal::setBlur(float f)
{
    _blur = f / getTexture()->getContentSizeInPixels().width;
}


ADBlurSpriteVertical::ADBlurSpriteVertical()
    : _blur(1.0f)
{
    _fragShader = blurVertical_frag;
}

void ADBlurSpriteVertical::buildCustomUniforms()
{
    _blurLocation = glGetUniformLocation( getShaderProgram()->getProgram(), "blurSize");
}

void ADBlurSpriteVertical::setCustomUniforms()
{
    getShaderProgram()->setUniformLocationWith1f(_blurLocation, _blur);
}

void ADBlurSpriteVertical::setBlur(float f)
{
    _blur = f / getTexture()->getContentSizeInPixels().height;
}
template<class Blur>
CCSprite* applyBlurOneDirection(CCSprite* sprite, float blur)
{
    Blur first_pass;
    first_pass.initWithTexture(sprite->getTexture());
    first_pass.initShader();
    first_pass.setBlur(blur);
    return ADBakeNode::bakeNode(&first_pass);
}

CCSprite* ADBlurSprite::applyBlur(CCSprite* sprite, const CCPoint& blur)
{

        //return applyBlurOneDirection<ADBlurSpriteVertical>(sprite, blur.y);
    return applyBlurOneDirection<ADBlurSpriteVertical>(
        applyBlurOneDirection<ADBlurSpriteHorizontal>(sprite, blur.x), blur.y);
}


