#ifndef ADBAKENODE_H
#define ADBAKENODE_H
#include "cocos2d.h"
class ADBakeNode
{
public:
    static cocos2d::CCSprite* bakeNode(cocos2d::CCNode* node);
private:
    ADBakeNode();
};

#endif // ADBAKENODE_H
