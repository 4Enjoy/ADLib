#include "ADShaderSprite.h"
using namespace cocos2d;
ADShaderSprite::ADShaderSprite()
    : _fragShader(nullptr)
{
}

ADShaderSprite::~ADShaderSprite()
{
}
void ADShaderSprite::initShader()
{
    CCAssert(_fragShader != 0, "Fragment shader should be initialized");

    CCGLProgram* program = new CCGLProgram();
    program->autorelease();

    program->initWithVertexShaderByteArray(ccPositionTextureColor_vert, _fragShader);
    setShaderProgram(program);

    CHECK_GL_ERROR_DEBUG();

    program->addAttribute(kCCAttributeNamePosition, kCCVertexAttrib_Position);
    program->addAttribute(kCCAttributeNameColor, kCCVertexAttrib_Color);
    program->addAttribute(kCCAttributeNameTexCoord, kCCVertexAttrib_TexCoords);

    CHECK_GL_ERROR_DEBUG();

    program->link();

    CHECK_GL_ERROR_DEBUG();

    program->updateUniforms();

    CHECK_GL_ERROR_DEBUG();

    buildCustomUniforms();

    CHECK_GL_ERROR_DEBUG();
}

void ADShaderSprite::draw()
{
    CC_NODE_DRAW_SETUP();

    setCustomUniforms();

    CC_NODE_DRAW_SETUP();

    ccGLBlendFunc( m_sBlendFunc.src, m_sBlendFunc.dst );

    ccGLBindTexture2D( m_pobTexture->getName() );
    ccGLEnableVertexAttribs( kCCVertexAttribFlag_PosColorTex );

#define kQuadSize sizeof(m_sQuad.bl)
#ifdef EMSCRIPTEN
    long offset = 0;
    setGLBufferData(&m_sQuad, 4 * kQuadSize, 0);
#else
    long offset = (long)&m_sQuad;
#endif // EMSCRIPTEN

    // vertex
    int diff = offsetof( ccV3F_C4B_T2F, vertices);
    glVertexAttribPointer(kCCVertexAttrib_Position, 3, GL_FLOAT, GL_FALSE, kQuadSize, (void*) (offset + diff));

    // texCoods
    diff = offsetof( ccV3F_C4B_T2F, texCoords);
    glVertexAttribPointer(kCCVertexAttrib_TexCoords, 2, GL_FLOAT, GL_FALSE, kQuadSize, (void*)(offset + diff));

    // color
    diff = offsetof( ccV3F_C4B_T2F, colors);
    glVertexAttribPointer(kCCVertexAttrib_Color, 4, GL_UNSIGNED_BYTE, GL_TRUE, kQuadSize, (void*)(offset + diff));


    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    CC_INCREMENT_GL_DRAWS(1);
}
