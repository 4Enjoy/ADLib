#include "ADBMFont.h"
using namespace cocos2d;
ADBMFont::ADBMFont(const char *fntFile)
    : m_pConfiguration(nullptr)
{
    m_pConfiguration = CCBMFontConfiguration::create(fntFile);
    m_pConfiguration->retain();

    //The font file should be valid
    assert(m_pConfiguration);

    CCTexture2D *texture = CCTextureCache::sharedTextureCache()->addImage(
                m_pConfiguration->getAtlasName());

    //The texture should be valid
    assert(texture);

    if (CCSpriteBatchNode::initWithTexture(texture, 10))
    {
        m_obContentSize = CCSize(0,0);
        m_obAnchorPoint = ccp(0.5f, 0.5f);
    }

}

ADBMFont::~ADBMFont()
{
    CC_SAFE_RELEASE(m_pConfiguration);
}


ADBMFont* ADBMFont::create(const char *fntFile)
{
    ADBMFont* obj = new ADBMFont(fntFile);
    obj->autorelease();
    return obj;
}


CCSprite* ADBMFont::getCharSprite(wchar_t char_code)
{
    CCRect rect;
    ccBMFontDef fontDef;

    tCCFontDefHashElement *element = NULL;

    // unichar is a short, and an int is needed on HASH_FIND_INT
    unsigned int key = char_code;
    HASH_FIND_INT(m_pConfiguration->m_pFontDefDictionary, &key, element);
    if (! element)
    {
        CCLOGWARN("ADBMFont: characer not found %d", char_code);
        return nullptr;
    }

    fontDef = element->fontDef;

    rect = fontDef.rect;
    rect = CC_RECT_PIXELS_TO_POINTS(rect);

    //rect.origin.x += m_tImageOffset.x;
    //rect.origin.y += m_tImageOffset.y;

    CCSprite* fontChar = new CCSprite;
    fontChar->autorelease();

    fontChar->initWithTexture(m_pobTextureAtlas->getTexture(), rect);
    addChild(fontChar);

    return fontChar;
}

void ADBMFont::arrangeLabel(cocos2d::CCSprite* letters[],
                  const unsigned int length,
                  cocos2d::CCPoint position,
                  cocos2d::CCSize max_size,
                  cocos2d::CCPoint position_to_attach)
{
    //Variables to calculate real size
    float label_width = 0;
    float label_height = 0;

    //First pass calculate label width and height
    for(unsigned int i=0; i<length; ++i)
    {
        CCSprite* sp = letters[i];

        CCSize sp_size = sp->getContentSize();
        label_width += sp_size.width;
        if(sp_size.height > label_height)
            label_height = sp_size.height;
    }

    //The scale of each letter
    float scale = MIN(max_size.width/label_width,
                      max_size.height/label_height);

    label_width *= scale;
    label_height *= scale;

    float dx = 0;

    for(unsigned int i=0; i<length; ++i)
    {
        CCSprite* sp = letters[i];
        sp->setScale(scale);
        sp->setPosition(position_to_attach);

        CCSize target_position = position;
        float letter_width = sp->getContentSize().width*scale;

        target_position.width += dx + letter_width*0.5f -label_width*0.5f;

        dx += letter_width;

        setPositionByChangingAnchorPoint(sp, target_position);
    }
}

void ADBMFont::setPositionByChangingAnchorPoint(
            cocos2d::CCNode* node,
            cocos2d::CCPoint new_position)
{
    CCSize node_size = node->getContentSize() * node->getScale();
    CCPoint diff = node->getPosition() - new_position;

    diff.x /= node_size.width;
    diff.y /= node_size.height;

    CCPoint old_anchor = node->getAnchorPoint();
    node->setAnchorPoint(old_anchor + diff);
}
