#include "ADMenuItem.h"
using namespace cocos2d;

ADMenuItem::ADMenuItem(const CCSize &size)
    : _base_scale(1),
      _click_action(nullptr)
{
    initWithTarget(0,0);
    this->setContentSize(size);
    this->setCascadeColorEnabled(true);
    this->setCascadeOpacityEnabled(true);
}

ADMenuItem::~ADMenuItem()
{
    for(CCNode* node : _nephews)
    {
        node->release();
    }
}

CCAction* ADMenuItem::createOutAction()
{
    if(_out_animation)
        return _out_animation();
    return CCScaleTo::create(0.05f, _base_scale*1.0f);
}

CCAction* ADMenuItem::createOverAction()
{
    if(_in_animation)
        return _in_animation();
    return CCScaleTo::create(0.05f, _base_scale*1.10f);
}

void ADMenuItem::addNephew(CCNode* node, const NephewMode mode)
{
    CCAssert(node, "");
    _nephews.push_back(node);

    node->setScaleX(node->getScaleX() * this->getScaleX());
    node->setScaleY(node->getScaleY() * this->getScaleY());
    node->retain();
    node->setTag(static_cast<int>(mode));
    if(mode == NephewMode::SamePosition)
    {
        node->setPosition(this->getPosition());
    }
}
void ADMenuItem::setVisible(bool visible)
{
    for(CCNode* node : _nephews)
    {
        node->setVisible(visible);
    }

    CCNode::setVisible(visible);
}

void ADMenuItem::setPosition(const CCPoint& newPosition)
{
    CCPoint old_position = this->getPosition();
    CCPoint diff = newPosition - old_position;

    for(CCNode* node : _nephews)
    {
        NephewMode mode = static_cast<NephewMode>(node->getTag());
        if(mode == NephewMode::DeltaPosition)
            node->setPosition(node->getPosition() + diff);
        else if(mode == NephewMode::SamePosition)
            node->setPosition(newPosition);
    }

    CCNode::setPosition(newPosition);
}

void ADMenuItem::setScaleX(float scale)
{
    float scale_diff = scale / CCNode::getScaleX();
    for(CCNode* node : _nephews)
    {
        node->setScaleX(node->getScaleX() * scale_diff);
    }
    CCNode::setScaleX(scale);
}

void ADMenuItem::setScaleY(float scale)
{
    float scale_diff = scale / CCNode::getScaleY();
    for(CCNode* node : _nephews)
    {
        node->setScaleY(node->getScaleY() * scale_diff);
    }
    CCNode::setScaleY(scale);
}

void ADMenuItem::setScaleBase(const float scale)
{
    _base_scale = scale;
    CCNode::setScale(scale);
    for(CCNode* node : _nephews)
    {
        node->setScale(scale);
    }
}

void ADMenuItem::setOpacityToAllChildren(CCNode* target, GLubyte opacity)
{
    CCObject* obj=0;
    CCARRAY_FOREACH(target->getChildren(), obj)
    {
        CCRGBAProtocol* node = dynamic_cast<CCRGBAProtocol*>(obj);
        if(node)
            node->setOpacity(opacity);
        else
        {
            CCNode* node = dynamic_cast<CCNode*>(obj);
            if(node)
                setOpacityToAllChildren(node, opacity);
        }
    }
}

void ADMenuItem::setOpacity(GLubyte opacity)
{
    _realOpacity = opacity;

    for(CCNode* obj : _nephews)
    {
        CCRGBAProtocol* node = dynamic_cast<CCRGBAProtocol*>(obj);
        if(node)
            node->setOpacity(opacity);
    }
    setOpacityToAllChildren(this, opacity);

}

void ADMenuItem::updateDisplayedOpacity(GLubyte parentOpacity)
{
    this->setOpacity(parentOpacity);
}
ADMenuItem::Action ADMenuItem::_on_all_click_action=[](){};
void ADMenuItem::setAllClicksAction(const Action& action)
{
    _on_all_click_action = action;
}

void ADMenuItem::activate()
{
    //onClick();
    if(m_bEnabled)
    {
        onClick();
        if(_on_all_click_action)
            _on_all_click_action();
    }
}

void ADMenuItem::selected()
{
    if(this->numberOfRunningActions() > 0)
        this->stopAllActions();
    this->runAction(createOverAction());

    CCMenuItem::selected();
}

void ADMenuItem::unselected()
{
    if(this->numberOfRunningActions() > 0)
        this->stopAllActions();
    this->runAction(createOutAction());

    CCMenuItem::unselected();
}

void ADMenuItem::setScale(float fScaleX, float fScaleY)
{
    setScaleX(fScaleX);
    setScaleY(fScaleY);
}

void ADMenuItem::setScale(float x)
{
    setScaleX(x);
    setScaleY(x);
}

void ADMenuItem::onClick()
{
    if(_click_action)
        _click_action();

    emit signalOnClick();
}

void ADMenuItem::initWithNode(CCNode* node)
{
    this->addChild(node);
    node->setAnchorPoint(ccp(0.5f, 0.5f));
    node->setPosition(this->getContentSize()*0.5f);
}

void ADMenuItem::initWithSpriteSheetSprite(cocos2d::CCSprite* sp)
{
    sp->setScale(_base_scale);
    this->addNephew(sp);
}

ADMenuItem* ADMenuItem::create(
        CCNode* node,
        const Action& action)
{
    ADMenuItem* item = new ADMenuItem(node->getContentSize());
    item->initWithNode(node);
    item->setClickAction(action);
    item->autorelease();
    return item;
}
 ADMenuItem* ADMenuItem::createWithSpriteSheetSprite(
        cocos2d::CCSprite* sp,
        const Action& action)
 {
     ADMenuItem* item = new ADMenuItem(sp->getContentSize());
     item->initWithSpriteSheetSprite(sp);
     item->setClickAction(action);
     item->autorelease();
     return item;
 }

 ADMenuItem* ADMenuItem::create(
             const cocos2d::CCSize size,
             const Action& action)
 {
     ADMenuItem* item = new ADMenuItem(size);
     item->setClickAction(action);
     item->autorelease();
     return item;
 }
