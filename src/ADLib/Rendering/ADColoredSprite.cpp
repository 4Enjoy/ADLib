#include "ADColoredSprite.h"
using namespace cocos2d;
const GLchar * oneColor_frag = " \n\
#ifdef GL_ES                        \n\
precision mediump float;            \n\
#endif                              \n\
                                    \n\
varying vec4 v_fragmentColor;       \n\
varying vec2 v_texCoord;            \n\
                                    \n\
uniform sampler2D CC_Texture0;      \n\
                                    \n\
uniform vec4 overColor;              \n\
                                    \n\
void main() {                       \n\
    vec4 color = texture2D(CC_Texture0, v_texCoord);           \n\
            //color.r = 1.0;            \n\
            gl_FragColor = overColor * color.a; \n\
}       \n\
";

ADColoredSprite::ADColoredSprite()
{
    _fragShader = oneColor_frag;
    //m_sBlendFunc.src = GL_SRC_ALPHA;
    //m_sBlendFunc.dst = GL_ONE_MINUS_SRC_ALPHA;
}

void ADColoredSprite::setTintColor(const cocos2d::ccColor4B& color)
{
    _color = color;
}

const ccColor4B &ADColoredSprite::getTintColor()
{
    return _color;
}
void ADColoredSprite::buildCustomUniforms()
{
    _color_location = glGetUniformLocation( getShaderProgram()->getProgram(), "overColor");
}

void ADColoredSprite::setCustomUniforms()
{
    ccColor4F color = ccc4FFromccc4B(_color);
    getShaderProgram()->setUniformLocationWith4f(_color_location, color.r, color.g, color.b, color.a);
}

