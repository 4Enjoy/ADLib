#include "ADBakeNode.h"
using namespace cocos2d;
CCSprite* ADBakeNode::bakeNode(CCNode* node)
{
    CCSize size = node->boundingBox().size;

    int width = size.width+0.5f;
    int height = size.height+0.5f;
    CCRenderTexture rt;
    rt.initWithWidthAndHeight(width, height, kTexture2DPixelFormat_RGBA8888);
    rt.begin();
    node->setAnchorPoint(CCPointZero);
    node->setPosition(CCPointZero);
    node->visit();
    rt.end();

    CCSprite first_pass;
    first_pass.initWithTexture(rt.getSprite()->getTexture());
    first_pass.initWithTexture(rt.getSprite()->getTexture()); //To times needed

    CCRenderTexture result;
    result.initWithWidthAndHeight(width, height, kTexture2DPixelFormat_RGBA8888);
    result.begin();
    first_pass.setAnchorPoint(CCPointZero);
    first_pass.setPosition(CCPointZero);
    first_pass.visit();
    result.end();

    CCSprite *sprite = CCSprite::createWithTexture(result.getSprite()->getTexture());
    CCTextureCache::sharedTextureCache()->removeTexture(rt.getSprite()->getTexture());
    return sprite;

}
