#ifndef ADCALLFUNC_H
#define ADCALLFUNC_H
#include "cocos2d.h"
#include <functional>
class ADCallFunc : public cocos2d::CCActionInstant //<NSCopying>
{
public:
    typedef std::function<void()> Action;
    /**
     *  @js ctor
     */
    ADCallFunc(const Action& action)
        : _action(action)
    {
    }

    virtual ~ADCallFunc();

    static ADCallFunc * create(const Action& action);

    /** executes the callback
     * @lua NA
     */
    virtual void execute();
    /** super methods
     * @lua NA
     */
    virtual void update(float time);
    /**
     * @js  NA
     * @lua NA
     */
    CCObject * copyWithZone(cocos2d::CCZone *pZone);

protected:
    Action _action;


};


#endif // ADCALLFUNC_H
