#ifndef ADMENUITEM_H
#define ADMENUITEM_H
#include "cocos2d.h"
#include <ADLib/Generic/ADMethods.h>
#include <ADLib/ADSignals.h>
#include <functional>
#include <vector>

/**
 * @brief How the position of nephews will be changed
 */
enum class NephewMode
{
    /**
     * @brief when parent position is changed the position of nephew is the same
     */
    NoChangeToPosition = 101,
    /**
     * @brief the position of nephew will be the same as parent
     */
    SamePosition = 102,
    /**
     * @brief the position of nephew will be changed by the same amount as parent's
     */
    DeltaPosition = 103
};

/**
 * @brief MenuItem which will be scaled when user presses it
 *
 * Can be used with spritesheet sprites.
 * (It is adviced to put spritesheet to same coordinate space as CCMenu)
 */
class ADMenuItem : public cocos2d::CCMenuItem
{
public:
    typedef std::function<cocos2d::CCAction*()> Animation;

    typedef std::function<void()> Action;
    ~ADMenuItem();

    /**
     * @brief Signal is emitted when user clicks on item
     */
    Signal<> signalOnClick;

    AD_Selector(const float, _base_scale, ScaleBase);

    /**
     * @brief Very important parameter.
     * When user touches the item it will be scaled relative to this parameter.
     * If you would like to make item bigger or smaller you should use this method.
     *
     * Please Note: If you will use normal setScale method then item may change its size after users first interection
     * @param scale
     */
    void setScaleBase(const float scale);

    /**
     * @brief Action which should be performed when user clicks on item
     * You can also connect to the signalOnClick. Both method can work in same time
     */
    AD_Modifier(const Action&, _click_action, ClickAction);

    /**
     * @brief You can speciefy action which will be performed on each click of each ADMenuItem object
     * The best way is to set this method on program startup.
     * Possible usage is to play sound when user clicks on item.
     * @param action
     */
    static void setAllClicksAction(const Action& action);

    /**
     * @brief this function works very similar to addChild while
     * the sprite do not become real child.
     *
     * Scale and SetPosition transformation will be rediricted to nephew
     *
     * @param node
     * @param mode - you can also specify how setPosition will be redirected to this object.
     * @see NephewMode for more details
     */
    void addNephew(CCNode* node,
                   const NephewMode mode=NephewMode::SamePosition);

    void setPosition(const cocos2d::CCPoint& newPosition);
    void setOpacity(GLubyte opacity);
    void updateDisplayedOpacity(GLubyte parentOpacity);

    /**
     *  @brief creates ADMenuItem. It will have the same size as
     *  node->getContentSize() is. And when you click on it the action will be executed
     */
    static ADMenuItem* create(
            CCNode* node,
            const Action& action=[](){});

    /**
     * @brief the same as ordinal create but for sprites which are the part
     * of sprite sheet. The ADMenuItem will have the size of given sprite
     */
    static ADMenuItem* createWithSpriteSheetSprite(
            cocos2d::CCSprite* sp,
            const Action& action=[](){});

    /**
     * @brief create empty node of given size
     */
    static ADMenuItem* create(
            const cocos2d::CCSize size,
            const Action& action=[](){});

    void setScale(float fScaleX, float fScaleY);
    void setScale(float x);
    void setScaleY(float fScaleY);
    void setScaleX(float fScaleX);
    void setVisible(bool visible);
    void setInAnimation(const Animation& a)
    {
        _in_animation = a;
    }

    void setOutAnimation(const Animation& a)
    {
        _out_animation = a;
    }

protected:
    void onClick();
    void activate();
    void unselected();
    void selected();
    ADMenuItem(const cocos2d::CCSize& size);

private:
    Animation _in_animation;
    Animation _out_animation;

    cocos2d::CCAction* createTouchOutAction();
    cocos2d::CCAction* createTouchOverAction();

    float _base_scale;
    Action _click_action;
    typedef std::vector<cocos2d::CCNode*> NephewVector;
    NephewVector _nephews;

    static Action _on_all_click_action;

    cocos2d::CCAction* createOutAction();
    cocos2d::CCAction* createOverAction();

    void setOpacityToAllChildren(CCNode* target, GLubyte opacity);

    void initWithNode(CCNode* node);
    void initWithSpriteSheetSprite(cocos2d::CCSprite* sp);
};

#endif // ADMENUITEM_H
