#ifndef ADLABELBMFONT_H
#define ADLABELBMFONT_H
#include "cocos2d.h"

/**
 * @brief The extension of CCLabelBMFont
 * This class helps to create multiple labels in one draw call
 *
 * ## Usage ##
 * 1. Create instance of this class with ADBMFont::create
 * 2. Add this instance as a child to any CCNode
 * 3. Use ADBMFont::getCharSprite to load letters
 */
class ADBMFont : public cocos2d::CCSpriteBatchNode
{
public:
    /**
     * @brief Loads font from given file
     * @param fntFile the name of file to load font
     */
    ADBMFont(const char *fntFile);

    ~ADBMFont();

    /**
     * @brief Creates autoreleased object with given font file name
     * @param fntFile font file name
     * @return The autoreleased object
     */
    static ADBMFont* create(const char *fntFile);

    /**
     * @brief Returns the sprite for given char
     * @param char_code the UTF-8 char code
     * @return The sprite with the char drawn or nullptr if such char is not found
     * in the font.
     * NOTE: the sprite already has parent.
     * It is forbidden to add this sprite as a child to other CCNodes
     */
    cocos2d::CCSprite* getCharSprite(wchar_t char_code);

    /**
     * @brief Arranges a set of letter like they are in label
     *
     * @param letters pointer to array of letters
     * (not std::vector to make it possible to work with static arrays)
     * @param length size of this array
     * @param position position of the center of label
     * @param max_size maximum width and height of the label
     * (if the label is bigger it will be scaled to this size)
     * @param position_to_attach position where each letter will be placed
     * (the anchor point transformation is used)
     * if you do not need this pass the same value as to the position argument
     */
    static void arrangeLabel(cocos2d::CCSprite* letters[],
                             const unsigned int length,
                             cocos2d::CCPoint position,
                             cocos2d::CCSize max_size,
                             cocos2d::CCPoint position_to_attach);

    /**
     * @brief Changes the anchot point of object to move it to new_position.
     * Old position value will not be changed.
     * NOTE: after applying this function you should not change the anchor point of node
     * @param node CCNode to move
     * @param new_position the new position of the object
     */
    static void setPositionByChangingAnchorPoint(
            cocos2d::CCNode* node,
            cocos2d::CCPoint new_position);
private:
    cocos2d::CCBMFontConfiguration *m_pConfiguration;
};

#endif // ADLABELBMFONT_H
