#include "ADFacebook.h"
#include <cassert>
#include <ADLib/Device/ADStatistics.h>
#include "cocos2d.h"

struct FriendsStorage
{
    FriendsStorage()
    {}
    FriendsStorage(const ADFacebookFriendPtrArr& friends)
        : _friends(friends)
    {}
    ADFacebookFriendPtrArr _friends;
};

ADStreamOut& operator<<(ADStreamOut& os, const FriendsStorage& st)
{
    uint32_t size = st._friends.size();
    os << size;

    for(auto& it : st._friends)
    {
        os << *it;
    }

    return os;
}

ADStreamIn& operator>>(ADStreamIn& is, FriendsStorage& st)
{
    uint32_t size = 0;
    is >> size;

    if(size < ADFacebook::MAX_FRIENDS && is.isOK())
    {
        st._friends.resize(0);
        st._friends.reserve(size);
        for(uint32_t i=0; i<size; ++i)
        {
            ADFacebookFriendPtr f = std::make_shared<ADFacebookFriend>();
            is >> (*f);

            if(!is.isOK())
                break;

            st._friends.push_back(f);
        }
    }

    return is;

}

void ADFacebook::init()
{
    if(_is_inited == false)
    {
        _is_inited = true;
        _implementation = getImplementation();

        CONNECT(ADStorage::signalCreateBlocks, &_obj, &ADFacebook::storageInit);
        CONNECT(ADStorage::signalInitialDataLoaded, &_obj, &ADFacebook::loadFromStorage);
    }
}

void ADFacebook::loadData()
{
    assert(_is_inited);

    if(_implementation)
        _implementation->loadData();

    emit signalFriendsLoaded(_friends);
    emit signalMeLoaded(_me);
}
void ADFacebook::logOut()
{
    assert(_is_inited);
    if(_implementation)
        _implementation->logOut();
}

void ADFacebook::logIn()
{
    assert(_is_inited);
    if(_implementation)
        _implementation->logIn();
}

void ADFacebook::share(const std::string& name,
                      const std::string& caption,
                      const std::string& description,
                      const std::string& link,
                      const std::string& url,
                      const ShareResult& res)
{
    assert(_is_inited);
    if(_implementation)
        _implementation->share(name, caption, description, link, url, res);
}

void ADFacebook::inviteFriends(const std::string& message)
{
    assert(_is_inited);
    if(_implementation)
        _implementation->inviteFriends(message);
}

bool ADFacebook::isLoggedIn()
{
    return _is_logged_in;
}

ADFacebook::Token ADFacebook::getToken()
{
    return _token;
}

void ADFacebook::Platform::sessionOpened(const Token& token)
{
    _is_logged_in = true;
    _token = token;
    cocos2d::CCLog("Token Recieved: %s", _token.c_str());
    //ADStatistics::logEvent("Facebook Online");
    emit signalOnLogIn();
}

void ADFacebook::Platform::sessionClosed()
{
    _is_logged_in = false;
    emit signalOnLogOut();
}

ADFacebook::Platform::~Platform()
{}
void ADFacebook::Platform::onPause()
{}
void ADFacebook::Platform::onResume()
{}

ADFacebook::PlatformPtr ADFacebook::_implementation = nullptr;
bool ADFacebook::_is_logged_in = false;
bool ADFacebook::_is_inited = false;
ADFacebook::Token ADFacebook::_token;

StaticSignal<> ADFacebook::signalOnLogIn;
StaticSignal<> ADFacebook::signalOnLogOut;
StaticSignal<const ADFacebookFriendPtrArr&> ADFacebook::signalFriendsLoaded;
StaticSignal<ADFacebookFriendPtr> ADFacebook::signalMeLoaded;
ADFacebookFriendPtr ADFacebook::_me;
ADFacebook ADFacebook::_obj;

ADFacebookFriendPtrArr ADFacebook::_friends;

void ADFacebook::storageInit()
{
    ADStorage::createValueBlock<FriendsStorage>(
                BLOCK_FRIENDS,
                ADStorage::mergeTakeFirst<FriendsStorage>());
    ADStorage::createValueBlock<ADFacebookFriend>(
                BLOCK_ME,
                ADStorage::mergeTakeFirst<ADFacebookFriend>());
}

void ADFacebook::loadFromStorage()
{
    //Load local friends
    FriendsStorage st = ADStorage::getValue<FriendsStorage>(
                BLOCK_FRIENDS, FriendsStorage());
    if(st._friends.size() > 0)
    {
        _friends = st._friends;
        //
    }

    //Load info about me
    ADFacebookFriend me = ADStorage::getValue<ADFacebookFriend>(
                BLOCK_ME, ADFacebookFriend());

    ADFacebookFriendPtr me_ptr = std::make_shared<ADFacebookFriend>(me);
    if(me_ptr->getUserID() > 0)
    {
        _me = me_ptr;
        //
    }
}

const ADFacebookFriendPtrArr& ADFacebook::getFriends()
{
    return _friends;
}

const ADFacebookFriendPtr& ADFacebook::getMeInfo()
{
    return _me;
}

void ADFacebook::Platform::friendsLoaded(const ADFacebookFriendPtrArr& friends)
{
//    for(ADFacebookFriendPtr f : friends)
//    {
//        cocos2d::CCLog("%s %s", f->getFirstName().c_str(), f->getLastName().c_str());
//    }
    ADStorage::setValue<FriendsStorage>(BLOCK_FRIENDS, FriendsStorage(friends));
    _friends = friends;
    emit signalFriendsLoaded(friends);
}

void ADFacebook::Platform::myInfoLoaded(const ADFacebookFriendPtr& me)
{
    cocos2d::CCLog("ME: %s %s", me->getFirstName().c_str(), me->getLastName().c_str());
    ADStorage::setValue<ADFacebookFriend>(BLOCK_ME, *me);
    _me = me;
    emit signalMeLoaded(_me);
}
