#include "ADFacebookFriend.h"

ADFacebookFriend::ADFacebookFriend()
    : _id(0)
{
}



ADStreamOut& operator<<(ADStreamOut& os, const ADFacebookFriend& f)
{
    os << f.getUserID() << f.getFirstName() << f.getLastName();
    return os;
}

ADStreamIn& operator>>(ADStreamIn& is, ADFacebookFriend& f)
{
    uint64_t id = 0;
    std::string first_name;
    std::string last_name;

    is >> id >> first_name >> last_name;

    if(is.isOK())
    {
        f.setUserID(id);
        f.setFirstName(first_name);
        f.setLastName(last_name);
    }

    return is;
}
