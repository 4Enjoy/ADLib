#ifndef ADFACEBOOKPHOTO_H
#define ADFACEBOOKPHOTO_H
#include <map>
#include "cocos2d.h"
#include "ADFacebookFriend.h"
#include <ADLib/ADSignals.h>
#include <ADLib/Networking/ADHttp.h>

class ADFacebookPhoto : public cocos2d::CCNodeRGBA, public HasSlots
{
public:
    static ADFacebookPhoto* create(const FacebookID user_id,
                                   const cocos2d::CCSize& size);

    /**
     * @brief sets the size of avatars which be requested to facebook
     * @param size
     */
    static void setAvatarSize(const int size);

    /**
     * @brief sets the texture which will be used when no defaul texture is found
     * @param texture
     */
    static void setNoAvatarFileName(const std::string& file_name);
private:
    CCNode* _image;
    ADFacebookPhoto(const FacebookID id,
                    const cocos2d::CCSize& size);

    FacebookID _user_id;

    void setMainImage(cocos2d::CCNode* node);
    void updateAvatar(cocos2d::CCTexture2D* texture);

    struct AvatarStorage
    {
        AvatarStorage(FacebookID user_id,
                      cocos2d::CCTexture2D* texture=nullptr)
            : _texture(texture), _is_updated(false),
              _id(user_id)
        {}
        cocos2d::CCTexture2D* _texture;
        Signal<cocos2d::CCTexture2D*> signalOnChange;
        bool _is_updated;
        FacebookID _id;
    };
    typedef std::shared_ptr<AvatarStorage> AvatarStoragePtr;

    typedef std::map<FacebookID, AvatarStoragePtr> AvatarMap;

    static int _avatar_size;
    static AvatarMap _avatars;
    static std::string _no_avatar_name;
    static AvatarStoragePtr getAvatarForUser(const FacebookID user_id);
    static AvatarStoragePtr createNewAvatar(const FacebookID user_id);
    static void reloadAvatar(const FacebookID user_id, AvatarStoragePtr storage, bool first_time);
    static void updateAvatarFromResponse(ADFacebookPhoto::AvatarStoragePtr avatar,
                                         const ADHttp::ResponsePtr& response);
    static bool updateAvatarFromData(ADFacebookPhoto::AvatarStoragePtr avatar,
                                     char *data, unsigned int length);
    static void saveAvatarToFile(ADFacebookPhoto::AvatarStoragePtr avatar,
                                 char *data, unsigned int length);
    static std::string getFileName(const ADFacebookPhoto::AvatarStoragePtr& avatar);
    static void loadAvatarFromFile(const ADFacebookPhoto::AvatarStoragePtr& avatar);
};

#endif // ADFACEBOOKPHOTO_H
