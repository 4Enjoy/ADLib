#include "ADHighscoreServer.h"
#include <ADLib/Networking/ADHttp.h>
using namespace cocos2d;

class ADHighscoreServer::ResponseReader
{
public:
    ResponseReader(const ADHttp::Response::Data& xml)
        : _xml(xml)
    {}
    bool parse()
    {
        CCSAXParser parser;
        Parser p;
        parser.setDelegator(&p);

        bool res = false;
        if(_xml.size() > 0)
            res = parser.parse(&_xml[0], _xml.size());

        return res;
    }

    ~ResponseReader()
    {}
private:
    class Parser : public CCSAXDelegator
    {
    public:
        Parser()
            : _is_id(false),
              _is_data(false)
        {

        }
    private:
        FacebookID _last_id;
        ScoreDataPtr _data;
        bool _is_id;
        bool _is_data;

        void startElement(void *ctx, const char *name, const char **atts)
        {
            CC_UNUSED_PARAM(ctx);
            CC_UNUSED_PARAM(atts);
            std::string sName((char*)name);

            _is_id = false;
            _is_data = false;
            if( sName == "id" )
            {
                _last_id = 0;
                _is_id = true;
            }
            else if(sName == "d")
            {
                _data = nullptr;
                _is_data = true;
            }
        }

        void endElement(void *ctx, const char *name)
        {
            CC_UNUSED_PARAM(ctx);

            std::string sName((char*)name);
            if( sName == "user" )
            {
                if(_last_id > 0 && _data != nullptr)
                {
                    //cocos2d::CCLog("User highscore: %d", _last_id);
                    setUserHighscore(_last_id, _data);
                }
            }

        }

        void textHandler(void *ctx, const char *ch, int len)
        {
            CC_UNUSED_PARAM(ctx);
            if(_is_id || _is_data)
            {
                std::string text((char*)ch,0,len);

                if(_is_id)
                {
                    std::stringstream ss(std::ios::in | std::ios::out);
                    ss << text;
                    ss >> _last_id;
                }
                else if(_is_data)
                {
                    ADBinaryDataPtr d = ADBinaryData::createFromBase64(text);
                    assert(_data_info);

                    std::stringstream ss(std::ios::binary | std::ios::in | std::ios::out);
                    const ADBinaryData::Data& bin_data = d->getData();

                    if(bin_data.size() > 0)
                    {
                        ss.write(&bin_data[0], bin_data.size());
                        ADStreamIn is(ss);
                        _data = _data_info->read(is);
                    }
                }

            }
        }


    };

    ADHttp::Response::Data _xml;
};

ADHighscoreServer::ADHighscoreServer()
{
    CONNECT(ADStorage::signalCreateBlocks, &_obj, &ADHighscoreServer::createBlocks);
    CONNECT(ADStorage::signalInitialDataLoaded, &_obj, &ADHighscoreServer::loadInitialData);
}

void ADHighscoreServer::setServerUrl(const std::string& s)
{
    _server_url = s;
}
typedef ADHighscoreServer::FriendsHighscoreData FriendsHighscoreData;
struct ADHighscoreServer::HighscoreBlock
{
    HighscoreBlock(const FriendsHighscoreData& data = FriendsHighscoreData())
        : _data(data)
    {}

    FriendsHighscoreData _data;
    static const uint32_t MAX_SCORES = 2000;

    static ScoreDataInfoPtr getDataInfo()
    {
        return _data_info;
    }

    friend ADStreamOut& operator<<(ADStreamOut& os, const HighscoreBlock& info)
    {
        uint32_t size = info._data.size();

        os << size;
        auto data_info = HighscoreBlock::getDataInfo();
        for(auto& it : info._data)
        {
            os << it.first;
            data_info->write(os, it.second);
        }
        return os;
    }

    friend ADStreamIn& operator>>(ADStreamIn& is, HighscoreBlock& info)
    {
        uint32_t size = 0;
        is >> size;
        if(size > MAX_SCORES)
            is.setError();
        else
        {
            auto data_info = HighscoreBlock::getDataInfo();
            for(uint32_t i = 0; i<size && is.isOK(); ++i)
            {
                FacebookID id = 0;
                is >> id;

                ScoreDataPtr data = data_info->read(is);

                if(id != 0 && data != nullptr)
                {
                    info._data[id] = data;
                }
            }
        }
        return is;
    }
};



void ADHighscoreServer::createBlocks()
{
    ADStorage::createValueBlock<HighscoreBlock>(BLOCK_HIGHSCORES, ADStorage::mergeTakeFirst<HighscoreBlock>());
}

void ADHighscoreServer::loadInitialData()
{
    HighscoreBlock block = ADStorage::getValue<HighscoreBlock>(BLOCK_HIGHSCORES, HighscoreBlock());

    _highscore = block._data;
}

void ADHighscoreServer::sendMyNewHighscore(
        const ADFacebookFriendPtr& me,
        const std::string& facebook_token,
        const ScoreDataPtr& data)
{
    assert(_data_info); //You should specify how to understand data
    if(_data_info && me)
    {

        std::stringstream ss(std::ios::binary | std::ios::in | std::ios::out);
        ADStreamOut os(ss);
        _data_info->write(os, data);

        if(os.isOK())
        {
            ADBinaryData bin_data(ss);

            std::string packed_data = bin_data.getDataBase64Encoded();
            uint64_t compare = _data_info->getCompare(data);

            ADHttp::Params params;
            params.add("id", me->getUserID());
            params.add("token", facebook_token);
            params.add("data", packed_data);
            params.add("compare", compare);
            params.add("r", "push");

            auto callback = [](const ADHttp::ResponsePtr& r)
            {
                if(r->getIsOk())
                    cocos2d::CCLog("Highscore updated!");
                else
                    cocos2d::CCLog("Highscore update failed");
            };

            ADHttp::sendRequestPOST(_server_url,
                                    params,
                                    callback,
                                    ADHttp::Priority::ExtremeHigh);

        }

    }

    if(me)
    {
        setUserHighscore(me->getUserID(), data);
        saveLocally();
    }

}

void ADHighscoreServer::setUserHighscore(
        FacebookID id, const ScoreDataPtr& score)
{
    _highscore[id] = score;
}

void ADHighscoreServer::saveLocally()
{
    ADStorage::setValue(BLOCK_HIGHSCORES, HighscoreBlock(_highscore));
}

void ADHighscoreServer::setScoreDataInfo(ScoreDataInfoPtr ptr)
{
    _data_info = ptr;
}

void ADHighscoreServer::highscoreFromServerRecieved(const ADHttp::ResponsePtr& r)
{
    if(r->getIsOk())
    {
        ResponseReader reader(r->getData());

        reader.parse();

        saveLocally();

        emit signalNewScoresArrived(_highscore);
    }
}

ADHighscoreServer::FriendsHighscoreData ADHighscoreServer::updateHighscoresForFriend(
        const ADFacebookFriendPtr& friends)
{
    return updateHighscoresForFriends(ADFacebookFriendPtrArr(1, friends));
}

ADHighscoreServer::FriendsHighscoreData ADHighscoreServer::updateHighscoresForFriends(
        const ADFacebookFriendPtrArr& friends)
{
    std::stringstream ids;
    bool written = false;
    for(size_t i = 0; i<friends.size(); ++i)
    {
        if(friends[i])
        {
            if(written)
                ids << ",";

            FacebookID id = friends[i]->getUserID();

            if(!isHighscoreRequested(id))
            {
                written = true;
                ids << id;
                markHighscoreAsRequested(id);
            }
        }
    }

    if(ids.str().size() > 0)
    {
        std::stringstream url;
        url << _server_url << "?r=get";

        ADHttp::Params params;
        params.add("ids", ids.str());

        auto callback = [](const ADHttp::ResponsePtr& r)
        {
            highscoreFromServerRecieved(r);
        };

        ADHttp::sendRequestPOST(url.str(), params, callback, ADHttp::Priority::High);
        cocos2d::CCLog("Highscore Request: %s", ids.str().c_str());
    }
    return _highscore;
}

bool ADHighscoreServer::isHighscoreRequested(const FacebookID id)
{
    bool res = false;
    auto it = _highscore_requested.find(id);
    if(it != _highscore_requested.end())
    {
        res = it->second;
    }
    return res;
}

void ADHighscoreServer::markHighscoreAsRequested(const FacebookID id)
{
    _highscore_requested[id] = true;
}

ADHighscoreServer::ScoreRequestMap ADHighscoreServer::_highscore_requested;

ADHighscoreServer ADHighscoreServer::_obj;
ADHighscoreServer::FriendsHighscoreData ADHighscoreServer::_highscore;
std::string ADHighscoreServer::_server_url;
ADHighscoreServer::ScoreDataInfoPtr ADHighscoreServer::_data_info;
StaticSignal<const ADHighscoreServer::FriendsHighscoreData&> ADHighscoreServer::signalNewScoresArrived;
