#include "ADFacebookPhoto.h"
#include <ADLib/Networking/ADHttp.h>
#include <fstream>
using namespace cocos2d;
ADFacebookPhoto::ADFacebookPhoto(const FacebookID user_id,
                                 const cocos2d::CCSize& size)
    : _user_id(user_id),
      _image(nullptr)
{
    this->setContentSize(size);
    AvatarStoragePtr avatar = getAvatarForUser(_user_id);
    CONNECT(avatar->signalOnChange, this, &ADFacebookPhoto::updateAvatar);

    updateAvatar(avatar->_texture);
}


int ADFacebookPhoto::_avatar_size = 100;
ADFacebookPhoto::AvatarMap ADFacebookPhoto::_avatars;
std::string ADFacebookPhoto::_no_avatar_name;
void ADFacebookPhoto::updateAvatar(cocos2d::CCTexture2D* texture)
{
    if(texture)
    {
        //Remove old texture
        if(_image != nullptr)
        {
            _image->removeFromParent();
            _image = nullptr;
        }

        //Show photo
        CCSprite* sprite = CCSprite::createWithTexture(texture);
        setMainImage(sprite);
    }
    else
    {
        //Show default
        if(_image == nullptr)
        {
            if(_no_avatar_name.size() > 0)
            {
                //Set default image
                CCSprite* sprite = CCSprite::create(_no_avatar_name.c_str());
                setMainImage(sprite);
            }
            else
            {
                cocos2d::CCLog("No default image for avatar");
            }
        }
    }
}

void ADFacebookPhoto::setMainImage(cocos2d::CCNode* node)
{
    CCSize parent_size = this->getContentSize();
    CCSize node_size = node->getContentSize();

    this->addChild(node);
    node->setAnchorPoint(ccp(0.5f, 0.5f));
    node->setPosition(parent_size*0.5f);



    float scale = MIN(parent_size.width / node_size.width,
                      parent_size.height / node_size.height);

    node->setScale(scale);
}


ADFacebookPhoto::AvatarStoragePtr ADFacebookPhoto::getAvatarForUser(const FacebookID user)
{
    auto it = _avatars.find(user);
    if(it == _avatars.end())
    {
        AvatarStoragePtr avatar = createNewAvatar(user);
        _avatars.insert(AvatarMap::value_type(user, avatar));
        return avatar;
    }
    else
    {
        AvatarStoragePtr avatar = it->second;
        if(!avatar->_is_updated)
            reloadAvatar(user, avatar, false);
        return avatar;
    }
}
void ADFacebookPhoto::updateAvatarFromResponse(ADFacebookPhoto::AvatarStoragePtr avatar,
                                               const ADHttp::ResponsePtr& response)
{
    if(response->getIsOk())
    {
        ADHttp::Response::Data& data = response->getData();
        if(data.size() > 0)
        {
            if(updateAvatarFromData(avatar, &data[0], data.size()))
                saveAvatarToFile(avatar, &data[0], data.size());
        }
    }
}

bool ADFacebookPhoto::updateAvatarFromData(
        ADFacebookPhoto::AvatarStoragePtr avatar,
        char* data, unsigned int length)
{
    cocos2d::CCImage* image = new cocos2d::CCImage;
    bool res = image->initWithImageData(data, length);

    bool is_loaded = false;

    if(!res)
    {
        delete image;
    }
    else
    {
        image->autorelease();
        cocos2d::CCTexture2D* texture = new cocos2d::CCTexture2D;
        bool tex_res = texture->initWithImage(image);
        if(!tex_res)
        {
            delete texture;
        }
        else
        {
            texture->autorelease();
            if(avatar->_texture)
                avatar->_texture->release();

            avatar->_texture = texture;

            if(texture)
                texture->retain();

            emit avatar->signalOnChange(texture);
            //cocos2d::CCLog("Texture was updated");
            is_loaded = true;
        }
    }
    return is_loaded;
}

ADFacebookPhoto::AvatarStoragePtr ADFacebookPhoto::createNewAvatar(
        const FacebookID user_id)
{
    AvatarStoragePtr ptr = std::make_shared<AvatarStorage>(user_id);

    loadAvatarFromFile(ptr);

    bool is_old_avatar_exists = (ptr->_texture != nullptr);

    reloadAvatar(user_id, ptr, !is_old_avatar_exists);

    return ptr;
}

void ADFacebookPhoto::saveAvatarToFile(
        ADFacebookPhoto::AvatarStoragePtr avatar,
        char *data, unsigned int length)
{
    std::string name = getFileName(avatar);
    std::ofstream os(name, std::ios::binary);
    os.write(data, length);
}

void ADFacebookPhoto::loadAvatarFromFile(const ADFacebookPhoto::AvatarStoragePtr& avatar)
{
    std::string file_name = getFileName(avatar);
    unsigned int long size = 0;
    unsigned char* data = CCFileUtils::sharedFileUtils()->getFileData(
                file_name.c_str(),
                "rb", &size);

    if(data)
    {
        if(size > 0)
            updateAvatarFromData(avatar, (char*)data, size);

        delete[] data;
    }
}

std::string ADFacebookPhoto::getFileName(const ADFacebookPhoto::AvatarStoragePtr& avatar)
{
    std::stringstream ss;
    ss << CCFileUtils::sharedFileUtils()->getWritablePath() << "fba_" << avatar->_id;
    return ss.str();
}

void ADFacebookPhoto::reloadAvatar(const FacebookID user_id, AvatarStoragePtr ptr, bool first_time)
{
    std::stringstream url;
    url << "http://graph.facebook.com/" << user_id << "/picture";

    ADHttp::Params params;
    params.add("width", _avatar_size);
    params.add("height", _avatar_size);
	params.add("return_ssl_resources", "0");

    ADHttp::Priority priority = ADHttp::Priority::Medium;
    ptr->_is_updated = true;

    if(!first_time)
        priority = ADHttp::Priority::Low;

    ADHttp::sendRequestGET(url.str(), params, [ptr](const ADHttp::ResponsePtr& response)
    {
        updateAvatarFromResponse(ptr, response);
    },
    priority);
}


ADFacebookPhoto* ADFacebookPhoto::create(
        const FacebookID user_id,
        const cocos2d::CCSize& size)
{
    ADFacebookPhoto* obj = new ADFacebookPhoto(user_id, size);
    obj->init();
    obj->autorelease();
    return obj;
}

void ADFacebookPhoto::setAvatarSize(const int size)
{
    _avatar_size = size;
}

void ADFacebookPhoto::setNoAvatarFileName(const std::string& name)
{
    _no_avatar_name = name;
}
