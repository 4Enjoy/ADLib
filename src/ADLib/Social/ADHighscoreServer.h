#ifndef ADHIGHSCORESERVER_H
#define ADHIGHSCORESERVER_H
#include <ADLib/Storage.h>
#include <ADLib/Social/ADFacebook.h>
#include <ADLib/Social/ADFacebookFriend.h>
#include <ADLib/Networking/ADHttp.h>

/**
 * @brief The ADHighscoreServer class
 * uses API of https://github.com/seagullua/HSS highscore server
 * each user can have only one highscore
 *
 * System can be used only for facebook friends
 */
class ADHighscoreServer : public HasSlots
{
    static const BlockID BLOCK_HIGHSCORES = 0xAD0000A1;
public:
    class ScoreData
    {
    public:
        virtual ~ScoreData()
        {}
    };

    typedef std::shared_ptr<ScoreData> ScoreDataPtr;

    class ScoreDataInfo
    {
    public:
        /**
         * @brief Returns the number. When number is bigger then the score is higher
         * @return
         */
        virtual uint64_t getCompare(const ScoreDataPtr&)=0;

        virtual void write(ADStreamOut&, const ScoreDataPtr&)=0;

        virtual ScoreDataPtr read(ADStreamIn&)=0;

        virtual ~ScoreDataInfo()
        {}
    };

    typedef std::shared_ptr<ScoreDataInfo> ScoreDataInfoPtr;
    typedef std::map<FacebookID, ScoreDataPtr> FriendsHighscoreData;

    static FriendsHighscoreData updateHighscoresForFriends(
            const ADFacebookFriendPtrArr& friends);
    static FriendsHighscoreData updateHighscoresForFriend(
            const ADFacebookFriendPtr& friends);

    static void sendMyNewHighscore(const ADFacebookFriendPtr& me,
                                   const std::string& facebook_token,
                                   const ScoreDataPtr& data);

    static StaticSignal<const FriendsHighscoreData&> signalNewScoresArrived;

    /**
     * @brief url where server is located
     * @param s
     */
    static void setServerUrl(const std::string& s);

    /**
     * @brief sets how the score data should be interpreted
     * @param ptr
     */
    static void setScoreDataInfo(ScoreDataInfoPtr ptr);

private:
    void createBlocks();
    void loadInitialData();

    static void highscoreFromServerRecieved(const ADHttp::ResponsePtr& r);
    static void saveLocally();
    static void setUserHighscore(FacebookID id, const ScoreDataPtr& score);
    static ADHighscoreServer _obj;
    static FriendsHighscoreData _highscore;
    static std::string _server_url;
    static ScoreDataInfoPtr _data_info;

    typedef std::map<FacebookID, bool> ScoreRequestMap;

    static ScoreRequestMap _highscore_requested;

    static bool isHighscoreRequested(const FacebookID);
    static void markHighscoreAsRequested(const FacebookID);

    ADHighscoreServer();
    class ResponseReader;
    struct HighscoreBlock;
};

#endif // ADHIGHSCORESERVER_H
