#ifndef ADFACEBOOKFRIEND_H
#define ADFACEBOOKFRIEND_H
#include <string>
#include <ADLib/Storage/ADStream.h>
#include <ADLib/Generic/ADMethods.h>
#include <memory>
#include <vector>
typedef uint64_t FacebookID;
class ADFacebookFriend
{
public:

    ADFacebookFriend();
    AD_SelectorModifier(const std::string&, _first_name, FirstName);
    AD_SelectorModifier(const std::string&, _last_name, LastName);
    AD_SelectorModifier(const FacebookID, _id, UserID);
private:
    std::string _first_name;
    std::string _last_name;
    FacebookID _id;
};

ADStreamOut& operator<<(ADStreamOut& os, const ADFacebookFriend&);
ADStreamIn& operator>>(ADStreamIn& is, ADFacebookFriend&);

typedef std::shared_ptr<ADFacebookFriend> ADFacebookFriendPtr;

typedef std::vector<ADFacebookFriendPtr> ADFacebookFriendPtrArr;
#endif // ADFACEBOOKFRIEND_H
