#ifndef ADFACEBOOK_H
#define ADFACEBOOK_H
#include <memory>
#include <string>
#include <functional>
#include <ADLib/ADSignals.h>
#include <ADLib/Social/ADFacebookFriend.h>
#include <ADLib/Storage.h>

class ADFacebook : public HasSlots
{
    static const BlockID BLOCK_FRIENDS = 0xAD0000F1;
    static const BlockID BLOCK_ME = 0xAD0000F2;
public:
    static const int MAX_FRIENDS = 2000;
    typedef std::string Token;
    /**
     * @brief Must be called as early as possible
     */
    static void init();

    static StaticSignal<> signalOnLogIn;
    static StaticSignal<> signalOnLogOut;
    static StaticSignal<const ADFacebookFriendPtrArr&> signalFriendsLoaded;
    static StaticSignal<ADFacebookFriendPtr> signalMeLoaded;

    typedef std::function<void (bool)> ShareResult;
    static void share(const std::string& name,
                      const std::string& caption,
                      const std::string& description,
                      const std::string& url,
                      const std::string& picture,
                      const ShareResult& res=[](const bool){});

    /**
     * @brief When the application is inited and data can be loaded
     */
    static void loadData();

    /**
     * @brief When user clicks log in button
     */
    static void logIn();

    /**
     * @brief When user clicks log out button
     */
    static void logOut();

    /**
     * @brief Whether the user is logged in
     * @return
     */
    static bool isLoggedIn();

    /**
     * @brief Returns the Facebook token
     * @return
     */
    static Token getToken();

    /**
     * @brief Returns currenly avaliable friends
     * @return
     */
    static const ADFacebookFriendPtrArr& getFriends();

    /**
     * @brief Returns info about current user
     * @return
     */
    static const ADFacebookFriendPtr& getMeInfo();

    static void inviteFriends(const std::string &message);
    /**
     * @brief Class to provide platform dependent implementation
     */
    class Platform
    {
    public:
        virtual ~Platform();


        /**
         * @brief How to implement
         * 1. Start session without GUI
         * 2. Load info about Me and friends
         * @param key
         */
        virtual void loadData() = 0;

        /**
         * @brief How to implement:
         * 1. If not logged in show log in window
         */
        virtual void logIn() = 0;

        /**
         * @brief How to implement:
         * 1. If logged in then log out
         */
        virtual void logOut() = 0;

        /**
         * @brief How to implement:
         * 1. Send pause event to module
         */
        virtual void onPause();

        /**
         * @brief How to implement:
         * 1. Send resume even to module
         */
        virtual void onResume();

        /**
         * @brief Call when session has started
         * @param token
         */
        void sessionOpened(const Token& token);

        /**
         * @brief Call when session has closed
         */
        void sessionClosed();

        /**
         * @brief Friends was loaded from facebook
         */
        void friendsLoaded(const ADFacebookFriendPtrArr&);

        /**
         * @brief Info about me is loaded
         */
        void myInfoLoaded(const ADFacebookFriendPtr&);


        virtual void inviteFriends(const std::string &message)
        {
            cocos2d::CCLog("Friends invite not implemented");
        }

        /**
         * @brief Perform share via Facebook.
         * Call res callback from Cocos thread with true or false depending on share result
         * @param name
         * @param caption
         * @param description
         * @param link
         * @param url
         * @param res
         */
        virtual void share(const std::string& name,
                           const std::string& caption,
                           const std::string& description,
                           const std::string& url,
                           const std::string& picture,
                           const ShareResult& res)=0;
    };
    typedef std::shared_ptr<Platform> PlatformPtr;
private:

    void storageInit();
    void loadFromStorage();
    static ADFacebook _obj;
    static ADFacebookFriendPtrArr _friends;
    static ADFacebookFriendPtr _me;
    static PlatformPtr getImplementation();
    static PlatformPtr _implementation;
    static bool _is_logged_in;
    static bool _is_inited;
    static Token _token;
};

#endif // ADFACEBOOK_H
