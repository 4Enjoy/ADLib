#include "ADHttp.h"
#include <queue>
#include <ADLib/Common/ADThreadPool.h>
namespace ADHttp_impl
{
static const int MAX_THREADS = 1;
static ADThreadPool _pool(MAX_THREADS);
}



using namespace ADHttp_impl;



void ADHttp::prepareAndMakeRequest(const RequestType& type,
                                   const std::string& url,
                                   const Params& params,
                                   const Callback& callback,
                                   const Priority priority)
{
    _pool.start();
    _pool.addTask([type, url, params, callback](){
        makeRequest(type, url, params, callback);
    }, static_cast<int>(priority));
}

void ADHttp::prepareAndMakeRequestRawPost(const std::string& url,
                                          const std::string& body,
                                          const Callback& callback,
                                          const Priority priority)
{
    _pool.start();
    _pool.addTask([url, body, callback](){
        makeRequestRawPost(url, body, callback);
    }, static_cast<int>(priority));
}


void ADHttp::downloadFile(const std::string& url,
                          const std::string& file_dir,
                          const Callback& callback,
                          const ProgressCallback& progress_callback,
                          const Priority priority)
{
    _pool.start();
    _pool.addTask([=](){
        platformDownloadFile(url, file_dir, callback, progress_callback);
    }, static_cast<int>(priority));
}

