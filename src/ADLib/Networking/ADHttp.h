#ifndef ADHTTP_H
#define ADHTTP_H
#include <functional>
#include <ADLib/Common/ADParams.h>
#include <ADLib/Generic/ADMethods.h>
#include <memory>

/**
 * @brief Class with basic HTTP operations
 */
class ADHttp
{
public:
    typedef ADParams Params;

    enum class Priority
    {
        Low = 1,
        Medium = 10,
        High = 100,
        ExtremeHigh = 1000
    };

    /**
     * @brief The Response class which has info about server response
     */
    class Response
    {
    public:
        typedef std::vector<char> Data;
        AD_SelectorModifier(const int, _code, Code);
        AD_SelectorModifier(const Data&, _data, Data);
        AD_SelectorModifier(const bool, _is_ok, IsOk);

        Data& getData()
        {
            return _data;
        }

        Response()
            : _code(-1), _is_ok(false)
        {}
    private:
        int _code;
        bool _is_ok;
        Data _data;

    };
    typedef std::shared_ptr<Response> ResponsePtr;

    typedef std::function<void (const ResponsePtr&)> Callback;
    typedef std::function<void (const uint64_t downloaded, const uint64_t overall)> ProgressCallback;

    /**
    * @brief sends POST request to the server
    */
    static void sendRequestPOST(const std::string& url,
                                const Params& params,
                                const Callback& callback = [](const ResponsePtr&){},
                                const Priority priority = Priority::Medium)
    {
        prepareAndMakeRequest(RequestType::POST, url, params, callback, priority);
    }

    /**
    * @brief sends POST request to the server
    */
    static void sendRequestPOST(const std::string& url,
                                const std::string& body,
                                const Callback& callback = [](const ResponsePtr&){},
                                const Priority priority = Priority::Medium)
    {
        prepareAndMakeRequestRawPost(url, body, callback, priority);
    }

    /**
    * @brief sends GET request to the server
    */
    static void sendRequestGET(const std::string& url,
                               const Params& params,
                               const Callback& callback = [](const ResponsePtr&){},
                               const Priority priority = Priority::Medium)
    {
        prepareAndMakeRequest(RequestType::GET, url, params, callback, priority);
    }


    /**
    * @brief Downloads file from given url and stores it on drive. Also reports download progress.
    * The request starts immediately. Only supported in CURL implementation
    */
    static void downloadFile(const std::string& url,
                             const std::string& file_dir,
                             const Callback& callback = [](const ResponsePtr&){},
                             const ProgressCallback& progress_callback = [](uint64_t, uint64_t){},
    const Priority priority=Priority::Medium);

    enum class RequestType
    {
        POST=1,
        GET=2
    };
private:

    static void prepareAndMakeRequest(const RequestType& type,
                                      const std::string& url,
                                      const Params& params,
                                      const Callback& callback,
                                      const Priority priority);
    static void prepareAndMakeRequestRawPost(const std::string& url,
                                             const std::string& body,
                                             const Callback& callback,
                                             const Priority priority);

    static void makeRequest(const RequestType& type,
                            const std::string& url,
                            const Params& params,
                            const Callback& callback);

    static void makeRequestRawPost(const std::string& url,
                                   const std::string& body,
                                   const Callback& callback);

    static void platformDownloadFile(const std::string& url,
                                     const std::string& file_dir,
                                     const Callback& callback = [](const ResponsePtr&){},
                                     const ProgressCallback& progress_callback = [](uint64_t, uint64_t){}
    );

};

#endif // ADHTTP_H
