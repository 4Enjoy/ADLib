#ifndef ADSIGNALS_H
#define ADSIGNALS_H
#include "sigslot.h"
#include <map>
namespace _impl_ADSignals
{
    class NoType
    {};
}

template <class A1=_impl_ADSignals::NoType,
          class A2=_impl_ADSignals::NoType,
          class A3=_impl_ADSignals::NoType,
          class A4=_impl_ADSignals::NoType,
          class A5=_impl_ADSignals::NoType>
class Signal;

template <>
class Signal<_impl_ADSignals::NoType,
        _impl_ADSignals::NoType,
        _impl_ADSignals::NoType,
        _impl_ADSignals::NoType,
        _impl_ADSignals::NoType>
        : public sigslot::signal0<>
{
};

template <class A1>
class Signal<A1,
        _impl_ADSignals::NoType,
        _impl_ADSignals::NoType,
        _impl_ADSignals::NoType,
        _impl_ADSignals::NoType>
        : public sigslot::signal1<A1>
{
};

template <class A1, class A2>
class Signal<A1,
        A2,
        _impl_ADSignals::NoType,
        _impl_ADSignals::NoType,
        _impl_ADSignals::NoType>
        : public sigslot::signal2<A1, A2>
{
};

template <class A1, class A2, class A3>
class Signal<A1,
        A2,
        A3,
        _impl_ADSignals::NoType,
        _impl_ADSignals::NoType>
        : public sigslot::signal3<A1, A2, A3>
{
};

class HasSlots : public sigslot::has_slots<>
{};

template <class A1, class A2, class A3, class A4>
class Signal<A1,
        A2,
        A3,
        A4,
        _impl_ADSignals::NoType>
        : public sigslot::signal4<A1, A2, A3, A4>
{
};

template <class A1=_impl_ADSignals::NoType,
          class A2=_impl_ADSignals::NoType,
          class A3=_impl_ADSignals::NoType,
          class A4=_impl_ADSignals::NoType>
class StaticSignal
{
public:
    typedef Signal<A1, A2, A3, A4> S;
    StaticSignal()
    {}
    void operator()()
    {
        signal()();
    }

    void operator()(const A1& a1)
    {
        signal()(a1);
    }

    void operator()(const A1& a1, const A2& a2)
    {
        signal()(a1, a2);
    }

    void operator()(const A1& a1, const A2& a2, const A3& a3)
    {
        signal()(a1, a2, a3);
    }

    void operator()(const A1& a1, const A2& a2, const A3& a3, const A4& a4)
    {
        signal()(a1, a2, a3, a4);
    }

    template<class Obj, class Method>
    void connect(Obj o, Method m)
    {
        signal().connect(o, m);
    }

    S& signal()
    {
        typedef std::map<StaticSignal*, S*> StorageMap;
        static StorageMap _m;

        auto it = _m.find(this);
        if(it == _m.end())
        {
            S* s = new S;
            _m[this] = s;
            return *s;
        }
        return *(it->second);
    }
private:

};

#define emit /**/
template<class Obj, class Method, class Signal>
inline void CONNECT(Signal& s, Obj o, Method m)
{
    s.connect(o, m);
}

//template<class Obj, class Method, class Signal>
//inline void CONNECT_STATIC(Signal& s, Obj o, Method m)
//{
//    s.signal().connect(o, m);
//}
#endif


