#include "ADStorage.h"
#include <mutex>
#include <memory>
#include <atomic>
#include <fstream>
#include <ADLib/Device/ADDeviceEvents.h>
#include "ADFileNameUTF8.h"
#include "cocos2d.h"
#include <thread>

using namespace cocos2d;

typedef ADStorage::MergeFunction MergeFunction;
typedef ADStorage::BlockID BlockID;



enum class BlockType
{
    Value,
    IntMap,
    StringMap

};
typedef int32_t KInt;
typedef std::string KString;

typedef std::lock_guard<std::mutex> Guard;

class Block
{
public:
    Block(const BlockID id,
          const BlockType type,
          const MergeFunction& merge,
          const std::type_index& value_type)
        : _id(id), _block_type(type),
          _merge_function(merge), _value_type(value_type)
    {}

    BlockID getID() const
    {
        return _id;
    }
    BlockType getBlockType() const
    {
        return _block_type;
    }
    const std::type_index& getValueType() const
    {
        return _value_type;
    }
private:
    MergeFunction _merge_function;
    std::type_index _value_type;
    BlockID _id;
    BlockType _block_type;
};

typedef std::mutex Mutex;

/**
 * @brief The block which stores one value
 * thread safe
 */
class ValueBlockStorage
{
public:
    ValueBlockStorage(const ADBinaryDataPtr& data=nullptr)
        : _lock(),
          _data(data)
    {}

    void setData(const ADBinaryDataPtr& data)
    {
        Guard lock(_lock);
        _data = data;

    }

    ADBinaryDataPtr getData(bool& is_ok)
    {
        ADBinaryDataPtr res = nullptr;
        Guard lock(_lock);
        res = _data;

        is_ok = (res != nullptr);
        return res;
    }

    void read(ADStreamIn& is)
    {
        Guard lock(_lock);

        auto d = ADBinaryData::createEmpty();

        d->read(is);

        if(is.isOK())
            _data = d;
    }

    void write(ADStreamOut& os)
    {
        Guard lock(_lock);
        _data->write(os);
    }
private:
    ValueBlockStorage(const ValueBlockStorage&);
    ValueBlockStorage& operator=(const ValueBlockStorage&);
    ADBinaryDataPtr _data;
    Mutex _lock;
};

/**
 * @brief The block which stores Map of values
 * thread safe
 */
template<class Key>
class MapBlockStorage
{
public:
    typedef std::map<Key, std::shared_ptr<ValueBlockStorage> > Map;
    MapBlockStorage()
        : _lock()
    {}

    void removeValue(const Key& id)
    {
        auto it = _data.find(id);
        if(it != _data.end())
        {
            _data.erase(it);
        }
    }

    ADBinaryDataPtr getData(const Key& key, bool& is_ok)
    {
        ADBinaryDataPtr res = nullptr;
        Guard lock(_lock);
        auto it = _data.find(key);
        if(it == _data.end())
            is_ok = false;
        else
            res = it->second->getData(is_ok);

        return res;
    }

    void setData(const Key& key, const ADBinaryDataPtr data)
    {
        Guard lock(_lock);
        auto it = _data.find(key);
        if(it == _data.end())
        {
            _data.insert(typename Map::value_type(
                             key,
                             std::make_shared<ValueBlockStorage>(data)));
        }
        else
            it->second->setData(data);
    }

    bool hasValue(const Key& key)
    {
        bool res = false;
        Guard lock(_lock);
        res = _data.find(key) != _data.end();

        return res;
    }

    void read(ADStreamIn& is)
    {
        Guard lock(_lock);

        uint32_t size = 0;
        is >> size;
        for(uint32_t i = 0; i < size; ++i)
        {
            typename Map::key_type k;
            is >> k;
            if(!is.isOK())
                break;

            std::shared_ptr<ValueBlockStorage> block(new ValueBlockStorage);
            block->read(is);

            if(!is.isOK())
                break;

            _data[k] = block;
        }
    }

    void write(ADStreamOut& os)
    {
        Guard lock(_lock);

        uint32_t size = _data.size();
        os << size;

        for(auto& it : _data)
        {
            os << it.first;
            it.second->write(os);
        }

    }

private:
    MapBlockStorage(const MapBlockStorage&);
    MapBlockStorage& operator=(const MapBlockStorage&);
    Map _data;
    Mutex _lock;
};

/**
 * @brief The block which stores other blocks
 * thread safe
 */
template <class T, class Key>
class BlockBlockStorage
{
    typedef std::shared_ptr<T> TPtr;
public:
    BlockBlockStorage()
        : _lock()
    {}


    void removeValue(const BlockID& id)
    {
        auto it = _data.find(id);
        if(it != _data.end())
        {
            _data.erase(it);
        }
    }

    ADBinaryDataPtr getMapData(const BlockID block_id ,
                               const Key& key,
                               bool& is_ok)
    {
        ADBinaryDataPtr res = nullptr;
        Guard lock(_lock);
        auto it = _data.find(block_id);
        if(it == _data.end())
            is_ok = false;
        else
            res = it->second->getData(key, is_ok);
        return res;
    }

    bool hasMapValue(const BlockID block_id, const Key& key)
    {
        bool res = false;
        Guard lock(_lock);
        auto it = _data.find(block_id);
        if(it != _data.end())
            res = it->second->hasValue(key);
        return res;
    }

    void setMapData(const BlockID block_id,
                    const Key& key,
                    const ADBinaryDataPtr& data)
    {
        Guard lock(_lock);
        auto it = _data.find(block_id);
        if(it == _data.end())
        {
            //Create new map
            TPtr bs(new T);
            bs->setData(key, data);
            _data.insert(typename Map::value_type(block_id, bs));
        }
        else
            it->second->setData(key, data);

    }

    void read(ADStreamIn& is)
    {
        Guard lock(_lock);

        uint32_t size = 0;
        is >> size;
        for(uint32_t i = 0; i < size; ++i)
        {
            typename Map::key_type k;
            is >> k;
            if(!is.isOK())
                break;

            TPtr block(new T);
            block->read(is);

            if(!is.isOK())
                break;

            _data[k] = block;
        }
    }

    void write(ADStreamOut& os)
    {
        Guard lock(_lock);

        uint32_t size = _data.size();
        os << size;

        for(auto& it : _data)
        {
            os << it.first;
            it.second->write(os);
        }

    }

private:
    BlockBlockStorage(const BlockBlockStorage&);
    BlockBlockStorage& operator=(const BlockBlockStorage&);
    typedef std::map<BlockID, TPtr> Map;
    Map _data;
    Mutex _lock;
};

class StorageStructure
{
public:
    static StorageStructure* getStructure()
    {
        return &_static_structure;
    }

    void addBlock(const Block& block)
    {
        _map.insert(BlockMap::value_type(block.getID(), block));
    }

    bool isValidBlock(const BlockID id,
                      const BlockType type)
    {
        auto it = _map.find(id);
        if(it == _map.end())
        {
            printBlockNotFound(id);
            return false;
        }

        const Block& b = it->second;
        bool res = b.getBlockType() == type;
        if(!res)
        {
            printTypeError(id, type, b.getBlockType());
        }
        return res;
    }

    bool hasBlock(const BlockID id)
    {
        return _map.find(id) != _map.end();
    }

    bool isValidBlock(const BlockID id,
                      const BlockType type,
                      const std::type_index& value_type)
    {
        auto it = _map.find(id);
        if(it == _map.end())
        {
            printBlockNotFound(id);
            return false;
        }

        const Block& b = it->second;

        bool b_type = b.getBlockType() == type;
        if(!b_type)
        {
            printTypeError(id, type, b.getBlockType());
        }

        bool b_value_type = b.getValueType() == value_type;
        if(!b_value_type)
        {
            printValueTypeError(id, value_type, b.getValueType());
        }
        return b_type && b_value_type;
    }

private:
    void printBlockNotFound(const BlockID id)
    {
        std::stringstream ss;
        ss << "Block with id ";
        pringBlockID(ss, id);
        ss << " is not found. " << std::endl;
        showError(ss);

    }
    void printValueTypeError(const BlockID id,
                             const std::type_index& access,
                             const std::type_index& real)
    {
        std::stringstream ss;
        ss << "Your are acessing block: ";
        pringBlockID(ss, id);
        ss << " using type ["
                  << access.name() << "] while it has type [" << real.name() << "] " << std::endl;
        showError(ss);
    }

    void printTypeError(const BlockID id,
                        const BlockType access,
                        const BlockType real)
    {
        std::stringstream ss;
        ss << "Your are acessing block: ";
        pringBlockID(ss, id);
        ss << " with type ";
        printType(ss, access);
        ss << " while it has type ";
        printType(ss, real);
        ss << std::endl;
        showError(ss);
    }
    void pringBlockID(std::stringstream& ss, const BlockID id)
    {
        ss << "#" << id << " (" << std::hex <<  id << ")";
    }

    void printType(std::stringstream& ss, const BlockType type)
    {
        if(type == BlockType::IntMap)
            ss << "(Map[int])";
        else if(type == BlockType::StringMap)
            ss << "(Map[string])";
        else if(type == BlockType::Value)
            ss << "(Value)";
        else
            ss << "(UNDEFINED)";
    }

    void showError(std::stringstream& ss)
    {
        cocos2d::CCMessageBox(ss.str().c_str(), "Storage Error");
        std::cerr << ss.str();
        cocos2d::CCLog(ss.str().c_str());
    }

    StorageStructure()
    {}
    typedef std::map<BlockID, Block> BlockMap;
    BlockMap _map;
    static StorageStructure _static_structure;
};

StorageStructure StorageStructure::_static_structure;
StaticSignal<> ADStorage::signalCreateBlocks;
StaticSignal<> ADStorage::signalInitialDataLoaded;
StaticSignal<BlockID, const ADBinaryDataPtr&> ADStorage::signalValueChanged;
StaticSignal<BlockID, const int, const ADBinaryDataPtr&> ADStorage::signalMapIntChanged;
StaticSignal<BlockID, const std::string&, const ADBinaryDataPtr&> ADStorage::signalMapStringChanged;

class StorageNode
{
public:
    StorageNode()
        : _is_dirty(false)
    {

    }

    bool hasValue(const BlockID id)
    {
        return _st_value.hasValue(id);
    }

    bool hasMapValue(const BlockID id,
                     const int key)
    {
        return _st_map_int.hasMapValue(id, key);
    }

    bool hasMapValue(const BlockID id,
                     const std::string& key)
    {
        return _st_map_string.hasMapValue(id, key);
    }

    void setValue(const BlockID id,
                  const ADBinaryDataPtr& data)
    {
        _is_dirty = true;
        _st_value.setData(id, data);
    }

    void setMapValue(
            const BlockID id,
            const int key,
            const ADBinaryDataPtr& data)
    {
        _is_dirty = true;
        _st_map_int.setMapData(id, key, data);
    }

    void setMapValue(
            const BlockID id,
            const std::string& key,
            const ADBinaryDataPtr& data)
    {
        _is_dirty = true;
        _st_map_string.setMapData(id, key, data);
    }

    void cleanBlock(const BlockID id)
    {
        _is_dirty = true;
        _st_value.removeValue(id);
    }

    void cleanMapBlock(const BlockID id, const KeyType type)
    {
        _is_dirty = true;
        if(type == KeyType::String)
        {
            _st_map_string.removeValue(id);
        }
        else if(type == KeyType::Int)
        {
            _st_map_int.removeValue(id);
        }
    }


    ADBinaryDataPtr getValue(
            const BlockID id,
            bool& is_ok)
    {
        return _st_value.getData(id, is_ok);
    }

    ADBinaryDataPtr getMapValue(
            const BlockID id,
            const int key,
            bool& is_ok)
    {
        return _st_map_int.getMapData(id, key, is_ok);
    }

    ADBinaryDataPtr getMapValue(
            const BlockID id,
            const std::string& key,
            bool& is_ok)
    {
        return _st_map_string.getMapData(id, key, is_ok);
    }

    bool isDirty()
    {
        return _is_dirty;
    }

    void markAsNotDirty()
    {
        _is_dirty = false;
    }

    void read(ADStreamIn& is)
    {
        _st_value.read(is);
        _st_map_int.read(is);
        _st_map_string.read(is);
    }

    void write(ADStreamOut& os)
    {
        _st_value.write(os);
        _st_map_int.write(os);
        _st_map_string.write(os);
    }

private:
    std::atomic<bool> _is_dirty;
    MapBlockStorage<BlockID> _st_value;
    BlockBlockStorage<MapBlockStorage<KInt>, KInt> _st_map_int;
    BlockBlockStorage<MapBlockStorage<KString>, KString> _st_map_string;
};


typedef std::shared_ptr<StorageNode> ADStorageNodePtr;
class MainNode : public cocos2d::CCObject, public HasSlots
{
public:
    static const ADStorageNodePtr& getNode()
    {
        if(_main_node == nullptr)
            prepare();
        return _main_node;
    }

    static void setFileName(const std::string& file_name)
    {
        //Can be changed before first read write operation
        assert(!_loaded);

        _file_name = file_name;
    }
    static bool isLoaded()
    {
        return _loaded;
    }

    static void setSaveInterval(float seconds)
    {
        //This must be changed before first read
        assert(!_loaded);

        _save_interval = seconds;
    }

    static void saveToFile();

    static void saveFileAsync();
private:
    void onAppClose()
    {
        if(_loaded)
        {
            unregisterSaveFunction();
            saveToFile();
        }
    }

    void onPause()
    {
        if(_loaded)
        {
            unregisterSaveFunction();
            saveToFile();
        }
    }

    void onResume()
    {
        if(_loaded)
        {
            registerSaveFunction();
        }
    }

    void registerSaveFunction()
    {
        cocos2d::CCDirector::sharedDirector()->getScheduler()->scheduleSelector(
                    schedule_selector(MainNode::saveFileAsyncByTime), &_obj, _save_interval, false);
    }

    void unregisterSaveFunction()
    {
        cocos2d::CCDirector::sharedDirector()->getScheduler()->unscheduleAllForTarget(&_obj);
    }

    void saveFileAsyncByTime(float)
    {
        //cocos2d::CCLog("File save by time");
        saveFileAsync();
    }

    static std::atomic<bool> _file_io_in_progress;

    static MainNode _obj;
    static float _save_interval;
    static bool _loaded;
    static void prepare();
    static void loadFromFile();
    static ADStorageNodePtr _main_node;
    static std::string _file_name;
};
float MainNode::_save_interval = 20;
std::atomic<bool> MainNode::_file_io_in_progress(false);
MainNode MainNode::_obj;

void MainNode::prepare()
{
    _main_node = ADStorageNodePtr(new StorageNode);
    _file_name = cocos2d::CCFileUtils::sharedFileUtils()->getWritablePath() + _file_name;

    emit ADStorage::signalCreateBlocks();

    loadFromFile();
    _loaded = true;
    _obj.registerSaveFunction();
    emit ADStorage::signalInitialDataLoaded();

    CONNECT(ADDeviceEvents::signalOnClose, &_obj, &MainNode::onAppClose);
    CONNECT(ADDeviceEvents::signalOnResume, &_obj, &MainNode::onResume);
    CONNECT(ADDeviceEvents::signalOnPause, &_obj, &MainNode::onPause);
}

void MainNode::loadFromFile()
{
    //This should be first io operation
    assert(!_file_io_in_progress);

    _file_io_in_progress = true;

    cocos2d::CCLog("Reading from file: %s", _file_name.c_str());

	std::ifstream f(ensureUtf8FileName(_file_name).c_str(), std::ios::binary);
    ADStreamIn ais(f);
    _main_node->read(ais);

    if(ais.isOK())
        cocos2d::CCLog("File Readed.");
    else
        cocos2d::CCLog("File read failed");
    f.close();
    _file_io_in_progress = false;
}

void MainNode::saveToFile()
{
    bool in_progress = _file_io_in_progress.load();
    if(!in_progress && _loaded && _main_node->isDirty())
    {
        if(_file_io_in_progress.compare_exchange_strong(in_progress, true))
        {
            cocos2d::CCLog("Saving file: %s", _file_name.c_str());
			auto file_name = ensureUtf8FileName(_file_name);
			std::ofstream f(file_name.c_str(), std::ios::binary);
            ADStreamOut aos(f);
            _main_node->write(aos);

            if(aos.isOK())
            {
                _main_node->markAsNotDirty();
                cocos2d::CCLog("File saved.");
            }
            else
                cocos2d::CCLog("File save failed.");

            f.close();
            _file_io_in_progress = false;
        }

    }
}

void MainNode::saveFileAsync()
{
    std::thread([]{ MainNode::saveToFile(); }).detach();
}

bool MainNode::_loaded = false;
ADStorageNodePtr MainNode::_main_node;
std::string MainNode::_file_name = "saves.sad";



void ADStorage::setSaveFileName(const std::string& s)
{
    MainNode::setFileName(s);
}

bool ADStorage::hasValue(const BlockID id)
{
    //Wrong block type or id
    assert(StorageStructure::getStructure()->isValidBlock(id, BlockType::Value));

    return MainNode::getNode()->hasValue(id);
}

bool ADStorage::hasMapValue(const BlockID id,
                            const int key)
{
    //Wrong block type or id
    assert(StorageStructure::getStructure()->isValidBlock(id, BlockType::IntMap));


    return MainNode::getNode()->hasMapValue(id, key);
}

bool ADStorage::hasMapValue(const BlockID id,
                            const std::string& key)
{
    //Wrong block type or id
    assert(StorageStructure::getStructure()->isValidBlock(id, BlockType::StringMap));


    return MainNode::getNode()->hasMapValue(id, key);
}
void ADStorage::saveAsync()
{
    MainNode::saveFileAsync();
}

void ADStorage::do_createMapBlock(const BlockID id,
                                  const KeyType type,
                                  const MergeFunction& function,
                                  const std::type_index& index)
{
    //Check map block type
    assert(type == KeyType::Int ||
           type == KeyType::String);

    //You are not allowed to create blocks after first read
    assert(!MainNode::isLoaded());

    //Block ID duplication
    assert(!StorageStructure::getStructure()->hasBlock(id));

    BlockType t = BlockType::IntMap;
    if(type == KeyType::String)
        t= BlockType::StringMap;
    StorageStructure::getStructure()->addBlock(Block(id, t, function, index));
}

void ADStorage::do_createValueBlock(
        const BlockID id,
        const MergeFunction& function,
        const std::type_index& index)
{
    //You are not allowed to create blocks after first read
    assert(!MainNode::isLoaded());

    //Block ID duplication
    assert(!StorageStructure::getStructure()->hasBlock(id));

    StorageStructure::getStructure()->addBlock(Block(id, BlockType::Value, function, index));
}

void ADStorage::do_setValue(const BlockID id,
                            const ADBinaryDataPtr& data,
                            const std::type_index& type)
{
    //Wrong block type or id or type
    assert(StorageStructure::getStructure()->isValidBlock(id, BlockType::Value, type));

    MainNode::getNode()->setValue(id, data);
    emit signalValueChanged(id, data);
}

void ADStorage::do_setMapValue(
        const BlockID id,
        const int key,
        const ADBinaryDataPtr& data,
        const std::type_index& type)
{
    //Wrong block type or id or type
    assert(StorageStructure::getStructure()->isValidBlock(
               id, BlockType::IntMap, type));

    MainNode::getNode()->setMapValue(id, key, data);
    emit signalMapIntChanged(id, key, data);
}
void ADStorage::loadInitialData()
{
    MainNode::getNode();
}

void ADStorage::do_setMapValue(
        const BlockID id,
        const std::string& key,
        const ADBinaryDataPtr& data,
        const std::type_index& type)
{
    //Wrong block type or id or type
    assert(StorageStructure::getStructure()->isValidBlock(
               id, BlockType::StringMap, type));

    MainNode::getNode()->setMapValue(id, key, data);
    emit signalMapStringChanged(id, key, data);
}

void ADStorage::cleanBlock(const BlockID id)
{
    MainNode::getNode()->cleanBlock(id);
}

void ADStorage::cleanMapBlock(const BlockID id, const KeyType type)
{
    MainNode::getNode()->cleanMapBlock(id, type);
}

void ADStorage::setSaveToFileInterval(float seconds)
{
    MainNode::setSaveInterval(seconds);
}

ADBinaryDataPtr ADStorage::do_getValue(
        const BlockID id,
        bool& is_ok,
        const std::type_index& type)
{
    //Wrong block type or id or type
    assert(StorageStructure::getStructure()->isValidBlock(
               id, BlockType::Value, type));

    return MainNode::getNode()->getValue(id, is_ok);
}

ADBinaryDataPtr ADStorage::do_getMapValue(
        const BlockID id,
        const int key,
        bool& is_ok,
        const std::type_index& type)
{
    //Wrong block type or id or type
    assert(StorageStructure::getStructure()->isValidBlock(
               id, BlockType::IntMap, type));

    return MainNode::getNode()->getMapValue(id, key, is_ok);
}

ADBinaryDataPtr ADStorage::do_getMapValue(
        const BlockID id,
        const std::string& key,
        bool& is_ok,
        const std::type_index& type)
{
    //Wrong block type or id or type
    assert(StorageStructure::getStructure()->isValidBlock(
               id, BlockType::StringMap, type));

    return MainNode::getNode()->getMapValue(id, key, is_ok);
}
