#include "ADFileNameUTF8.h"
#include "cocos2d.h"
#include "../Device/ADInfo.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
#include "CCWinRTUtils.h"
const std::wstring ensureUtf8FileName(const std::string& f)
{
	return cocos2d::CCUtf8ToUnicode(f.c_str(), -1);
}
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
void ensureNonSyncDir(const std::string& location);
#else
void ensureNonSyncDir(const std::string&)
{}
#endif


const std::string ADFileStorage::getPathForUserFiles()
{
    static std::string res;
    if(res.size() == 0)
    {
        res = cocos2d::CCFileUtils::sharedFileUtils()->getWritablePath();
    }
    return res;
}
const std::string ADFileStorage::getPathForCacheFiles()
{
    static std::string res;
    if(res.size() == 0)
    {
        res = cocos2d::CCFileUtils::sharedFileUtils()->getWritablePath();
        if(ADInfo::getPlatform() == ADPlatform::iOS)
        {            
            std::string needle = "Documents/";
            if(res.size() > needle.size())
            {
                std::string test = res.substr(res.size() - needle.size());
                if(test == needle)
                {
                    std::string short_path = res.substr(0, res.size() - needle.size());
                    short_path += "Library/Application Support/Local/";
                    res = short_path;
                    ensureNonSyncDir(res);
                }
                cocos2d::CCLog("iOS Platform");
            }
            
        }
    }
    return res;
}
