//
//  ADFileNameUTF8.m
//  MavkaUk
//
//  Created by Admin on 30.12.14.
//
//

#import <Foundation/Foundation.h>
#include <string>
void ensureNonSyncDir(const std::string& path) {
    NSString* location = [NSString stringWithUTF8String:path.c_str()];
    NSURL *URL = [NSURL fileURLWithPath:location];
    
    if(![[NSFileManager defaultManager] fileExistsAtPath: [URL path]])
    {
        NSError *error = nil;
        if(![[NSFileManager defaultManager] createDirectoryAtURL:URL withIntermediateDirectories:YES
                                                      attributes:nil error:&error]) {
            NSLog(@"Can't create dir %@ from backup %@", [URL lastPathComponent], error);
        }
        
        BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                      forKey: NSURLIsExcludedFromBackupKey error: &error];
        if(!success){
            NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
        }
    }
    
}
