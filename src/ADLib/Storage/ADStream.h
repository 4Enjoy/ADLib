#ifndef ADSTREAM_H
#define ADSTREAM_H
#include <iostream>
#include <string>
#include <vector>
#include <cassert>
#include <stdint.h>
namespace impl
{
class ADStreamValues
{
public:
    static const uint32_t MaxArraySize = 255 * 100;
};
}

class ADStreamIn
{
public:
    explicit ADStreamIn(std::istream& is) : _is(is), _ok(true)
    {
    }
    bool isOK() const
    {
        return _is.good() && _ok;
    }
    void setError()
    {
        _ok = false;
    }
	
    void readBytes(char* x, unsigned int size)
    {
        _is.read(x, size);
    }

    void readBytes(char& x)
    {
        readBytes(&x, 1);
    }

    ADStreamIn& operator>>(uint32_t& x)
    {
        typedef uint32_t Ui;
        typedef unsigned char Uc;

        char r[4];
        readBytes(r, 4);

        x = Ui(Uc(r[0])) + Ui(Uc(r[1])) * 0x100
                + Ui(Uc(r[2])) * 0x10000 + Ui(Uc(r[3])) * 0x1000000;
        return *this;
    }
    ADStreamIn& operator>>(int64_t& x)
    {
        typedef uint32_t Ui;
        Ui a=0;
        Ui b=0;

        *this >> b >> a;
        uint64_t x_ = (uint64_t(a) << 32) | uint64_t(b);

        x = static_cast<int64_t>(x_);

        //Check -1
        assert(int64_t(-1) == static_cast<int64_t>(static_cast<uint64_t>(int64_t(-1))));


        return *this;
    }
    ADStreamIn& operator>>(bool& t)
    {
        char buf = 0;
        readBytes(buf);
        if(buf == 0)
            t = false;
        else
            t = true;
        return *this;
    }

    ADStreamIn& operator>>(uint16_t& x)
    {
        typedef uint16_t Ui;
        typedef unsigned char Uc;

        char r[2];
        readBytes(r, 2);

        x = Ui(Uc(r[0])) + Ui(Uc(r[1])) * 0x100;
        return *this;
    }
private:

    ADStreamIn(const ADStreamIn&);
    ADStreamIn& operator=(const ADStreamIn&);
    std::istream& _is;
    bool _ok;
};

class ADStreamOut
{
public:
    explicit ADStreamOut(std::ostream& os)
        : _ok(true), _os(os)
    {
    }

    void writeBytes(const char* x, unsigned int size)
    {
        _os.write(x, size);
    }

    void writeBytes(const char x)
    {
        writeBytes(&x, 1);
    }
    bool isOK() const
    {
        return _os.good() && _ok;
    }

    ADStreamOut& operator<<(bool t)
    {
        char bc = 1;
        if(!t)
            bc = 0;
        this->writeBytes(bc);
        return *this;
    }

    ADStreamOut& operator<<(uint32_t t)
    {
        char buf[4];
        buf[0] = t % 256;
        t /= 256;
        buf[1] = t % 256;
        t /= 256;
        buf[2] = t % 256;
        t /= 256;
        buf[3] = t % 256;
        writeBytes(buf, 4);
        return *this;
    }
    ADStreamOut& operator<<(int64_t t_)
    {
        //Check -1
        assert(int64_t(-1) == static_cast<int64_t>(static_cast<uint64_t>(int64_t(-1))));
        uint64_t t = static_cast<int64_t>(t_);

        uint32_t a = 0;
        uint32_t b = 0;

        uint64_t modulus = (uint64_t(1) << 32);
        a = uint32_t(t % modulus);
        t /= modulus;
        b = uint32_t(t % modulus);
        *this << a << b;
        return *this;
    }
    ADStreamOut& operator<<(uint16_t t)
    {
        char buf[2];
        buf[0] = t % 256;
        t /= 256;
        buf[1] = t % 256;
        writeBytes(buf, 2);
        return *this;
    }
private:
    ADStreamOut(const ADStreamOut&);
    ADStreamOut& operator=(const ADStreamOut&);
    std::ostream& _os;
    bool _ok;
};




inline ADStreamOut& operator<<(ADStreamOut& os, const std::string& str)
{
	uint16_t str_size = static_cast<uint16_t>(str.size());
    assert(str.size() < impl::ADStreamValues::MaxArraySize);

    os << str_size;
    os.writeBytes(&str[0], str_size);
    return os;
}

template <class T>
inline ADStreamOut& operator<<(ADStreamOut& os, const std::vector<T>& vec)
{
    uint16_t vec_size = static_cast<uint16_t>(vec.size());
    assert(vec.size() < impl::ADStreamValues::MaxArraySize);

    os << vec_size;
    for(uint16_t i=0; i<vec_size; ++i)
    {
        os << vec[i];
    }
    return os;
}

inline ADStreamOut& operator<<(ADStreamOut& os, const std::vector<char>& vec)
{
    uint16_t vec_size = static_cast<uint16_t>(vec.size());
    assert(vec.size() < impl::ADStreamValues::MaxArraySize);

    os << vec_size;
    os.writeBytes(&vec[0], vec_size);
    return os;
}

inline ADStreamIn& operator>>(ADStreamIn& is, std::string& str)
{
    uint16_t size = 0;
    is >> size;
    if(is.isOK() && size < impl::ADStreamValues::MaxArraySize)
    {
        str.resize(size);
        is.readBytes(&str[0], size);
    }
    else
    {
        is.setError();
    }
    return is;
}

template <class T>
inline ADStreamIn& operator>>(ADStreamIn& is, std::vector<T>& vec)
{
    uint16_t size = 0;
    is >> size;
    if(is.isOK() && size < impl::ADStreamValues::MaxArraySize)
    {
        vec.resize(0);
        vec.reserve(size);
        T buffer;
        for(uint16_t i=0; i<size && is.isOK(); ++i)
        {
            is >> buffer;
            vec.push_back(buffer);
        }
    }
    else
    {
        is.setError();
    }
    return is;
}


inline ADStreamIn& operator>>(ADStreamIn& is, std::vector<char>& str)
{
    uint16_t size = 0;
    is >> size;
    if(is.isOK() && size < impl::ADStreamValues::MaxArraySize)
    {
        str.resize(size);
        is.readBytes(&str[0], size);
    }
    else
    {
        is.setError();
    }

    return is;
}

inline ADStreamIn& operator>>(ADStreamIn& is, int32_t& id)
{
    uint32_t b = 0;
    is >> b;

    id = b;
    return is;
}

inline ADStreamOut& operator<<(ADStreamOut& os, const int32_t id)
{
    os << uint32_t(id);
    return os;
}

inline ADStreamIn& operator>>(ADStreamIn& is, uint64_t& id)
{
    int64_t b = 0;
    is >> b;

    id = static_cast<uint64_t>(b);
    return is;
}

inline ADStreamOut& operator<<(ADStreamOut& os, const uint64_t id)
{
    os << static_cast<int64_t>(id);
    return os;
}

#endif // ADSTREAM_H
