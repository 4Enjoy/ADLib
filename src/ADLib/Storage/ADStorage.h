#ifndef ADSTORAGE_H
#define ADSTORAGE_H
#include <string>
#include <functional>
#include <ADLib/Storage/ADBinaryData.h>
#include <typeindex>
#include <ADLib/ADSignals.h>
enum class KeyType
{
    Int,
    String
};

/**
 * @brief Each function which would like to
 * create blocks in storage must be connected to signalCreateBlocks
 * before first read from ADStorage.
 *
 * You can read data from ADStorage only after  signalInitialDataLoaded
 * was emitted
 */
class ADStorage
{
public:
    typedef std::function<ADBinaryDataPtr (const ADBinaryDataPtr&, const ADBinaryDataPtr&)> MergeFunction;
    static StaticSignal<> signalCreateBlocks;
    static StaticSignal<> signalInitialDataLoaded;

    static void loadInitialData();
    static void setSaveToFileInterval(float seconds);


    template<class T>
    static std::function<const T(const T&, const T&)> mergeMax();

    template<class T>
    static std::function<const T(const T&, const T&)> mergeMin();

    template<class T>
    static std::function<const T(const T&, const T&)> mergeTakeFirst();

    template<class T>
    static std::function<const T(const T&, const T&)> mergeTakeSecond();


    typedef int32_t BlockID;

    /**
     * @brief Name of local file where data will be saved
     * @param s
     */
    static void setSaveFileName(const std::string& s);

    template <class T>
    static void createValueBlock(const BlockID,
                                 const std::function<const T (const T&, const T&)>& merge_function);

    template <class T>
    static void createValueBlock(const BlockID id)
    {
        auto f = mergeMax<T>();
        createValueBlock(id, f);
    }

    template <class T>
    static void createMapBlock(const BlockID,
                              const KeyType,
                              const std::function<const T (const T&, const T&)>& merge_function);

    template <class T>
    static void createMapBlock(const BlockID id,
                               const KeyType type)
    {
        auto f = mergeMax<T>();
        createMapBlock(id, type, f);
    }

    static void cleanBlock(const BlockID id);
    static void cleanMapBlock(const BlockID id, const KeyType type);

    template <class T>
    static void setValue(const BlockID id,
                         const T&);

    template <class T>
    static void setMapValue(const BlockID id,
                            const int key,
                            const T&);

    template <class T>
    static void setMapValue(const BlockID id,
                            const std::string& key,
                            const T&);

    template <class T>
    static T getValue(const BlockID id,
                      const T& default_value);

    template <class T>
    static T getMapValue(const BlockID id,
                         const int key,
                         const T& default_value);

    template <class T>
    static T getMapValue(const BlockID id,
                         const std::string& key,
                         const T& default_value);

    static bool hasValue(const BlockID id);
    static bool hasMapValue(const BlockID id,
                            const int key);
    static bool hasMapValue(const BlockID id,
                            const std::string& key);
    static void saveAsync();
private:
    friend class ADStorageListener;

    static StaticSignal<BlockID, const ADBinaryDataPtr&> signalValueChanged;
    static StaticSignal<BlockID, const int, const ADBinaryDataPtr&> signalMapIntChanged;
    static StaticSignal<BlockID, const std::string&, const ADBinaryDataPtr&> signalMapStringChanged;

    static void do_createValueBlock(const BlockID,
                                    const MergeFunction& function,
                                    const std::type_index& index);

    static void do_createMapBlock(const BlockID,
                                  const KeyType,
                                  const MergeFunction& function,
                                  const std::type_index& index);


    static void do_setValue(const BlockID id,
                            const ADBinaryDataPtr& data,
                            const std::type_index& type);

    static void do_setMapValue(
            const BlockID id,
            const int key,
            const ADBinaryDataPtr& data,
            const std::type_index& type);

    static void do_setMapValue(
            const BlockID id,
            const std::string& key,
            const ADBinaryDataPtr& data,
            const std::type_index& type);


    static ADBinaryDataPtr do_getValue(
            const BlockID id,
            bool& is_ok,
            const std::type_index& type);

    static ADBinaryDataPtr do_getMapValue(
            const BlockID id,
            const int key,
            bool& is_ok,
            const std::type_index& type);

    static ADBinaryDataPtr do_getMapValue(
            const BlockID id,
            const std::string& key,
            bool& is_ok,
            const std::type_index& type);

    template <class T>
    static MergeFunction wrapMergeFunction(const std::function<const T (const T&, const T&)>& func);

};


template <class T>
inline ADStorage::MergeFunction ADStorage::wrapMergeFunction(const std::function<const T (const T&, const T&)>& func)
{
    return [=](const ADBinaryDataPtr& a, const ADBinaryDataPtr& b) -> ADBinaryDataPtr
    {
        T a_v;
        T b_v;

        bool a_read_res = a->getValue<T>(a_v);
        bool b_read_res = b->getValue<T>(b_v);

        //Failed to read from package
        assert(a_read_res && b_read_res);

        T res = func(a_v, b_v);

        return ADBinaryData::create<T>(res);
    };
}

template <class T>
inline void ADStorage::createValueBlock(
        const BlockID id,
        const std::function<const T (const T&, const T&)>& merge_function)
{
    do_createValueBlock(id,
                        wrapMergeFunction<T>(merge_function),
                        typeid(T));
}

template <class T>
inline void ADStorage::createMapBlock(
        const BlockID id,
        const KeyType key_type,
        const std::function<const T (const T&, const T&)>& merge_function)
{
    do_createMapBlock(id,
                      key_type,
                      wrapMergeFunction<T>(merge_function),
                      typeid(T));
}

template <class T>
inline void ADStorage::setValue(
        const BlockID id,
        const T& val)
{
    do_setValue(id, ADBinaryData::create<T>(val), typeid(T));
}

template <class T>
inline void ADStorage::setMapValue(
        const BlockID id,
        const int key,
        const T& val)
{
    do_setMapValue(id, key, ADBinaryData::create<T>(val), typeid(T));
}

template <class T>
inline void ADStorage::setMapValue(
        const BlockID id,
        const std::string& key,
        const T& val)
{
    do_setMapValue(id, key, ADBinaryData::create<T>(val), typeid(T));
}

template <class T>
inline T readADBinaryData(const ADBinaryDataPtr& data)
{
    assert(data); //The data can not be nullptr
    T res;
    bool read_ok = data->getValue<T>(res);
    assert(read_ok); //If assertion fails then in this cell corrupted data is stored

    return res;
}

template <class T>
inline T ADStorage::getValue(
        const BlockID id,
        const T& default_value)
{
    bool is_ok = true;
    ADBinaryDataPtr data = do_getValue(id, is_ok, typeid(T));

    if(!is_ok)
        return default_value;
    else
        return readADBinaryData<T>(data);
}

template <class T>
inline T ADStorage::getMapValue(const BlockID id,
                                const int key,
                                const T& default_value)
{

    bool is_ok = true;
    ADBinaryDataPtr data = do_getMapValue(id, key, is_ok, typeid(T));

    if(!is_ok)
        return default_value;
    else
        return readADBinaryData<T>(data);

}

template <class T>
inline T ADStorage::getMapValue(const BlockID id,
                                const std::string& key,
                                const T& default_value)
{

    bool is_ok = true;
    ADBinaryDataPtr data = do_getMapValue(id, key, is_ok, typeid(T));

    if(!is_ok)
        return default_value;
    else
        return readADBinaryData<T>(data);

}


template<class T>
inline std::function<const T(const T&, const T&)> ADStorage::mergeMax()
{
    return [](const T& a, const T& b) -> T
    {
        if(a < b)
            return a;
        return a;
    };
}

template<class T>
inline std::function<const T(const T&, const T&)> ADStorage::mergeMin()
{
    return [](const T& a, const T& b) -> T
    {
        if(a < b)
            return a;
        return b;
    };
}

template<class T>
inline std::function<const T(const T&, const T&)> ADStorage::mergeTakeFirst()
{
    return [](const T& a, const T&) -> T
    {
        return a;
    };
}

template<class T>
inline std::function<const T(const T&, const T&)> ADStorage::mergeTakeSecond()
{
    return [](const T&, const T& b) -> T
    {
        return b;
    };
}

typedef ADStorage::BlockID BlockID;
#endif // ADSTORAGE_H
