#include "ADBinaryData.h"
#include "cocos2d.h"
static uint32_t MAX_CHUNK_SIZE = 1000000;


#ifndef NDEBUG

static int __test_num_ad = 1;
class ADBinaryDataTester
{
public:
    static bool runTests();
    static bool _t;
};
bool ADBinaryDataTester::_t = ADBinaryDataTester::runTests();

template <class InputType, class OutputType>
void runTest(const InputType& input)
{
    ADBinaryDataPtr a = ADBinaryData::create<InputType>(input);
    OutputType s;
    a->getValue<OutputType>(s);
    cocos2d::CCLog("Test #%d data size: %d", __test_num_ad++, a->getData().size());
    assert(s == input);
}



bool ADBinaryDataTester::runTests()
{
    runTest<int, unsigned int>(1000);
    runTest<int, char>(100);
    runTest<uint64_t, uint64_t>(0xFFFFFFFEAAFFFFFF);
    runTest<std::string, std::string>("abccccccbbb");
    runTest<short int, int>(-100);
    return true;
}

#endif
void ADBinaryData::write(ADStreamOut& os)
{
    uint32_t size = _data.size();
    os << size;
    os.writeBytes(&_data[0], size);
}

void ADBinaryData::read(ADStreamIn& is)
{
    uint32_t size = 0;
    is >> size;

    if(size > MAX_CHUNK_SIZE)
        is.setError();

    if(is.isOK())
    {
        _data.resize(size);
        is.readBytes(&_data[0], size);
    }
}

static const std::string base64_chars =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz"
        "0123456789+/";


static inline bool is_base64(unsigned char c)
{
    return (isalnum(c) || (c == '+') || (c == '/'));
}

std::string base64_encode(unsigned char const* bytes_to_encode, unsigned int in_len)
{
    std::string ret;
    int i = 0;
    int j = 0;
    unsigned char char_array_3[3];
    unsigned char char_array_4[4];

    while (in_len--)
    {
        char_array_3[i++] = *(bytes_to_encode++);
        if (i == 3)
        {
            char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
            char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
            char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
            char_array_4[3] = char_array_3[2] & 0x3f;

            for(i = 0; (i <4) ; i++)
                ret += base64_chars[char_array_4[i]];
            i = 0;
        }
    }

    if (i)
    {
        for(j = i; j < 3; j++)
            char_array_3[j] = '\0';

        char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
        char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
        char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
        char_array_4[3] = char_array_3[2] & 0x3f;

        for (j = 0; (j < i + 1); j++)
            ret += base64_chars[char_array_4[j]];

        while((i++ < 3))
            ret += '=';

    }

    return ret;
}

void base64_decode(std::string const& encoded_string, ADBinaryData::Data& ret)
{
    int in_len = encoded_string.size();
    int i = 0;
    int j = 0;
    int in_ = 0;
    unsigned char char_array_4[4], char_array_3[3];

    ret.reserve(encoded_string.size());

    while (in_len-- && ( encoded_string[in_] != '=') && is_base64(encoded_string[in_]))
    {
        char_array_4[i++] = encoded_string[in_]; in_++;
        if (i ==4)
        {
            for (i = 0; i <4; i++)
                char_array_4[i] = base64_chars.find(char_array_4[i]);

            char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
            char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
            char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

            for (i = 0; (i < 3); i++)
                ret.push_back(char_array_3[i]);
            i = 0;
        }
    }

    if (i)
    {
        for (j = i; j <4; j++)
            char_array_4[j] = 0;

        for (j = 0; j <4; j++)
            char_array_4[j] = base64_chars.find(char_array_4[j]);

        char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
        char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
        char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

        for (j = 0; (j < i - 1); j++)
            ret.push_back(char_array_3[j]);
    }

}

std::string ADBinaryData::getDataBase64Encoded() const
{
    std::string res = "";
    if(_data.size())
    {
        res = base64_encode((const unsigned char*)&_data[0], _data.size());
    }
    return res;
}

ADBinaryDataPtr ADBinaryData::createFromBase64(const std::string& base64)
{
    ADBinaryDataPtr res = createEmpty();
    base64_decode(base64, res->_data);

    return res;
}
