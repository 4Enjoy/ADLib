#ifndef ADBINARYDATA_H
#define ADBINARYDATA_H

#include "cocos2d.h"
#include <ADLib/Storage/ADStream.h>
#include <sstream>
#include <vector>
#include <memory>
/**
 * @brief The class which stores any data in binary format
 */
class ADBinaryData
{
public:
    typedef std::vector<char> Data;
    typedef std::shared_ptr<ADBinaryData> ADBinaryDataPtr;

    static ADBinaryDataPtr createEmpty()
    {
        return std::make_shared<ADBinaryData>();
    }

    /**
     * Any integer type convert to int64_t
     */
    template<class T>
    static ADBinaryDataPtr create(
            typename std::enable_if<std::is_integral<T>::value, T>::type value)
    {
        return doCreate(static_cast<int64_t>(value));
    }

    /**
     * Normal Type
     */
    template<class T>
    static ADBinaryDataPtr create(
            const typename std::enable_if<!std::is_integral<T>::value, T>::type& value)
    {
        return doCreate(value);
    }

    static ADBinaryDataPtr createFromBase64(const std::string& base64);

    const Data& getData()
    {
        return _data;
    }


    template<class T>
    bool getValue(
            typename std::enable_if<std::is_integral<T>::value, T>::type& value)
    {
        int64_t v = 0;
        bool res = doGetValue(v);
        value = static_cast<T>(v);
        return res;
    }

    template<class T>
    bool getValue(
            typename std::enable_if<!std::is_integral<T>::value, T>::type& value)
    {
        return doGetValue(value);
    }

    std::string getDataBase64Encoded() const;

    void write(ADStreamOut& os);

    void read(ADStreamIn& is);

    /**
     * @brief usde create to create object
     * @param is
     */
    ADBinaryData(std::stringstream& is)
        : _data()
    {
        is.seekg (0, is.end);
        int length = is.tellg();
        is.seekg (0, is.beg);

        _data.resize(length);
        is.read(&_data[0],length);
    }

    ADBinaryData()
        : _data()
    {
    }

    /**
     * @brief Runs assertion based test to test whether class works normaly on launching platform
     */
    static void runPlatformTests();
private:



    template<class T>
    static ADBinaryDataPtr doCreate(const T& v)
    {
        std::stringstream ss(std::ios::in | std::ios::out | std::ios::binary);
        ADStreamOut os(ss);
        os << v;
        return std::make_shared<ADBinaryData>(ss);
    }

    template <class T>
    bool doGetValue(T& obj)
    {
        std::stringstream ss(std::ios::in | std::ios::out | std::ios::binary);
        ss.write(&_data[0], _data.size());
        ss.seekg(0, ss.beg);
        ADStreamIn bs(ss);

        bs >> obj;
        return bs.isOK();
    }

    Data _data;
};
typedef std::shared_ptr<ADBinaryData> ADBinaryDataPtr;
#endif // ADBINARYDATA_H
