#ifndef ADBLOCK_H
#define ADBLOCK_H
#include "ADStorageListener.h"
namespace ADBlockImpl
{
static ADStorageListener STORAGE_LISTENER;
}

template <class T, int BLOCK_ID>
class ADBlock : public HasSlots
{
public:
    ADBlock(const T& default_value)
        : _default_value(default_value)
    {
        CONNECT(ADStorage::signalCreateBlocks, this, &ADBlock::initBlock);
        ADBlockImpl::STORAGE_LISTENER.addValueChangeAction<T>(BLOCK_ID, [this](const T& new_value){
            emit this->signalOnChange(new_value);
        });
    }

    T getValue() const
    {
        return ADStorage::getValue<T>(BLOCK_ID, _default_value);
    }

    void setValue(const T& value)
    {
        ADStorage::setValue<T>(BLOCK_ID, value);
    }

    Signal<T> signalOnChange;
private:
    void initBlock()
    {
        ADStorage::createValueBlock<T>(BLOCK_ID);
    }

    T _default_value;
};

#endif // ADBLOCK_H
