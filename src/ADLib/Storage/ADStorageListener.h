#ifndef ADSTORAGELISTENER_H
#define ADSTORAGELISTENER_H
#include <functional>
#include "ADStorage.h"

/**
 * @brief Class which can notify of changes of ADStorage
 */
class ADStorageListener : public HasSlots
{
    typedef std::function<void()> BlockChangeAction;
    typedef std::function<void(const ADBinaryDataPtr&)> ValueChangeAction;
    typedef ADStorage::BlockID BlockID;
public:
    /**
     * @brief notifies when block is changed.
     * The block type does not matter.
     * If the block has Value type use addValueChangeAction
     */
    void addBlockChangeAction(const BlockID,
                              const BlockChangeAction&);

    /**
     * @brief adds callback for change of Value block
     * if block has other type this method will never be called
     */
    template <class T>
    void addValueChangeAction(const BlockID,
                              const std::function<void (const T&)>& action);

    /**
     * @brief adds callback for change of one value in Map block with Int keys
     * if block has other type this method will never be called.
     * If you need to respond to any change in the block use addBlockChangeAction
     */
    template <class T>
    void addMapChangeAction(const BlockID,
                            const int key,
                            const std::function<void (const T&)>& action);

    /**
     * @brief adds callback for change of one value in Map block with String keys
     * if block has other type this method will never be called.
     * If you need to respond to any change in the block use addBlockChangeAction
     */
    template <class T>
    void addMapChangeAction(const BlockID,
                            const std::string& key,
                            const std::function<void (const T&)>& action);


    ADStorageListener();
private:
    template <class Key>
    struct BlockKey
    {
        BlockKey(const BlockID id, const Key key)
            : _id(id), _key(key)
        {}

        Key _key;
        BlockID _id;

        bool operator<(const BlockKey& a) const
        {
            return _id < a._id || (_id == a._id && _key < a._key);
        }
    };

    typedef std::vector<ValueChangeAction> ValueChangeArr;
    typedef std::vector<BlockChangeAction> BlockChangeArr;
    typedef std::map<BlockKey<int>, ValueChangeArr> IntKeyMap;
    typedef std::map<BlockKey<std::string>, ValueChangeArr> StringKeyMap;
    typedef std::map<BlockID, ValueChangeArr> ValueMap;
    typedef std::map<BlockID, BlockChangeArr> BlockMap;

    void onValueChanged(const BlockID id, const ADBinaryDataPtr& data);
    void onMapValueChangedInt(const BlockID id,
                           const int key,
                           const ADBinaryDataPtr& data);

    void onMapValueChangedString(const BlockID id,
                           const std::string& key,
                           const ADBinaryDataPtr& data);

    void callAll(const ValueChangeArr& arr, const ADBinaryDataPtr& data);
    void callAll(const BlockChangeArr& arr);

    void onBlockChanged(const BlockID id);

    void daddValueChangeAction(const BlockID,
                              const ValueChangeAction& action);

    void daddMapChangeAction(const BlockID,
                            const int key,
                            const ValueChangeAction& action);

    void daddMapChangeAction(const BlockID,
                            const std::string& key,
                            const ValueChangeAction& action);

    template <class T>
    ValueChangeAction wrapFunction(const std::function<void (const T&)>& func)
    {
        return [func](const ADBinaryDataPtr& a)
        {
            T a_v;
            bool a_read_res = a->getValue<T>(a_v);

            //Failed to read from package
            assert(a_read_res);

            func(a_v);
        };
    }

    IntKeyMap _st_int;
    StringKeyMap _st_string;
    ValueMap _st_value;
    BlockMap _st_block;
};

template <class T>
inline void ADStorageListener::addValueChangeAction(
        const BlockID id,
        const std::function<void (const T&)>& action)
{
    daddValueChangeAction(id, wrapFunction<T>(action));
}

template <class T>
inline void ADStorageListener::addMapChangeAction(
        const BlockID id,
        const int key,
        const std::function<void (const T&)>& action)
{
    daddMapChangeAction(id, key, wrapFunction<T>(action));
}

template <class T>
inline void ADStorageListener::addMapChangeAction(
        const BlockID id,
        const std::string& key,
        const std::function<void (const T&)>& action)
{
    daddMapChangeAction(id, key, wrapFunction<T>(action));
}


#endif // ADSTORAGELISTENER_H
