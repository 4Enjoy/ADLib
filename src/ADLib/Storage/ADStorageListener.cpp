#include "ADStorageListener.h"

ADStorageListener::ADStorageListener()
{
    CONNECT(ADStorage::signalValueChanged, this,
            &ADStorageListener::onValueChanged);
    CONNECT(ADStorage::signalMapIntChanged, this,
            &ADStorageListener::onMapValueChangedInt);
    CONNECT(ADStorage::signalMapStringChanged, this,
            &ADStorageListener::onMapValueChangedString);
}


void ADStorageListener::onValueChanged(const BlockID id, const ADBinaryDataPtr& data)
{
    auto it = _st_value.find(id);
    if(it != _st_value.end())
        callAll(it->second, data);
    onBlockChanged(id);
}

void ADStorageListener::onMapValueChangedInt(const BlockID id,
                                             const int key,
                                             const ADBinaryDataPtr& data)
{
    auto it = _st_int.find(BlockKey<int>(id, key));
    if(it != _st_int.end())
        callAll(it->second, data);
    onBlockChanged(id);
}

void ADStorageListener::onMapValueChangedString(const BlockID id,
                                                const std::string& key,
                                                const ADBinaryDataPtr& data)
{
    auto it = _st_string.find(BlockKey<std::string>(id, key));
    if(it != _st_string.end())
        callAll(it->second, data);
    onBlockChanged(id);
}

void ADStorageListener::callAll(const ValueChangeArr& arr, const ADBinaryDataPtr& data)
{
    for(auto& i : arr)
        i(data);
}

void ADStorageListener::callAll(const BlockChangeArr& arr)
{
    for(auto& i : arr)
        i();
}

void ADStorageListener::onBlockChanged(const BlockID id)
{
    auto it = _st_block.find(id);
    if(it != _st_block.end())
        callAll(it->second);
}

void ADStorageListener::daddValueChangeAction(
        const BlockID id,
        const ValueChangeAction& action)
{
    _st_value[id].push_back(action);
}

void ADStorageListener::daddMapChangeAction(
        const BlockID id,
        const int key,
        const ValueChangeAction& action)
{
    _st_int[BlockKey<int>(id, key)].push_back(action);
}

void ADStorageListener::daddMapChangeAction(
        const BlockID id,
        const std::string& key,
        const ValueChangeAction& action)
{
    _st_string[BlockKey<std::string>(id, key)].push_back(action);
}

void ADStorageListener::addBlockChangeAction(
        const BlockID id,
        const BlockChangeAction& action)
{
    _st_block[id].push_back(action);
}
