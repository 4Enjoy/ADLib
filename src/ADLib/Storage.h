#ifndef STORAGE_H
#define STORAGE_H
#include <ADLib/Storage/ADStream.h>
#include <ADLib/Storage/ADStorage.h>
#include <ADLib/Storage/ADStorageListener.h>
#include <ADLib/Storage/ADBlock.h>
#endif // STORAGE_H
