#ifndef ADRANDOM_H
#define ADRANDOM_H
#include <random>
class ADRandom
{
public:
    typedef long long SeedValue;
    ADRandom();
    ADRandom(const SeedValue seed);

    void seed(const SeedValue seed)
    {
        _generator.seed(seed);
    }

    /**
     * @brief From interval [min, max]
     * @param min
     * @param max
     * @return
     */
    int getNumber(int min, int max)
    {
        std::uniform_int_distribution<> distr(min, max);
        return distr(_generator);
    }
private:
    std::minstd_rand _generator;
};
#endif // ADRANDOM_H
