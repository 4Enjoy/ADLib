#ifndef ADCOCOSTHREAD_H
#define ADCOCOSTHREAD_H
#include <functional>
#include "cocos2d.h"

/**
 * @brief Launches action in cocos2d thread
 */
class ADCocosThread : public cocos2d::CCObject
{
public:
    typedef std::function<void ()> Action;
    static void onCocos2dThread(const Action& task, const void* parent=0);
    static void stopPendingTasks(const void* parent);
private:
    void performNextCocosTask(float);

};

#endif // ADCOCOSTHREAD_H
