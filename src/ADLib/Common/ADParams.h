#ifndef ADADParams_H
#define ADADParams_H

#include <string>
#include <vector>
#include <sstream>

class ADParams
{
public:
    typedef std::vector<std::string> SVec;
    ADParams()
    {}

    ADParams& add(const std::string& key, const std::string& value)
    {
        _key_string.push_back(key);
        _value_string.push_back(value);
        return *this;
    }

    template <class T>
    ADParams& add(const std::string& key, const T& value)
    {
        _key_string.push_back(key);
        std::stringstream ss;
        ss << value;
        _value_string.push_back(ss.str());
        return *this;
    }

    const SVec& getKeysString() const
    {
        return _key_string;
    }



    const SVec& getValuesString() const
    {
        return _value_string;
    }


private:
    SVec _key_string;
    SVec _value_string;
};
#endif // ADADParams_H
