#ifndef ADADThreadPool_H
#define ADADThreadPool_H
#include <functional>
#include <queue>
#include <mutex>
#include <thread>
#include <condition_variable>
#include <memory>
class ADThreadPool
{
public:
    typedef std::function<void ()> Task;
    struct TaskPriority
    {
        Task _task;
        int _priority;
        const bool operator<(const TaskPriority& p) const
        {
            return _priority < p._priority;
        }
    };
    typedef std::priority_queue<TaskPriority> Queue;

    ADThreadPool(const int threads);
    ~ADThreadPool();

    void addTask(const Task& task, const int priority=0);
    void start();
    void setOnStopAction(const Task& task);
    void stop(bool join=false);
    void stopAndWait();
private:
    void worker(int id, std::shared_ptr<std::condition_variable> cond,
                std::shared_ptr<std::mutex> mutex);
    int _threads;
    Task _on_stop;
    bool _stop_action_done;
    std::vector< std::shared_ptr<std::thread> > workers;
    std::vector<bool> workers_finished;
    // synchronization
    std::shared_ptr<std::mutex> queue_mutex;
    std::shared_ptr<std::condition_variable> condition;
    Queue tasks;
    bool _is_stop;
};

#endif // ADADThreadPool_H
