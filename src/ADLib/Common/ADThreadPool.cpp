#include "ADThreadPool.h"
#include "cocos2d.h"
ADThreadPool::ADThreadPool(const int threads)
    :
      _threads(threads),
      _stop_action_done(false),
      _is_stop(false),
      queue_mutex(new std::mutex),
      condition(new std::condition_variable)
{
}

ADThreadPool::~ADThreadPool()
{
    stop();
}

void ADThreadPool::addTask(const Task& task, const int priority)
{
    TaskPriority tp;
    tp._task = task;
    tp._priority = priority;

    { // acquire lock
        std::unique_lock<std::mutex> lock(*queue_mutex);

        // add the task
        tasks.push(tp);
    } // release lock

    // wake up one thread
    condition->notify_one();
}

void ADThreadPool::start()
{
    if(workers.size() < _threads)
    {
        for(size_t i = 0;i<_threads;++i)
        {
            auto cond = condition;
            auto mutex = queue_mutex;
            //workers_finished.push_back(false);
            workers.push_back(std::make_shared<std::thread>([this,i,cond,mutex](){
                worker(i, cond, mutex);
                //workers_finished[i] = true;
                //cocos2d::CCLog("Exit 2: %d", i);
            }));
        }
    }
}
void ADThreadPool::stopAndWait()
{
    stop(true);
}

void ADThreadPool::stop(bool join)
{
    bool stop_actions = false;
    { // acquire lock
        std::unique_lock<std::mutex> lock(*queue_mutex);
        if(!_is_stop)
        {
            stop_actions = true;
            _is_stop = true;
        }

        while(!tasks.empty())
        {
            tasks.pop();
        }

    }

    if(stop_actions)
    {
        condition->notify_all();

        // join them
        for(size_t i = 0;i<workers.size();++i)
        {
            //cocos2d::CCLog("Join: %d", i);
            std::shared_ptr<std::thread> thread = workers[i];
            //assert(i < workers_finished.size());
            //if(!workers_finished[i])
            //{
            if(join)
            {
                thread->join();
            }
            else
            {
                thread->detach();
            }
            //}
        }
    }
}
void ADThreadPool::setOnStopAction(const Task& task)
{
    _on_stop = task;
}

void ADThreadPool::worker(int id,
                          std::shared_ptr<std::condition_variable> cond,
                          std::shared_ptr<std::mutex> mutex)
{
    TaskPriority task;
    while(true)
    {
        {   // acquire lock
            std::unique_lock<std::mutex>
                    lock(*mutex);

            // look for a work item
            while(!_is_stop && tasks.empty())
            { // if there are none wait for notification
                cond->wait(lock);
            }

            if(_is_stop)
            {
                if(_stop_action_done)
                {
                    //cocos2d::CCLog("Exit: %d", id);
                    return;
                }
                else
                {
                    _stop_action_done = true;
                    task._task = _on_stop;
                }
            }
            else
            {
                // get the task from the queue
                task = tasks.top();
                tasks.pop();
            }

        }   // release lock

        //cocos2d::CCLog("Task priority: %d", task._priority);
        // execute the task
        if(task._task)
        {
            task._task();
        }

    }
}
