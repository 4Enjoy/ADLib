#include "ADCocosThread.h"

#include <list>
#include <mutex>
#include <algorithm>
#include "cocos2d.h"

namespace ADCocosThreadImplt {
    struct TTask
    {
        TTask(const ADCocosThread::Action& action,
             const void* tag)
            :_action(action),
              _tag(tag)
        {}
        ADCocosThread::Action _action;
        const void* _tag;
    };

    static ADCocosThread _ui_thread;
    static std::list<TTask> _cocos_tasks;
    static std::mutex _cocos_tasks_mutex;
}

using namespace ADCocosThreadImplt;
using namespace cocos2d;

void ADCocosThread::performNextCocosTask(float)
{
    bool repeat = false;
    TTask task(nullptr, 0);
    _cocos_tasks_mutex.lock();

    if(_cocos_tasks.size() > 0)
    {
        task = _cocos_tasks.front();
        _cocos_tasks.pop_front();

        if(_cocos_tasks.size() > 0)
            repeat = true;
    }

    if(!repeat)
        CCDirector::sharedDirector()->getScheduler()->unscheduleSelector(schedule_selector(ADCocosThread::performNextCocosTask), &_ui_thread);
    _cocos_tasks_mutex.unlock();

    if(task._action)
    {
        task._action();
    }
}
void ADCocosThread::stopPendingTasks(const void* parent)
{
    auto compare = [parent](const TTask& a)->bool{
        return (a._tag == parent);
    };
    _cocos_tasks_mutex.lock();
    _cocos_tasks.remove_if(compare);
    _cocos_tasks_mutex.unlock();
}

void ADCocosThread::onCocos2dThread(const Action& task, const void *tag)
{
    _cocos_tasks_mutex.lock();
    TTask t(task, tag);
    _cocos_tasks.push_back(t);

    CCDirector::sharedDirector()->getScheduler()->scheduleSelector(schedule_selector(ADCocosThread::performNextCocosTask), &_ui_thread, 0, false);
    _cocos_tasks_mutex.unlock();

}
