#include "ADJNI.h"
#include "ADOnActivityResult.h"

extern "C"
{


void Java_{package}_Main_onActivityResult(JNIEnv*  env, jobject thiz, jint requestCode, jint resultCode, jobject data)
{
	ADOnActivityResult::handleResult(env, thiz, requestCode, resultCode, data);
}

}
