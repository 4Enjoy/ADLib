LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := hellocpp_shared

LOCAL_MODULE_FILENAME := libhellocpp

   
FILE_LIST := $(wildcard $(LOCAL_PATH)/*.cpp)
{file_list}


LOCAL_SRC_FILES := $(FILE_LIST:$(LOCAL_PATH)/%=%)

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../
{includes}

LOCAL_CPPFLAGS += -std=c++11
LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static cocosdenshion_static
{static_libraries}

include $(BUILD_SHARED_LIBRARY)

$(call import-add-path, {cocos2dx_path})
$(call import-add-path, {cocos2dx_external})
{custom_import_path}

$(call import-module,cocos2dx)
$(call import-module,CocosDenshion/android)
{custom_module}
