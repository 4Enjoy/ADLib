package {packageName};

import com.x4enjoy.ADLib.ADActivity;
import android.os.Bundle;
import android.content.Intent;

public class Main extends ADActivity{

	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
	}
	
	@Override
    native protected void onActivityResult(int requestCode, int resultCode, Intent data);
	
    static {
         System.loadLibrary("hellocpp");
    }
}
