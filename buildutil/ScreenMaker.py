import os
import shutil
import subprocess

class ScreenMaker:
    def __init__(self, environment, exe_name, resource_dir):
        self.output_dir = '../../TestScreens'
        self.env = environment
        self.exe_name = exe_name
        self.resource_dir = resource_dir

    def prepareExe(self):
        dlls = ['glew32.dll', 'libcocos2d.dll', 'libCocosDenshion.dll',
                'libtiff.dll', 'zlib1.dll', 'pthreadVCE2.dll', 'libcurl.dll'];

        dll_dir = os.path.join(self.env.cocos2dx_dir, 'Debug.win32')
        exe_dir = self.env.win_executable_dir

        for dll in dlls:
            shutil.copyfile(os.path.join(dll_dir, dll),
                            os.path.join(exe_dir, dll))

    def makeOneScreen(self, device, orientation, language):

        cur_dir = os.getcwd()
        os.chdir(self.resource_dir)

        command = [os.path.join(self.env.win_executable_dir, self.exe_name),
                   'TEST',
                   'device:{0}'.format(device),
                   'orientation:{0}'.format(orientation),
                   'language:{0}'.format(language),
                   'output:{0}'.format(self.output_dir)]

        print(' '.join(command))
        subprocess.Popen(command).wait()

        os.chdir(cur_dir)

    def makeScreens(self, devices, orientations, languages):
        self.prepareExe()

        for device in devices:
            for orientation in orientations:
                for language in languages:
                    self.makeOneScreen(device, orientation, language)