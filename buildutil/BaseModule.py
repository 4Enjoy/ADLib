class Module:

    def __init__(self, name):
        self.__name = name
        self.__requires = set()

    def getName(self):
        return self.__name

    def getRequiredModules(self):
        return self.__requires

    def addRequiredModule(self, name):
        self.__requires.add(name)