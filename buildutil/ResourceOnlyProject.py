from .General import *
class ResourceOnlyProject:

    def __init__(self, env):
        self.resource_task = None
        self.resource_output = ''
        self.env = env

    def buildProject(self):
        compileResources(self.env, self.resource_output, self.resource_task)

