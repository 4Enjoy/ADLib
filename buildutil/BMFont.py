from wand.image import Image
import os
import subprocess
import re
from wand.drawing import Drawing
from wand.color import Color

def resizeCanvas(image, new_width, new_height):
    if image.width > new_width:
        image.crop(0, 0, new_width, image.height)
    if image.height > new_height:
        image.crop(0, 0, image.width, new_height)

    if image.width == new_width and image.height == new_height:
        return image

    canvas = Image(width=new_width, height=new_height)
    canvas.composite(image, 0, 0)
    return canvas

def getFontMetrics(text, font_name, size):
    command = 'convert -debug annotate xc: -font {font} -pointsize {size} -annotate 0 \'{text}\' null:'.format(
        font=font_name,
        size=size,
        text=text
    )
    output = str(subprocess.check_output(command, shell=True, stderr=subprocess.STDOUT))
    matchObj = re.search( r'width: (\d+); height: (\d+);', output, re.M|re.I)
    if matchObj:
        return {'width': int(matchObj.group(1)),
                'height': int(matchObj.group(2))}
    else:
        return None

class BMFont:
    def __init__(self):
        self._image_list = []

    def addImage(self, char, image=None, filename=None):
        pic = image
        if image is None:
            pic = Image(filename=filename)

        self._image_list.append({'image': pic, 'char': ord(char)})

    def saveToFile(self, output_dir, file_name, texture_name, texture_max_width=512, margin=2):
        y_offset = 0
        current_row_height = 0
        current_row_width = 0

        line_height = 0

        max_width = 1
        max_height = 1

        symbols = []

        for node in self._image_list:
            image = node['image']
            w = image.width + margin*2
            h = image.height + margin*2

            if current_row_width + w > texture_max_width:
                if current_row_width > max_width:
                    max_width = current_row_width

                y_offset = y_offset + current_row_height
                current_row_width = 0
                current_row_height = 0

            if h > current_row_height:
                current_row_height = h
                if current_row_height > line_height:
                    line_height = current_row_height

                if max_height < current_row_height + y_offset:
                    max_height = current_row_height + y_offset

            x = current_row_width
            y = y_offset
            symbols.append({'node': node, 'x': x+margin, 'y': y+margin})
            current_row_width += w

        if current_row_width > max_width:
            max_width = current_row_width

        texture = Image(width=max_width, height=max_height)
        for info in symbols:
            texture.composite(info['node']['image'], info['x'], info['y'])

        texture.save(filename=os.path.join(output_dir, texture_name))

        f = open(os.path.join(output_dir,file_name), 'w')
        f.write('info face="Arial" size=32 bold=0 italic=0 charset="" unicode=1 stretchH=100 smooth=1 aa=1 padding=0,0,0,0 spacing=1,1 outline=0\n')
        f.write('common lineHeight={line_height} base=26 scaleW={width} scaleH={height} pages=1 packed=0 alphaChnl=1 redChnl=0 greenChnl=0 blueChnl=0\n'.format(
            line_height=line_height,
            width=max_width,
            height=max_height
        ))
        f.write('page id=0 file="{texture}"\n'.format(
            texture=texture_name
        ))
        f.write('chars count={0}\n'.format(
            len(symbols)
        ))

        for info in symbols:
            f.write('char id={char_id}	x={x}	y={y}	width={width}	height={height}	xoffset=0	yoffset=0	xadvance={xadvance}	page=0	chnl=15\n'.format(
                char_id=info['node']['char'],
                x=info['x'],
                y=info['y'],
                width=info['node']['image'].width,
                height=info['node']['image'].height,
                xadvance=info['node']['image'].width
            ))
        f.close()

def createBMFontForSymbols(str_of_symbols,
                           symbol_function,
                           font_name,
                           texture_name,
                           output_dir='.'):
    font = BMFont()
    for char in str_of_symbols:
        img = symbol_function(char)
        font.addImage(char, img)

    font.saveToFile(output_dir, font_name, texture_name)

def renderText(text, font, size, color):
    out_file = '__render.png'

    fileName, fileExtension = os.path.splitext(font)
    font_file = '__font.{0}'.format(fileExtension);

    import shutil
    shutil.copyfile(font, font_file);

    command = "convert -background  rgba(0,0,0,0) -fill {color} -font {font} -pointsize {size} label:\"{text}\" {out_file}".format(
        color=color,
        font=font_file,
        size=size,
        text=text,
        out_file=out_file
    )
    os.system(command)

    img = Image(filename=out_file)
    os.remove(out_file)
    os.remove(font_file)
    return img

def addShadow(img, opacity, spread, diff_x, diff_y):
    tmp_img1 = '__shadow1.png'
    tmp_img2 = '__shadow2.png'

    img.save(filename=tmp_img1)

    x = str(diff_x)
    y = str(diff_y)

    if diff_x >= 0:
        x = '+{0}'.format(diff_x)
    if diff_y >= 0:
        y = '+{0}'.format(diff_y)

    command = 'convert {input} -background  black  ( +clone -shadow {opacity}x{spread}{x}{y} ) +swap -background none -layers merge +repage {output}'.format(
        input=tmp_img1,
        opacity=opacity,
        spread=spread,
        x=x,
        y=y,
        output=tmp_img2
    )

    os.system(command)

    res = Image(filename=tmp_img2)
    os.remove(tmp_img1)
    os.remove(tmp_img2)

    return res

def bevel(img, width=5, depth=100, soften=0, azimuth=135, elevation=30):
    source = '__bevel0.png'
    tmp_img1 = '__bevel1.mpc'
    tmp_img1_a = '__bevel1.cache'
    tmp_img2 = '__bevel2.mpc'
    tmp_img2_a = '__bevel2.cache'
    tmp_img3 = '__bevel3.png'

    img.save(filename=source)
    command1 = 'convert {source} -set colorspace RGB ( -clone 0 -alpha extract ) ( -clone 0 -background gray(50%) -flatten ) -delete 0 +swap -compose copy_opacity -composite {img1}'.format(
        source=source,
        img1=tmp_img1
    )
    os.system(command1)
    wfact=1000*width
    leveling="-level 0,{0}".format(wfact)
    contr_command='convert xc: -format "%[fx:({0})]" info:'.format(0.5*depth-100)
    contr = subprocess.check_output(contr_command, shell=True, stderr=subprocess.STDOUT).decode('utf-8')

    deepening="-brightness-contrast 0,{0}".format(contr)
    softening="-blur {0}x65000".format(soften)

    command2 = 'convert {img1} -alpha extract -write {img2}  \
    +level 0,1000 -white-threshold 999 \
    -morphology Distance:-1 Euclidean:{width},1000 {leveling} \
    -shade {azimuth}x{elevation}  \
    -auto-level {deepening} {softening} \
    ( +clone -fill "gray(50%)" -colorize 100% ) +swap ( {img2} -threshold 0 ) \
    -compose over -composite \
    ( {img1} -alpha off ) +swap -set colorspace RGB -compose hardlight -composite \
    {img2} -alpha off -compose copy_opacity -composite \
    {output}'.format(
        img1=tmp_img1,
        img2=tmp_img2,
        output=tmp_img3,
        width=width,
        leveling=leveling,
        azimuth=azimuth,
        elevation=elevation,
        deepening=deepening,
        softening=softening
    )

    #print (command2)

    os.system(command2)

    res = Image(filename=tmp_img3)
    os.remove(tmp_img1)
    os.remove(tmp_img1_a)
    os.remove(tmp_img2)
    os.remove(tmp_img2_a)
    os.remove(tmp_img3)
    os.remove(source)

    return res



#without_shadow = lambda x: renderText(x, 'Sniglet-Regular.otf', 70, 'white')
#with_shadow = lambda x: addShadow(x, 60, 1.7, 0, 0)
#with_bevel = lambda x: bevel(x, width=10, soften=2, depth=100, elevation=30)
#createBMFontForSymbols(u'Score: MvsJliTm-1234567890', lambda x: with_shadow(with_bevel(without_shadow(x))),
#                       'test.fnt','test.png', output_dir='../hd/')

#with_bevel(Image(filename='cairo_banner.png'))






