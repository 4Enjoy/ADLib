class Environment:
    def __init__(self):
        self.android_sdk = ''
        self.android_ndk = ''
        self.php_bin = ''
        self.texture_packer_bin = ''
        self.resource_compiler_dir = ''
        self.cocos2dx_dir = ''
        self.ant_dir = ''
        self.win_executable_dir = ''