import os
import subprocess

def buildDoxygenDocumentation(doxygen_file):
    folder = os.path.dirname(doxygen_file)
    file = os.path.basename(doxygen_file)

    cur_dir = os.getcwd()


    try:
        os.chdir(folder)

        command = ['doxygen',
                   file]
        subprocess.Popen(command).wait()
        os.chdir(cur_dir)
    except:
        os.chdir(cur_dir)
        raise