
from . import BaseModule

class Modules:

    def __init__(self):
        self.__modules = {}


    def addModule(self, module):
        #if isinstance(module, BaseModule.Module):
        self.__modules[module.getName()] = module
        #else:
        #    raise Exception("Module must be instance of Module class")

    def getModule(self, name):
        if name not in self.__modules:
            raise Exception('Module not found: {0}'.format(name))
        return self.__modules[name]

    def getRequiredModules(self, lst):
        to_check = set(lst)
        checked = set()

        while len(checked) != len(to_check):
            added = set()
            for n in to_check:
                if n not in checked:
                    checked.add(n)

                module = self.getModule(n)
                required = module.getRequiredModules()
                added = added.union(required)
            to_check = to_check.union(added)

        return list(checked)


