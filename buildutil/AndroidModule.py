from . import BaseModule

class AndroidModule(BaseModule.Module):

    def __init__(self, name):
        self.__defines = []
        self.__permissions = []
        self.__java_files = []
        self.__cpp_files = []
        self.__manifest_additional = []
        self.__libs = []
        self.__res = []
        self.__external = []
        self.__android_static_libs = []
        self.__permissions_manual = []

        BaseModule.Module.__init__(self, name)

    def getPermissions(self):
        return self.__permissions

    def getPermissionsManual(self):
        return self.__permissions_manual

    def getStaticLibs(self):
        return self.__android_static_libs;

    def getManifestAdditional(self):
        return self.__manifest_additional

    def getCppFiles(self):
        return self.__cpp_files

    def getJavaLibs(self):
        return self.__libs

    def getJavaFiles(self):
        return self.__java_files

    def getResFiles(self):
        return self.__res

    def getExternalProjects(self):
        return self.__external

    def addExternalProject(self, project):
        self.__external.append(project)

    def addResFile(self, file):
        self.__res.append(file)

    def addDefine(self, define):
        self.__defines.append(define)

    def addPermission(self, permission):
        self.__permissions.append(permission)

    def addPermissionManual(self, permission):
        self.__permissions_manual.append(permission)

    def addJavaFile(self, file):
        self.__java_files.append(file)

    def addCppFile(self, file):
        self.__cpp_files.append(file)

    def addLibFile(self, file):
        self.__libs.append(file)

    def addStaticLib(self, static_lib_name, module_name, mk_file):
        self.__android_static_libs.append({lib_name: static_lib_name,
                                           module_name: module_name,
                                           mk_file: mk_file})

    def addManifestAdditional(self, manifest):
        self.__manifest_additional.append(manifest)
