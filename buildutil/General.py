import os
import subprocess
import sys

def showOutputError(error, file='build', line='0', code='B0000'):
        print('{file}({line}) : error {code}: {error}'.format(
            file=file,
            line=line,
            code=code,
            error=error
        ))

def showStepMessage(step_number, step_name):
        print('\n\n=================================')
        print('STEP {0}: {1}'.format(step_number, step_name))
        print('=================================')
        sys.stdout.flush()

def sha1(data):
    import hashlib

    if isinstance(data, bytes):
        return hashlib.sha1(data).hexdigest()
    else:
        return hashlib.sha1(data.encode('utf-8')).hexdigest()


def file_get_contents(filename, mode='r'):
    with open(filename, mode) as f:
        return f.read()


def sha1_file(filepath):
    return sha1(file_get_contents(filepath, mode='rb'))


def file_put_contents(filename, data):
    with open(filename, 'w') as f:
        f.write(data)


def saveToFileIfNeeded(file, data):
    if not os.path.exists(file):
        print('File created: {0}'.format(file))
        file_put_contents(file, data)
    else:

        new_hash = sha1(data)
        old_hash = sha1(file_get_contents(file))

        if new_hash != old_hash:
            print('File updated: {0}'.format(file))

            file_put_contents(file, data)


def preparePath(name):
    if not os.path.isdir(name):
        print("Dir created: {0}".format(name))
        os.makedirs(name)

def createAndroidSignature(key_file, key_alias, file_password, alias_password):
    return {"key_file": key_file,
            "key_alias": key_alias,
            "file_password": file_password,
            "alias_password": alias_password}

def createResourceCompilerTask(folder, project, platform, translations=['en'], translations_dir=None):
    if translations_dir is None:
        translations_dir = os.path.join(folder, 'translations')
    return {"dir": folder,
            "project": project,
            "platform": platform,
            "translations": translations,
            "translations_dir": translations_dir}

def compileResources(env, destination, task):
    cur_dir = os.getcwd()

    try:
        params_tpl = 'texture_packer={TEXTURE_PACKER_DIR};platform={PLATFORM};' \
                     'project={BUILD_PROJECT};source={src};' \
                     'destination={dst};translate={translations};' \
                     'translation_dir={translations_dir}'
        params = params_tpl.format(
            TEXTURE_PACKER_DIR=env.texture_packer_bin,
            PLATFORM=task['platform'],
            BUILD_PROJECT=task['project'],
            src=task['dir'],
            dst=destination,
            translations=','.join(task['translations']),
            translations_dir=task['translations_dir']
        )

        os.chdir(env.resource_compiler_dir)
        res = subprocess.Popen([env.php_bin, "ResourceCompiler.php", params]).wait()
        if res != 0:
            raise Exception('Resources build failed')
    except:
        os.chdir(cur_dir)
        raise


def unixSlashes(path):
    return path.replace('\\', '/')