from .Modules import *
import os
import shutil
import subprocess
import sys
import re
import errno

from .General import *

class AndroidProject:
    def __init__(self, env):
        #templates path
        path = os.path.dirname(os.path.realpath(__file__))
        self.templates_dir = os.path.join(path, 'templates/Android')

        self.package_name = "com.example"
        self.project_name = "Untitiled"
        self.version_code = "1"
        self.version_name = "1.00"
        self.custom_library = None
        self.target_sdk = "18"
        self.min_sdk = "9"
        self.label = "@string/app_name"
        self.icon = "@drawable/icon"
        self.main_activity_definition = self.getMainActivityDeclaration()
        self.debuggable = True
        self.release_build = True
        self.__modules = set()
        self.__manifest_permisions = set()
        self.__manifest_permisions_manual = []
        self.__manifest_additionals = []
        self.working_directory = 'project_dir'
        self.output_name = 'tmp'
        self.__output_dir = ''
        self.__cpp_files = []
        self.__cpp_dirs = []
        self.__look_up_dirs = []
        self.env = env
        self.signature = None
        self.res_dir = 'res'
        self.__java_libs = []
        self.__java_files = []
        self.__res_files = []
        self.__external = []
        self.resources_compile_task = None
        self.modules = None

    def addCppFile(self, path):
        self.__cpp_files.append(path)

    def addCppDir(self, dir):
        self.__cpp_dirs.append(dir)

    def addLookUpDir(self, dir):
        self.__look_up_dirs.append(dir)

    def addModule(self, name):
        self.__modules.add(name)

    def findDir(self, dir_to_find):
        directory = None
        for dir in self.__look_up_dirs:
            path = os.path.join(self.working_directory,
                                dir,
                                dir_to_find)
            if os.path.isdir(path):
                directory = dir
                break

        if directory is None:
            raise Exception('Dir not found: {0}'.format(dir_to_find))

        return os.path.join(directory, dir_to_find)

    def findFile(self, file):
        directory = None
        for dir in self.__look_up_dirs:
            path = os.path.join(self.working_directory,
                                dir,
                                file)
            if os.path.exists(path):
                directory = dir
                break

        if directory is None:
            raise Exception('File not found: {0}'.format(file))

        return os.path.join(directory, file)

    def gatherPermissionsAndAdditionals(self):
        #system = Modules.Modules()

        self.__modules = self.modules.getRequiredModules(list(self.__modules))

        for m in self.__modules:
            module = self.modules.getModule(m)
            self.__manifest_permisions = self.__manifest_permisions.union(set(module.getPermissions()))
            self.__manifest_additionals += module.getManifestAdditional()
            self.__manifest_permisions_manual += module.getPermissionsManual()
            cpp_files = module.getCppFiles()

            for file in cpp_files:
                self.__cpp_files.append(self.findFile(file))

            java_libs = module.getJavaLibs()
            for file in java_libs:
                self.__java_libs.append(self.findFile(file))

            self.__java_files += module.getJavaFiles()
            self.__res_files += module.getResFiles()
            self.__external += module.getExternalProjects();


    def getMainActivityDeclaration(self,
                                   screenOrientation="sensorLandscape",
                                   label="@string/app_name",
                                   theme="@android:style/Theme.NoTitleBar.Fullscreen",
                                   configChanges="orientation"):
        main_activity_definition = self.getTemplate('AndroidManifest_main_activity.xml')
        return main_activity_definition.format(
            screenOrientation=screenOrientation,
            label=label,
            theme=theme,
            configChanges=configChanges)

    def processCppDir(self, d):
        self.__cpp_files.append(os.path.join(d, "*.cpp"))
        for x in os.listdir(os.path.join(self.working_directory, d)):
            if(os.path.isdir(os.path.join(self.working_directory, d, x))):
                self.processCppDir(os.path.join(d,x))


    def buildJNIFolder(self):
        jni_dir = os.path.join(self.__output_dir, 'jni')
        preparePath(jni_dir)

        #Create Application.mk
        app_mk_tpl = self.getTemplate('jni/Application.mk')
        saveToFileIfNeeded(os.path.join(jni_dir, 'Application.mk'),
                           app_mk_tpl)

        main_cpp = self.getTemplate('jni/main.cpp')
        saveToFileIfNeeded(os.path.join(jni_dir, 'main.cpp'),
                           main_cpp)

        activity_result_cpp = self.getTemplate('jni/OnActivityResult.cpp')
        activity_result_cpp = activity_result_cpp.replace('{package}', self.package_name.replace('.','_'))
        saveToFileIfNeeded(os.path.join(jni_dir, 'OnActivityResult.cpp'),
                           activity_result_cpp)

        for d in self.__cpp_dirs:
            self.processCppDir(d)

        android_mk_tpl = self.getTemplate('jni/Android.mk')
        cpp_file = sorted(map(unixSlashes, self.__cpp_files))

        cocos2dx_dir = self.env.cocos2dx_dir
        cocos2dx_external = os.path.join(cocos2dx_dir, 'cocos2dx/platform/third_party/android/prebuilt')

        static_libraries = ''
        custom_import_path = ''
        custom_module = ''
        
        if self.custom_library is not None:
            static_libraries = 'LOCAL_STATIC_LIBRARIES  = ' + self.custom_library + '_static'
            folder = os.path.join(self.working_directory, self.findFile(self.custom_library));
            folder = folder + '/..';
            folder = folder.replace('\\', '/');
            
            #LOCAL_WHOLE_STATIC = mavka_static
            custom_module = '$(call import-module,' + self.custom_library + ')';
            custom_import_path = '$(call import-add-path, '+ folder +')'
        

        file_list = '\n'.join(
            map(
                lambda x:
                'FILE_LIST += $(wildcard $(LOCAL_PATH)/../../{0})'.format(x),
                cpp_file)
        )

        look_up = sorted(map(unixSlashes, self.__look_up_dirs))
        includes = '\n'.join(
            map(
                lambda x:
                'LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../{0}'.format(x),
                look_up)
        )
        android_mk = android_mk_tpl.format(
            file_list=file_list,
            includes=includes,
            cocos2dx_path=cocos2dx_dir,
            cocos2dx_external=cocos2dx_external,
            static_libraries=static_libraries,
            custom_module=custom_module,
            custom_import_path=custom_import_path
        )
        saveToFileIfNeeded(os.path.join(jni_dir, 'Android.mk'),
                           android_mk)

    def prepareRes(self):
        res_dir = os.path.join(self.__output_dir, 'res')
        if os.path.isdir(res_dir):
            shutil.rmtree(res_dir)

        original_res = os.path.join(self.working_directory, self.res_dir)
        if not os.path.isdir(original_res):
            raise Exception('Dir not found {0}'.format(self.res_dir))

        shutil.copytree(original_res, res_dir)

        for res_file in self.__res_files:
            file = os.path.join(self.working_directory, self.findFile(res_file))
            target = os.path.join(res_dir, res_file)

            target_dir = os.path.dirname(target)
            if not os.path.isdir(target_dir):
                os.makedirs(target_dir)

            shutil.copy(file, target)

    def prepareSrc(self):
        src_dir = os.path.join(self.__output_dir, 'src')
        if os.path.isdir(src_dir):
            shutil.rmtree(src_dir)

        os.mkdir(src_dir)

        main_java = os.path.join(src_dir, self.package_name.replace('.','/'), 'Main.java')
        main_tpl = self.getTemplate('src/Main.java')
        dir = os.path.dirname(main_java)
        if not os.path.isdir(dir):
            os.makedirs(dir)

        file_put_contents(main_java, main_tpl.replace(
            '{packageName}',self.package_name
        ))

        for file in self.__java_files:
            target = os.path.join(src_dir, file)
            source = os.path.join(self.working_directory, self.findFile(file))

            target_dir = os.path.dirname(target)
            if not os.path.isdir(target_dir):
                os.makedirs(target_dir)

            shutil.copy(source, target)

    def showError(self, error, file='build_android.py', line='0', code='B0000'):
        showOutputError(error, file=file, line=line, code=code)

    def findLinkErrors(self, text):
        err = re.findall(r"([^\d][^:]+[^\d]):([\d]+):([\d]+[:]){0,1} (fatal )*error: (.*)", text)
        for e in err:
            self.showError(e[4], line=e[1], file=e[0], code='L0001')

    def getApkPath(self):
        apk_name = '{name}-{type}.apk'

        type = 'debug'
        if self.release_build:
            type = 'release'

        apk_name = apk_name.format(name=self.project_name,
                                   type=type)

        apk_path = os.path.join(self.__output_dir, 'bin', apk_name)
        return apk_path

    def installOnDevice(self):
        adb_command = os.path.join(self.env.android_sdk, 'platform-tools/adb')

        apk_path = self.getApkPath()

        command = [adb_command,
                   'install',
                   '-r',
                   apk_path]

        print('Installing on device...')
        sys.stdout.flush()

        res = subprocess.Popen(command).wait(timeout=300)
        if res != 0:
            raise Exception('Install to device failed')

        launch_command = [adb_command,
                          'shell',
                          'am',
                          'start',
                          '-n',
                          '{0}/{0}.Main'.format(self.package_name)]

        print('Running on device...')
        sys.stdout.flush()
        res = subprocess.Popen(launch_command).wait(timeout=10)
        if res != 0:
            raise Exception('Launch on device failed')


    def buildApk(self):
        ant_dir = self.env.ant_dir
        ant_cmd = os.path.join(ant_dir, 'bin/ant.bat')

        build_type = 'debug'
        if self.release_build:
            build_type = 'release'

        cur_dir = os.getcwd()

        try:
            os.chdir(self.__output_dir)
            command = [ant_cmd,
                          build_type,
                          '-Dsdk.dir={0}/'.format(self.env.android_sdk)]

            sp = subprocess.Popen(command, stderr=subprocess.PIPE)
            out, err = sp.communicate()

            error_text = ''
            try:
                error_text = err.decode('utf-8')
            except:
                error_text = err
            print(error_text, file=sys.stderr)

            res = sp.returncode
            if res != 0:
                raise Exception('Build .apk Failed')

            os.chdir(cur_dir)
        except:
            os.chdir(cur_dir)
            raise



    def compileNativePart(self):
        ndk_build = os.path.join(self.env.android_ndk, 'ndk-build.cmd')

        cocos2dx_root = self.env.cocos2dx_dir
        app_android_root = self.__output_dir

        command = [ndk_build,
                          '-C',
                          app_android_root]

        sp = subprocess.Popen(command, stderr=subprocess.PIPE)
        out, err = sp.communicate()

        error_text = err.decode('utf-8')
        print(error_text, file=sys.stderr)
        self.findLinkErrors(error_text)

        res = sp.returncode
        if res != 0:
            raise Exception('Build Native Part Failed')

    def prepareAssets(self):
        assets_dir = os.path.join(self.__output_dir, 'assets')
        if not os.path.isdir(assets_dir):
            os.mkdir(assets_dir)

        if self.resources_compile_task is not None:
            compileResources(self.env, assets_dir, self.resources_compile_task)


    def prepareLib(self):
        lib_dir = os.path.join(self.__output_dir, 'libs')

        if not os.path.isdir(lib_dir):
            os.mkdir(lib_dir)

        libs_hashes = {}
        libs_in_place = set()

        for p in os.listdir(lib_dir):
            path = os.path.join(lib_dir, p)
            if not os.path.isdir(path):
                libs_hashes[p] = sha1_file(path)
                libs_in_place.add(p)

        for lib in self.__java_libs:
            copy_from = os.path.join(self.working_directory, lib)

            lib_name = os.path.basename(lib)
            copy_to = os.path.join(lib_dir, lib_name)

            if lib_name not in libs_in_place:
                print("Lib added: {0}".format(lib_name))
                shutil.copy(copy_from, copy_to)
            else:
                hash = sha1_file(copy_from)
                if hash != libs_hashes[lib_name]:
                    print("Lib updated: {0}".format(lib_name))
                    shutil.copy(copy_from, copy_to)
                libs_in_place.remove(lib_name)

        for lib in libs_in_place:
            print("Lib deleted: {0}".format(lib))
            path = os.path.join(lib_dir, lib)
            os.remove(path)

    def getExternalDir(self, dirname):
        first = self.__output_dir
        second = os.path.join(self.working_directory,self.findDir(dirname))

        return unixSlashes(os.path.relpath(second,first))

    def buildManifest(self):
        manifest_name = os.path.join(self.__output_dir, 'AndroidManifest.xml')
        manifest = self.createManifest()
        saveToFileIfNeeded(manifest_name, manifest)

        project_properties_tpl = self.getTemplate('project.properties')
        one_external_tpl = 'android.library.reference.{id}={path}\n'

        externals = ''

        id = 2
        for dir in self.__external:
            externals += one_external_tpl.format(
                id=id,
                path=self.getExternalDir(dir)
            )
            id += 1

        project_properties = project_properties_tpl.format(
            sdkNum=self.target_sdk,
            external=externals
        )
        saveToFileIfNeeded(os.path.join(self.__output_dir, 'project.properties'),
                           project_properties)

        local_properties_tpl = self.getTemplate('local.properties')
        local_properties = local_properties_tpl.format(
            sdkDir=self.env.android_sdk
        )
        saveToFileIfNeeded(os.path.join(self.__output_dir, 'local.properties'),
                           local_properties)

        ndk_stack_tpl = self.getTemplate('ndk_stack.bat')
        ndk_stack = ndk_stack_tpl.format(
            sdkDir=self.env.android_sdk,
            ndkDir=self.env.android_ndk,
            projectDir=self.__output_dir
        )
        saveToFileIfNeeded(os.path.join(self.__output_dir, 'ndk_stack.bat'),
                           ndk_stack)

        build_xml_tpl = self.getTemplate('build.xml')
        build_xml = build_xml_tpl.replace('{projectName}', self.project_name)
        saveToFileIfNeeded(os.path.join(self.__output_dir, 'build.xml'),
                           build_xml)

        if self.signature is not None:
            ant_properties_tpl = self.getTemplate('ant.properties')
            ant_properties = ant_properties_tpl.format(
                file=unixSlashes(os.path.join('..', self.signature["key_file"])),
                alias=self.signature["key_alias"],
                file_password=self.signature["file_password"],
                alias_password=self.signature["alias_password"]
            )
            saveToFileIfNeeded(os.path.join(self.__output_dir, 'ant.properties'),
                               ant_properties)


    def getTemplate(self, name):
        file = os.path.join(self.templates_dir, name)
        if not os.path.exists(file):
            raise Exception('Template not found {0}'.format(name))
        return file_get_contents(file)

    def stepMessage(self, step_number, step_name):
        showStepMessage(step_number, step_name)

    def runProject(self):
        try:
            self.__output_dir = os.path.join(self.working_directory, self.output_name)

            print('Running project: {0}'.format(self.__output_dir))

            self.stepMessage(1, 'Install to device')
            self.installOnDevice()

        except Exception as e:
            self.showError(e.__str__())
            sys.exit(errno.ESHUTDOWN)

    def buildProject(self):

        try:
            self.__output_dir = os.path.join(self.working_directory, self.output_name)

            print('Building project: {0}'.format(self.__output_dir))

            self.stepMessage(1, 'Prepare project data')
            preparePath(self.__output_dir)

            self.gatherPermissionsAndAdditionals()
            self.buildManifest()
            self.buildJNIFolder()
            self.prepareRes()
            self.prepareLib()
            self.prepareSrc()

            self.stepMessage(2, 'Build resources')
            self.prepareAssets()

            self.stepMessage(3, 'Compile native part')
            self.compileNativePart()

            self.stepMessage(4, 'Build .apk')
            self.buildApk()

            print("BUILD SUCCESSFULL")
            print("Apk: {0}".format(self.getApkPath()))

        except Exception as e:
            self.showError(e.__str__())
            sys.exit(errno.ESHUTDOWN)

    def putPackageName(self):
        return lambda x: x.replace('{PACKAGE}', self.package_name)
    
    def createManifest(self):

        perm = '\n'.join(
            list(
                map(
                    lambda x:
                    '\t<uses-permission android:name="{0}"/>'.format(x),
                    sorted(self.__manifest_permisions))
            )
        )

        custom_perm = '\n'.join(
            list(
                map(self.putPackageName(),
                    self.__manifest_permisions_manual)
            )
        )

        additional = '\n\n'.join(
            list(
                map(self.putPackageName(),
                    self.__manifest_additionals)
            )
        )

        perm += custom_perm;

        debuggable_str = ''
        if self.debuggable:
            debuggable_str = 'android:debuggable="true"'

        manifest_template = self.getTemplate('AndroidManifest.xml')
        manifest = manifest_template.format(
            package=self.package_name,
            versionCode=self.version_code,
            versionName=self.version_name,
            minSdk=self.min_sdk,
            targetSdk=self.target_sdk,
            permissions=perm,
            label=self.label,
            icon=self.icon,
            debuggable=debuggable_str,
            additional=additional,
            main_activity=self.main_activity_definition
        )

        return manifest




