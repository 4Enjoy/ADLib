import sys
import os

sys.path.append(os.path.dirname(os.path.realpath(__file__)))
from buildutil.AndroidModule import *
from buildutil.AndroidProject import *
from buildutil.Modules import *

modules = [


	{
        "name":         "android_ADOnActivityResult",
        "cpp":          ["ADOnActivityResult.cpp"],
        "java":         ["com/x4enjoy/ADLib/ADActivity.java"],
    },

    {
    "name":         "android_ADScreenLock",
    "cpp":          ["ADScreenLock.cpp"],
    "java":         ["com/x4enjoy/ADLib/ADScreenLock.java"],
},

    {
        "name":         "android_ADUIThread",
        "java":         ["com/x4enjoy/ADLib/ADUIThread.java"],
        "cpp":          ["Device/ADUIThread.cpp"],
        "require":      ['android_ADJNI'],
    },
    {
        "name":         "android_ADJNI",
        "cpp":          ["ADJNI.cpp"],
		"require":      ['android_ADOnActivityResult'],
        "java":         ["com/x4enjoy/ADLib/App.java"],
    },
    {
        "name":         "android_ADDeviceID",
        "cpp":          ['Device/ADDeviceID.cpp'],
        "java":         ["com/x4enjoy/ADLib/ADDeviceID.java"],
    },
    {
        "name":         "android_ADAds_AdMob",
        "cpp":          ['Device/ADAds_AdMob.cpp'],
        "require":      ['android_ADUIThread',
                         'android_ADJNI'],
        "require":      ["googlePlayServices", "android_AdMobLib"]
    },

    {
        "name":         "android_AdMobLib",
        "permissions":  ['android.permission.INTERNET',
                         'android.permission.ACCESS_NETWORK_STATE'],
        "external":     ['google-play-services_ADMob'],
        "manifest":     ['<meta-data android:name="com.google.android.gms.version" android:value="@integer/google_play_services_version"/>',
            '<activity android:name="com.google.android.gms.ads.AdActivity" android:theme="@android:style/Theme.Translucent" android:configChanges="keyboard|keyboardHidden|orientation|screenLayout|uiMode|screenSize|smallestScreenSize"/>'],
        "require":      ["googlePlayServices"]
    },

    {
        "name":         "googlePlayServices",
        "external":     ['google-play-services_ADMob'],
        "manifest":     ['<meta-data android:name="com.google.android.gms.version" android:value="@integer/google_play_services_version"/>'],

    },
    {
        "name":         "android_ADAds_AdMob_Standalone", #old sdk
        "permissions":  ['android.permission.INTERNET',
                         'android.permission.ACCESS_NETWORK_STATE'],
        "cpp":          ['Device/ADAds_AdMob_Standalone.cpp'],
        "lib":          ['GoogleAdMobAdsSdk-6.3.0.jar'],
        "require":      ['android_ADUIThread',
                         'android_ADJNI'],
        "manifest":     ['<activity android:name="com.google.ads.AdActivity" android:configChanges="keyboard|keyboardHidden|orientation|screenLayout|uiMode|screenSize|smallestScreenSize"/>'],
    },

    {
        "name":         "android_ADAds_None",
        "cpp":          ['Device/ADAds_None.cpp'],
    },
    {
        "name":         "android_ADAds_ChartboostSDK",
        "permissions":	[
            'android.permission.INTERNET',
            'android.permission.WRITE_EXTERNAL_STORAGE',
            'android.permission.ACCESS_WIFI_STATE',
            'android.permission.ACCESS_NETWORK_STATE'],
        "manifest":		['<activity android:name="com.chartboost.sdk.CBImpressionActivity" android:theme="@android:style/Theme.Translucent.NoTitleBar" android:excludeFromRecents="true" />'],
        "lib":          ['chartboost.jar'],
    },
    {
        "name":         "android_ADAds_AdColony",
        "require":      ["android_ADAds_AdColonySDK"]
    },
    {
        "name":         "android_ADAds_AdColonySDK",
        "permissions":	[
            'android.permission.INTERNET',
            'android.permission.WRITE_EXTERNAL_STORAGE',
            'android.permission.ACCESS_NETWORK_STATE'],
        "manifest":		['<activity android:name="com.jirbo.adcolony.AdColonyOverlay" android:configChanges="keyboardHidden|orientation|screenSize" android:theme="@android:style/Theme.Translucent.NoTitleBar.Fullscreen" />',
                         '<activity android:name="com.jirbo.adcolony.AdColonyFullscreen" android:configChanges="keyboardHidden|orientation|screenSize" android:theme="@android:style/Theme.Black.NoTitleBar.Fullscreen" />',
                         '<activity android:name="com.jirbo.adcolony.AdColonyBrowser" android:configChanges="keyboardHidden|orientation|screenSize" android:theme="@android:style/Theme.Black.NoTitleBar.Fullscreen" />'],
        "lib":          ['adcolony.jar'],
    },
    {
        "name":         "android_ADAds_HeyZap",
        "cpp":          ["Device/ADAds_HeyZap.cpp"],
        "permissions":	['android.permission.ACCESS_NETWORK_STATE', 'android.permission.INTERNET'],
        "manifest":		['<activity android:name="com.heyzap.sdk.ads.HeyzapInterstitialActivity" android:configChanges="keyboardHidden|orientation|screenSize|smallestScreenSize" />',
                         '<activity android:name="com.heyzap.sdk.ads.HeyzapVideoActivity" android:configChanges="keyboardHidden|orientation|screenSize|smallestScreenSize" />',
                         '<receiver android:name="com.heyzap.sdk.ads.PackageAddedReceiver"><intent-filter><data android:scheme="package"/><action android:name="android.intent.action.PACKAGE_ADDED"/></intent-filter></receiver>'],
        "lib":          ['heyzap-ads-sdk.jar'],
    },
    {
        "name":         "android_ADAds_HeyZapTest",
        "cpp":          ["Device/ADAds_HeyZapTest.cpp"],
        "manifest":		['<activity android:name="com.heyzap.sdk.ads.MediationTestActivity" android:configChanges="keyboardHidden|orientation|screenSize|smallestScreenSize" />'],

        "require":      ["android_ADAds_HeyZap"]
    },

    {
        "name":         "android_ADRewardedAds_HeyZap",
        "cpp":          ["Device/ADRewardedAds_HeyZap.cpp"],
        "java":         ["com/x4enjoy/ADLib/ADRewardedAdsHeyZap.java"],
        "require":      ["android_ADAds_HeyZap"]
    },
	
	{
        "name":         "android_ADInApp_None",
        "cpp":          ['Device/ADInApp_None.cpp'],
    },

	{
        "name":         "android_ADInApp_GooglePlay",
        "cpp":          ['Device/ADInApp_GooglePlay.cpp'],
                "permissions":  ['com.android.vending.BILLING'],
		"java":         ["com/x4enjoy/ADLib/inapp/google/ADInApp.java",
						 "com/x4enjoy/ADLib/inapp/google/Base64.java",
						 "com/x4enjoy/ADLib/inapp/google/Base64DecoderException.java",
						 "com/x4enjoy/ADLib/inapp/google/IabException.java",
						 "com/x4enjoy/ADLib/inapp/google/IabHelper.java",
						 "com/x4enjoy/ADLib/inapp/google/IabResult.java",
						 "com/x4enjoy/ADLib/inapp/google/Inventory.java",
						 "com/x4enjoy/ADLib/inapp/google/Purchase.java",
						 "com/x4enjoy/ADLib/inapp/google/Security.java",
						 "com/x4enjoy/ADLib/inapp/google/SkuDetails.java",
						 "com/android/vending/billing/IInAppBillingService.aidl"],
		"require":      ['android_ADJNI',
						 'android_ADUIThread',
						 'android_ADNotification'],
    },

    {
        "name":         "android_ADPushWoosh",
        "cpp":          ['Messanger/ADPushWoosh.cpp'],
        "permissions":  ['android.permission.ACCESS_NETWORK_STATE',
                         'android.permission.READ_PHONE_STATE',
                         'android.permission.INTERNET',
                         'android.permission.GET_ACCOUNTS',
                         'android.permission.WAKE_LOCK',
                         'com.google.android.c2dm.permission.RECEIVE'],
        "permissionsManual": ['<permission android:name="{PACKAGE}.C2D_MESSAGE" android:protectionLevel="signature"/>',
                        '<uses-permission android:name="{PACKAGE}.permission.C2D_MESSAGE"/>',
                        ],
        "manifest":     [
                         '<activity android:name="com.arellomobile.android.push.PushWebview"/>',
                         '<activity android:name="com.arellomobile.android.push.MessageActivity"/>',
                         '<activity android:name="com.arellomobile.android.push.PushHandlerActivity"/>',
                         '<receiver android:name="com.google.android.gcm.GCMBroadcastReceiver" android:permission="com.google.android.c2dm.permission.SEND"><intent-filter><action android:name="com.google.android.c2dm.intent.RECEIVE"/><action android:name="com.google.android.c2dm.intent.REGISTRATION"/><category android:name="{PACKAGE}"/></intent-filter></receiver>',
                         '<service android:name="com.arellomobile.android.push.PushGCMIntentService"/>'],
        "java":         ["com/x4enjoy/ADLib/messanger/ADPushWoosh.java"],
        "lib":          ['Pushwoosh.jar']
    },



    {
        "name":         "android_ADPush",
        "cpp":          ['Messanger/ADPush.cpp'],
        "permissions":  ['android.permission.ACCESS_NETWORK_STATE',
                         'android.permission.READ_PHONE_STATE',
                         'android.permission.INTERNET',
                         'android.permission.GET_ACCOUNTS',
                         'android.permission.WAKE_LOCK',
                         'com.google.android.c2dm.permission.RECEIVE'],
        "permissionsManual": ['<permission android:name="{PACKAGE}.C2D_MESSAGE" android:protectionLevel="signature"/>',
                        '<uses-permission android:name="{PACKAGE}.permission.C2D_MESSAGE"/>',
                        ],
        "manifest":     [

                         '<receiver android:name="com.x4enjoy.ADLib.messanger.GcmBroadcastReceiver" android:permission="com.google.android.c2dm.permission.SEND"><intent-filter><action android:name="com.google.android.c2dm.intent.RECEIVE"/><action android:name="com.google.android.c2dm.intent.REGISTRATION"/><category android:name="{PACKAGE}"/></intent-filter></receiver>',
                         '<service android:name="com.x4enjoy.ADLib.messanger.GcmIntentService"/>'],
        "java":         ["com/x4enjoy/ADLib/messanger/ADPush.java",
                        "com/x4enjoy/ADLib/messanger/GcmBroadcastReceiver.java",
                        "com/x4enjoy/ADLib/messanger/GcmIntentService.java"],
        "require":     ['googlePlayServices']
    },

    {
        "name":         "android_ADInApp_SamsungStore",
        "cpp":          ['Device/ADInApp_SamsungStore.cpp'],
        "permissions":  ['com.sec.android.iap.permission.BILLING',
                         'android.permission.INTERNET'],
        "manifest":     ['<activity android:name="com.sec.android.iap.lib.activity.InboxActivity" android:theme="@style/Theme.Empty" android:configChanges="orientation|screenSize"/>',
                         '<activity android:name="com.sec.android.iap.lib.activity.PaymentActivity" android:theme="@style/Theme.Empty" android:configChanges="orientation|screenSize"/>',
                         '<activity android:name="com.sec.android.iap.lib.activity.ItemActivity" android:theme="@style/Theme.Empty" android:configChanges="orientation|screenSize"/>'],
        "external":     ['Samsung_IAP2.0_Lib'],
        "java":         ["com/x4enjoy/ADLib/inapp/samsung/ADInApp.java"],
        "require":      ['android_ADJNI',
						 'android_ADUIThread',
						 'android_ADNotification'],
    },

    {
        "name":         "android_ADInApp_Amazon",
        "cpp":          ['Device/ADInApp_Amazon.cpp'],
        "permissions":  ['com.sec.android.iap.permission.BILLING',
                         'android.permission.INTERNET'],
        "manifest":     ['<receiver android:name = "com.amazon.inapp.purchasing.ResponseReceiver" ><intent-filter><action android:name = "com.amazon.inapp.purchasing.NOTIFY" android:permission = "com.amazon.inapp.purchasing.Permission.NOTIFY" /></intent-filter></receiver>'],
        "lib":          ['Amazon-InApp-1.0.3.jar'],
        "java":         ["com/x4enjoy/ADLib/inapp/amazon/ADInApp.java"],
        "require":      ['android_ADJNI',
						 'android_ADUIThread',
						 'android_ADNotification'],
    },
	
    {
        "name":         "android_ADStatistics_Flurry",
        "permissions":  ['android.permission.INTERNET',
                         'android.permission.ACCESS_NETWORK_STATE'],
        "cpp":          ['Device/ADStatistics_Flurry.cpp'],
        "lib":          ['FlurryAnalytics_3.4.0.jar'],
        "require":      ['android_ADJNI'],
    },

    {
        "name":         "android_ADStatistics_None",
        "cpp":          ['Device/ADStatistics_None.cpp'],
    },

    {
        "name":         "android_ADLanguage",
        "cpp":          ['Device/ADLanguage.cpp'],
        "require":      ['android_ADJNI'],
    },

    {
        "name":         "android_ADInfo_Store_AmazonStore",
        "cpp":          ['Device/ADInfo_Store_AmazonStore.cpp'],
    },

    {
        "name":         "android_ADInfo_Store_GooglePlay",
        "cpp":          ['Device/ADInfo_Store_GooglePlay.cpp'],
    },

    {
        "name":         "android_ADInfo_Store_SamsungStore",
        "cpp":          ['Device/ADInfo_Store_SamsungStore.cpp'],
    },

    {
        "name":         "android_ADInfo_Store_Undefined",
        "cpp":          ['Device/ADInfo_Store_Undefined.cpp'],
    },
	
	{
        "name":         "android_ADInfo_Store_StoreLess",
        "cpp":          ['Device/ADInfo_Store_StoreLess.cpp'],
    },

    {
        "name":         "android_ADNotification",
        "cpp":          ['Device/ADNotification.cpp'],
        "require":      ['android_ADUIThread',
                         'android_ADJNI'],
    },

    {
        "name":         "android_ADBrowser",
        "cpp":          ['Device/ADBrowser.cpp'],
        "require":      ['android_ADUIThread',
                         'android_ADJNI'],
    },

    {
        "name":         "android_ADVirtualCurrency_TapJoy",
        "cpp":          ['Device/ADVirtualCurrency_TapJoy.cpp'],
        "permissions":  ['android.permission.INTERNET',
                         'android.permission.ACCESS_NETWORK_STATE',
                         'android.permission.READ_PHONE_STATE',
                         'android.permission.ACCESS_WIFI_STATE',
                         'android.permission.ACCESS_COARSE_LOCATION'],
        "manifest":     ['<activity android:name="com.tapjoy.TJCOffersWebView" android:configChanges="orientation|keyboardHidden|screenSize" />',
                        '<activity android:name="com.tapjoy.TapjoyFullScreenAdWebView" android:configChanges="orientation|keyboardHidden|screenSize" />',
                        '<activity android:name="com.tapjoy.TapjoyDailyRewardAdWebView" android:configChanges="orientation|keyboardHidden|screenSize" />',
                        '<activity android:name="com.tapjoy.TapjoyVideoView" android:configChanges="orientation|keyboardHidden|screenSize" />',
                        '<activity android:name="com.tapjoy.TJAdUnitView" android:configChanges="orientation|keyboardHidden|screenSize" android:theme="@android:style/Theme.Translucent.NoTitleBar.Fullscreen" android:hardwareAccelerated="true" />',
                        '<activity android:name="com.tapjoy.mraid.view.ActionHandler" android:configChanges="orientation|keyboardHidden|screenSize" />',
                        '<activity android:name="com.tapjoy.mraid.view.Browser" android:configChanges="orientation|keyboardHidden|screenSize" />',],
        "require":      ['android_ADUIThread',
                         'android_ADJNI'],
        "java":         ["com/x4enjoy/ADLib/virtualcurrency/tapjoy/ADVirtualCurrency.java"],
        "lib":          ['tapjoyconnectlibrary_9.1.5.jar'],
    },

    {
        "name": "android_ADVirtualCurrency_None",
        "cpp":          ['Device/ADVirtualCurrency_None.cpp'],
    },

    {
        "name": "android_ADFacebook",
        "cpp": ['Social/ADFacebook_Android.cpp'],
        "external":     ['Facebook_3.23.0'],
        "permissions":  ['android.permission.INTERNET',],
        "manifest":     ['<activity android:name="com.facebook.LoginActivity" android:theme="@android:style/Theme.Translucent.NoTitleBar" android:label="@string/app_name" />',
                        '<meta-data android:name="com.facebook.sdk.ApplicationId" android:value="@string/app_id"/>'],
        "java":         ["com/x4enjoy/ADLib/social/ADFacebook.java"],
    },

    {
        "name": "android_ADHttp",
        "cpp": ['Networking/ADHttp_Android.cpp'],
        "permissions":  ['android.permission.INTERNET',],
        "java":         ["com/x4enjoy/ADLib/networking/ADHttp.java"],
    },
	
	{
        "name": "android_ADHttp_CURL",
        "cpp": ['Networking/ADHttp_CURL.cpp'],
        "permissions":  ['android.permission.INTERNET',],
    },

    {
        "name": "android_ADHttp_None",
        "cpp": ['Networking/ADHttp_None.cpp'],
    },

    {
        "name": "android_ADFacebook_None",
        "cpp": ['Social/ADFacebook_None.cpp'],
    },
	
	{
        "name": "android_ADOrientation",
        "cpp": ['Device/ADOrientation_Android.cpp'],
    },
]

def loadModules(system):

    for info in modules:
        #print('Module loaded: {0}'.format(info['name']))
        m = AndroidModule(info["name"])
        for k, v in info.items():
            if k == 'permissions':
                for perm in v:
                    m.addPermission(perm)
            if k == 'cpp':
                for cpp in v:
                    m.addCppFile(cpp)
            if k == 'java':
                for java in v:
                    m.addJavaFile(java)
            if k == 'lib':
                for lib in v:
                    m.addLibFile(lib)
            if k == 'require':
                for req in v:
                    m.addRequiredModule(req)
            if k == 'manifest':
                for man in v:
                    m.addManifestAdditional(man)
            if k == 'res':
                for res in v:
                    m.addResFile(res)
            if k == 'external':
                for prj in v:
                    m.addExternalProject(prj)
            if k == 'permissionsManual':
                for prj in v:
                    m.addPermissionManual(prj)
        system.addModule(m)
        #Modules.Modules.addModule(m)







